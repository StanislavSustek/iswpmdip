/*
Navicat PGSQL Data Transfer

Source Server         : public
Source Server Version : 90401
Source Host           : forwarding.isw.sk:5000
Source Database       : public
Source Schema         : pm

Target Server Type    : PGSQL
Target Server Version : 90401
File Encoding         : 65001

Date: 2017-07-18 15:20:05
*/


-- ----------------------------
-- Sequence structure for additional_permision_log_additional_permision_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."additional_permision_log_additional_permision_log_id_seq";
CREATE SEQUENCE "public"."additional_permision_log_additional_permision_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_company_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_company_id_seq";
CREATE SEQUENCE "public"."company_company_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_company_customer_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_company_customer_id_seq";
CREATE SEQUENCE "public"."company_customer_company_customer_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_contact_company_customer_contact_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_contact_company_customer_contact_id_seq";
CREATE SEQUENCE "public"."company_customer_contact_company_customer_contact_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_contact_log_company_customer_contact_log_i_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_contact_log_company_customer_contact_log_i_seq";
CREATE SEQUENCE "public"."company_customer_contact_log_company_customer_contact_log_i_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_contact_stat_company_customer_contact_stat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_contact_stat_company_customer_contact_stat_seq";
CREATE SEQUENCE "public"."company_customer_contact_stat_company_customer_contact_stat_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_log_company_customer_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_log_company_customer_log_id_seq";
CREATE SEQUENCE "public"."company_customer_log_company_customer_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_customer_status_company_customer_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_customer_status_company_customer_status_id_seq";
CREATE SEQUENCE "public"."company_customer_status_company_customer_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_internal_company_internal_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_internal_company_internal_id_seq";
CREATE SEQUENCE "public"."company_internal_company_internal_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_internal_log_company_internal_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_internal_log_company_internal_log_id_seq";
CREATE SEQUENCE "public"."company_internal_log_company_internal_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_internal_logo_logo_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_internal_logo_logo_id_seq";
CREATE SEQUENCE "public"."company_internal_logo_logo_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_internal_status_company_internal_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_internal_status_company_internal_status_id_seq";
CREATE SEQUENCE "public"."company_internal_status_company_internal_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_skill_company_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_skill_company_skill_id_seq";
CREATE SEQUENCE "public"."company_skill_company_skill_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for company_user_company_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_user_company_user_id_seq";
CREATE SEQUENCE "public"."company_user_company_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for email_notification_email_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."email_notification_email_notification_id_seq";
CREATE SEQUENCE "public"."email_notification_email_notification_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for email_notification_user_email_notification_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."email_notification_user_email_notification_user_id_seq";
CREATE SEQUENCE "public"."email_notification_user_email_notification_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_holiday_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_holiday_id_seq";
CREATE SEQUENCE "public"."holiday_holiday_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_holiday_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_holiday_type_id_seq";
CREATE SEQUENCE "public"."holiday_holiday_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_log_holiday_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_log_holiday_log_id_seq";
CREATE SEQUENCE "public"."holiday_log_holiday_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_restriction_holiday_restriction_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_restriction_holiday_restriction_id_seq";
CREATE SEQUENCE "public"."holiday_restriction_holiday_restriction_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_status_holiday_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_status_holiday_status_id_seq";
CREATE SEQUENCE "public"."holiday_status_holiday_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for holiday_user_restriction_holiday_user_restriction_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."holiday_user_restriction_holiday_user_restriction_id_seq";
CREATE SEQUENCE "public"."holiday_user_restriction_holiday_user_restriction_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for invoice_invoice_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."invoice_invoice_id_seq";
CREATE SEQUENCE "public"."invoice_invoice_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for invoice_item_invoice_item_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."invoice_item_invoice_item_id_seq";
CREATE SEQUENCE "public"."invoice_item_invoice_item_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for invoice_log_invoice_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."invoice_log_invoice_log_id_seq";
CREATE SEQUENCE "public"."invoice_log_invoice_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for invoice_status_invoice_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."invoice_status_invoice_status_id_seq";
CREATE SEQUENCE "public"."invoice_status_invoice_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for measure_unit_measure_unit_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."measure_unit_measure_unit_id_seq";
CREATE SEQUENCE "public"."measure_unit_measure_unit_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_item_log_order_item_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_item_log_order_item_log_id_seq";
CREATE SEQUENCE "public"."order_item_log_order_item_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_item_order_item_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_item_order_item_id_seq";
CREATE SEQUENCE "public"."order_item_order_item_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_item_status_order_item_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_item_status_order_item_status_id_seq";
CREATE SEQUENCE "public"."order_item_status_order_item_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_log_order_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_log_order_log_id_seq";
CREATE SEQUENCE "public"."order_log_order_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for order_order_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_order_id_seq";
CREATE SEQUENCE "public"."order_order_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
-- ----------------------------
-- Sequence structure for order_status_order_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_status_order_status_id_seq";
CREATE SEQUENCE "public"."order_status_order_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for password_reset_hash_password_reset_hash_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."password_reset_hash_password_reset_hash_id_seq";
CREATE SEQUENCE "public"."password_reset_hash_password_reset_hash_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for permision_permision_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."permision_permision_id_seq";
CREATE SEQUENCE "public"."permision_permision_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_budget_project_budget_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_budget_project_budget_id_seq";
CREATE SEQUENCE "public"."project_budget_project_budget_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_cost_item_project_cost_item_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_cost_item_project_cost_item_id_seq";
CREATE SEQUENCE "public"."project_cost_item_project_cost_item_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_log_project_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_log_project_log_id_seq";
CREATE SEQUENCE "public"."project_log_project_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_project_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_project_id_seq";
CREATE SEQUENCE "public"."project_project_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_skill_project_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_skill_project_skill_id_seq";
CREATE SEQUENCE "public"."project_skill_project_skill_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_status_project_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_status_project_status_id_seq";
CREATE SEQUENCE "public"."project_status_project_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_user_log_project_user_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_user_log_project_user_log_id_seq";
CREATE SEQUENCE "public"."project_user_log_project_user_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

DROP SEQUENCE IF EXISTS "public"."project_user_id_seq";
CREATE SEQUENCE "public"."project_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for project_user_status_project_user_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."project_user_status_project_user_status_id_seq";
CREATE SEQUENCE "public"."project_user_status_project_user_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for report_summary_report_summary_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."report_summary_report_summary_id_seq";
CREATE SEQUENCE "public"."report_summary_report_summary_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for role_permision_role_permision_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_permision_role_permision_id_seq";
CREATE SEQUENCE "public"."role_permision_role_permision_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for role_role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_role_id_seq";
CREATE SEQUENCE "public"."role_role_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sallary_history_sallary_hist_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sallary_history_sallary_hist_id_seq";
CREATE SEQUENCE "public"."sallary_history_sallary_hist_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sallary_log_sallary_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sallary_log_sallary_log_id_seq";
CREATE SEQUENCE "public"."sallary_log_sallary_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sallary_sallary_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sallary_sallary_id_seq";
CREATE SEQUENCE "public"."sallary_sallary_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for saved_sallary_saved_sallary_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."saved_sallary_saved_sallary_id_seq";
CREATE SEQUENCE "public"."saved_sallary_saved_sallary_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for setting_setting_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."setting_setting_id_seq";
CREATE SEQUENCE "public"."setting_setting_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_log_task_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_log_task_log_id_seq";
CREATE SEQUENCE "public"."task_log_task_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_report_log_task_report_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_report_log_task_report_log_id_seq";
CREATE SEQUENCE "public"."task_report_log_task_report_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_report_status_task_report_status_id
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_report_status_task_report_status_id";
CREATE SEQUENCE "public"."task_report_status_task_report_status_id"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_report_task_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_report_task_report_id_seq";
CREATE SEQUENCE "public"."task_report_task_report_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_status_task_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_status_task_status_id_seq";
CREATE SEQUENCE "public"."task_status_task_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for task_task_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_task_id_seq";
CREATE SEQUENCE "public"."task_task_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for translation_translation_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."translation_translation_id_seq";
CREATE SEQUENCE "public"."translation_translation_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_company_skill_user_company_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_company_skill_user_company_skill_id_seq";
CREATE SEQUENCE "public"."user_company_skill_user_company_skill_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_log_user_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_log_user_log_id_seq";
CREATE SEQUENCE "public"."user_log_user_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_role_user_role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_role_user_role_id_seq";
CREATE SEQUENCE "public"."user_role_user_role_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_status_user_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_status_user_status_id_seq";
CREATE SEQUENCE "public"."user_status_user_status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for user_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_user_id_seq";
CREATE SEQUENCE "public"."user_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

DROP SEQUENCE IF EXISTS "public"."messages_id_seq";
CREATE SEQUENCE "public"."messages_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;


DROP SEQUENCE IF EXISTS "public"."user_messages_id_seq";
CREATE SEQUENCE "public"."user_messages_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;


-- ----------------------------
-- Table structure for additional_permision
-- ----------------------------
DROP TABLE IF EXISTS "public"."additional_permision";
CREATE TABLE "public"."additional_permision" (
"user_id" int8 NOT NULL,
"permision_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for additional_permision_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."additional_permision_log";
CREATE TABLE "public"."additional_permision_log" (
"additional_permision_log_id" int8 DEFAULT nextval('"public".additional_permision_log_additional_permision_log_id_seq'::regclass) NOT NULL,
"user_id" int8 NOT NULL,
"permision_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL,
"modified_user_id" int8 NOT NULL,
"deleted" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for app_audit_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_audit_log";
CREATE TABLE "public"."app_audit_log" (
"modified" timestamp(6) NOT NULL,
"logger" text COLLATE "default",
"level" text COLLATE "default",
"message" text COLLATE "default",
"exception" text COLLATE "default",
"method" text COLLATE "default",
"class" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for app_error_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_error_log";
CREATE TABLE "public"."app_error_log" (
"modified" timestamp(6) NOT NULL,
"logger" text COLLATE "default",
"level" text COLLATE "default",
"exception" text COLLATE "default",
"message" text COLLATE "default" NOT NULL,
"method" text COLLATE "default",
"class" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS "public"."company";
CREATE TABLE "public"."company" (
"company_id" int8 DEFAULT nextval('"public".company_company_id_seq'::regclass) NOT NULL,
"ico" text COLLATE "default",
"dic" text COLLATE "default",
"street" text COLLATE "default",
"street_number" text COLLATE "default",
"psc" text COLLATE "default",
"company_name" text COLLATE "default" NOT NULL,
"city" text COLLATE "default",
"modified" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer";
CREATE TABLE "public"."company_customer" (
"company_customer_id" int8 DEFAULT nextval('"public".company_customer_company_customer_id_seq'::regclass) NOT NULL,
"company_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"company_customer_status_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer_contact
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer_contact";
CREATE TABLE "public"."company_customer_contact" (
"company_customer_contact_id" int8 DEFAULT nextval('"public".company_customer_contact_company_customer_contact_id_seq'::regclass) NOT NULL,
"first_name" text COLLATE "default",
"last_name" text COLLATE "default",
"email" text COLLATE "default" NOT NULL,
"phone" text COLLATE "default",
"company_customer_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL,
"company_customer_contact_status_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer_contact_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer_contact_log";
CREATE TABLE "public"."company_customer_contact_log" (
"company_customer_contact_log_id" int8 DEFAULT nextval('"public".company_customer_contact_log_company_customer_contact_log_i_seq'::regclass) NOT NULL,
"company_customer_contact_status_id" int8 NOT NULL,
"company_customer_contact_id" int8 NOT NULL,
"company_customer_id" int8 NOT NULL,
"comment" text COLLATE "default",
"modified_user_id" int8 NOT NULL,
"email" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer_contact_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer_contact_status";
CREATE TABLE "public"."company_customer_contact_status" (
"company_customer_contact_status_id" int8 DEFAULT nextval('"public".company_customer_contact_stat_company_customer_contact_stat_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer_log";
CREATE TABLE "public"."company_customer_log" (
"company_customer_log_id" int8 DEFAULT nextval('"public".company_customer_log_company_customer_log_id_seq'::regclass) NOT NULL,
"comment" text COLLATE "default",
"company_customer_status_id" int8 NOT NULL,
"modified_user_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"company_customer_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_customer_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_customer_status";
CREATE TABLE "public"."company_customer_status" (
"company_customer_status_id" int8 DEFAULT nextval('"public".company_customer_status_company_customer_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_internal
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_internal";
CREATE TABLE "public"."company_internal" (
"company_internal_id" int8 DEFAULT nextval('"public".company_internal_company_internal_id_seq'::regclass) NOT NULL,
"company_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL,
"company_internal_status_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_internal_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_internal_log";
CREATE TABLE "public"."company_internal_log" (
"company_internal_log_id" int8 DEFAULT nextval('"public".company_internal_log_company_internal_log_id_seq'::regclass) NOT NULL,
"comment" text COLLATE "default",
"company_internal_status_id" int8 NOT NULL,
"modified_user_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_internal_logo
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_internal_logo";
CREATE TABLE "public"."company_internal_logo" (
"logo_id" int8 DEFAULT nextval('"public".company_internal_logo_logo_id_seq'::regclass) NOT NULL,
"company_internal_id" int8 NOT NULL,
"image" bytea NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_internal_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_internal_status";
CREATE TABLE "public"."company_internal_status" (
"company_internal_status_id" int8 DEFAULT nextval('"public".company_internal_status_company_internal_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_skill
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_skill";
CREATE TABLE "public"."company_skill" (
"company_skill_id" int8 DEFAULT nextval('"public".company_skill_company_skill_id_seq'::regclass) NOT NULL,
"company_internal_id" int8 NOT NULL,
"name" text COLLATE "default" NOT NULL,
"group" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_user";
CREATE TABLE "public"."company_user" (
"company_user_id" int8 DEFAULT nextval('"public".company_user_company_user_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"deleted" bool NOT NULL,
"deleted_timestamp" timestamp(6),
"user_id" int8 NOT NULL,
"company_internal_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for email_notification
-- ----------------------------
DROP TABLE IF EXISTS "public"."email_notification";
CREATE TABLE "public"."email_notification" (
"email_notification_id" int8 DEFAULT nextval('"public".email_notification_email_notification_id_seq'::regclass) NOT NULL,
"company_internal_id" int8 NOT NULL,
"task_id" int8,
"project_id" int8,
"dates_change" bool NOT NULL,
"status_change" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for email_notification_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."email_notification_user";
CREATE TABLE "public"."email_notification_user" (
"email_notification_user_id" int8 DEFAULT nextval('"public".email_notification_user_email_notification_user_id_seq'::regclass) NOT NULL,
"email_notification_id" int8 NOT NULL,
"user_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday";
CREATE TABLE "public"."holiday" (
"holiday_id" int8 DEFAULT nextval('"public".holiday_holiday_id_seq'::regclass) NOT NULL,
"user_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"from" timestamp(6) NOT NULL,
"to" timestamp(6) NOT NULL,
"comment" text COLLATE "default",
"holiday_status_id" int8 NOT NULL,
"holiday_type_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday_log";
CREATE TABLE "public"."holiday_log" (
"holiday_log_id" int8 DEFAULT nextval('"public".holiday_log_holiday_log_id_seq'::regclass) NOT NULL,
"holiday_id" int8 NOT NULL,
"from" timestamp(6),
"to" timestamp(6),
"comment" text COLLATE "default",
"holiday_status_id" int8,
"modified" timestamp(6) NOT NULL,
"modified_user_id" int8 NOT NULL,
"holiday_type_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday_restriction
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday_restriction";
CREATE TABLE "public"."holiday_restriction" (
"holiday_restriction_id" int8 DEFAULT nextval('"public".holiday_restriction_holiday_restriction_id_seq'::regclass) NOT NULL,
"company_internal_id" int8 NOT NULL,
"description" text COLLATE "default",
"max_days_per_year" varchar COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday_status";
CREATE TABLE "public"."holiday_status" (
"holiday_status_id" int8 DEFAULT nextval('"public".holiday_status_holiday_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday_type";
CREATE TABLE "public"."holiday_type" (
"holiday_type_id" int4 DEFAULT nextval('"public".holiday_holiday_type_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for holiday_user_restriction
-- ----------------------------
DROP TABLE IF EXISTS "public"."holiday_user_restriction";
CREATE TABLE "public"."holiday_user_restriction" (
"holiday_user_restriction_id" int8 DEFAULT nextval('"public".holiday_user_restriction_holiday_user_restriction_id_seq'::regclass) NOT NULL,
"holiday_restriction_id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"valid_to" timestamp(6),
"valid_from" timestamp(6) DEFAULT now() NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for invoice
-- ----------------------------
DROP TABLE IF EXISTS "public"."invoice";
CREATE TABLE "public"."invoice" (
"invoice_id" int8 DEFAULT nextval('"public".invoice_invoice_id_seq'::regclass) NOT NULL,
"created" timestamp(6) NOT NULL,
"total_price" numeric(15,2) NOT NULL,
"company_internal_id" int8 NOT NULL,
"company_customer_id" int8 NOT NULL,
"invoice_number" text COLLATE "default" NOT NULL,
"invoice_status_id" int8 NOT NULL,
"currency_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for invoice_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."invoice_item";
CREATE TABLE "public"."invoice_item" (
"invoice_item_id" int8 DEFAULT nextval('"public".invoice_item_invoice_item_id_seq'::regclass) NOT NULL,
"description" text COLLATE "default",
"total_price" numeric(15,2) NOT NULL,
"quantity" numeric(15,2) NOT NULL,
"order_item_id" int8 NOT NULL,
"invoice_id" int8 NOT NULL,
"price" numeric(15,2) NOT NULL,
"measure_unit_id" int8 NOT NULL,
"vat" numeric(15,3) DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for invoice_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."invoice_log";
CREATE TABLE "public"."invoice_log" (
"invoice_log_id" int8 DEFAULT nextval('"public".invoice_log_invoice_log_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"total_price" numeric(15,2) NOT NULL,
"invoice_status_id" int8 NOT NULL,
"invoice_id" int8,
"modified_user_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for invoice_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."invoice_status";
CREATE TABLE "public"."invoice_status" (
"invoice_status_id" int8 DEFAULT nextval('"public".invoice_status_invoice_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for measure_unit
-- ----------------------------
DROP TABLE IF EXISTS "public"."measure_unit";
CREATE TABLE "public"."measure_unit" (
"measure_unit_id" int8 DEFAULT nextval('"public".measure_unit_measure_unit_id_seq'::regclass) NOT NULL,
"unit" text COLLATE "default" NOT NULL,
"active" bool NOT NULL,
"type" varchar(10) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS "public"."order";
CREATE TABLE "public"."order" (
"order_id" int8 DEFAULT nextval('"public".order_order_id_seq'::regclass) NOT NULL,
"description" text COLLATE "default",
"name" text COLLATE "default" NOT NULL,
"project_id" int8,
"unissued_price" numeric(15,2),
"company_customer_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"date_from" timestamp(6) NOT NULL,
"date_to" timestamp(6) NOT NULL,
"order_status_id" int8 NOT NULL,
"total_price" numeric(15,2),
"currency_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_item";
CREATE TABLE "public"."order_item" (
"order_item_id" int8 DEFAULT nextval('"public".order_item_order_item_id_seq'::regclass) NOT NULL,
"description" text COLLATE "default",
"date_from" timestamp(6),
"date_to" timestamp(6),
"name" text COLLATE "default" NOT NULL,
"price" numeric(15,2) NOT NULL,
"total_price" numeric(15,2) NOT NULL,
"order_id" int8 NOT NULL,
"quantity" numeric(15,2) NOT NULL,
"measure_unit_id" int8 NOT NULL,
"order_item_status_id" int8 NOT NULL,
"vat" numeric(15,2) DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order_item_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_item_log";
CREATE TABLE "public"."order_item_log" (
"order_item_log_id" int8 DEFAULT nextval('"public".order_item_log_order_item_log_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"comment" text COLLATE "default",
"order_item_id" int8 NOT NULL,
"modified_user_id" int8 NOT NULL,
"price" numeric(15,2),
"date_from" timestamp(6),
"date_to" timestamp(6),
"name" text COLLATE "default",
"measure_unit_id" int8,
"quantity" int8,
"order_item_status_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order_item_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_item_status";
CREATE TABLE "public"."order_item_status" (
"order_item_status_id" int8 DEFAULT nextval('"public".order_item_status_order_item_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_log";
CREATE TABLE "public"."order_log" (
"order_log_id" int8 DEFAULT nextval('"public".order_log_order_log_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"comment" text COLLATE "default",
"order_id" int8 NOT NULL,
"order_status_id" int8,
"project_id" int8,
"modified_user_id" int8 NOT NULL,
"unissued_price" numeric(15,2),
"date_from" timestamp(6),
"date_to" timestamp(6),
"total_price" numeric(15,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for order_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."order_status";
CREATE TABLE "public"."order_status" (
"order_status_id" int8 DEFAULT nextval('"public".order_status_order_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for password_reset_token
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_reset_token";
CREATE TABLE "public"."password_reset_token" (
"password_reset_token_id" int8 DEFAULT nextval('"public".password_reset_hash_password_reset_hash_id_seq'::regclass) NOT NULL,
"user_id" int8 NOT NULL,
"token" text COLLATE "default" NOT NULL,
"created" timestamp(6) NOT NULL,
"expired" timestamp(6) NOT NULL,
"used" bool DEFAULT false NOT NULL,
"ip_address" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for permision
-- ----------------------------
DROP TABLE IF EXISTS "public"."permision";
CREATE TABLE "public"."permision" (
"permision_id" int8 DEFAULT nextval('"public".permision_permision_id_seq'::regclass) NOT NULL,
"description" text COLLATE "default",
"name" text COLLATE "default" NOT NULL,
"group" varchar(20) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS "public"."project";
CREATE TABLE "public"."project" (
"project_id" int8 DEFAULT nextval('"public".project_project_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"budget" numeric(15,2),
"comment" text COLLATE "default",
"date_from" timestamp(6) NOT NULL,
"date_to" timestamp(6) NOT NULL,
"project_status_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL,
"created" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_budget
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_budget";
CREATE TABLE "public"."project_budget" (
"project_budget_id" int8 DEFAULT nextval('"public".project_budget_project_budget_id_seq'::regclass) NOT NULL,
"project_id" int8 NOT NULL,
"invoice_items_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"project_cost_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"hour_payments_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"month_payments_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"hour_reward_payments_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"special_reward_payments_sum" numeric(15,2) DEFAULT 0 NOT NULL,
"invoice_items_sum_vat" numeric(15,2) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_cost_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_cost_item";
CREATE TABLE "public"."project_cost_item" (
"project_cost_item_id" int8 DEFAULT nextval('"public".project_cost_item_project_cost_item_id_seq'::regclass) NOT NULL,
"project_id" int8 NOT NULL,
"name" text COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"item_price" numeric(15,2) NOT NULL,
"invoice_number" text COLLATE "default",
"modified" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_log";
CREATE TABLE "public"."project_log" (
"project_log_id" int8 DEFAULT nextval('"public".project_log_project_log_id_seq'::regclass) NOT NULL,
"comment" text COLLATE "default",
"modified" timestamp(6) NOT NULL,
"project_id" int8 NOT NULL,
"project_status_id" int8,
"modified_user_id" int8 NOT NULL,
"date_from" timestamp(6),
"date_to" timestamp(6),
"name" text COLLATE "default",
"description" text COLLATE "default",
"budget" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_skill
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_skill";
CREATE TABLE "public"."project_skill" (
"project_skill_id" int8 DEFAULT nextval('"public".project_skill_project_skill_id_seq'::regclass) NOT NULL,
"project_id" int8 NOT NULL,
"company_skill_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_status";
CREATE TABLE "public"."project_status" (
"project_status_id" int8 DEFAULT nextval('"public".project_status_project_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_user";
CREATE TABLE "public"."project_user" (
"id" int8 DEFAULT nextval('"public".project_user_id_seq'::regclass) NOT NULL,
"project_id" int8 NOT NULL,
"user_id" int8 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."project_user" add PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for project_user_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_user_log";
CREATE TABLE "public"."project_user_log" (
"project_user_log_id" int8 DEFAULT nextval('"public".project_user_log_project_user_log_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"project_user_status_id" int8 NOT NULL,
"modified_user_id" int8 NOT NULL,
"project_id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"comment" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for project_user_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_user_status";
CREATE TABLE "public"."project_user_status" (
"project_user_status_id" int8 DEFAULT nextval('"public".project_user_status_project_user_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for report_summary
-- ----------------------------
DROP TABLE IF EXISTS "public"."report_summary";
CREATE TABLE "public"."report_summary" (
"report_summary_id" int8 DEFAULT nextval('"public".report_summary_report_summary_id_seq'::regclass) NOT NULL,
"hours_reported" numeric(15,2) NOT NULL,
"modified_user_id" int8 NOT NULL,
"date_from" timestamp(6) NOT NULL,
"date_to" timestamp(6) NOT NULL,
"modified" timestamp(6) NOT NULL,
"saved_sallary_id" int8,
"company_internal_id" int8 NOT NULL,
"hours_confirmed" numeric(15,2),
"user_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS "public"."role";
CREATE TABLE "public"."role" (
"role_id" int8 DEFAULT nextval('"public".role_role_id_seq'::regclass) NOT NULL,
"description" text COLLATE "default",
"name" text COLLATE "default" NOT NULL,
"company_internal_id" int8 NOT NULL,
"read_only" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for role_permision
-- ----------------------------
DROP TABLE IF EXISTS "public"."role_permision";
CREATE TABLE "public"."role_permision" (
"role_permision_id" int8 DEFAULT nextval('"public".role_permision_role_permision_id_seq'::regclass) NOT NULL,
"role_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL,
"permision_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for sallary
-- ----------------------------
DROP TABLE IF EXISTS "public"."sallary";
CREATE TABLE "public"."sallary" (
"sallary_id" int8 DEFAULT nextval('"public".sallary_sallary_id_seq'::regclass) NOT NULL,
"hour_rate" numeric(15,2),
"monthly_rate" numeric(15,2),
"modified" timestamp(6) NOT NULL,
"modified_user_id" int8 NOT NULL,
"date_from" timestamp(6) NOT NULL,
"date_to" timestamp(6),
"user_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for sallary_history
-- ----------------------------
DROP TABLE IF EXISTS "public"."sallary_history";
CREATE TABLE "public"."sallary_history" (
"sallary_history_id" int8 DEFAULT nextval('"public".sallary_history_sallary_hist_id_seq'::regclass) NOT NULL,
"month" int4 NOT NULL,
"year" int4 NOT NULL,
"base_sallary" numeric(15,2),
"special_reward" numeric(15,2),
"sallary_total" numeric(15,2) NOT NULL,
"user_user_id" int8 NOT NULL,
"company_internal_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for sallary_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."sallary_log";
CREATE TABLE "public"."sallary_log" (
"sallary_log_id" int8 DEFAULT nextval('"public".sallary_log_sallary_log_id_seq'::regclass) NOT NULL,
"sallary_id" int8 NOT NULL,
"hour_rate" numeric(15,2),
"monthly_rate" numeric(15,2),
"created" timestamp(6) NOT NULL,
"date_from" timestamp(6),
"date_to" timestamp(6),
"modified_user_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for saved_sallary
-- ----------------------------
DROP TABLE IF EXISTS "public"."saved_sallary";
CREATE TABLE "public"."saved_sallary" (
"saved_sallary_id" int8 DEFAULT nextval('"public".saved_sallary_saved_sallary_id_seq'::regclass) NOT NULL,
"year" int4 NOT NULL,
"month" int4 NOT NULL,
"base_sallary" numeric(15,2) NOT NULL,
"special_reward" numeric(15,2),
"additional_special_reward" numeric(15,2),
"sallary_total" numeric(15,2) NOT NULL,
"user_id" int8 NOT NULL,
"sallary_history_id" int8,
"company_internal_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS "public"."setting";
CREATE TABLE "public"."setting" (
"setting_id" int8 DEFAULT nextval('"public".setting_setting_id_seq'::regclass) NOT NULL,
"company_internal_id" int8 NOT NULL,
"user_id" int8,
"date_format" text COLLATE "default",
"payment_create_day" int2,
"show_calendar" bool,
"show_users_role_chart" bool,
"show_tasks_chart" bool,
"show_reported_hours_chart" bool,
"show_last_task_history" bool,
"show_stats_widgets" bool,
"payment_email_notification" bool DEFAULT false NOT NULL,
"holiday_email_notification" bool DEFAULT false NOT NULL,
"assigning_removing" bool DEFAULT false NOT NULL,
"vat" numeric(20,2) DEFAULT 0
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS "public"."task";
CREATE TABLE "public"."task" (
"task_id" int8 DEFAULT nextval('"public".task_task_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"special_reward_hour" numeric(15,2),
"special_reward" numeric(15,2),
"task_task_id" int8,
"project_id" int8 NOT NULL,
"task_price" numeric(15,2),
"hours_assigned" numeric(15,2) NOT NULL,
"hours_reported" numeric(15,2),
"date_from" timestamp(6) NOT NULL,
"date_to" timestamp(6) NOT NULL,
"progress" numeric(15,2) NOT NULL,
"task_status_id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"order_item_id" int8,
"confirmed_hours" numeric(15,2) DEFAULT 0,
"priority" char
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for task_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_log";
CREATE TABLE "public"."task_log" (
"task_log_id" int8 DEFAULT nextval('"public".task_log_task_log_id_seq'::regclass) NOT NULL,
"task_id" int8 NOT NULL,
"task_status_id" int8,
"modified_user_id" int8 NOT NULL,
"comment" text COLLATE "default",
"name" text COLLATE "default",
"special_reward_hour" numeric(15,2),
"special_reward" numeric(15,2),
"date_from" timestamp(6),
"date_to" timestamp(6),
"modified" timestamp(6) NOT NULL,
"progress" numeric(15,2),
"user_id" int8 NOT NULL,
"order_item_id" int8,
"hours_reported" numeric(15,2),
"hours_assigned" numeric(15,2),
"task_price" numeric(15,2),
"priority" char
)
WITH (OIDS=FALSE);

-- ----------------------------
-- Table structure for task_report
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_report";
CREATE TABLE "public"."task_report" (
"task_report_id" int8 DEFAULT nextval('"public".task_report_task_report_id_seq'::regclass) NOT NULL,
"comment" text COLLATE "default",
"hours_reported" numeric(15,2),
"task_id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"modified" timestamp(6) NOT NULL,
"date_report" timestamp(6) NOT NULL,
"report_summary_id" int8,
"task_report_status_id" int8 DEFAULT 0,
"hours_confirmed" numeric(15,2),
"special_reward_used" bool DEFAULT false,
"hours_reward_hour" numeric(12,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for task_report_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_report_log";
CREATE TABLE "public"."task_report_log" (
"task_report_log_id" int8 DEFAULT nextval('"public".task_report_log_task_report_log_id_seq'::regclass) NOT NULL,
"task_report_id" int4 NOT NULL,
"comment" text COLLATE "default",
"hours_reported" numeric(15,2),
"date_report" timestamp(6),
"created" timestamp(6) NOT NULL,
"task_report_status_id" int8,
"user_id" int8 NOT NULL,
"report_summary_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for task_report_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_report_status";
CREATE TABLE "public"."task_report_status" (
"task_report_status_id" int8 DEFAULT nextval('"public".task_report_status_task_report_status_id'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"active" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for task_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_status";
CREATE TABLE "public"."task_status" (
"task_status_id" int8 DEFAULT nextval('"public".task_status_task_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for translation
-- ----------------------------
DROP TABLE IF EXISTS "public"."translation";
CREATE TABLE "public"."translation" (
"translation_id" int8 DEFAULT nextval('"public".translation_translation_id_seq'::regclass) NOT NULL,
"key" text COLLATE "default" NOT NULL,
"value_sk" text COLLATE "default" NOT NULL,
"value_en" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
"user_id" int8 DEFAULT nextval('"public".user_user_id_seq'::regclass) NOT NULL,
"first_name" text COLLATE "default" NOT NULL,
"last_name" text COLLATE "default" NOT NULL,
"email" text COLLATE "default" NOT NULL,
"password" text COLLATE "default",
"login" text COLLATE "default" NOT NULL,
"comment" text COLLATE "default",
"modified" timestamp(6) NOT NULL,
"user_status_id" int8 NOT NULL,
"created" timestamp(6) NOT NULL,
"last_login" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_company_skill
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_company_skill";
CREATE TABLE "public"."user_company_skill" (
"user_company_skill_id" int8 DEFAULT nextval('"public".user_company_skill_user_company_skill_id_seq'::regclass) NOT NULL,
"user_id" int8 NOT NULL,
"company_skill_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_log";
CREATE TABLE "public"."user_log" (
"user_log_id" int8 DEFAULT nextval('"public".user_log_user_log_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"comment" text COLLATE "default",
"user_status_id" int8,
"modified_user_id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"first_name" text COLLATE "default",
"last_name" text COLLATE "default",
"email" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_role";
CREATE TABLE "public"."user_role" (
"user_role_id" int8 DEFAULT nextval('"public".user_role_user_role_id_seq'::regclass) NOT NULL,
"modified" timestamp(6) NOT NULL,
"user_id" int8 NOT NULL,
"role" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_status";
CREATE TABLE "public"."user_status" (
"user_status_id" int8 DEFAULT nextval('"public".user_status_user_status_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default" NOT NULL,
"active" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."additional_permision_log_additional_permision_log_id_seq" OWNED BY "public"."additional_permision_log"."additional_permision_log_id";
ALTER SEQUENCE "public"."company_company_id_seq" OWNED BY "public"."company"."company_id";
ALTER SEQUENCE "public"."company_customer_company_customer_id_seq" OWNED BY "public"."company_customer"."company_customer_id";
ALTER SEQUENCE "public"."company_customer_contact_company_customer_contact_id_seq" OWNED BY "public"."company_customer_contact"."company_customer_contact_id";
ALTER SEQUENCE "public"."company_customer_contact_log_company_customer_contact_log_i_seq" OWNED BY "public"."company_customer_contact_log"."company_customer_contact_log_id";
ALTER SEQUENCE "public"."company_customer_contact_stat_company_customer_contact_stat_seq" OWNED BY "public"."company_customer_contact_status"."company_customer_contact_status_id";
ALTER SEQUENCE "public"."company_customer_log_company_customer_log_id_seq" OWNED BY "public"."company_customer_log"."company_customer_log_id";
ALTER SEQUENCE "public"."company_customer_status_company_customer_status_id_seq" OWNED BY "public"."company_customer_status"."company_customer_status_id";
ALTER SEQUENCE "public"."company_internal_company_internal_id_seq" OWNED BY "public"."company_internal"."company_internal_id";
ALTER SEQUENCE "public"."company_internal_log_company_internal_log_id_seq" OWNED BY "public"."company_internal_log"."company_internal_log_id";
ALTER SEQUENCE "public"."company_internal_logo_logo_id_seq" OWNED BY "public"."company_internal_logo"."logo_id";
ALTER SEQUENCE "public"."company_internal_status_company_internal_status_id_seq" OWNED BY "public"."company_internal_status"."company_internal_status_id";
ALTER SEQUENCE "public"."company_skill_company_skill_id_seq" OWNED BY "public"."company_skill"."company_skill_id";
ALTER SEQUENCE "public"."company_user_company_user_id_seq" OWNED BY "public"."company_user"."company_user_id";
ALTER SEQUENCE "public"."email_notification_email_notification_id_seq" OWNED BY "public"."email_notification"."email_notification_id";
ALTER SEQUENCE "public"."email_notification_user_email_notification_user_id_seq" OWNED BY "public"."email_notification_user"."email_notification_user_id";
ALTER SEQUENCE "public"."holiday_holiday_id_seq" OWNED BY "public"."holiday"."holiday_id";
ALTER SEQUENCE "public"."holiday_holiday_type_id_seq" OWNED BY "public"."holiday_type"."holiday_type_id";
ALTER SEQUENCE "public"."holiday_log_holiday_log_id_seq" OWNED BY "public"."holiday_log"."holiday_log_id";
ALTER SEQUENCE "public"."holiday_restriction_holiday_restriction_id_seq" OWNED BY "public"."holiday_restriction"."holiday_restriction_id";
ALTER SEQUENCE "public"."holiday_status_holiday_status_id_seq" OWNED BY "public"."holiday_status"."holiday_status_id";
ALTER SEQUENCE "public"."holiday_user_restriction_holiday_user_restriction_id_seq" OWNED BY "public"."holiday_user_restriction"."holiday_user_restriction_id";
ALTER SEQUENCE "public"."invoice_invoice_id_seq" OWNED BY "public"."invoice"."invoice_id";
ALTER SEQUENCE "public"."invoice_item_invoice_item_id_seq" OWNED BY "public"."invoice_item"."invoice_item_id";
ALTER SEQUENCE "public"."invoice_log_invoice_log_id_seq" OWNED BY "public"."invoice_log"."invoice_log_id";
ALTER SEQUENCE "public"."invoice_status_invoice_status_id_seq" OWNED BY "public"."invoice_status"."invoice_status_id";
ALTER SEQUENCE "public"."measure_unit_measure_unit_id_seq" OWNED BY "public"."measure_unit"."measure_unit_id";
ALTER SEQUENCE "public"."order_item_log_order_item_log_id_seq" OWNED BY "public"."order_item_log"."order_item_log_id";
ALTER SEQUENCE "public"."order_item_order_item_id_seq" OWNED BY "public"."order_item"."order_item_id";
ALTER SEQUENCE "public"."order_item_status_order_item_status_id_seq" OWNED BY "public"."order_item_status"."order_item_status_id";
ALTER SEQUENCE "public"."order_log_order_log_id_seq" OWNED BY "public"."order_log"."order_log_id";
ALTER SEQUENCE "public"."order_order_id_seq" OWNED BY "public"."order"."order_id";
ALTER SEQUENCE "public"."order_status_order_status_id_seq" OWNED BY "public"."order_status"."order_status_id";
ALTER SEQUENCE "public"."password_reset_hash_password_reset_hash_id_seq" OWNED BY "public"."password_reset_token"."password_reset_token_id";
ALTER SEQUENCE "public"."permision_permision_id_seq" OWNED BY "public"."permision"."permision_id";
ALTER SEQUENCE "public"."project_budget_project_budget_id_seq" OWNED BY "public"."project_budget"."project_budget_id";
ALTER SEQUENCE "public"."project_cost_item_project_cost_item_id_seq" OWNED BY "public"."project_cost_item"."project_cost_item_id";
ALTER SEQUENCE "public"."project_log_project_log_id_seq" OWNED BY "public"."project_log"."project_log_id";
ALTER SEQUENCE "public"."project_project_id_seq" OWNED BY "public"."project"."project_id";
ALTER SEQUENCE "public"."project_skill_project_skill_id_seq" OWNED BY "public"."project_skill"."project_skill_id";
ALTER SEQUENCE "public"."project_status_project_status_id_seq" OWNED BY "public"."project_status"."project_status_id";
ALTER SEQUENCE "public"."project_user_log_project_user_log_id_seq" OWNED BY "public"."project_user_log"."project_user_log_id";
ALTER SEQUENCE "public"."project_user_status_project_user_status_id_seq" OWNED BY "public"."project_user_status"."project_user_status_id";
ALTER SEQUENCE "public"."report_summary_report_summary_id_seq" OWNED BY "public"."report_summary"."report_summary_id";
ALTER SEQUENCE "public"."role_permision_role_permision_id_seq" OWNED BY "public"."role_permision"."role_permision_id";
ALTER SEQUENCE "public"."role_role_id_seq" OWNED BY "public"."role"."role_id";
ALTER SEQUENCE "public"."sallary_history_sallary_hist_id_seq" OWNED BY "public"."sallary_history"."sallary_history_id";
ALTER SEQUENCE "public"."sallary_log_sallary_log_id_seq" OWNED BY "public"."sallary_log"."sallary_log_id";
ALTER SEQUENCE "public"."sallary_sallary_id_seq" OWNED BY "public"."sallary"."sallary_id";
ALTER SEQUENCE "public"."saved_sallary_saved_sallary_id_seq" OWNED BY "public"."saved_sallary"."saved_sallary_id";
ALTER SEQUENCE "public"."setting_setting_id_seq" OWNED BY "public"."setting"."setting_id";
ALTER SEQUENCE "public"."task_log_task_log_id_seq" OWNED BY "public"."task_log"."task_log_id";
ALTER SEQUENCE "public"."task_report_log_task_report_log_id_seq" OWNED BY "public"."task_report_log"."task_report_log_id";
ALTER SEQUENCE "public"."task_report_status_task_report_status_id" OWNED BY "public"."task_report_status"."task_report_status_id";
ALTER SEQUENCE "public"."task_report_task_report_id_seq" OWNED BY "public"."task_report"."task_report_id";
ALTER SEQUENCE "public"."task_status_task_status_id_seq" OWNED BY "public"."task_status"."task_status_id";
ALTER SEQUENCE "public"."task_task_id_seq" OWNED BY "public"."task"."task_id";
ALTER SEQUENCE "public"."translation_translation_id_seq" OWNED BY "public"."translation"."translation_id";
ALTER SEQUENCE "public"."user_company_skill_user_company_skill_id_seq" OWNED BY "public"."user_company_skill"."user_company_skill_id";
ALTER SEQUENCE "public"."user_log_user_log_id_seq" OWNED BY "public"."user_log"."user_log_id";
ALTER SEQUENCE "public"."user_role_user_role_id_seq" OWNED BY "public"."user_role"."user_role_id";
ALTER SEQUENCE "public"."user_status_user_status_id_seq" OWNED BY "public"."user_status"."user_status_id";
ALTER SEQUENCE "public"."user_user_id_seq" OWNED BY "public"."user"."user_id";

-- ----------------------------
-- Primary Key structure for table additional_permision_log
-- ----------------------------
ALTER TABLE "public"."additional_permision_log" ADD PRIMARY KEY ("additional_permision_log_id");

-- ----------------------------
-- Uniques structure for table company
-- ----------------------------
ALTER TABLE "public"."company" ADD UNIQUE ("ico");
ALTER TABLE "public"."company" ADD UNIQUE ("dic");

-- ----------------------------
-- Primary Key structure for table company
-- ----------------------------
ALTER TABLE "public"."company" ADD PRIMARY KEY ("company_id");

-- ----------------------------
-- Primary Key structure for table company_customer
-- ----------------------------
ALTER TABLE "public"."company_customer" ADD PRIMARY KEY ("company_customer_id");

-- ----------------------------
-- Primary Key structure for table company_customer_contact
-- ----------------------------
ALTER TABLE "public"."company_customer_contact" ADD PRIMARY KEY ("company_customer_contact_id");

-- ----------------------------
-- Primary Key structure for table company_customer_contact_log
-- ----------------------------
ALTER TABLE "public"."company_customer_contact_log" ADD PRIMARY KEY ("company_customer_contact_log_id");

-- ----------------------------
-- Primary Key structure for table company_customer_contact_status
-- ----------------------------
ALTER TABLE "public"."company_customer_contact_status" ADD PRIMARY KEY ("company_customer_contact_status_id");

-- ----------------------------
-- Primary Key structure for table company_customer_log
-- ----------------------------
ALTER TABLE "public"."company_customer_log" ADD PRIMARY KEY ("company_customer_log_id");

-- ----------------------------
-- Primary Key structure for table company_customer_status
-- ----------------------------
ALTER TABLE "public"."company_customer_status" ADD PRIMARY KEY ("company_customer_status_id");

-- ----------------------------
-- Primary Key structure for table company_internal
-- ----------------------------
ALTER TABLE "public"."company_internal" ADD PRIMARY KEY ("company_internal_id");

-- ----------------------------
-- Primary Key structure for table company_internal_log
-- ----------------------------
ALTER TABLE "public"."company_internal_log" ADD PRIMARY KEY ("company_internal_log_id");

-- ----------------------------
-- Uniques structure for table company_internal_logo
-- ----------------------------
ALTER TABLE "public"."company_internal_logo" ADD UNIQUE ("company_internal_id");

-- ----------------------------
-- Primary Key structure for table company_internal_logo
-- ----------------------------
ALTER TABLE "public"."company_internal_logo" ADD PRIMARY KEY ("logo_id");

-- ----------------------------
-- Primary Key structure for table company_internal_status
-- ----------------------------
ALTER TABLE "public"."company_internal_status" ADD PRIMARY KEY ("company_internal_status_id");

-- ----------------------------
-- Uniques structure for table company_skill
-- ----------------------------
ALTER TABLE "public"."company_skill" ADD UNIQUE ("company_internal_id", "name");

-- ----------------------------
-- Primary Key structure for table company_skill
-- ----------------------------
ALTER TABLE "public"."company_skill" ADD PRIMARY KEY ("company_skill_id");

-- ----------------------------
-- Primary Key structure for table company_user
-- ----------------------------
ALTER TABLE "public"."company_user" ADD PRIMARY KEY ("company_user_id");

-- ----------------------------
-- Uniques structure for table email_notification
-- ----------------------------
ALTER TABLE "public"."email_notification" ADD UNIQUE ("company_internal_id", "task_id", "project_id");

-- ----------------------------
-- Primary Key structure for table email_notification
-- ----------------------------
ALTER TABLE "public"."email_notification" ADD PRIMARY KEY ("email_notification_id");

-- ----------------------------
-- Uniques structure for table email_notification_user
-- ----------------------------
ALTER TABLE "public"."email_notification_user" ADD UNIQUE ("email_notification_id", "user_id");

-- ----------------------------
-- Primary Key structure for table email_notification_user
-- ----------------------------
ALTER TABLE "public"."email_notification_user" ADD PRIMARY KEY ("email_notification_user_id");

-- ----------------------------
-- Primary Key structure for table holiday
-- ----------------------------
ALTER TABLE "public"."holiday" ADD PRIMARY KEY ("holiday_id");

-- ----------------------------
-- Primary Key structure for table holiday_log
-- ----------------------------
ALTER TABLE "public"."holiday_log" ADD PRIMARY KEY ("holiday_log_id");

-- ----------------------------
-- Primary Key structure for table holiday_restriction
-- ----------------------------
ALTER TABLE "public"."holiday_restriction" ADD PRIMARY KEY ("holiday_restriction_id");

-- ----------------------------
-- Primary Key structure for table holiday_status
-- ----------------------------
ALTER TABLE "public"."holiday_status" ADD PRIMARY KEY ("holiday_status_id");

-- ----------------------------
-- Primary Key structure for table holiday_type
-- ----------------------------
ALTER TABLE "public"."holiday_type" ADD PRIMARY KEY ("holiday_type_id");

-- ----------------------------
-- Primary Key structure for table holiday_user_restriction
-- ----------------------------
ALTER TABLE "public"."holiday_user_restriction" ADD PRIMARY KEY ("holiday_user_restriction_id");

-- ----------------------------
-- Primary Key structure for table invoice
-- ----------------------------
ALTER TABLE "public"."invoice" ADD PRIMARY KEY ("invoice_id");

-- ----------------------------
-- Primary Key structure for table invoice_item
-- ----------------------------
ALTER TABLE "public"."invoice_item" ADD PRIMARY KEY ("invoice_item_id");

-- ----------------------------
-- Primary Key structure for table invoice_log
-- ----------------------------
ALTER TABLE "public"."invoice_log" ADD PRIMARY KEY ("invoice_log_id");

-- ----------------------------
-- Primary Key structure for table invoice_status
-- ----------------------------
ALTER TABLE "public"."invoice_status" ADD PRIMARY KEY ("invoice_status_id");

-- ----------------------------
-- Primary Key structure for table measure_unit
-- ----------------------------
ALTER TABLE "public"."measure_unit" ADD PRIMARY KEY ("measure_unit_id");

-- ----------------------------
-- Primary Key structure for table order
-- ----------------------------
ALTER TABLE "public"."order" ADD PRIMARY KEY ("order_id");

-- ----------------------------
-- Primary Key structure for table order_item
-- ----------------------------
ALTER TABLE "public"."order_item" ADD PRIMARY KEY ("order_item_id");

-- ----------------------------
-- Primary Key structure for table order_item_log
-- ----------------------------
ALTER TABLE "public"."order_item_log" ADD PRIMARY KEY ("order_item_log_id");

-- ----------------------------
-- Primary Key structure for table order_item_status
-- ----------------------------
ALTER TABLE "public"."order_item_status" ADD PRIMARY KEY ("order_item_status_id");

-- ----------------------------
-- Primary Key structure for table order_log
-- ----------------------------
ALTER TABLE "public"."order_log" ADD PRIMARY KEY ("order_log_id");

-- ----------------------------
-- Primary Key structure for table order_status
-- ----------------------------
ALTER TABLE "public"."order_status" ADD PRIMARY KEY ("order_status_id");

-- ----------------------------
-- Uniques structure for table password_reset_token
-- ----------------------------
ALTER TABLE "public"."password_reset_token" ADD UNIQUE ("token");

-- ----------------------------
-- Primary Key structure for table permision
-- ----------------------------
ALTER TABLE "public"."permision" ADD PRIMARY KEY ("permision_id");

-- ----------------------------
-- Primary Key structure for table project
-- ----------------------------
ALTER TABLE "public"."project" ADD PRIMARY KEY ("project_id");

-- ----------------------------
-- Uniques structure for table project_budget
-- ----------------------------
ALTER TABLE "public"."project_budget" ADD UNIQUE ("project_id");

-- ----------------------------
-- Primary Key structure for table project_budget
-- ----------------------------
ALTER TABLE "public"."project_budget" ADD PRIMARY KEY ("project_budget_id");

-- ----------------------------
-- Primary Key structure for table project_cost_item
-- ----------------------------
ALTER TABLE "public"."project_cost_item" ADD PRIMARY KEY ("project_cost_item_id");

-- ----------------------------
-- Primary Key structure for table project_log
-- ----------------------------
ALTER TABLE "public"."project_log" ADD PRIMARY KEY ("project_log_id");

-- ----------------------------
-- Uniques structure for table project_skill
-- ----------------------------
ALTER TABLE "public"."project_skill" ADD UNIQUE ("project_id", "company_skill_id");

-- ----------------------------
-- Primary Key structure for table project_skill
-- ----------------------------
ALTER TABLE "public"."project_skill" ADD PRIMARY KEY ("project_skill_id");

-- ----------------------------
-- Primary Key structure for table project_status
-- ----------------------------
ALTER TABLE "public"."project_status" ADD PRIMARY KEY ("project_status_id");

-- ----------------------------
-- Primary Key structure for table project_user_log
-- ----------------------------
ALTER TABLE "public"."project_user_log" ADD PRIMARY KEY ("project_user_log_id");

-- ----------------------------
-- Primary Key structure for table project_user_status
-- ----------------------------
ALTER TABLE "public"."project_user_status" ADD PRIMARY KEY ("project_user_status_id");

-- ----------------------------
-- Primary Key structure for table report_summary
-- ----------------------------
ALTER TABLE "public"."report_summary" ADD PRIMARY KEY ("report_summary_id");

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "public"."role" ADD PRIMARY KEY ("role_id");

-- ----------------------------
-- Primary Key structure for table role_permision
-- ----------------------------
ALTER TABLE "public"."role_permision" ADD PRIMARY KEY ("role_permision_id");

-- ----------------------------
-- Uniques structure for table sallary
-- ----------------------------
ALTER TABLE "public"."sallary" ADD UNIQUE ("user_id");

-- ----------------------------
-- Primary Key structure for table sallary
-- ----------------------------
ALTER TABLE "public"."sallary" ADD PRIMARY KEY ("sallary_id");

-- ----------------------------
-- Primary Key structure for table sallary_history
-- ----------------------------
ALTER TABLE "public"."sallary_history" ADD PRIMARY KEY ("sallary_history_id");

-- ----------------------------
-- Primary Key structure for table saved_sallary
-- ----------------------------
ALTER TABLE "public"."saved_sallary" ADD PRIMARY KEY ("saved_sallary_id");

-- ----------------------------
-- Uniques structure for table setting
-- ----------------------------
ALTER TABLE "public"."setting" ADD UNIQUE ("company_internal_id", "user_id");

-- ----------------------------
-- Primary Key structure for table setting
-- ----------------------------
ALTER TABLE "public"."setting" ADD PRIMARY KEY ("setting_id");

-- ----------------------------
-- Primary Key structure for table task
-- ----------------------------
ALTER TABLE "public"."task" ADD PRIMARY KEY ("task_id");

-- ----------------------------
-- Primary Key structure for table task_log
-- ----------------------------
ALTER TABLE "public"."task_log" ADD PRIMARY KEY ("task_log_id");

-- ----------------------------
-- Primary Key structure for table task_report
-- ----------------------------
ALTER TABLE "public"."task_report" ADD PRIMARY KEY ("task_report_id");

-- ----------------------------
-- Primary Key structure for table task_report_status
-- ----------------------------
ALTER TABLE "public"."task_report_status" ADD PRIMARY KEY ("task_report_status_id");

-- ----------------------------
-- Primary Key structure for table task_status
-- ----------------------------
ALTER TABLE "public"."task_status" ADD PRIMARY KEY ("task_status_id");

-- ----------------------------
-- Indexes structure for table translation
-- ----------------------------
CREATE UNIQUE INDEX "translation_key_id" ON "public"."translation" USING btree (key);

-- ----------------------------
-- Uniques structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD UNIQUE ("login");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Uniques structure for table user_company_skill
-- ----------------------------
ALTER TABLE "public"."user_company_skill" ADD UNIQUE ("user_id", "company_skill_id");

-- ----------------------------
-- Primary Key structure for table user_company_skill
-- ----------------------------
ALTER TABLE "public"."user_company_skill" ADD PRIMARY KEY ("user_company_skill_id");

-- ----------------------------
-- Primary Key structure for table user_log
-- ----------------------------
ALTER TABLE "public"."user_log" ADD PRIMARY KEY ("user_log_id");

-- ----------------------------
-- Primary Key structure for table user_role
-- ----------------------------
ALTER TABLE "public"."user_role" ADD PRIMARY KEY ("user_role_id");

-- ----------------------------
-- Primary Key structure for table user_status
-- ----------------------------
ALTER TABLE "public"."user_status" ADD PRIMARY KEY ("user_status_id");

-- ----------------------------
-- Foreign Key structure for table "public"."additional_permision"
-- ----------------------------
ALTER TABLE "public"."additional_permision" ADD FOREIGN KEY ("permision_id") REFERENCES "public"."permision" ("permision_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."additional_permision" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."additional_permision" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."additional_permision_log"
-- ----------------------------
ALTER TABLE "public"."additional_permision_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."additional_permision_log" ADD FOREIGN KEY ("permision_id") REFERENCES "public"."permision" ("permision_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."additional_permision_log" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."additional_permision_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_customer"
-- ----------------------------
ALTER TABLE "public"."company_customer" ADD FOREIGN KEY ("company_customer_status_id") REFERENCES "public"."company_customer_status" ("company_customer_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer" ADD FOREIGN KEY ("company_id") REFERENCES "public"."company" ("company_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_customer_contact"
-- ----------------------------
ALTER TABLE "public"."company_customer_contact" ADD FOREIGN KEY ("company_customer_contact_status_id") REFERENCES "public"."company_customer_contact_status" ("company_customer_contact_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_contact" ADD FOREIGN KEY ("company_customer_id") REFERENCES "public"."company_customer" ("company_customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_customer_contact_log"
-- ----------------------------
ALTER TABLE "public"."company_customer_contact_log" ADD FOREIGN KEY ("company_customer_contact_status_id") REFERENCES "public"."company_customer_contact_status" ("company_customer_contact_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_contact_log" ADD FOREIGN KEY ("company_customer_id") REFERENCES "public"."company_customer" ("company_customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_contact_log" ADD FOREIGN KEY ("company_customer_contact_id") REFERENCES "public"."company_customer_contact" ("company_customer_contact_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_customer_log"
-- ----------------------------
ALTER TABLE "public"."company_customer_log" ADD FOREIGN KEY ("company_customer_id") REFERENCES "public"."company_customer" ("company_customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_log" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_customer_log" ADD FOREIGN KEY ("company_customer_status_id") REFERENCES "public"."company_customer_status" ("company_customer_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_internal"
-- ----------------------------
ALTER TABLE "public"."company_internal" ADD FOREIGN KEY ("company_id") REFERENCES "public"."company" ("company_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_internal" ADD FOREIGN KEY ("company_internal_status_id") REFERENCES "public"."company_internal_status" ("company_internal_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_internal_log"
-- ----------------------------
ALTER TABLE "public"."company_internal_log" ADD FOREIGN KEY ("company_internal_status_id") REFERENCES "public"."company_internal_status" ("company_internal_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_internal_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_internal_log" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_internal_logo"
-- ----------------------------
ALTER TABLE "public"."company_internal_logo" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_skill"
-- ----------------------------
ALTER TABLE "public"."company_skill" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."company_user"
-- ----------------------------
ALTER TABLE "public"."company_user" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."company_user" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."email_notification"
-- ----------------------------
ALTER TABLE "public"."email_notification" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."email_notification" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."email_notification" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."email_notification_user"
-- ----------------------------
ALTER TABLE "public"."email_notification_user" ADD FOREIGN KEY ("email_notification_id") REFERENCES "public"."email_notification" ("email_notification_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."email_notification_user" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."holiday"
-- ----------------------------
ALTER TABLE "public"."holiday" ADD FOREIGN KEY ("holiday_type_id") REFERENCES "public"."holiday_type" ("holiday_type_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday" ADD FOREIGN KEY ("holiday_status_id") REFERENCES "public"."holiday_status" ("holiday_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."holiday_log"
-- ----------------------------
ALTER TABLE "public"."holiday_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday_log" ADD FOREIGN KEY ("holiday_type_id") REFERENCES "public"."holiday_type" ("holiday_type_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday_log" ADD FOREIGN KEY ("holiday_status_id") REFERENCES "public"."holiday_status" ("holiday_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday_log" ADD FOREIGN KEY ("holiday_id") REFERENCES "public"."holiday" ("holiday_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."holiday_restriction"
-- ----------------------------
ALTER TABLE "public"."holiday_restriction" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."holiday_user_restriction"
-- ----------------------------
ALTER TABLE "public"."holiday_user_restriction" ADD FOREIGN KEY ("holiday_restriction_id") REFERENCES "public"."holiday_restriction" ("holiday_restriction_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."holiday_user_restriction" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."invoice"
-- ----------------------------
ALTER TABLE "public"."invoice" ADD FOREIGN KEY ("company_customer_id") REFERENCES "public"."company_customer" ("company_customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice" ADD FOREIGN KEY ("invoice_status_id") REFERENCES "public"."invoice_status" ("invoice_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice" ADD FOREIGN KEY ("currency_id") REFERENCES "public"."measure_unit" ("measure_unit_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."invoice_item"
-- ----------------------------
ALTER TABLE "public"."invoice_item" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice_item" ADD FOREIGN KEY ("invoice_id") REFERENCES "public"."invoice" ("invoice_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice_item" ADD FOREIGN KEY ("measure_unit_id") REFERENCES "public"."measure_unit" ("measure_unit_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."invoice_log"
-- ----------------------------
ALTER TABLE "public"."invoice_log" ADD FOREIGN KEY ("invoice_id") REFERENCES "public"."invoice" ("invoice_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."invoice_log" ADD FOREIGN KEY ("invoice_status_id") REFERENCES "public"."invoice_status" ("invoice_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."order"
-- ----------------------------
ALTER TABLE "public"."order" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order" ADD FOREIGN KEY ("order_status_id") REFERENCES "public"."order_status" ("order_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order" ADD FOREIGN KEY ("company_customer_id") REFERENCES "public"."company_customer" ("company_customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order" ADD FOREIGN KEY ("currency_id") REFERENCES "public"."measure_unit" ("measure_unit_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."order_item"
-- ----------------------------
ALTER TABLE "public"."order_item" ADD FOREIGN KEY ("order_id") REFERENCES "public"."order" ("order_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_item" ADD FOREIGN KEY ("measure_unit_id") REFERENCES "public"."measure_unit" ("measure_unit_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_item" ADD FOREIGN KEY ("order_item_status_id") REFERENCES "public"."order_item_status" ("order_item_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."order_item_log"
-- ----------------------------
ALTER TABLE "public"."order_item_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_item_log" ADD FOREIGN KEY ("order_item_status_id") REFERENCES "public"."order_item_status" ("order_item_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_item_log" ADD FOREIGN KEY ("measure_unit_id") REFERENCES "public"."measure_unit" ("measure_unit_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_item_log" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."order_log"
-- ----------------------------
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("order_status_id") REFERENCES "public"."order_status" ("order_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("order_id") REFERENCES "public"."order" ("order_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("order_id") REFERENCES "public"."order" ("order_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("order_id") REFERENCES "public"."order" ("order_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."order_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."password_reset_token"
-- ----------------------------
ALTER TABLE "public"."password_reset_token" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."password_reset_token" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."password_reset_token" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project"
-- ----------------------------
ALTER TABLE "public"."project" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_budget"
-- ----------------------------
ALTER TABLE "public"."project_budget" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_cost_item"
-- ----------------------------
ALTER TABLE "public"."project_cost_item" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_cost_item" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_cost_item" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_log"
-- ----------------------------
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_status_id") REFERENCES "public"."project_status" ("project_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_skill"
-- ----------------------------
ALTER TABLE "public"."project_skill" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_skill" ADD FOREIGN KEY ("company_skill_id") REFERENCES "public"."company_skill" ("company_skill_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_user"
-- ----------------------------
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."project_user_log"
-- ----------------------------
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_user_status_id") REFERENCES "public"."project_user_status" ("project_user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_user_status_id") REFERENCES "public"."project_user_status" ("project_user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_user_status_id") REFERENCES "public"."project_user_status" ("project_user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."project_user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."report_summary"
-- ----------------------------
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("saved_sallary_id") REFERENCES "public"."saved_sallary" ("saved_sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("saved_sallary_id") REFERENCES "public"."saved_sallary" ("saved_sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("saved_sallary_id") REFERENCES "public"."saved_sallary" ("saved_sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."report_summary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."role"
-- ----------------------------
ALTER TABLE "public"."role" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."role_permision"
-- ----------------------------
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("permision_id") REFERENCES "public"."permision" ("permision_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("permision_id") REFERENCES "public"."permision" ("permision_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("permision_id") REFERENCES "public"."permision" ("permision_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("role_id") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("role_id") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_permision" ADD FOREIGN KEY ("role_id") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."sallary"
-- ----------------------------
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."sallary_history"
-- ----------------------------
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("user_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("user_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("user_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_history" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."sallary_log"
-- ----------------------------
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("sallary_id") REFERENCES "public"."sallary" ("sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("sallary_id") REFERENCES "public"."sallary" ("sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("sallary_id") REFERENCES "public"."sallary" ("sallary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."sallary_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."saved_sallary"
-- ----------------------------
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("sallary_history_id") REFERENCES "public"."sallary_history" ("sallary_history_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("sallary_history_id") REFERENCES "public"."sallary_history" ("sallary_history_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("sallary_history_id") REFERENCES "public"."sallary_history" ("sallary_history_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."saved_sallary" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."setting"
-- ----------------------------
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."setting" ADD FOREIGN KEY ("company_internal_id") REFERENCES "public"."company_internal" ("company_internal_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."task"
-- ----------------------------
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("project_id") REFERENCES "public"."project" ("project_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("task_task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."task_log"
-- ----------------------------
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("task_status_id") REFERENCES "public"."task_status" ("task_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("order_item_id") REFERENCES "public"."order_item" ("order_item_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."task_report"
-- ----------------------------
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."task_report_log"
-- ----------------------------
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_id") REFERENCES "public"."task_report" ("task_report_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_id") REFERENCES "public"."task_report" ("task_report_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_status_id") REFERENCES "public"."task_report_status" ("task_report_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("task_report_id") REFERENCES "public"."task_report" ("task_report_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("report_summary_id") REFERENCES "public"."report_summary" ("report_summary_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."task_report_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."user"
-- ----------------------------
ALTER TABLE "public"."user" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."user_company_skill"
-- ----------------------------
ALTER TABLE "public"."user_company_skill" ADD FOREIGN KEY ("company_skill_id") REFERENCES "public"."company_skill" ("company_skill_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_company_skill" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."user_log"
-- ----------------------------
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("modified_user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_log" ADD FOREIGN KEY ("user_status_id") REFERENCES "public"."user_status" ("user_status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."user_role"
-- ----------------------------
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("role") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("role") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("role") REFERENCES "public"."role" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_role" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


--  ----------------------------
-- create table with sequence and FK "public"."task_task_attachment_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."task_task_attachment_id_seq";
CREATE SEQUENCE "public"."task_task_attachment_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

DROP TABLE IF EXISTS "public"."task_attachment";
CREATE TABLE "public"."task_attachment" (
 "task_atachment_id" int8 DEFAULT nextval('"public".task_task_attachment_id_seq'::regclass) NOT NULL,
 "filename" text NOT NULL,
 "path" text NOT NULL,
 "active" bool NOT NULL,
 "task_id" int8 NOT NULL,
 "extension" text NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."task_attachment" ADD FOREIGN KEY ("task_id") REFERENCES "public"."task" ("task_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER SEQUENCE "public"."task_task_attachment_id_seq" OWNED BY "public"."task_attachment"."task_atachment_id_id";
ALTER TABLE "public"."task_attachment" add PRIMARY KEY ("task_atachment_id");

DROP TABLE IF EXISTS "public"."messages";
CREATE TABLE "public"."messages"
(
 "messageId" int8 DEFAULT nextval('"public".messages_id_seq'::regclass) NOT NULL,
 "header"          text                                                                   NOT NULL,
 "text"              text                                                                   NOT NULL,
 "active"            bool                                                                   NOT NULL
)
 WITH (OIDS=FALSE);

DROP TABLE IF EXISTS "public"."user_messages";
CREATE TABLE "public"."user_messages"
(
 "user_message_id" int8 DEFAULT nextval('"public".user_messages_id_seq'::regclass) NOT NULL,
 "user_id"          text                                                                   NOT NULL,
 "message_id"              text                                                                   NOT NULL,
 "read"            bool                                                                   NOT NULL
)
 WITH (OIDS=FALSE);
ALTER TABLE "public"."user_messages" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_messages" ADD FOREIGN KEY ("message_id") REFERENCES "public"."messages" ("message_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
