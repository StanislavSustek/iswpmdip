/*
Navicat PGSQL Data Transfer

Source Server         : iSWPM
Source Server Version : 90401
Source Host           : forwarding.isw.sk:5000
Source Database       : iswpm
Source Schema         : pm

Target Server Type    : PGSQL
Target Server Version : 90401
File Encoding         : 65001

Date: 2017-07-18 15:36:59
*/


-- ----------------------------
-- +++++ BEGIN STATUS TABLES DATA
-- ----------------------------

-- ----------------------------
-- Records of company_customer_contact_status
-- ----------------------------
INSERT INTO "public"."company_customer_contact_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."company_customer_contact_status" ("name", "active") VALUES ('DELETED', 't');

-- ----------------------------
-- Records of company_customer_status
-- ----------------------------
INSERT INTO "public"."company_customer_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."company_customer_status" ("name", "active") VALUES ('DELETED', 't');

-- ----------------------------
-- Records of company_internal_status
-- ----------------------------
INSERT INTO "public"."company_internal_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."company_internal_status" ("name", "active") VALUES ('DELETED', 't');

-- ----------------------------
-- Records of holiday_status
-- ----------------------------
INSERT INTO "public"."holiday_status" ("name", "active") VALUES ('REQUESTED', 't');
INSERT INTO "public"."holiday_status" ("name", "active") VALUES ('CONFIRMED', 't');
INSERT INTO "public"."holiday_status" ("name", "active") VALUES ('DENIED', 't');
INSERT INTO "public"."holiday_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."holiday_status" ("name", "active") VALUES ('ENDED', 't');

-- ----------------------------
-- Records of invoice_status
-- ----------------------------
INSERT INTO "public"."invoice_status" ("name", "active") VALUES ('ISSUED', 't');
INSERT INTO "public"."invoice_status" ("name", "active") VALUES ('CANCELED', 't');
INSERT INTO "public"."invoice_status" ("name", "active") VALUES ('DELETED', 't');
INSERT INTO "public"."invoice_status" ("name", "active") VALUES ('PAID', 't');
INSERT INTO "public"."invoice_status" ("name", "active") VALUES ('SAVED', 'f');

-- ----------------------------
-- Records of order_item_status
-- ----------------------------
INSERT INTO "public"."order_item_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."order_item_status" ("name", "active") VALUES ('DELETED', 't');

-- ----------------------------
-- Records of order_status
-- ----------------------------
INSERT INTO "public"."order_status" ("name", "active") VALUES ('OPEN', 't');
INSERT INTO "public"."order_status" ("name", "active") VALUES ('IN_PROGRESS', 't');
INSERT INTO "public"."order_status" ("name", "active") VALUES ('COMPLETED', 'f');
INSERT INTO "public"."order_status" ("name", "active") VALUES ('CANCELED', 'f');
INSERT INTO "public"."order_status" ("name", "active") VALUES ('DELETED', 'f');
INSERT INTO "public"."order_status" ("name", "active") VALUES ('PAID', 't');

-- ----------------------------
-- Records of project_status
-- ----------------------------
INSERT INTO "public"."project_status" ("name", "active") VALUES ('OPEN', 't');
INSERT INTO "public"."project_status" ("name", "active") VALUES ('COMPLETED', 't');
INSERT INTO "public"."project_status" ("name", "active") VALUES ('IN_PROGRESS', 't');
INSERT INTO "public"."project_status" ("name", "active") VALUES ('STOPPED', 't');
INSERT INTO "public"."project_status" ("name", "active") VALUES ('DELETED', 't');

-- ----------------------------
-- Records of project_user_status
-- ----------------------------
INSERT INTO "public"."project_user_status" ("name", "active") VALUES ('ADDED', 't');
INSERT INTO "public"."project_user_status" ("name", "active") VALUES ('REMOVED', 't');

-- ----------------------------
-- Records of task_report_status
-- ----------------------------
INSERT INTO "public"."task_report_status" ("name", "description", "active") VALUES ('wait_for_check', 'Caka na schvalenie', 't');
INSERT INTO "public"."task_report_status" ("name", "description", "active") VALUES ('confirmed', 'Schvaleny', 't');
INSERT INTO "public"."task_report_status" ("name", "description", "active") VALUES ('rejected', 'Zamietnuty', 't');
INSERT INTO "public"."task_report_status" ("name", "description", "active") VALUES ('blocked', 'Zablokovany', 't');

-- ----------------------------
-- Records of task_status
-- ----------------------------
INSERT INTO "public"."task_status" ("name", "active") VALUES ('new', 't');
INSERT INTO "public"."task_status" ("name", "active") VALUES ('closed', 'f');
INSERT INTO "public"."task_status" ("name", "active") VALUES ('rejected', 'f');
INSERT INTO "public"."task_status" ("name", "active") VALUES ('in_progress', 't');
INSERT INTO "public"."task_status" ("name", "active") VALUES ('resolved', 't');
INSERT INTO "public"."task_status" ("name", "active") VALUES ('deleted', 'f');

-- ----------------------------
-- Records of user_status
-- ----------------------------
INSERT INTO "public"."user_status" ("name", "active") VALUES ('ACTIVE', 't');
INSERT INTO "public"."user_status" ("name", "active") VALUES ('DELETED', 't');
INSERT INTO "public"."user_status" ("name", "active") VALUES ('BLOCKED', 't');
INSERT INTO "public"."user_status" ("name", "active") VALUES ('BLOCKED_PASSWORD_CHANGING', 't');
-- ----------------------------
-- +++++ END STATUS TABLES DATA
-- ----------------------------



















-- ----------------------------
-- +++++ BEGIN CONSTANT TABLES DATA
-- ----------------------------

-- ----------------------------
-- Records of holiday_type
-- ----------------------------
INSERT INTO "public"."holiday_type" ("name") VALUES ('HOLIDAY');
INSERT INTO "public"."holiday_type" ("name") VALUES ('SICKNESS_ABSENCE');
INSERT INTO "public"."holiday_type" ("name") VALUES ('UNPAID_HOLIDAY');
INSERT INTO "public"."holiday_type" ("name") VALUES ('DOCTOR');

-- ----------------------------
-- Records of measure_unit
-- ----------------------------
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('pc', 't', 'quantity');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('md', 't', 'quantity');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('h', 't', 'quantity');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('EUR', 't', 'currency');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('USD', 't', 'currency');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('LIB', 't', 'currency');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('CZK', 't', 'currency');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('PLN', 't', 'currency');
INSERT INTO "public"."measure_unit" ("unit", "active", "type") VALUES ('RUB', 't', 'currency');

-- ----------------------------
-- Records of permision
-- ----------------------------
INSERT INTO "public"."permision" ("permision_id", "description", "name", "group") VALUES ( '-1', 'Pravo urcena pre super usera', 'SYSTEM_ADMIN', null);
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_DELETE_ROLE', 'ROLE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_DELETE_ROLE_OF_USER', 'ROLE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_DELETE_ADDITIONAL_PERMS_OF_USER', 'ROLE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_TASK_REPORT_OVERVIEW', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_TASK_REPORT_HISTORY', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MAKE_TASK_REPORT_CONTROL', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'CREATE_TASK_REPORT', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'CREATE_PAYMENT_AUTOMATICALLY', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'CREATE_PAYMENT_MANUALLY', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PAYMENT_DETAIL_PAGE', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_SAVED_PAYMENT_DETAIL_PAGE', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PAYMENT_HISTORY_PAGE', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'CONFRIM_PAYMENT', 'PAYMENT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PROJECTS_PAGE', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_PROJECT', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_PROJECT', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PROJECT_DETAIL_PAGE', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYEES_ON_PROJECT', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_DELETE_EMPLOYEES_ON_PROJECT', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PROJECT_COSTS_PAGE', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_PROJECT_COST', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_PROJECT_COST', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PROJECT_HISTORY_PAGE', 'PROJECT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_PROJECT_TAKS_PAGE', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_TASK', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_TASK', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_TASK_DETAIL_PAGE', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'CLOSE_UNCLOSE_TASK', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_TASK_HISTORY_PAGE', 'TASK');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_TASK_TASK_REPORT_PAGE', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_MY_PROJECTS_REPORTS_TASKS', 'REPORT');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_ORDERS_PAGE', 'ORDER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_ORDER', 'ORDER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_ORDER', 'ORDER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MARK_ORDER_AS_COMPLETED', 'ORDER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MARK_ORDER_AS_CANCELLED', 'ORDER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_INVOICE', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_INVOICE', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_INVOICE_PAGES', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MARK_INVOICE_AS_PAID', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MARK_INVOICE_AS_CANCELED', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'MARK_INVOICE_AS_ISSUED', 'INVOICE');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYEES_PAGE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYEE_DETAIL_PAGE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_EMPLOYEE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_EMPLOYEE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYEE_CONTRACT', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'RESTORE_PASSWORD_OF_EMPLOYEE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_ASSIGNED_TASKS_OF_EMPLOYEE_PAGE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_ASSIGNED_PROJECTS_OF_EMPLOYEE_PAGE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYEE_STATS', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMPLOYE_HISTORY_PAGE', 'USER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_CUSTOMER', 'CUSTOMER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'DELETE_CUSTOMER', 'CUSTOMER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_CUSTOMERS_PAGES', 'CUSTOMER');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_HOLIDAYS', 'HOLIDAY_PN');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'PROCCESS_REQUEST_FOR_HOLIDAY', 'HOLIDAY_PN');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_EMAIL_NOTIFICATION', 'EMAIL_NOTIFICATION');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_EDIT_EMAIL_NOTIFICATION', 'EMAIL_NOTIFICATION');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'ADD_REMOVE_EMPLOYEE_HOLIDAY_RESTRICTION', 'HOLIDAY_PN');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'EDIT_COMPANY_ADMIN', 'ADMIN');
INSERT INTO "public"."permision" ("description", "name", "group") VALUES ( null, 'SHOW_BUDGET', 'ADMIN');

-- ----------------------------
-- Records of translation
-- ----------------------------
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('ACTIVE', 'Aktivny', 'Active');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('DELETED', 'Zmazany', 'Deleted');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('BLOCKED', 'Blokovany', 'Blocked');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('BLOCKED_PASSWORD_CHANGING', 'Blokovany - zmena hesla', 'Blocked - password changing');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('OPEN', 'Otvoreny', 'Open');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('IN_PROGRESS', 'Prebieha', 'In progress');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('CANCELED', 'Zruseny', 'Canceled');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('COMPLETED', 'Hotovy', 'Completed');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ('PAID', 'Zaplateny', 'Paid');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'TRUE', 'Ano', 'Yes');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'FALSE', 'Nie', 'No');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'INVOICE', 'FAKTÚRA', 'INVOICE');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'issueDate', 'Dátum vystavenia', 'Date of issue');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'invoiceNumber', 'Číslo faktúry', 'Invoice number');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'customer', 'Zákazník', 'Customer');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'provider', 'Dodávateľ', 'Provider');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'ico', 'IČO', 'Business ID');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'dic', 'DIČ', 'Tax ID');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'name', 'Názov', 'Name');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'without_VAT', 'bez DPH', 'without VAT');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'with_VAT', 's DPH', 'with VAT');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'quantity', 'Množstvo', 'Quantity');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'unit', 'Jednotka', 'Unit');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'Price_with_VAT', 'Cena s DPH', 'Price with VAT');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'total', 'Celkovo', 'Total');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'onInvoices', 'Vyfaktúrovaná suma', 'Issued sum');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'notOnInvoices', 'Nevyfaktúrovaná suma', 'Not issued sum');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'orderNotContainsCompleteInvoicedItemns', 'Objednávka neobsahuje položky, ktoré už boli kompletne vyfaktúrované.', 'Invoice not contains items,which were completely issued.');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'ORDER', 'OBJEDNÁVKA', 'ORDER');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'dateFromTo', 'Dátum od - do', 'Date from - to');
INSERT INTO "public"."translation" ("key", "value_sk", "value_en") VALUES ( 'VAT', 'DPH', 'VAT');

-- ----------------------------
-- +++++ END CONSTANT TABLES DATA
-- ----------------------------







-- ----------------------------
-- +++++ BEGIN INIT TABLES DATA
-- ----------------------------

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO "public"."company" ("company_id", "ico", "dic", "street", "street_number", "psc", "company_name", "city", "modified") VALUES ('-1', NULL, NULL, NULL, NULL, NULL, 'iSWpublic Admistration', NULL, now());

-- ----------------------------
-- Records of company_internal
-- ----------------------------
INSERT INTO "public"."company_internal" ("company_internal_id", "company_id", "modified", "company_internal_status_id") VALUES ('-1', '-1', now(), '1');

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES ('-1', 'System', 'Admin', 'benco@isw.sk', '2e48a27cda5d62fa80a8c0bd2888040707b3d67c', 'superuser', 'administrator systemu, heslo: superuser963', now(), '1', now(), NULL);

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO "public"."role" ("role_id", "description", "name", "company_internal_id", "read_only") VALUES ('-1', 'Spravca systemu', 'system_administrator', '-1', 't');

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO "public"."user_role" ("user_role_id", "modified", "user_id", "role") VALUES ('-1', now(), '-1', '-1');

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO "public"."role_permision" ("role_permision_id", "role_id", "modified", "permision_id") VALUES ('-1', '-1', now(), '-1');

-- ----------------------------
-- Records of company_user
-- ----------------------------
INSERT INTO "public"."company_user" ("modified", "deleted", "deleted_timestamp", "user_id", "company_internal_id") VALUES (now(), 'f', NULL, '-1', '-1');

-- ----------------------------
-- Records of additional_permission
-- ----------------------------
INSERT INTO "public"."additional_permision" ("user_id", "permision_id", "company_internal_id") VALUES ('-1', '-1', '-1');


-- ----------------------------
-- +++++ END INIT TABLES DATA
-- ----------------------------



