FROM postgres:12.2-alpine
ENV POSTGRES_PASSWORD secret
ENV POSTGRES_DB iswpm
COPY ./SQL/Script/1-init-structure.sql /docker-entrypoint-initdb.d/
COPY ./SQL/Script/2-init-data.sql /docker-entrypoint-initdb.d/

