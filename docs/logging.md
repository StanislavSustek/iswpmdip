# iSWPM Logging

## Log categories
Each log category is prefixed with namespace `sk.isw.iswpm`.

**AUDIT**

## JBoss 6.4.0 Logging configuration

### Instalation settings
Create logging profile in CLI

Create log handlers
/subsystem=logging/periodic-rotating-file-handler=iswpm:add(file={path=>iswpm.log,relative-to="jboss.server.log.dir"},suffix=.yyyy-MM-dd,formatter="%d{HH:mm:ss,SSS} %-5p [%c] (%t) %X{session} %s%E%n",level=INFO)"
/subsystem=logging/periodic-rotating-file-handler=iswpm-debug:add(file={path=iswpm-debug.log,relative-to="jboss.server.log.dir"},suffix=.yyyy-MM-dd,formatter="%d{HH:mm:ss,SSS} %-5p [%c] (%t) %X{session} %s%E%n",level=DEBUG)

Create log categories
/subsystem=logging/logger=sk.isw.iswpm.AUDIT:add(level=INFO)

Assign log categories to log handlers
/subsystem=logging/logger=sk.isw.iswpm.AUDIT:assign-handler(name="iswpm")
/subsystem=logging/logger=sk.isw.iswpm.AUDIT:assign-handler(name="iswpm-debug")

### Change log level at runtime

To change actual log level of category AUDIT use this command
/subsystem=logging/logger=sk.isw.iswpm.AUDIT:change-log-level(level=DEBUG)

To check actual log level use this command
/subsystem=logging/logger=sk.isw.iswpm.AUDIT:read-attribute(name=level)