## iSWPM PostgreSQL Database JBoss configuration
# Adding Data Source to JBoss 6.4

## Download PostgreSQL JDBC driver
https://jdbc.postgresql.org/download.html

## Copy JDBC driver to $JBOSS_HOME/bin directory

## Create module for PostgreSQL JDBC driver
module add --name=org.postgresql.jdbc --resources=postgresql-9.4-1201.jdbc41.jar --dependencies=javax.api,javax.transaction.api

## Add JDBC driver to WildFly or JBoss
/subsystem=datasources/jdbc-driver=postgresql/:add(driver-name=postgresql, driver-module-name=org.postgresql.jdbc, driver-xa-datasource-class-name=org.postgresql.xa.PGXADataSource)

## Add PostgreSQL data source to JBoss
data-source add \
    --name=iswpmDS \
    --jndi-name=java:/iswpmDS \
    --use-java-context=true \
    --use-ccm=true \
    --connection-url=jdbc:postgresql://forwarding.isw.sk:5000/iswpm?currentSchema=pm \
    --driver-name=postgresql \
    --user-name=iswpm \
    --password=iswpm123 \
    --check-valid-connection-sql="SELECT 1" \
    --background-validation=true \
    --background-validation-millis=60000 \
    --flush-strategy=IdleConnections \
    --min-pool-size=10 \
    --max-pool-size=50 \
    --pool-prefill=false

## Enable datasource
/subsystem=datasources/data-source=iswpmDS:enable

## Test connection of datasource
/subsystem=datasources/data-source=iswpmDS:test-connection-in-pool

