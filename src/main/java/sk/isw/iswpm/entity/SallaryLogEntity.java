/*
package sk.isw.iswpm.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "sallary_log", schema = "public", catalog = "iswpm")
public class SallaryLogEntity {
    private long sallaryLogId;
    private long sallaryId;
    private BigDecimal hourRate;
    private BigDecimal monthlyRate;
    private Timestamp created;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private long modifiedUserId;

    @Basic
    @Column(name = "sallary_log_id")
    public long getSallaryLogId() {
        return sallaryLogId;
    }

    public void setSallaryLogId(long sallaryLogId) {
        this.sallaryLogId = sallaryLogId;
    }

    @Basic
    @Column(name = "sallary_id")
    public long getSallaryId() {
        return sallaryId;
    }

    public void setSallaryId(long sallaryId) {
        this.sallaryId = sallaryId;
    }

    @Basic
    @Column(name = "hour_rate")
    public BigDecimal getHourRate() {
        return hourRate;
    }

    public void setHourRate(BigDecimal hourRate) {
        this.hourRate = hourRate;
    }

    @Basic
    @Column(name = "monthly_rate")
    public BigDecimal getMonthlyRate() {
        return monthlyRate;
    }

    public void setMonthlyRate(BigDecimal monthlyRate) {
        this.monthlyRate = monthlyRate;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SallaryLogEntity that = (SallaryLogEntity) o;
        return sallaryLogId == that.sallaryLogId &&
                sallaryId == that.sallaryId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(hourRate, that.hourRate) &&
                Objects.equals(monthlyRate, that.monthlyRate) &&
                Objects.equals(created, that.created) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sallaryLogId, sallaryId, hourRate, monthlyRate, created, dateFrom, dateTo, modifiedUserId);
    }
}
*/
