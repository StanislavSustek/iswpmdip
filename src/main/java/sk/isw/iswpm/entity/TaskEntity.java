package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "task", schema = "public", catalog = "iswpm")
public class TaskEntity {
    private long taskId;
    private String name;
    private String description;
    private BigDecimal specialRewardHour;
    private BigDecimal specialReward;
    private Long taskTaskId;
    private long projectId;
    private BigDecimal taskPrice;
    private BigDecimal hoursAssigned;
    private BigDecimal hoursReported;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private BigDecimal progress;
    private long taskStatusId;
    private long userId;
    private Long orderItemId;
    private BigDecimal confirmedHours;
    private String priority;

    @Id
    @Column(name = "task_id")
    @GeneratedValue(generator = "taskSequence")
    @SequenceGenerator(name = "taskSequence", sequenceName = "task_task_id_seq")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "special_reward_hour")
    public BigDecimal getSpecialRewardHour() {
        return specialRewardHour;
    }

    public void setSpecialRewardHour(BigDecimal specialRewardHour) {
        this.specialRewardHour = specialRewardHour;
    }

    @Basic
    @Column(name = "special_reward")
    public BigDecimal getSpecialReward() {
        return specialReward;
    }

    public void setSpecialReward(BigDecimal specialReward) {
        this.specialReward = specialReward;
    }

    @Basic
    @Column(name = "task_task_id")
    public Long getTaskTaskId() {
        return taskTaskId;
    }

    public void setTaskTaskId(Long taskTaskId) {
        this.taskTaskId = taskTaskId;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "task_price")
    public BigDecimal getTaskPrice() {
        return taskPrice;
    }

    public void setTaskPrice(BigDecimal taskPrice) {
        this.taskPrice = taskPrice;
    }

    @Basic
    @Column(name = "hours_assigned")
    public BigDecimal getHoursAssigned() {
        return hoursAssigned;
    }

    public void setHoursAssigned(BigDecimal hoursAssigned) {
        this.hoursAssigned = hoursAssigned;
    }

    @Basic
    @Column(name = "hours_reported")
    public BigDecimal getHoursReported() {
        return hoursReported;
    }

    public void setHoursReported(BigDecimal hoursReported) {
        this.hoursReported = hoursReported;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "progress")
    public BigDecimal getProgress() {
        return progress;
    }

    public void setProgress(BigDecimal progress) {
        this.progress = progress;
    }

    @Basic
    @Column(name = "task_status_id")
    public long getTaskStatusId() {
        return taskStatusId;
    }

    public void setTaskStatusId(long taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "order_item_id")
    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Basic
    @Column(name = "confirmed_hours")
    public BigDecimal getConfirmedHours() {
        return confirmedHours;
    }

    public void setConfirmedHours(BigDecimal confirmedHours) {
        this.confirmedHours = confirmedHours;
    }

    @Column(name = "priority")
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskEntity that = (TaskEntity) o;
        return taskId == that.taskId &&
                projectId == that.projectId &&
                taskStatusId == that.taskStatusId &&
                userId == that.userId &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(specialRewardHour, that.specialRewardHour) &&
                Objects.equals(specialReward, that.specialReward) &&
                Objects.equals(taskTaskId, that.taskTaskId) &&
                Objects.equals(taskPrice, that.taskPrice) &&
                Objects.equals(hoursAssigned, that.hoursAssigned) &&
                Objects.equals(hoursReported, that.hoursReported) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(progress, that.progress) &&
                Objects.equals(orderItemId, that.orderItemId) &&
                Objects.equals(confirmedHours, that.confirmedHours);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, name, description, specialRewardHour, specialReward, taskTaskId, projectId, taskPrice, hoursAssigned, hoursReported, dateFrom, dateTo, progress, taskStatusId, userId, orderItemId, confirmedHours);
    }
}
