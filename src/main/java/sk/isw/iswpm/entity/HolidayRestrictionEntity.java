package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "holiday_restriction", schema = "public", catalog = "iswpm")
public class HolidayRestrictionEntity {
    private long holidayRestrictionId;
    private long companyInternalId;
    private String description;
    private String maxDaysPerYear;

    @Id
    @Column(name = "holiday_restriction_id")
    public long getHolidayRestrictionId() {
        return holidayRestrictionId;
    }

    public void setHolidayRestrictionId(long holidayRestrictionId) {
        this.holidayRestrictionId = holidayRestrictionId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "max_days_per_year")
    public String getMaxDaysPerYear() {
        return maxDaysPerYear;
    }

    public void setMaxDaysPerYear(String maxDaysPerYear) {
        this.maxDaysPerYear = maxDaysPerYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayRestrictionEntity that = (HolidayRestrictionEntity) o;
        return holidayRestrictionId == that.holidayRestrictionId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(description, that.description) &&
                Objects.equals(maxDaysPerYear, that.maxDaysPerYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayRestrictionId, companyInternalId, description, maxDaysPerYear);
    }
}
