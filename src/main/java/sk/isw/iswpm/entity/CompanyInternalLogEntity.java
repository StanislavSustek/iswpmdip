package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "company_internal_log", schema = "public", catalog = "iswpm")
public class CompanyInternalLogEntity {
    private long companyInternalLogId;
    private String comment;
    private long companyInternalStatusId;
    private long modifiedUserId;
    private long companyInternalId;
    private Timestamp modified;

    @Id
    @Column(name = "company_internal_log_id")
    public long getCompanyInternalLogId() {
        return companyInternalLogId;
    }

    public void setCompanyInternalLogId(long companyInternalLogId) {
        this.companyInternalLogId = companyInternalLogId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "company_internal_status_id")
    public long getCompanyInternalStatusId() {
        return companyInternalStatusId;
    }

    public void setCompanyInternalStatusId(long companyInternalStatusId) {
        this.companyInternalStatusId = companyInternalStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyInternalLogEntity that = (CompanyInternalLogEntity) o;
        return companyInternalLogId == that.companyInternalLogId &&
                companyInternalStatusId == that.companyInternalStatusId &&
                modifiedUserId == that.modifiedUserId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyInternalLogId, comment, companyInternalStatusId, modifiedUserId, companyInternalId, modified);
    }
}
