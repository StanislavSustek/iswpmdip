package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "company_internal", schema = "public", catalog = "iswpm")
public class CompanyInternalEntity {
    private long companyInternalId;
    private long companyId;
    private Timestamp modified;
    private long companyInternalStatusId;

    @Id
    @Column(name = "company_internal_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "company_id")
    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "company_internal_status_id")
    public long getCompanyInternalStatusId() {
        return companyInternalStatusId;
    }

    public void setCompanyInternalStatusId(long companyInternalStatusId) {
        this.companyInternalStatusId = companyInternalStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyInternalEntity that = (CompanyInternalEntity) o;
        return companyInternalId == that.companyInternalId &&
                companyId == that.companyId &&
                companyInternalStatusId == that.companyInternalStatusId &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyInternalId, companyId, modified, companyInternalStatusId);
    }
}
