package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "order_log", schema = "public", catalog = "iswpm")
public class OrderLogEntity {
    private long orderLogId;
    private Timestamp modified;
    private String comment;
    private long orderId;
    private Long orderStatusId;
    private Long projectId;
    private long modifiedUserId;
    private BigDecimal unissuedPrice;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private BigDecimal totalPrice;

    @Id
    @Column(name = "order_log_id")
    public long getOrderLogId() {
        return orderLogId;
    }

    public void setOrderLogId(long orderLogId) {
        this.orderLogId = orderLogId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "order_id")
    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "order_status_id")
    public Long getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Long orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    @Basic
    @Column(name = "project_id")
    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "unissued_price")
    public BigDecimal getUnissuedPrice() {
        return unissuedPrice;
    }

    public void setUnissuedPrice(BigDecimal unissuedPrice) {
        this.unissuedPrice = unissuedPrice;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderLogEntity that = (OrderLogEntity) o;
        return orderLogId == that.orderLogId &&
                orderId == that.orderId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(orderStatusId, that.orderStatusId) &&
                Objects.equals(projectId, that.projectId) &&
                Objects.equals(unissuedPrice, that.unissuedPrice) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(totalPrice, that.totalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderLogId, modified, comment, orderId, orderStatusId, projectId, modifiedUserId, unissuedPrice, dateFrom, dateTo, totalPrice);
    }
}
