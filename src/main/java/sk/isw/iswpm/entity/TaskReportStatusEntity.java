package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "task_report_status", schema = "public", catalog = "iswpm")
public class TaskReportStatusEntity {
    private long taskReportStatusId;
    private String name;
    private String description;
    private boolean active;

    @Id
    @Column(name = "task_report_status_id")
    public long getTaskReportStatusId() {
        return taskReportStatusId;
    }

    public void setTaskReportStatusId(long taskReportStatusId) {
        this.taskReportStatusId = taskReportStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskReportStatusEntity that = (TaskReportStatusEntity) o;
        return taskReportStatusId == that.taskReportStatusId &&
                active == that.active &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskReportStatusId, name, description, active);
    }
}
