package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "email_notification", schema = "public", catalog = "iswpm")
public class EmailNotificationEntity {
    private long emailNotificationId;
    private long companyInternalId;
    private Long taskId;
    private Long projectId;
    private boolean datesChange;
    private boolean statusChange;

    @Id
    @Column(name = "email_notification_id")
    public long getEmailNotificationId() {
        return emailNotificationId;
    }

    public void setEmailNotificationId(long emailNotificationId) {
        this.emailNotificationId = emailNotificationId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "task_id")
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "project_id")
    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "dates_change")
    public boolean isDatesChange() {
        return datesChange;
    }

    public void setDatesChange(boolean datesChange) {
        this.datesChange = datesChange;
    }

    @Basic
    @Column(name = "status_change")
    public boolean isStatusChange() {
        return statusChange;
    }

    public void setStatusChange(boolean statusChange) {
        this.statusChange = statusChange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailNotificationEntity that = (EmailNotificationEntity) o;
        return emailNotificationId == that.emailNotificationId &&
                companyInternalId == that.companyInternalId &&
                datesChange == that.datesChange &&
                statusChange == that.statusChange &&
                Objects.equals(taskId, that.taskId) &&
                Objects.equals(projectId, that.projectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailNotificationId, companyInternalId, taskId, projectId, datesChange, statusChange);
    }
}
