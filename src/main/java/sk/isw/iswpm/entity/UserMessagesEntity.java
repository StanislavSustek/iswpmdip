package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_messages", schema = "public", catalog = "iswpm")
public class UserMessagesEntity {

    private long userMessageId;
    private boolean read;

    private long userId;
    private long messageId;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_message_id")
    public long getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(long userMessageId) {
        this.userMessageId = userMessageId;
    }

    @Basic
    @Column(name = "read")
    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "message_id")
    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMessagesEntity that = (UserMessagesEntity) o;
        return userMessageId == that.userMessageId &&
                read == that.read;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userMessageId, read);
    }
}
