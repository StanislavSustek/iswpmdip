package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "project_status", schema = "public", catalog = "iswpm")
public class ProjectStatusEntity {
    private long projectStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "project_status_id")
    public long getProjectStatusId() {
        return projectStatusId;
    }

    public void setProjectStatusId(long projectStatusId) {
        this.projectStatusId = projectStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectStatusEntity that = (ProjectStatusEntity) o;
        return projectStatusId == that.projectStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectStatusId, name, active);
    }
}
