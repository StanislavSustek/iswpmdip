package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "sallary", schema = "public", catalog = "iswpm")
public class SallaryEntity {
    private long sallaryId;
    private BigDecimal hourRate;
    private BigDecimal monthlyRate;
    private Timestamp modified;
    private long modifiedUserId;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private long userId;
    private long companyInternalId;

    @Id
    @Column(name = "sallary_id")
    public long getSallaryId() {
        return sallaryId;
    }

    public void setSallaryId(long sallaryId) {
        this.sallaryId = sallaryId;
    }

    @Basic
    @Column(name = "hour_rate")
    public BigDecimal getHourRate() {
        return hourRate;
    }

    public void setHourRate(BigDecimal hourRate) {
        this.hourRate = hourRate;
    }

    @Basic
    @Column(name = "monthly_rate")
    public BigDecimal getMonthlyRate() {
        return monthlyRate;
    }

    public void setMonthlyRate(BigDecimal monthlyRate) {
        this.monthlyRate = monthlyRate;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SallaryEntity that = (SallaryEntity) o;
        return sallaryId == that.sallaryId &&
                modifiedUserId == that.modifiedUserId &&
                userId == that.userId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(hourRate, that.hourRate) &&
                Objects.equals(monthlyRate, that.monthlyRate) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sallaryId, hourRate, monthlyRate, modified, modifiedUserId, dateFrom, dateTo, userId, companyInternalId);
    }
}
