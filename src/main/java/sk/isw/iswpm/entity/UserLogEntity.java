package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user_log", schema = "public", catalog = "iswpm")
public class UserLogEntity {
    private long userLogId;
    private Timestamp modified;
    private String comment;
    private Long userStatusId;
    private long modifiedUserId;
    private long userId;
    private String firstName;
    private String lastName;
    private String email;

    @Id
    @Column(name = "user_log_id")
    public long getUserLogId() {
        return userLogId;
    }

    public void setUserLogId(long userLogId) {
        this.userLogId = userLogId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "user_status_id")
    public Long getUserStatusId() {
        return userStatusId;
    }

    public void setUserStatusId(Long userStatusId) {
        this.userStatusId = userStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLogEntity that = (UserLogEntity) o;
        return userLogId == that.userLogId &&
                modifiedUserId == that.modifiedUserId &&
                userId == that.userId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(userStatusId, that.userStatusId) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userLogId, modified, comment, userStatusId, modifiedUserId, userId, firstName, lastName, email);
    }
}
