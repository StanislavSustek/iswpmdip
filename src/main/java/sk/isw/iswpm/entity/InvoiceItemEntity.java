package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "invoice_item", schema = "public", catalog = "iswpm")
public class InvoiceItemEntity {
    private long invoiceItemId;
    private String description;
    private BigDecimal totalPrice;
    private BigDecimal quantity;
    private long orderItemId;
    private long invoiceId;
    private BigDecimal price;
    private long measureUnitId;
    private BigDecimal vat;

    @Id
    @Column(name = "invoice_item_id")
    public long getInvoiceItemId() {
        return invoiceItemId;
    }

    public void setInvoiceItemId(long invoiceItemId) {
        this.invoiceItemId = invoiceItemId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "order_item_id")
    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Basic
    @Column(name = "invoice_id")
    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "measure_unit_id")
    public long getMeasureUnitId() {
        return measureUnitId;
    }

    public void setMeasureUnitId(long measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    @Basic
    @Column(name = "vat")
    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceItemEntity that = (InvoiceItemEntity) o;
        return invoiceItemId == that.invoiceItemId &&
                orderItemId == that.orderItemId &&
                invoiceId == that.invoiceId &&
                measureUnitId == that.measureUnitId &&
                Objects.equals(description, that.description) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(price, that.price) &&
                Objects.equals(vat, that.vat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceItemId, description, totalPrice, quantity, orderItemId, invoiceId, price, measureUnitId, vat);
    }
}
