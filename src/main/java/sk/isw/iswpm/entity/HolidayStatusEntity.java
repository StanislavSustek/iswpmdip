package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "holiday_status", schema = "public", catalog = "iswpm")
public class HolidayStatusEntity {
    private long holidayStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "holiday_status_id")
    public long getHolidayStatusId() {
        return holidayStatusId;
    }

    public void setHolidayStatusId(long holidayStatusId) {
        this.holidayStatusId = holidayStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayStatusEntity that = (HolidayStatusEntity) o;
        return holidayStatusId == that.holidayStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayStatusId, name, active);
    }
}
