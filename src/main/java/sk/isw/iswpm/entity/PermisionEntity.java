package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "permision", schema = "public", catalog = "iswpm")
public class PermisionEntity {
    private long permisionId;
    private String description;
    private String name;
    private String group;

    @Id
    @Column(name = "permision_id")
    public long getPermisionId() {
        return permisionId;
    }

    public void setPermisionId(long permisionId) {
        this.permisionId = permisionId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "group")
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermisionEntity that = (PermisionEntity) o;
        return permisionId == that.permisionId &&
                Objects.equals(description, that.description) &&
                Objects.equals(name, that.name) &&
                Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(permisionId, description, name, group);
    }
}
