package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "invoice_status", schema = "public", catalog = "iswpm")
public class InvoiceStatusEntity {
    private long invoiceStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "invoice_status_id")
    public long getInvoiceStatusId() {
        return invoiceStatusId;
    }

    public void setInvoiceStatusId(long invoiceStatusId) {
        this.invoiceStatusId = invoiceStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceStatusEntity that = (InvoiceStatusEntity) o;
        return invoiceStatusId == that.invoiceStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceStatusId, name, active);
    }
}
