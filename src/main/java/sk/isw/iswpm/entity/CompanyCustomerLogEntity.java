package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "company_customer_log", schema = "public", catalog = "iswpm")
public class CompanyCustomerLogEntity {
    private long companyCustomerLogId;
    private String comment;
    private long companyCustomerStatusId;
    private long modifiedUserId;
    private long companyInternalId;
    private long companyCustomerId;
    private Timestamp modified;

    @Id
    @Column(name = "company_customer_log_id")
    public long getCompanyCustomerLogId() {
        return companyCustomerLogId;
    }

    public void setCompanyCustomerLogId(long companyCustomerLogId) {
        this.companyCustomerLogId = companyCustomerLogId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "company_customer_status_id")
    public long getCompanyCustomerStatusId() {
        return companyCustomerStatusId;
    }

    public void setCompanyCustomerStatusId(long companyCustomerStatusId) {
        this.companyCustomerStatusId = companyCustomerStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "company_customer_id")
    public long getCompanyCustomerId() {
        return companyCustomerId;
    }

    public void setCompanyCustomerId(long companyCustomerId) {
        this.companyCustomerId = companyCustomerId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerLogEntity that = (CompanyCustomerLogEntity) o;
        return companyCustomerLogId == that.companyCustomerLogId &&
                companyCustomerStatusId == that.companyCustomerStatusId &&
                modifiedUserId == that.modifiedUserId &&
                companyInternalId == that.companyInternalId &&
                companyCustomerId == that.companyCustomerId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerLogId, comment, companyCustomerStatusId, modifiedUserId, companyInternalId, companyCustomerId, modified);
    }
}
