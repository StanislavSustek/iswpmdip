package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "task_report", schema = "public", catalog = "iswpm")
public class TaskReportEntity {
    private long taskReportId;
    private String comment;
    private BigDecimal hoursReported;
    private long taskId;
    private long userId;
    private Timestamp modified;
    private Timestamp dateReport;
    private Long reportSummaryId;
    private Long taskReportStatusId;
    private BigDecimal hoursConfirmed;
    private Boolean specialRewardUsed;
    private BigDecimal hoursRewardHour;

    @Id
    @Column(name = "task_report_id")
    public long getTaskReportId() {
        return taskReportId;
    }

    public void setTaskReportId(long taskReportId) {
        this.taskReportId = taskReportId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "hours_reported")
    public BigDecimal getHoursReported() {
        return hoursReported;
    }

    public void setHoursReported(BigDecimal hoursReported) {
        this.hoursReported = hoursReported;
    }

    @Basic
    @Column(name = "task_id")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "date_report")
    public Timestamp getDateReport() {
        return dateReport;
    }

    public void setDateReport(Timestamp dateReport) {
        this.dateReport = dateReport;
    }

    @Basic
    @Column(name = "report_summary_id")
    public Long getReportSummaryId() {
        return reportSummaryId;
    }

    public void setReportSummaryId(Long reportSummaryId) {
        this.reportSummaryId = reportSummaryId;
    }

    @Basic
    @Column(name = "task_report_status_id")
    public Long getTaskReportStatusId() {
        return taskReportStatusId;
    }

    public void setTaskReportStatusId(Long taskReportStatusId) {
        this.taskReportStatusId = taskReportStatusId;
    }

    @Basic
    @Column(name = "hours_confirmed")
    public BigDecimal getHoursConfirmed() {
        return hoursConfirmed;
    }

    public void setHoursConfirmed(BigDecimal hoursConfirmed) {
        this.hoursConfirmed = hoursConfirmed;
    }

    @Basic
    @Column(name = "special_reward_used")
    public Boolean getSpecialRewardUsed() {
        return specialRewardUsed;
    }

    public void setSpecialRewardUsed(Boolean specialRewardUsed) {
        this.specialRewardUsed = specialRewardUsed;
    }

    @Basic
    @Column(name = "hours_reward_hour")
    public BigDecimal getHoursRewardHour() {
        return hoursRewardHour;
    }

    public void setHoursRewardHour(BigDecimal hoursRewardHour) {
        this.hoursRewardHour = hoursRewardHour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskReportEntity that = (TaskReportEntity) o;
        return taskReportId == that.taskReportId &&
                taskId == that.taskId &&
                userId == that.userId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(hoursReported, that.hoursReported) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(dateReport, that.dateReport) &&
                Objects.equals(reportSummaryId, that.reportSummaryId) &&
                Objects.equals(taskReportStatusId, that.taskReportStatusId) &&
                Objects.equals(hoursConfirmed, that.hoursConfirmed) &&
                Objects.equals(specialRewardUsed, that.specialRewardUsed) &&
                Objects.equals(hoursRewardHour, that.hoursRewardHour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskReportId, comment, hoursReported, taskId, userId, modified, dateReport, reportSummaryId, taskReportStatusId, hoursConfirmed, specialRewardUsed, hoursRewardHour);
    }
}
