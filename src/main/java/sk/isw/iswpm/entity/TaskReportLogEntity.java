/*
package sk.isw.iswpm.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "task_report_log", schema = "public", catalog = "iswpm")
public class TaskReportLogEntity {
    private long taskReportLogId;
    private int taskReportId;
    private String comment;
    private BigDecimal hoursReported;
    private Timestamp dateReport;
    private Timestamp created;
    private Long taskReportStatusId;
    private long userId;
    private Long reportSummaryId;

    @Basic
    @Column(name = "task_report_log_id")
    public long getTaskReportLogId() {
        return taskReportLogId;
    }

    public void setTaskReportLogId(long taskReportLogId) {
        this.taskReportLogId = taskReportLogId;
    }

    @Basic
    @Column(name = "task_report_id")
    public int getTaskReportId() {
        return taskReportId;
    }

    public void setTaskReportId(int taskReportId) {
        this.taskReportId = taskReportId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "hours_reported")
    public BigDecimal getHoursReported() {
        return hoursReported;
    }

    public void setHoursReported(BigDecimal hoursReported) {
        this.hoursReported = hoursReported;
    }

    @Basic
    @Column(name = "date_report")
    public Timestamp getDateReport() {
        return dateReport;
    }

    public void setDateReport(Timestamp dateReport) {
        this.dateReport = dateReport;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "task_report_status_id")
    public Long getTaskReportStatusId() {
        return taskReportStatusId;
    }

    public void setTaskReportStatusId(Long taskReportStatusId) {
        this.taskReportStatusId = taskReportStatusId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "report_summary_id")
    public Long getReportSummaryId() {
        return reportSummaryId;
    }

    public void setReportSummaryId(Long reportSummaryId) {
        this.reportSummaryId = reportSummaryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskReportLogEntity that = (TaskReportLogEntity) o;
        return taskReportLogId == that.taskReportLogId &&
                taskReportId == that.taskReportId &&
                userId == that.userId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(hoursReported, that.hoursReported) &&
                Objects.equals(dateReport, that.dateReport) &&
                Objects.equals(created, that.created) &&
                Objects.equals(taskReportStatusId, that.taskReportStatusId) &&
                Objects.equals(reportSummaryId, that.reportSummaryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskReportLogId, taskReportId, comment, hoursReported, dateReport, created, taskReportStatusId, userId, reportSummaryId);
    }
}
*/
