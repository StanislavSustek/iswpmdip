package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "task_attachment", schema = "public", catalog = "iswpm")
public class TaskAttachmentEntity {
    private long taskAtachmentId;
    private String filename;
    private String path;
    private boolean active;
    private long taskId;
    private String extension;
    private Timestamp createdate;
    private byte[] file;

    @Id
    @Column(name = "task_atachment_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getTaskAtachmentId() {
        return taskAtachmentId;
    }

    public void setTaskAtachmentId(long taskAtachmentId) {
        this.taskAtachmentId = taskAtachmentId;
    }

    @Basic
    @Column(name = "filename")
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Basic
    @Column(name = "path")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "task_id")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "extension")
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Basic
    @Column(name = "createdate")
    public Timestamp getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskAttachmentEntity that = (TaskAttachmentEntity) o;
        return taskAtachmentId == that.taskAtachmentId &&
                active == that.active &&
                taskId == that.taskId &&
                Objects.equals(filename, that.filename) &&
                Objects.equals(path, that.path) &&
                Objects.equals(extension, that.extension) &&
                Objects.equals(createdate, that.createdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskAtachmentId, filename, path, active, taskId, extension, createdate);
    }

    @Basic
    @Column(name = "file")
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}
