package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "company_skill", schema = "public", catalog = "iswpm")
public class CompanySkillEntity {
    private long companySkillId;
    private long companyInternalId;
    private String name;
    private String group;

    @Id
    @Column(name = "company_skill_id")
    public long getCompanySkillId() {
        return companySkillId;
    }

    public void setCompanySkillId(long companySkillId) {
        this.companySkillId = companySkillId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "group")
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanySkillEntity that = (CompanySkillEntity) o;
        return companySkillId == that.companySkillId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(name, that.name) &&
                Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companySkillId, companyInternalId, name, group);
    }
}
