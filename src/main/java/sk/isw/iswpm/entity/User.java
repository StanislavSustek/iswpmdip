package sk.isw.iswpm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.controllerEntity.CompanyPersmisions;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Accessors(chain = true)
public class User {
    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private Long userStatusId;
    private Long roleId;
    @JsonIgnore
    private String password; // hashed
    private List<String> permisions;
    private List<CompanyPersmisions> companiesWithPermisions;
    @JsonIgnore
    private String comment;
    private Timestamp modified;
    private Timestamp created;
    private Timestamp lastLogin;
}
