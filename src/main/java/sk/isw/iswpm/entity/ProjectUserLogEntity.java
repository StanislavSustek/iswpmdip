package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "project_user_log", schema = "public", catalog = "iswpm")
public class ProjectUserLogEntity {
    private long projectUserLogId;
    private Timestamp modified;
    private long projectUserStatusId;
    private long modifiedUserId;
    private long projectId;
    private long userId;
    private String comment;

    @Id
    @Column(name = "project_user_log_id")
    public long getProjectUserLogId() {
        return projectUserLogId;
    }

    public void setProjectUserLogId(long projectUserLogId) {
        this.projectUserLogId = projectUserLogId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "project_user_status_id")
    public long getProjectUserStatusId() {
        return projectUserStatusId;
    }

    public void setProjectUserStatusId(long projectUserStatusId) {
        this.projectUserStatusId = projectUserStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectUserLogEntity that = (ProjectUserLogEntity) o;
        return projectUserLogId == that.projectUserLogId &&
                projectUserStatusId == that.projectUserStatusId &&
                modifiedUserId == that.modifiedUserId &&
                projectId == that.projectId &&
                userId == that.userId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(comment, that.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectUserLogId, modified, projectUserStatusId, modifiedUserId, projectId, userId, comment);
    }
}
