package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "task_comment", schema = "public", catalog = "iswpm")
public class TaskCommentEntity {
    private long id;
    private long taskId;
    private long userId;
    private String comment;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "task_id")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskCommentEntity that = (TaskCommentEntity) o;
        return id == that.id &&
                taskId == that.taskId &&
                userId == that.userId &&
                Objects.equals(comment, that.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskId, userId, comment);
    }
}
