package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "company_customer_contact_status", schema = "public", catalog = "iswpm")
public class CompanyCustomerContactStatusEntity {
    private long companyCustomerContactStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "company_customer_contact_status_id")
    public long getCompanyCustomerContactStatusId() {
        return companyCustomerContactStatusId;
    }

    public void setCompanyCustomerContactStatusId(long companyCustomerContactStatusId) {
        this.companyCustomerContactStatusId = companyCustomerContactStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerContactStatusEntity that = (CompanyCustomerContactStatusEntity) o;
        return companyCustomerContactStatusId == that.companyCustomerContactStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerContactStatusId, name, active);
    }
}
