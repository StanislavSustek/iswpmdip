package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "task_log", schema = "public", catalog = "iswpm")
public class TaskLogEntity {
    private long taskLogId;
    private long taskId;
    private Long taskStatusId;
    private long modifiedUserId;
    private String comment;
    private String name;
    private BigDecimal specialRewardHour;
    private BigDecimal specialReward;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private Timestamp modified;
    private BigDecimal progress;
    private long userId;
    private Long orderItemId;
    private BigDecimal hoursReported;
    private BigDecimal hoursAssigned;
    private BigDecimal taskPrice;
    private String priority;


    @Id
    @Column(name = "task_log_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getTaskLogId() {
        return taskLogId;
    }

    public void setTaskLogId(long taskLogId) {
        this.taskLogId = taskLogId;
    }

    @Basic
    @Column(name = "task_id")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "task_status_id")
    public Long getTaskStatusId() {
        return taskStatusId;
    }

    public void setTaskStatusId(Long taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "special_reward_hour")
    public BigDecimal getSpecialRewardHour() {
        return specialRewardHour;
    }

    public void setSpecialRewardHour(BigDecimal specialRewardHour) {
        this.specialRewardHour = specialRewardHour;
    }

    @Basic
    @Column(name = "special_reward")
    public BigDecimal getSpecialReward() {
        return specialReward;
    }

    public void setSpecialReward(BigDecimal specialReward) {
        this.specialReward = specialReward;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "progress")
    public BigDecimal getProgress() {
        return progress;
    }

    public void setProgress(BigDecimal progress) {
        this.progress = progress;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "order_item_id")
    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Basic
    @Column(name = "hours_reported")
    public BigDecimal getHoursReported() {
        return hoursReported;
    }

    public void setHoursReported(BigDecimal hoursReported) {
        this.hoursReported = hoursReported;
    }

    @Basic
    @Column(name = "hours_assigned")
    public BigDecimal getHoursAssigned() {
        return hoursAssigned;
    }

    public void setHoursAssigned(BigDecimal hoursAssigned) {
        this.hoursAssigned = hoursAssigned;
    }

    @Column(name = "priority")
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "task_price")
    public BigDecimal getTaskPrice() {
        return taskPrice;
    }

    public void setTaskPrice(BigDecimal taskPrice) {
        this.taskPrice = taskPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskLogEntity that = (TaskLogEntity) o;
        return taskLogId == that.taskLogId &&
                taskId == that.taskId &&
                modifiedUserId == that.modifiedUserId &&
                userId == that.userId &&
                Objects.equals(taskStatusId, that.taskStatusId) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(name, that.name) &&
                Objects.equals(specialRewardHour, that.specialRewardHour) &&
                Objects.equals(specialReward, that.specialReward) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(progress, that.progress) &&
                Objects.equals(orderItemId, that.orderItemId) &&
                Objects.equals(hoursReported, that.hoursReported) &&
                Objects.equals(hoursAssigned, that.hoursAssigned) &&
                Objects.equals(taskPrice, that.taskPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskLogId, taskId, taskStatusId, modifiedUserId, comment, name, specialRewardHour, specialReward, dateFrom, dateTo, modified, progress, userId, orderItemId, hoursReported, hoursAssigned, taskPrice);
    }
}
