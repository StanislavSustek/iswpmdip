package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "project_skill", schema = "public", catalog = "iswpm")
public class ProjectSkillEntity {
    private long projectSkillId;
    private long projectId;
    private long companySkillId;

    @Id
    @Column(name = "project_skill_id")
    public long getProjectSkillId() {
        return projectSkillId;
    }

    public void setProjectSkillId(long projectSkillId) {
        this.projectSkillId = projectSkillId;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "company_skill_id")
    public long getCompanySkillId() {
        return companySkillId;
    }

    public void setCompanySkillId(long companySkillId) {
        this.companySkillId = companySkillId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectSkillEntity that = (ProjectSkillEntity) o;
        return projectSkillId == that.projectSkillId &&
                projectId == that.projectId &&
                companySkillId == that.companySkillId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectSkillId, projectId, companySkillId);
    }
}
