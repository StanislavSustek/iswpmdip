package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "order_item", schema = "public", catalog = "iswpm")
public class OrderItemEntity {
    private long orderItemId;
    private String description;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private String name;
    private BigDecimal price;
    private BigDecimal totalPrice;
    private long orderId;
    private BigDecimal quantity;
    private long measureUnitId;
    private long orderItemStatusId;
    private BigDecimal vat;

    @Id
    @Column(name = "order_item_id")
    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "order_id")
    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "measure_unit_id")
    public long getMeasureUnitId() {
        return measureUnitId;
    }

    public void setMeasureUnitId(long measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    @Basic
    @Column(name = "order_item_status_id")
    public long getOrderItemStatusId() {
        return orderItemStatusId;
    }

    public void setOrderItemStatusId(long orderItemStatusId) {
        this.orderItemStatusId = orderItemStatusId;
    }

    @Basic
    @Column(name = "vat")
    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItemEntity that = (OrderItemEntity) o;
        return orderItemId == that.orderItemId &&
                orderId == that.orderId &&
                measureUnitId == that.measureUnitId &&
                orderItemStatusId == that.orderItemStatusId &&
                Objects.equals(description, that.description) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(name, that.name) &&
                Objects.equals(price, that.price) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(vat, that.vat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemId, description, dateFrom, dateTo, name, price, totalPrice, orderId, quantity, measureUnitId, orderItemStatusId, vat);
    }
}
