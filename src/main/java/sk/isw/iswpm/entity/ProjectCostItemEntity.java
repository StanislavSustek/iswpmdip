package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "project_cost_item", schema = "public", catalog = "iswpm")
public class ProjectCostItemEntity {
    private long projectCostItemId;
    private long projectId;
    private String name;
    private String description;
    private BigDecimal itemPrice;
    private String invoiceNumber;
    private Timestamp modified;

    @Id
    @Column(name = "project_cost_item_id")
    public long getProjectCostItemId() {
        return projectCostItemId;
    }

    public void setProjectCostItemId(long projectCostItemId) {
        this.projectCostItemId = projectCostItemId;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "item_price")
    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Basic
    @Column(name = "invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectCostItemEntity that = (ProjectCostItemEntity) o;
        return projectCostItemId == that.projectCostItemId &&
                projectId == that.projectId &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(itemPrice, that.itemPrice) &&
                Objects.equals(invoiceNumber, that.invoiceNumber) &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectCostItemId, projectId, name, description, itemPrice, invoiceNumber, modified);
    }
}
