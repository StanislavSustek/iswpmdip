package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "company_internal_logo", schema = "public", catalog = "iswpm")
public class CompanyInternalLogoEntity {
    private long logoId;
    private long companyInternalId;
    private byte[] image;

    @Id
    @Column(name = "logo_id")
    public long getLogoId() {
        return logoId;
    }

    public void setLogoId(long logoId) {
        this.logoId = logoId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "image")
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyInternalLogoEntity that = (CompanyInternalLogoEntity) o;
        return logoId == that.logoId &&
                companyInternalId == that.companyInternalId &&
                Arrays.equals(image, that.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(logoId, companyInternalId);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }
}
