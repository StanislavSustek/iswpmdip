package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "invoice_log", schema = "public", catalog = "iswpm")
public class InvoiceLogEntity {
    private long invoiceLogId;
    private Timestamp modified;
    private BigDecimal totalPrice;
    private long invoiceStatusId;
    private Long invoiceId;
    private Long modifiedUserId;

    @Id
    @Column(name = "invoice_log_id")
    public long getInvoiceLogId() {
        return invoiceLogId;
    }

    public void setInvoiceLogId(long invoiceLogId) {
        this.invoiceLogId = invoiceLogId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "invoice_status_id")
    public long getInvoiceStatusId() {
        return invoiceStatusId;
    }

    public void setInvoiceStatusId(long invoiceStatusId) {
        this.invoiceStatusId = invoiceStatusId;
    }

    @Basic
    @Column(name = "invoice_id")
    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceLogEntity that = (InvoiceLogEntity) o;
        return invoiceLogId == that.invoiceLogId &&
                invoiceStatusId == that.invoiceStatusId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(invoiceId, that.invoiceId) &&
                Objects.equals(modifiedUserId, that.modifiedUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceLogId, modified, totalPrice, invoiceStatusId, invoiceId, modifiedUserId);
    }
}
