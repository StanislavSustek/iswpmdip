package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "holiday_type", schema = "public", catalog = "iswpm")
public class HolidayTypeEntity {
    private int holidayTypeId;
    private String name;

    @Id
    @Column(name = "holiday_type_id")
    public int getHolidayTypeId() {
        return holidayTypeId;
    }

    public void setHolidayTypeId(int holidayTypeId) {
        this.holidayTypeId = holidayTypeId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayTypeEntity that = (HolidayTypeEntity) o;
        return holidayTypeId == that.holidayTypeId &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayTypeId, name);
    }
}
