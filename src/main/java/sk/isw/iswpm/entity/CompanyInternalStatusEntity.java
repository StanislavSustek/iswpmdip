package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "company_internal_status", schema = "public", catalog = "iswpm")
public class CompanyInternalStatusEntity {
    private long companyInternalStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "company_internal_status_id")
    public long getCompanyInternalStatusId() {
        return companyInternalStatusId;
    }

    public void setCompanyInternalStatusId(long companyInternalStatusId) {
        this.companyInternalStatusId = companyInternalStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyInternalStatusEntity that = (CompanyInternalStatusEntity) o;
        return companyInternalStatusId == that.companyInternalStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyInternalStatusId, name, active);
    }
}
