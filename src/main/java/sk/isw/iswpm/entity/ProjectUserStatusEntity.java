package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "project_user_status", schema = "public", catalog = "iswpm")
public class ProjectUserStatusEntity {
    private long projectUserStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "project_user_status_id")
    public long getProjectUserStatusId() {
        return projectUserStatusId;
    }

    public void setProjectUserStatusId(long projectUserStatusId) {
        this.projectUserStatusId = projectUserStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectUserStatusEntity that = (ProjectUserStatusEntity) o;
        return projectUserStatusId == that.projectUserStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectUserStatusId, name, active);
    }
}
