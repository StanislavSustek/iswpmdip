package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "setting", schema = "public", catalog = "iswpm")
public class SettingEntity {
    private long settingId;
    private long companyInternalId;
    private Long userId;
    private String dateFormat;
    private Short paymentCreateDay;
    private Boolean showCalendar;
    private Boolean showUsersRoleChart;
    private Boolean showTasksChart;
    private Boolean showReportedHoursChart;
    private Boolean showLastTaskHistory;
    private Boolean showStatsWidgets;
    private boolean paymentEmailNotification;
    private boolean holidayEmailNotification;
    private boolean assigningRemoving;
    private BigDecimal vat;

    @Id
    @Column(name = "setting_id")
    public long getSettingId() {
        return settingId;
    }

    public void setSettingId(long settingId) {
        this.settingId = settingId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "date_format")
    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Basic
    @Column(name = "payment_create_day")
    public Short getPaymentCreateDay() {
        return paymentCreateDay;
    }

    public void setPaymentCreateDay(Short paymentCreateDay) {
        this.paymentCreateDay = paymentCreateDay;
    }

    @Basic
    @Column(name = "show_calendar")
    public Boolean getShowCalendar() {
        return showCalendar;
    }

    public void setShowCalendar(Boolean showCalendar) {
        this.showCalendar = showCalendar;
    }

    @Basic
    @Column(name = "show_users_role_chart")
    public Boolean getShowUsersRoleChart() {
        return showUsersRoleChart;
    }

    public void setShowUsersRoleChart(Boolean showUsersRoleChart) {
        this.showUsersRoleChart = showUsersRoleChart;
    }

    @Basic
    @Column(name = "show_tasks_chart")
    public Boolean getShowTasksChart() {
        return showTasksChart;
    }

    public void setShowTasksChart(Boolean showTasksChart) {
        this.showTasksChart = showTasksChart;
    }

    @Basic
    @Column(name = "show_reported_hours_chart")
    public Boolean getShowReportedHoursChart() {
        return showReportedHoursChart;
    }

    public void setShowReportedHoursChart(Boolean showReportedHoursChart) {
        this.showReportedHoursChart = showReportedHoursChart;
    }

    @Basic
    @Column(name = "show_last_task_history")
    public Boolean getShowLastTaskHistory() {
        return showLastTaskHistory;
    }

    public void setShowLastTaskHistory(Boolean showLastTaskHistory) {
        this.showLastTaskHistory = showLastTaskHistory;
    }

    @Basic
    @Column(name = "show_stats_widgets")
    public Boolean getShowStatsWidgets() {
        return showStatsWidgets;
    }

    public void setShowStatsWidgets(Boolean showStatsWidgets) {
        this.showStatsWidgets = showStatsWidgets;
    }

    @Basic
    @Column(name = "payment_email_notification")
    public boolean isPaymentEmailNotification() {
        return paymentEmailNotification;
    }

    public void setPaymentEmailNotification(boolean paymentEmailNotification) {
        this.paymentEmailNotification = paymentEmailNotification;
    }

    @Basic
    @Column(name = "holiday_email_notification")
    public boolean isHolidayEmailNotification() {
        return holidayEmailNotification;
    }

    public void setHolidayEmailNotification(boolean holidayEmailNotification) {
        this.holidayEmailNotification = holidayEmailNotification;
    }

    @Basic
    @Column(name = "assigning_removing")
    public boolean isAssigningRemoving() {
        return assigningRemoving;
    }

    public void setAssigningRemoving(boolean assigningRemoving) {
        this.assigningRemoving = assigningRemoving;
    }

    @Basic
    @Column(name = "vat")
    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SettingEntity that = (SettingEntity) o;
        return settingId == that.settingId &&
                companyInternalId == that.companyInternalId &&
                paymentEmailNotification == that.paymentEmailNotification &&
                holidayEmailNotification == that.holidayEmailNotification &&
                assigningRemoving == that.assigningRemoving &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(dateFormat, that.dateFormat) &&
                Objects.equals(paymentCreateDay, that.paymentCreateDay) &&
                Objects.equals(showCalendar, that.showCalendar) &&
                Objects.equals(showUsersRoleChart, that.showUsersRoleChart) &&
                Objects.equals(showTasksChart, that.showTasksChart) &&
                Objects.equals(showReportedHoursChart, that.showReportedHoursChart) &&
                Objects.equals(showLastTaskHistory, that.showLastTaskHistory) &&
                Objects.equals(showStatsWidgets, that.showStatsWidgets) &&
                Objects.equals(vat, that.vat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(settingId, companyInternalId, userId, dateFormat, paymentCreateDay, showCalendar, showUsersRoleChart, showTasksChart, showReportedHoursChart, showLastTaskHistory, showStatsWidgets, paymentEmailNotification, holidayEmailNotification, assigningRemoving, vat);
    }
}
