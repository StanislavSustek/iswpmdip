package sk.isw.iswpm.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "security.LoginResponse")
@Data
@Accessors(chain = true)
public class LoginResponse {

    private Boolean success;
    private String tokken;
    private String role;
}
