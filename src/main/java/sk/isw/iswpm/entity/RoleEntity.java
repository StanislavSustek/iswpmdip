package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "role", schema = "public", catalog = "iswpm")
public class RoleEntity {
    private long roleId;
    private String description;
    private String name;
    private long companyInternalId;
    private boolean readOnly;

    @Id
    @Column(name = "role_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "read_only")
    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleEntity that = (RoleEntity) o;
        return roleId == that.roleId &&
                companyInternalId == that.companyInternalId &&
                readOnly == that.readOnly &&
                Objects.equals(description, that.description) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, description, name, companyInternalId, readOnly);
    }
}
