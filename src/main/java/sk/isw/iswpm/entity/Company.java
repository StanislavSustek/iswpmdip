package sk.isw.iswpm.entity;

        import com.fasterxml.jackson.annotation.JsonIgnore;
        import lombok.Data;

        import javax.persistence.*;
        import java.util.Date;

@Data
@Entity
@Table(name = "company", schema = "public")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String ico;
    private String dic;
    private String street;
    private String psc;
    private String city;
    @JsonIgnore
    private Date modified;

    @Column(name = "street_number")
    private String streetNumber;

    @Column(name = "company_name")
    private String name;
}
