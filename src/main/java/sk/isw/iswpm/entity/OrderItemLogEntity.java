package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "order_item_log", schema = "public", catalog = "iswpm")
public class OrderItemLogEntity {
    private long orderItemLogId;
    private Timestamp modified;
    private String comment;
    private long orderItemId;
    private long modifiedUserId;
    private BigDecimal price;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private String name;
    private Long measureUnitId;
    private Long quantity;
    private Long orderItemStatusId;

    @Id
    @Column(name = "order_item_log_id")
    public long getOrderItemLogId() {
        return orderItemLogId;
    }

    public void setOrderItemLogId(long orderItemLogId) {
        this.orderItemLogId = orderItemLogId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "order_item_id")
    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "measure_unit_id")
    public Long getMeasureUnitId() {
        return measureUnitId;
    }

    public void setMeasureUnitId(Long measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    @Basic
    @Column(name = "quantity")
    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "order_item_status_id")
    public Long getOrderItemStatusId() {
        return orderItemStatusId;
    }

    public void setOrderItemStatusId(Long orderItemStatusId) {
        this.orderItemStatusId = orderItemStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItemLogEntity that = (OrderItemLogEntity) o;
        return orderItemLogId == that.orderItemLogId &&
                orderItemId == that.orderItemId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(price, that.price) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(name, that.name) &&
                Objects.equals(measureUnitId, that.measureUnitId) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(orderItemStatusId, that.orderItemStatusId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemLogId, modified, comment, orderItemId, modifiedUserId, price, dateFrom, dateTo, name, measureUnitId, quantity, orderItemStatusId);
    }
}
