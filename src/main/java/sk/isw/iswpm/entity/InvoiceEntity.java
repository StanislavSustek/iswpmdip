package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "invoice", schema = "public", catalog = "iswpm")
public class InvoiceEntity {
    private long invoiceId;
    private Timestamp created;
    private BigDecimal totalPrice;
    private long companyInternalId;
    private long companyCustomerId;
    private String invoiceNumber;
    private long invoiceStatusId;
    private long currencyId;

    @Id
    @Column(name = "invoice_id")
    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "company_customer_id")
    public long getCompanyCustomerId() {
        return companyCustomerId;
    }

    public void setCompanyCustomerId(long companyCustomerId) {
        this.companyCustomerId = companyCustomerId;
    }

    @Basic
    @Column(name = "invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Basic
    @Column(name = "invoice_status_id")
    public long getInvoiceStatusId() {
        return invoiceStatusId;
    }

    public void setInvoiceStatusId(long invoiceStatusId) {
        this.invoiceStatusId = invoiceStatusId;
    }

    @Basic
    @Column(name = "currency_id")
    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceEntity that = (InvoiceEntity) o;
        return invoiceId == that.invoiceId &&
                companyInternalId == that.companyInternalId &&
                companyCustomerId == that.companyCustomerId &&
                invoiceStatusId == that.invoiceStatusId &&
                currencyId == that.currencyId &&
                Objects.equals(created, that.created) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(invoiceNumber, that.invoiceNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceId, created, totalPrice, companyInternalId, companyCustomerId, invoiceNumber, invoiceStatusId, currencyId);
    }
}
