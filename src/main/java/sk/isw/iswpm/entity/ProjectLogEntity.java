package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "project_log", schema = "public", catalog = "iswpm")
public class ProjectLogEntity {
    private long projectLogId;
    private String comment;
    private Timestamp modified;
    private long projectId;
    private Long projectStatusId;
    private long modifiedUserId;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private String name;
    private String description;
    private String budget;

    @Id
    @Column(name = "project_log_id")
    public long getProjectLogId() {
        return projectLogId;
    }

    public void setProjectLogId(long projectLogId) {
        this.projectLogId = projectLogId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "project_status_id")
    public Long getProjectStatusId() {
        return projectStatusId;
    }

    public void setProjectStatusId(Long projectStatusId) {
        this.projectStatusId = projectStatusId;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "budget")
    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectLogEntity that = (ProjectLogEntity) o;
        return projectLogId == that.projectLogId &&
                projectId == that.projectId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(projectStatusId, that.projectStatusId) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(budget, that.budget);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectLogId, comment, modified, projectId, projectStatusId, modifiedUserId, dateFrom, dateTo, name, description, budget);
    }
}
