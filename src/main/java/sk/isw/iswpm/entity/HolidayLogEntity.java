package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "holiday_log", schema = "public", catalog = "iswpm")
public class HolidayLogEntity {
    private long holidayLogId;
    private long holidayId;
    private Timestamp from;
    private Timestamp to;
    private String comment;
    private Long holidayStatusId;
    private Timestamp modified;
    private long modifiedUserId;
    private Long holidayTypeId;

    @Id
    @Column(name = "holiday_log_id")
    public long getHolidayLogId() {
        return holidayLogId;
    }

    public void setHolidayLogId(long holidayLogId) {
        this.holidayLogId = holidayLogId;
    }

    @Basic
    @Column(name = "holiday_id")
    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    @Basic
    @Column(name = "from")
    public Timestamp getFrom() {
        return from;
    }

    public void setFrom(Timestamp from) {
        this.from = from;
    }

    @Basic
    @Column(name = "to")
    public Timestamp getTo() {
        return to;
    }

    public void setTo(Timestamp to) {
        this.to = to;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "holiday_status_id")
    public Long getHolidayStatusId() {
        return holidayStatusId;
    }

    public void setHolidayStatusId(Long holidayStatusId) {
        this.holidayStatusId = holidayStatusId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "holiday_type_id")
    public Long getHolidayTypeId() {
        return holidayTypeId;
    }

    public void setHolidayTypeId(Long holidayTypeId) {
        this.holidayTypeId = holidayTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayLogEntity that = (HolidayLogEntity) o;
        return holidayLogId == that.holidayLogId &&
                holidayId == that.holidayId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(holidayStatusId, that.holidayStatusId) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(holidayTypeId, that.holidayTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayLogId, holidayId, from, to, comment, holidayStatusId, modified, modifiedUserId, holidayTypeId);
    }
}
