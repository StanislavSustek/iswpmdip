package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "order_item_status", schema = "public", catalog = "iswpm")
public class OrderItemStatusEntity {
    private long orderItemStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "order_item_status_id")
    public long getOrderItemStatusId() {
        return orderItemStatusId;
    }

    public void setOrderItemStatusId(long orderItemStatusId) {
        this.orderItemStatusId = orderItemStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItemStatusEntity that = (OrderItemStatusEntity) o;
        return orderItemStatusId == that.orderItemStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemStatusId, name, active);
    }
}
