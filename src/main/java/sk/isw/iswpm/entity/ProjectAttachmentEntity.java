package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "project_attachment", schema = "public", catalog = "iswpm")
public class ProjectAttachmentEntity {
    private long projectAtachmentId;
    private String filename;
    private String path;
    private boolean active;
    private long projectId;
    private String comment;
    private String extension;
    private byte[] file;

    @Id
    @Column(name = "project_atachment_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getProjectAtachmentId() {
        return projectAtachmentId;
    }

    public void setProjectAtachmentId(long projectAtachmentId) {
        this.projectAtachmentId = projectAtachmentId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "filename")
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Basic
    @Column(name = "path")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "extension")
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Basic
    @Column(name = "file")
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectAttachmentEntity that = (ProjectAttachmentEntity) o;
        return projectAtachmentId == that.projectAtachmentId &&
                active == that.active &&
                Objects.equals(filename, that.filename) &&
                Objects.equals(path, that.path) &&
                Objects.equals(extension, that.extension) &&
                Arrays.equals(file, that.file);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(projectAtachmentId, filename, path, active, extension);
        result = 31 * result + Arrays.hashCode(file);
        return result;
    }
}
