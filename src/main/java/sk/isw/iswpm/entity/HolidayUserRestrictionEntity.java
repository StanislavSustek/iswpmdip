package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "holiday_user_restriction", schema = "public", catalog = "iswpm")
public class HolidayUserRestrictionEntity {
    private long holidayUserRestrictionId;
    private long holidayRestrictionId;
    private long userId;
    private Timestamp validTo;
    private Timestamp validFrom;

    @Id
    @Column(name = "holiday_user_restriction_id")
    public long getHolidayUserRestrictionId() {
        return holidayUserRestrictionId;
    }

    public void setHolidayUserRestrictionId(long holidayUserRestrictionId) {
        this.holidayUserRestrictionId = holidayUserRestrictionId;
    }

    @Basic
    @Column(name = "holiday_restriction_id")
    public long getHolidayRestrictionId() {
        return holidayRestrictionId;
    }

    public void setHolidayRestrictionId(long holidayRestrictionId) {
        this.holidayRestrictionId = holidayRestrictionId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "valid_to")
    public Timestamp getValidTo() {
        return validTo;
    }

    public void setValidTo(Timestamp validTo) {
        this.validTo = validTo;
    }

    @Basic
    @Column(name = "valid_from")
    public Timestamp getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayUserRestrictionEntity that = (HolidayUserRestrictionEntity) o;
        return holidayUserRestrictionId == that.holidayUserRestrictionId &&
                holidayRestrictionId == that.holidayRestrictionId &&
                userId == that.userId &&
                Objects.equals(validTo, that.validTo) &&
                Objects.equals(validFrom, that.validFrom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayUserRestrictionId, holidayRestrictionId, userId, validTo, validFrom);
    }
}
