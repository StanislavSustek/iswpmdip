package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "holiday", schema = "public", catalog = "iswpm")
public class HolidayEntity {
    private long holidayId;
    private long userId;
    private long companyInternalId;
    private Timestamp from;
    private Timestamp to;
    private String comment;
    private long holidayStatusId;
    private long holidayTypeId;

    @Id
    @Column(name = "holiday_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "\"from\"")
    public Timestamp getFrom() {
        return from;
    }

    public void setFrom(Timestamp from) {
        this.from = from;
    }

    @Basic
    @Column(name = "\"to\"")
    public Timestamp getTo() {
        return to;
    }

    public void setTo(Timestamp to) {
        this.to = to;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "holiday_status_id")
    public long getHolidayStatusId() {
        return holidayStatusId;
    }

    public void setHolidayStatusId(long holidayStatusId) {
        this.holidayStatusId = holidayStatusId;
    }

    @Basic
    @Column(name = "holiday_type_id")
    public long getHolidayTypeId() {
        return holidayTypeId;
    }

    public void setHolidayTypeId(long holidayTypeId) {
        this.holidayTypeId = holidayTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolidayEntity that = (HolidayEntity) o;
        return holidayId == that.holidayId &&
                userId == that.userId &&
                companyInternalId == that.companyInternalId &&
                holidayStatusId == that.holidayStatusId &&
                holidayTypeId == that.holidayTypeId &&
                Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(comment, that.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayId, userId, companyInternalId, from, to, comment, holidayStatusId, holidayTypeId);
    }
}
