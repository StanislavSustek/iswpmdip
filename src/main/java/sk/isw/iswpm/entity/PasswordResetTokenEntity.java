package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "password_reset_token", schema = "public", catalog = "iswpm")
public class PasswordResetTokenEntity {
    private long passwordResetTokenId;
    private long userId;
    private String token;
    private Timestamp created;
    private Timestamp expired;
    private boolean used;
    private String ipAddress;

    @Id
    @Column(name = "password_reset_token_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getPasswordResetTokenId() {
        return passwordResetTokenId;
    }

    public void setPasswordResetTokenId(long passwordResetTokenId) {
        this.passwordResetTokenId = passwordResetTokenId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "expired")
    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Basic
    @Column(name = "used")
    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    @Basic
    @Column(name = "ip_address")
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordResetTokenEntity that = (PasswordResetTokenEntity) o;
        return passwordResetTokenId == that.passwordResetTokenId &&
                userId == that.userId &&
                used == that.used &&
                Objects.equals(token, that.token) &&
                Objects.equals(created, that.created) &&
                Objects.equals(expired, that.expired) &&
                Objects.equals(ipAddress, that.ipAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(passwordResetTokenId, userId, token, created, expired, used, ipAddress);
    }
}
