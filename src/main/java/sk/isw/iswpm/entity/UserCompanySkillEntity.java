package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_company_skill", schema = "public", catalog = "iswpm")
public class UserCompanySkillEntity {
    private long userCompanySkillId;
    private long userId;
    private long companySkillId;

    @Id
    @Column(name = "user_company_skill_id")
    public long getUserCompanySkillId() {
        return userCompanySkillId;
    }

    public void setUserCompanySkillId(long userCompanySkillId) {
        this.userCompanySkillId = userCompanySkillId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "company_skill_id")
    public long getCompanySkillId() {
        return companySkillId;
    }

    public void setCompanySkillId(long companySkillId) {
        this.companySkillId = companySkillId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCompanySkillEntity that = (UserCompanySkillEntity) o;
        return userCompanySkillId == that.userCompanySkillId &&
                userId == that.userId &&
                companySkillId == that.companySkillId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userCompanySkillId, userId, companySkillId);
    }
}
