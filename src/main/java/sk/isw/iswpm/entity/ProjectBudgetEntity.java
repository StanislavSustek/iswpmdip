package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "project_budget", schema = "public", catalog = "iswpm")
public class ProjectBudgetEntity {
    private long projectBudgetId;
    private long projectId;
    private BigDecimal invoiceItemsSum;
    private BigDecimal projectCostSum;
    private BigDecimal hourPaymentsSum;
    private BigDecimal monthPaymentsSum;
    private BigDecimal hourRewardPaymentsSum;
    private BigDecimal specialRewardPaymentsSum;
    private BigDecimal invoiceItemsSumVat;

    @Id
    @Column(name = "project_budget_id")
    public long getProjectBudgetId() {
        return projectBudgetId;
    }

    public void setProjectBudgetId(long projectBudgetId) {
        this.projectBudgetId = projectBudgetId;
    }

    @Basic
    @Column(name = "project_id")
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "invoice_items_sum")
    public BigDecimal getInvoiceItemsSum() {
        return invoiceItemsSum;
    }

    public void setInvoiceItemsSum(BigDecimal invoiceItemsSum) {
        this.invoiceItemsSum = invoiceItemsSum;
    }

    @Basic
    @Column(name = "project_cost_sum")
    public BigDecimal getProjectCostSum() {
        return projectCostSum;
    }

    public void setProjectCostSum(BigDecimal projectCostSum) {
        this.projectCostSum = projectCostSum;
    }

    @Basic
    @Column(name = "hour_payments_sum")
    public BigDecimal getHourPaymentsSum() {
        return hourPaymentsSum;
    }

    public void setHourPaymentsSum(BigDecimal hourPaymentsSum) {
        this.hourPaymentsSum = hourPaymentsSum;
    }

    @Basic
    @Column(name = "month_payments_sum")
    public BigDecimal getMonthPaymentsSum() {
        return monthPaymentsSum;
    }

    public void setMonthPaymentsSum(BigDecimal monthPaymentsSum) {
        this.monthPaymentsSum = monthPaymentsSum;
    }

    @Basic
    @Column(name = "hour_reward_payments_sum")
    public BigDecimal getHourRewardPaymentsSum() {
        return hourRewardPaymentsSum;
    }

    public void setHourRewardPaymentsSum(BigDecimal hourRewardPaymentsSum) {
        this.hourRewardPaymentsSum = hourRewardPaymentsSum;
    }

    @Basic
    @Column(name = "special_reward_payments_sum")
    public BigDecimal getSpecialRewardPaymentsSum() {
        return specialRewardPaymentsSum;
    }

    public void setSpecialRewardPaymentsSum(BigDecimal specialRewardPaymentsSum) {
        this.specialRewardPaymentsSum = specialRewardPaymentsSum;
    }

    @Basic
    @Column(name = "invoice_items_sum_vat")
    public BigDecimal getInvoiceItemsSumVat() {
        return invoiceItemsSumVat;
    }

    public void setInvoiceItemsSumVat(BigDecimal invoiceItemsSumVat) {
        this.invoiceItemsSumVat = invoiceItemsSumVat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectBudgetEntity that = (ProjectBudgetEntity) o;
        return projectBudgetId == that.projectBudgetId &&
                projectId == that.projectId &&
                Objects.equals(invoiceItemsSum, that.invoiceItemsSum) &&
                Objects.equals(projectCostSum, that.projectCostSum) &&
                Objects.equals(hourPaymentsSum, that.hourPaymentsSum) &&
                Objects.equals(monthPaymentsSum, that.monthPaymentsSum) &&
                Objects.equals(hourRewardPaymentsSum, that.hourRewardPaymentsSum) &&
                Objects.equals(specialRewardPaymentsSum, that.specialRewardPaymentsSum) &&
                Objects.equals(invoiceItemsSumVat, that.invoiceItemsSumVat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectBudgetId, projectId, invoiceItemsSum, projectCostSum, hourPaymentsSum, monthPaymentsSum, hourRewardPaymentsSum, specialRewardPaymentsSum, invoiceItemsSumVat);
    }
}
