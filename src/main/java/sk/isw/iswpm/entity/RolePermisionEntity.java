package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "role_permision", schema = "public", catalog = "iswpm")
public class RolePermisionEntity {
    private long rolePermisionId;
    private long roleId;
    private Timestamp modified;
    private long permisionId;

    @Id
    @Column(name = "role_permision_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getRolePermisionId() {
        return rolePermisionId;
    }

    public void setRolePermisionId(long rolePermisionId) {
        this.rolePermisionId = rolePermisionId;
    }

    @Basic
    @Column(name = "role_id")
    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "permision_id")
    public long getPermisionId() {
        return permisionId;
    }

    public void setPermisionId(long permisionId) {
        this.permisionId = permisionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RolePermisionEntity that = (RolePermisionEntity) o;
        return rolePermisionId == that.rolePermisionId &&
                roleId == that.roleId &&
                permisionId == that.permisionId &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rolePermisionId, roleId, modified, permisionId);
    }
}
