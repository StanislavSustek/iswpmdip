/*
package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "additional_permision", schema = "public", catalog = "iswpm")
public class AdditionalPermisionEntity {
    @EmbeddedId
    private UserEntity userId;

    private long permisionId;
    private long companyInternalId;

    @Basic
    @Column(name = "permision_id")
    public long getPermisionId() {
        return permisionId;
    }

    public void setPermisionId(long permisionId) {
        this.permisionId = permisionId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdditionalPermisionEntity that = (AdditionalPermisionEntity) o;
        return userId == that.userId &&
                permisionId == that.permisionId &&
                companyInternalId == that.companyInternalId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, permisionId, companyInternalId);
    }
}
*/
