package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "saved_sallary", schema = "public", catalog = "iswpm")
public class SavedSallaryEntity {
    private long savedSallaryId;
    private int year;
    private int month;
    private BigDecimal baseSallary;
    private BigDecimal specialReward;
    private BigDecimal additionalSpecialReward;
    private BigDecimal sallaryTotal;
    private long userId;
    private Long sallaryHistoryId;
    private long companyInternalId;

    @Id
    @Column(name = "saved_sallary_id")
    public long getSavedSallaryId() {
        return savedSallaryId;
    }

    public void setSavedSallaryId(long savedSallaryId) {
        this.savedSallaryId = savedSallaryId;
    }

    @Basic
    @Column(name = "year")
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Basic
    @Column(name = "month")
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Basic
    @Column(name = "base_sallary")
    public BigDecimal getBaseSallary() {
        return baseSallary;
    }

    public void setBaseSallary(BigDecimal baseSallary) {
        this.baseSallary = baseSallary;
    }

    @Basic
    @Column(name = "special_reward")
    public BigDecimal getSpecialReward() {
        return specialReward;
    }

    public void setSpecialReward(BigDecimal specialReward) {
        this.specialReward = specialReward;
    }

    @Basic
    @Column(name = "additional_special_reward")
    public BigDecimal getAdditionalSpecialReward() {
        return additionalSpecialReward;
    }

    public void setAdditionalSpecialReward(BigDecimal additionalSpecialReward) {
        this.additionalSpecialReward = additionalSpecialReward;
    }

    @Basic
    @Column(name = "sallary_total")
    public BigDecimal getSallaryTotal() {
        return sallaryTotal;
    }

    public void setSallaryTotal(BigDecimal sallaryTotal) {
        this.sallaryTotal = sallaryTotal;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "sallary_history_id")
    public Long getSallaryHistoryId() {
        return sallaryHistoryId;
    }

    public void setSallaryHistoryId(Long sallaryHistoryId) {
        this.sallaryHistoryId = sallaryHistoryId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavedSallaryEntity that = (SavedSallaryEntity) o;
        return savedSallaryId == that.savedSallaryId &&
                year == that.year &&
                month == that.month &&
                userId == that.userId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(baseSallary, that.baseSallary) &&
                Objects.equals(specialReward, that.specialReward) &&
                Objects.equals(additionalSpecialReward, that.additionalSpecialReward) &&
                Objects.equals(sallaryTotal, that.sallaryTotal) &&
                Objects.equals(sallaryHistoryId, that.sallaryHistoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(savedSallaryId, year, month, baseSallary, specialReward, additionalSpecialReward, sallaryTotal, userId, sallaryHistoryId, companyInternalId);
    }
}
