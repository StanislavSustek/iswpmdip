package sk.isw.iswpm.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "security.LoginRequest")
@Data
@Accessors(chain = true)
public class LoginRequest {
    private String login;
    private String password;
}
