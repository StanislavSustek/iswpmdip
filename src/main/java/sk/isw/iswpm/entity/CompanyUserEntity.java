package sk.isw.iswpm.entity;

import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Accessors(chain = true)
@Table(name = "company_user", schema = "public", catalog = "iswpm")
public class CompanyUserEntity {
    private long companyUserId;
    private Timestamp modified;
    private boolean deleted;
    private Timestamp deletedTimestamp;
    private long userId;
    private Long companyInternalId;

    @Id
    @Column(name = "company_user_id")
    public long getCompanyUserId() {
        return companyUserId;
    }

    public void setCompanyUserId(long companyUserId) {
        this.companyUserId = companyUserId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "deleted")
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "deleted_timestamp")
    public Timestamp getDeletedTimestamp() {
        return deletedTimestamp;
    }

    public void setDeletedTimestamp(Timestamp deletedTimestamp) {
        this.deletedTimestamp = deletedTimestamp;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public Long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(Long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyUserEntity that = (CompanyUserEntity) o;
        return companyUserId == that.companyUserId &&
                deleted == that.deleted &&
                userId == that.userId &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(deletedTimestamp, that.deletedTimestamp) &&
                Objects.equals(companyInternalId, that.companyInternalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyUserId, modified, deleted, deletedTimestamp, userId, companyInternalId);
    }
}
