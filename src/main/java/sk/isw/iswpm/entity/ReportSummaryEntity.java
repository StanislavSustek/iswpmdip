package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "report_summary", schema = "public", catalog = "iswpm")
public class ReportSummaryEntity {
    private long reportSummaryId;
    private BigDecimal hoursReported;
    private long modifiedUserId;
    private Timestamp dateFrom;
    private Timestamp dateTo;
    private Timestamp modified;
    private Long savedSallaryId;
    private long companyInternalId;
    private BigDecimal hoursConfirmed;
    private Long userId;

    @Id
    @Column(name = "report_summary_id")
    public long getReportSummaryId() {
        return reportSummaryId;
    }

    public void setReportSummaryId(long reportSummaryId) {
        this.reportSummaryId = reportSummaryId;
    }

    @Basic
    @Column(name = "hours_reported")
    public BigDecimal getHoursReported() {
        return hoursReported;
    }

    public void setHoursReported(BigDecimal hoursReported) {
        this.hoursReported = hoursReported;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "date_from")
    public Timestamp getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Timestamp dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public Timestamp getDateTo() {
        return dateTo;
    }

    public void setDateTo(Timestamp dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "saved_sallary_id")
    public Long getSavedSallaryId() {
        return savedSallaryId;
    }

    public void setSavedSallaryId(Long savedSallaryId) {
        this.savedSallaryId = savedSallaryId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "hours_confirmed")
    public BigDecimal getHoursConfirmed() {
        return hoursConfirmed;
    }

    public void setHoursConfirmed(BigDecimal hoursConfirmed) {
        this.hoursConfirmed = hoursConfirmed;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportSummaryEntity that = (ReportSummaryEntity) o;
        return reportSummaryId == that.reportSummaryId &&
                modifiedUserId == that.modifiedUserId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(hoursReported, that.hoursReported) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(savedSallaryId, that.savedSallaryId) &&
                Objects.equals(hoursConfirmed, that.hoursConfirmed) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reportSummaryId, hoursReported, modifiedUserId, dateFrom, dateTo, modified, savedSallaryId, companyInternalId, hoursConfirmed, userId);
    }
}
