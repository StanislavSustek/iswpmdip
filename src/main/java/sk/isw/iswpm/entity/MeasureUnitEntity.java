package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "measure_unit", schema = "public", catalog = "iswpm")
public class MeasureUnitEntity {
    private long measureUnitId;
    private String unit;
    private boolean active;
    private String type;

    @Id
    @Column(name = "measure_unit_id")
    public long getMeasureUnitId() {
        return measureUnitId;
    }

    public void setMeasureUnitId(long measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    @Basic
    @Column(name = "unit")
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MeasureUnitEntity that = (MeasureUnitEntity) o;
        return measureUnitId == that.measureUnitId &&
                active == that.active &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(measureUnitId, unit, active, type);
    }
}
