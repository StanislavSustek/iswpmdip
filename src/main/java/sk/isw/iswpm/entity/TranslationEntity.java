/*
package sk.isw.iswpm.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "translation", schema = "public", catalog = "iswpm")
public class TranslationEntity {
    private long translationId;
    private String key;
    private String valueSk;
    private String valueEn;

    @Basic
    @Column(name = "translation_id")
    public long getTranslationId() {
        return translationId;
    }

    public void setTranslationId(long translationId) {
        this.translationId = translationId;
    }

    @Basic
    @Column(name = "key")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Basic
    @Column(name = "value_sk")
    public String getValueSk() {
        return valueSk;
    }

    public void setValueSk(String valueSk) {
        this.valueSk = valueSk;
    }

    @Basic
    @Column(name = "value_en")
    public String getValueEn() {
        return valueEn;
    }

    public void setValueEn(String valueEn) {
        this.valueEn = valueEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TranslationEntity that = (TranslationEntity) o;
        return translationId == that.translationId &&
                Objects.equals(key, that.key) &&
                Objects.equals(valueSk, that.valueSk) &&
                Objects.equals(valueEn, that.valueEn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(translationId, key, valueSk, valueEn);
    }
}
*/
