package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "sallary_history", schema = "public", catalog = "iswpm")
public class SallaryHistoryEntity {
    private long sallaryHistoryId;
    private int month;
    private int year;
    private BigDecimal baseSallary;
    private BigDecimal specialReward;
    private BigDecimal sallaryTotal;
    private long userUserId;
    private long companyInternalId;

    @Id
    @Column(name = "sallary_history_id")
    public long getSallaryHistoryId() {
        return sallaryHistoryId;
    }

    public void setSallaryHistoryId(long sallaryHistoryId) {
        this.sallaryHistoryId = sallaryHistoryId;
    }

    @Basic
    @Column(name = "month")
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Basic
    @Column(name = "year")
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Basic
    @Column(name = "base_sallary")
    public BigDecimal getBaseSallary() {
        return baseSallary;
    }

    public void setBaseSallary(BigDecimal baseSallary) {
        this.baseSallary = baseSallary;
    }

    @Basic
    @Column(name = "special_reward")
    public BigDecimal getSpecialReward() {
        return specialReward;
    }

    public void setSpecialReward(BigDecimal specialReward) {
        this.specialReward = specialReward;
    }

    @Basic
    @Column(name = "sallary_total")
    public BigDecimal getSallaryTotal() {
        return sallaryTotal;
    }

    public void setSallaryTotal(BigDecimal sallaryTotal) {
        this.sallaryTotal = sallaryTotal;
    }

    @Basic
    @Column(name = "user_user_id")
    public long getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(long userUserId) {
        this.userUserId = userUserId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SallaryHistoryEntity that = (SallaryHistoryEntity) o;
        return sallaryHistoryId == that.sallaryHistoryId &&
                month == that.month &&
                year == that.year &&
                userUserId == that.userUserId &&
                companyInternalId == that.companyInternalId &&
                Objects.equals(baseSallary, that.baseSallary) &&
                Objects.equals(specialReward, that.specialReward) &&
                Objects.equals(sallaryTotal, that.sallaryTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sallaryHistoryId, month, year, baseSallary, specialReward, sallaryTotal, userUserId, companyInternalId);
    }
}
