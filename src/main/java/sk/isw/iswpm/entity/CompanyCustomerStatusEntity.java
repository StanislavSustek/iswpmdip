package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "company_customer_status", schema = "public", catalog = "iswpm")
public class CompanyCustomerStatusEntity {
    private long companyCustomerStatusId;
    private String name;
    private boolean active;

    @Id
    @Column(name = "company_customer_status_id")
    public long getCompanyCustomerStatusId() {
        return companyCustomerStatusId;
    }

    public void setCompanyCustomerStatusId(long companyCustomerStatusId) {
        this.companyCustomerStatusId = companyCustomerStatusId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerStatusEntity that = (CompanyCustomerStatusEntity) o;
        return companyCustomerStatusId == that.companyCustomerStatusId &&
                active == that.active &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerStatusId, name, active);
    }
}
