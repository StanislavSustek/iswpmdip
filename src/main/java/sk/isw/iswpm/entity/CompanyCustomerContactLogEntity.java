package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "company_customer_contact_log", schema = "public", catalog = "iswpm")
public class CompanyCustomerContactLogEntity {
    private long companyCustomerContactLogId;
    private long companyCustomerContactStatusId;
    private long companyCustomerContactId;
    private long companyCustomerId;
    private String comment;
    private long modifiedUserId;
    private String email;

    @Id
    @Column(name = "company_customer_contact_log_id")
    public long getCompanyCustomerContactLogId() {
        return companyCustomerContactLogId;
    }

    public void setCompanyCustomerContactLogId(long companyCustomerContactLogId) {
        this.companyCustomerContactLogId = companyCustomerContactLogId;
    }

    @Basic
    @Column(name = "company_customer_contact_status_id")
    public long getCompanyCustomerContactStatusId() {
        return companyCustomerContactStatusId;
    }

    public void setCompanyCustomerContactStatusId(long companyCustomerContactStatusId) {
        this.companyCustomerContactStatusId = companyCustomerContactStatusId;
    }

    @Basic
    @Column(name = "company_customer_contact_id")
    public long getCompanyCustomerContactId() {
        return companyCustomerContactId;
    }

    public void setCompanyCustomerContactId(long companyCustomerContactId) {
        this.companyCustomerContactId = companyCustomerContactId;
    }

    @Basic
    @Column(name = "company_customer_id")
    public long getCompanyCustomerId() {
        return companyCustomerId;
    }

    public void setCompanyCustomerId(long companyCustomerId) {
        this.companyCustomerId = companyCustomerId;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerContactLogEntity that = (CompanyCustomerContactLogEntity) o;
        return companyCustomerContactLogId == that.companyCustomerContactLogId &&
                companyCustomerContactStatusId == that.companyCustomerContactStatusId &&
                companyCustomerContactId == that.companyCustomerContactId &&
                companyCustomerId == that.companyCustomerId &&
                modifiedUserId == that.modifiedUserId &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerContactLogId, companyCustomerContactStatusId, companyCustomerContactId, companyCustomerId, comment, modifiedUserId, email);
    }
}
