package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "additional_permision_log", schema = "public", catalog = "iswpm")
public class AdditionalPermisionLogEntity {
    private long additionalPermisionLogId;
    private long userId;
    private long permisionId;
    private long companyInternalId;
    private Timestamp modified;
    private long modifiedUserId;
    private boolean deleted;

    @Id
    @Column(name = "additional_permision_log_id")
    public long getAdditionalPermisionLogId() {
        return additionalPermisionLogId;
    }

    public void setAdditionalPermisionLogId(long additionalPermisionLogId) {
        this.additionalPermisionLogId = additionalPermisionLogId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "permision_id")
    public long getPermisionId() {
        return permisionId;
    }

    public void setPermisionId(long permisionId) {
        this.permisionId = permisionId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "modified_user_id")
    public long getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    @Basic
    @Column(name = "deleted")
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdditionalPermisionLogEntity that = (AdditionalPermisionLogEntity) o;
        return additionalPermisionLogId == that.additionalPermisionLogId &&
                userId == that.userId &&
                permisionId == that.permisionId &&
                companyInternalId == that.companyInternalId &&
                modifiedUserId == that.modifiedUserId &&
                deleted == that.deleted &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(additionalPermisionLogId, userId, permisionId, companyInternalId, modified, modifiedUserId, deleted);
    }
}
