package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "company_customer_contact", schema = "public", catalog = "iswpm")
public class CompanyCustomerContactEntity {
    private long companyCustomerContactId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private long companyCustomerId;
    private Timestamp modified;
    private long companyCustomerContactStatusId;

    @Id
    @Column(name = "company_customer_contact_id")
    public long getCompanyCustomerContactId() {
        return companyCustomerContactId;
    }

    public void setCompanyCustomerContactId(long companyCustomerContactId) {
        this.companyCustomerContactId = companyCustomerContactId;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "company_customer_id")
    public long getCompanyCustomerId() {
        return companyCustomerId;
    }

    public void setCompanyCustomerId(long companyCustomerId) {
        this.companyCustomerId = companyCustomerId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "company_customer_contact_status_id")
    public long getCompanyCustomerContactStatusId() {
        return companyCustomerContactStatusId;
    }

    public void setCompanyCustomerContactStatusId(long companyCustomerContactStatusId) {
        this.companyCustomerContactStatusId = companyCustomerContactStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerContactEntity that = (CompanyCustomerContactEntity) o;
        return companyCustomerContactId == that.companyCustomerContactId &&
                companyCustomerId == that.companyCustomerId &&
                companyCustomerContactStatusId == that.companyCustomerContactStatusId &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerContactId, firstName, lastName, email, phone, companyCustomerId, modified, companyCustomerContactStatusId);
    }
}
