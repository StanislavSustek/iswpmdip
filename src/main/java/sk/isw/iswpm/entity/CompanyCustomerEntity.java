package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "company_customer", schema = "public", catalog = "iswpm")
public class CompanyCustomerEntity {
    private long companyCustomerId;
    private long companyId;
    private long companyInternalId;
    private long companyCustomerStatusId;
    private Timestamp modified;

    @Id
    @Column(name = "company_customer_id")
    public long getCompanyCustomerId() {
        return companyCustomerId;
    }

    public void setCompanyCustomerId(long companyCustomerId) {
        this.companyCustomerId = companyCustomerId;
    }

    @Basic
    @Column(name = "company_id")
    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "company_internal_id")
    public long getCompanyInternalId() {
        return companyInternalId;
    }

    public void setCompanyInternalId(long companyInternalId) {
        this.companyInternalId = companyInternalId;
    }

    @Basic
    @Column(name = "company_customer_status_id")
    public long getCompanyCustomerStatusId() {
        return companyCustomerStatusId;
    }

    public void setCompanyCustomerStatusId(long companyCustomerStatusId) {
        this.companyCustomerStatusId = companyCustomerStatusId;
    }

    @Basic
    @Column(name = "modified")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCustomerEntity that = (CompanyCustomerEntity) o;
        return companyCustomerId == that.companyCustomerId &&
                companyId == that.companyId &&
                companyInternalId == that.companyInternalId &&
                companyCustomerStatusId == that.companyCustomerStatusId &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyCustomerId, companyId, companyInternalId, companyCustomerStatusId, modified);
    }
}
