package sk.isw.iswpm.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "email_notification_user", schema = "public", catalog = "iswpm")
public class EmailNotificationUserEntity {
    private long emailNotificationUserId;
    private long emailNotificationId;
    private long userId;

    @Id
    @Column(name = "email_notification_user_id")
    public long getEmailNotificationUserId() {
        return emailNotificationUserId;
    }

    public void setEmailNotificationUserId(long emailNotificationUserId) {
        this.emailNotificationUserId = emailNotificationUserId;
    }

    @Basic
    @Column(name = "email_notification_id")
    public long getEmailNotificationId() {
        return emailNotificationId;
    }

    public void setEmailNotificationId(long emailNotificationId) {
        this.emailNotificationId = emailNotificationId;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailNotificationUserEntity that = (EmailNotificationUserEntity) o;
        return emailNotificationUserId == that.emailNotificationUserId &&
                emailNotificationId == that.emailNotificationId &&
                userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailNotificationUserId, emailNotificationId, userId);
    }
}
