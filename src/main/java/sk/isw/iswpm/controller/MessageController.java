package sk.isw.iswpm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.UserMessageChangeRequest;
import sk.isw.iswpm.service.MessagesService;
import sk.isw.iswpm.service.socket.SocketService;

import java.security.Principal;

@Controller
@MessageMapping("/message")
@RequiredArgsConstructor
public class MessageController {

    private final MessagesService messagesService;
    private final SocketService socketService;


    @MessageMapping("/getMessages")
    public  void getUserMessages(@Header("token") String token) {
        refreshMessageForUser(token);
    }

    @MessageMapping("/marAsRead")
    public Boolean markAsRead(Principal principal, @Header("token") String token, @RequestBody UserMessageChangeRequest request) {
        messagesService.markAsRead(token, request);
        refreshMessageForUser(token);
        return true;
    }

    @MessageMapping("/deleteMessage")
    public Boolean deleteUserMessage(@Header("token") String token, @RequestBody UserMessageChangeRequest request) {
        messagesService.deleteUserMessage(token, request);
        refreshMessageForUser(token);
        return true;
    }

    private void refreshMessageForUser(String token) {
        socketService.sendToCurrenctUser(token, "getMessages" ,messagesService.getUserMessagees(token));
    }

}
