package sk.isw.iswpm.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.CreateHolidayRequest;
import sk.isw.iswpm.controllerEntity.HolidayFilterRequest;
import sk.isw.iswpm.controllerEntity.MessageChannelResponse;
import sk.isw.iswpm.service.HolidayService;
import sk.isw.iswpm.service.socket.SocketService;

@Slf4j
@Controller
@MessageMapping("holiday")
@AllArgsConstructor
public class HolidayController {

    private final SocketService socketService;
    private final HolidayService holidayService;
    private final String PREFIX = "holiday";

    @MessageMapping("getHolidays")
    public void getHolidays(@Header(value = "token") String token, @RequestBody HolidayFilterRequest request) {
        socketService.sendToCurrenctUser(token, "getHolidays", holidayService.getHolidays(request));
    }

    @MessageMapping("getHolidaysForApproval")
    public void getHolidaysForApproval(@Header(value = "token") String token, @RequestBody HolidayFilterRequest request) {
        socketService.sendToCurrenctUser(token, "getHolidaysForApproval", holidayService.getHolidays(request));
    }

    @MessageMapping("createHoliday")
    public void createHoliday(@Header(value = "token") String token, @RequestBody CreateHolidayRequest request) {
        holidayService.createHoliday(request);
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getHolidays").setRefresh(true));
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getHolidaysForApproval").setRefresh(true));
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getMessages").setRefresh(true));
    }

    @MessageMapping("changeHolidayState")
    public void changeHolidayState(@Header(value = "token") String token, @RequestBody CreateHolidayRequest request) {
        holidayService.createHoliday(request);
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getMessages").setRefresh(true));
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getHolidays").setRefresh(true));
        socketService.sendForAll("refreshChanel", new MessageChannelResponse().setMessage("getHolidaysForApproval").setRefresh(true));
    }
}
