package sk.isw.iswpm.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.entity.CompanyEntity;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.security.HasRole;
import sk.isw.iswpm.service.AdminService;
import sk.isw.iswpm.service.SecurityService;
import sk.isw.iswpm.service.socket.SocketService;
import sk.isw.iswpm.types.CompanyType;
import sk.isw.iswpm.types.GetCompanyRequest;

import java.util.List;

@Controller
@MessageMapping(value = "/admin")
@AllArgsConstructor
public class AdminController {

    private final AdminService adminService;
    private final SimpMessagingTemplate template;
    private final SecurityService securityService;
    private final SocketService socketService;

    @ApiOperation(value = "Spoločnosti")
    @MessageMapping("/getCompanies")
    @HasRole("SYSTEM_ADMIN")
    public void getCompanies(@Header(value = "token") String token, @ApiParam(required = true) @RequestBody GetCompanyRequest getCompanyRequest) {
        CacheUser u = securityService.getUserFromCache(token);
        List<GetCompaniesResponse> e = adminService.getCompaniesCompanies(getCompanyRequest.getCompanyType());
        socketService.sendForAll("getCompanies", e);
    }


    @ApiOperation(value = "Uzivatelia")
    @MessageMapping("/getUsers")
    @HasRole("SYSTEM_ADMIN")
    public void getUsers(@Header(value = "token") String token) {
        socketService.sendForAll("getAllUsers", securityService.getAllUsers());
    }

    @ApiOperation(value = "Vytvorit uzivatela")
    @MessageMapping("/createUser")
    @HasRole("SYSTEM_ADMIN")
    public void createUser(@Header(value = "token") String token, @ApiParam(required = true) @RequestBody User request) {
        securityService.createUser(request);
        socketService.sendForAll("getAllUsers", securityService.getAllUsers());
    }

    @ApiOperation(value = "Vytvorit spoločnost")
    @MessageMapping("/createCompany")
    @HasRole("SYSTEM_ADMIN")
    public void createCompany(@Header(value = "token") String token, @ApiParam(required = true) @RequestBody CompanyEntity companyEntity) {
        if (adminService.createCompany(companyEntity)) {
            refreshCompaniesForAll();
        }
    }

    @ApiOperation(value = "Zmazat spoločnost")
    @MessageMapping("/deleteCompany")
    @HasRole("SYSTEM_ADMIN")
    public void deleteCompany(@Header(value = "token") String token, @ApiParam(required = true) @RequestBody CompanyEntity companyEntity) {
        adminService.deleteCompany(companyEntity);
        refreshCompaniesForAll();
    }

    @HasRole("ADD_EDIT_DELETE_ROLE")
    @MessageMapping("/getRolesWithPrivileges")
    public void getRolesWithPrivileges(@Header("token") String token, @RequestBody GetRoleWithPrivilegesRequest request) {
        socketService.sendToObject(request.getCompanyId(), "company","getRolesWithPrivileges", adminService.getRolesWithPermissions(request));
    }


    @HasRole("ADD_EDIT_DELETE_ROLE")
    @MessageMapping("/addPrivilegeToRole")
    public void addPrivilegeToRole(@Header("token") String token, @RequestBody AddPrivilegeToRoleRequest request) {
        socketService.sendToObject(request.getCompanyId(), "company","getRolesWithPrivileges", adminService.addPrivilegeToRole(request));
    }

    @HasRole("ADD_EDIT_DELETE_ROLE")
    @MessageMapping("/createRole")
    public void createRole(@Header("token") String token, RoleRequest request) {
        adminService.createRole(request);
        socketService.sendToObject(request.getCompanyId(), "company","getRolesWithPrivileges", adminService.getRolesWithPermissions(new GetRoleWithPrivilegesRequest().setCompanyId(request.getCompanyId())));
    }

    @HasRole("ADD_EDIT_DELETE_ROLE")
    @MessageMapping("/getPrivileges")
    public void getPrivileges(@Header("token") String token) {
        socketService.sendForAll("getPrivileges", adminService.getAllPermisions());
    }

    private void refreshCompaniesForAll() {
        this.template.convertAndSend("/topic/getCompanies",
                adminService.getCompaniesCompanies(CompanyType.ALL));
    }


}
