package sk.isw.iswpm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import sk.isw.iswpm.entity.HelloMessage;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.service.SecurityService;

import java.util.List;

@Controller
public class WebSocketController {

    @Autowired
    SecurityService securityService;

    @Autowired
    public WebSocketController() {
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    @SendToUser("")
    public HelloMessage gretting(HelloMessage message) {
        return new HelloMessage().setName("nooooooo");
    }

}
