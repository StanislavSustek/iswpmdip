package sk.isw.iswpm.controller;

import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.GetProjectTaskRequest;
import sk.isw.iswpm.entity.TaskEntity;
import sk.isw.iswpm.service.TaskService;

import java.util.List;

@MessageMapping("/project")
@Controller
@AllArgsConstructor
public class TaskController {

    private final TaskService taskService;
    private final SimpMessagingTemplate template;

    @MessageMapping("getTasks")
    @SendTo("/topic/project/tasks")
    public List<TaskEntity>  getProjectTasks(@Header(value = "token") String token, @RequestBody GetProjectTaskRequest request) {
        sendTasksToUser(token, taskService.getProjectTasks(request));
        return  taskService.getProjectTasks(request);
    }

    private void sendTasksToUser(String token, List<TaskEntity> list) {
        template.convertAndSendToUser("1", "project/task", list);
    }

}
