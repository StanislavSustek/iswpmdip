package sk.isw.iswpm.controller;

import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.entity.Company;
import sk.isw.iswpm.entity.TaskEntity;
import sk.isw.iswpm.repository.crud.ProjectRepository;
import sk.isw.iswpm.security.HasRole;
import sk.isw.iswpm.service.CompanyManagementService;
import sk.isw.iswpm.service.ProjectService;
import sk.isw.iswpm.service.TaskService;
import sk.isw.iswpm.service.socket.SocketService;

@Controller
@MessageMapping("projects")
@AllArgsConstructor
public class ProjectController {

    public ProjectRepository projectRepository;
    private final TaskService taskService;
    private final ProjectService projectService;
    private final SocketService socketService;
    private final CompanyManagementService companyManagementService;
    private final String PREFIX = "project";
    private final String TASK_PREFIX = "/task";


    @MessageMapping("getCompanyProjects")
    @SendTo("/topic/company/projects")
    public CompanyProjectsResponse getCompanyProjects(@RequestBody CompanyProjectsRequest request) {
        CompanyProjectsResponse response = new CompanyProjectsResponse();
        projectRepository.findAll().forEach(response.getProjectList()::add);
        return response;
    }


   /* @MessageMapping("getUserProjects")
    @SendTo("/user/projects")
    public CompanyProjectsResponse getUserProjects(@RequestBody CompanyProjectsRequest request) {
        CompanyProjectsResponse response = new CompanyProjectsResponse();
        projectRepository.findAll().forEach(response.getProjectList()::add);
        return response;

    }*/

    @MessageMapping("getTasks")
    public void getProjectTasks(@Header(value = "token") String token, @RequestBody GetProjectTaskRequest request) {
        socketService.sendToObject(request.getProjectId(), PREFIX,"task", taskService.getProjectTasks(request));
    }

    @MessageMapping("getUserTasks")
    public void getUserProjectTasks(@Header(value = "token") String token, @RequestBody GetProjectTaskRequest request) {
        socketService.sendToCurrenctUser(token, "project/task", taskService.getUserTasks(token, request.getProjectId(), 0));
    }


    @MessageMapping("getUsers")
    public void getProjectUsers(@Header(value = "token") String token, @RequestBody GetProjectTaskRequest request) {
        socketService.sendToObject(request.getProjectId(), PREFIX, "users", projectService.getProjectUsers(request.getProjectId()));
    }

    @MessageMapping("getTaskDetail")
    public void getTaskDetail(@Header(value = "token") String token, @RequestBody TaskDetailRequest request) {
        socketService.sendToObject(request.getTaskId(), PREFIX + TASK_PREFIX, "detail", taskService.getTaskDetail(request));
    }

    @MessageMapping("getProjectDetail")
    public void getProjectDetail(@Header(value = "token") String token, @RequestBody ProjectDetailRequest request) {
        socketService.sendToObject(request.getProjectId(), PREFIX, "detail", projectService.getProjectDetail(request));
    }

    @MessageMapping("sendComment")
    public void sendComment(@Header(value = "token") String token, @RequestBody SendCommentRequest request) {
        taskService.sendComment(request, token);
        socketService.sendToObject(request.getTaskId(), PREFIX + TASK_PREFIX, "detail", taskService.getTaskDetail(new TaskDetailRequest().setTaskId(request.getTaskId())));
    }

    @MessageMapping("uploadProjectDocument")
    public void uploadDocumentForProject(@Header(value = "token") String token, @RequestBody UloadDocumentRequest request) {
        taskService.uloadProjectDocument(request, token);
        socketService.sendToObject(request.getProjectId(), PREFIX, "detail", projectService.getProjectDetail(new ProjectDetailRequest().setProjectId(request.getProjectId())));
    }

    @MessageMapping("getProjectDocument")
    public void getProjectDocuments(@Header(value = "token") String token, @RequestBody GetProjectDocumentRequest request) {
        socketService.sendToCurrenctUser(token, "getProjectDocument",  taskService.getProjectDocument(request));
    }

    @MessageMapping("uloadTaskDocument")
    public void uloadTaskDocument(@Header(value = "token") String token, @RequestBody UloadDocumentRequest request) {
        taskService.uloadDocument(request, token);
        socketService.sendToObject(request.getTaskId(), PREFIX + TASK_PREFIX, "detail", taskService.getTaskDetail(new TaskDetailRequest().setTaskId(request.getTaskId())));
    }

    @MessageMapping("updateTask")
    @HasRole("ADD_EDIT_TASK")
    public void updateTask(@Header(value = "token") String token, @RequestBody TaskEntity request) {
        socketService.sendToCurrenctUser(token, "updateTask", taskService.updateTask(request, token));
        socketService.sendToObject(request.getProjectId(), PREFIX, "detail", projectService.getProjectDetail(new ProjectDetailRequest().setProjectId(request.getProjectId())));
        socketService.sendToObject(request.getProjectId(), PREFIX, "task", taskService.getProjectTasks(new GetProjectTaskRequest().setProjectId(request.getProjectId())));
        socketService.sendToCustomUser(request.getUserId(), "project/task", taskService.getUserTasks(null, request.getProjectId(), request.getUserId()));
    }

    @MessageMapping("addUserToProject")
    public void addUserToProject(@Header(value = "token") String token, @RequestBody AddUserToProjectRequest request) {
        projectService.addUserToProject(request);
        request.getAddUsers().forEach(x -> {
            socketService.sendToCustomUser(x.getUserId(), "getProjects",  companyManagementService.getProjectsForUser(null, x.getUserId()));
        });
        request.getRemoveUsers().forEach(x -> {
            socketService.sendToCustomUser(x.getUserId(), "getProjects",  companyManagementService.getProjectsForUser(null, x.getUserId()));
        });
        socketService.sendToObject(request.getProjectId(), PREFIX, "users", projectService.getProjectUsers(request.getProjectId()));
    }

    @MessageMapping(value = "/getUserProjects")
    public void getUserProjects(@Header("token") String token) {
        socketService.sendToCurrenctUser(token ,"getProjects", companyManagementService.getProjectsForUser(token, null));
    }


}
