package sk.isw.iswpm.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.service.CompanyManagementService;
import sk.isw.iswpm.service.socket.SocketService;

@Controller
@MessageMapping(value = "/company")
public class CompanyManagementController {

    @Autowired
    private CompanyManagementService companyManagementService;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private SocketService socketService;

    @ApiOperation(value = "Uzivatelia nepriradeni spolocnosti", response = GetUsersForCompanyResponse.class)
    @MessageMapping(value = "/getUsersForCompany")
    @SendTo("/topic/getUsersForCompany")
    public GetUsersForCompanyResponse getUsersForCompany(@RequestBody  GetUsersForCompanyRequest request) {
        return companyManagementService.getUsersForCompany(request);
    }

    @ApiOperation(value = "Uzivatelia priradeni spolocnosti", response = GetUsersForCompanyResponse.class)
    @MessageMapping(value = "/getCompanyUsers")
    @SendTo("/topic/getCompanyUsers")
    public GetUsersForCompanyResponse getCompanyUsers(@Header("token") String token, @RequestBody  GetUsersForCompanyRequest request) {
        return companyManagementService.getCompanyUsers(request);
    }

    @ApiOperation(value = "Priradenie uzivatela spolocnosti")
    @MessageMapping(value = "/addUserIntoCompany")
    @SubscribeMapping("/topic/succesMessage")
    public void addUserIntoCompany(@RequestBody AddUserIntoCompanyRequest addUserIntoCompanyRequest) {
        companyManagementService.addUserIntoCompany(addUserIntoCompanyRequest);
        refreshCompanyUsers(addUserIntoCompanyRequest.getCompanyId());
    }

    private void refreshCompanyUsers(long companyId) {
        /*refresnutie uzivatelov spolocnosti*/
        this.template.convertAndSend("/topic/getUsersForCompany",
                companyManagementService.getCompanyUsers(new GetUsersForCompanyRequest().setCompanyId(companyId)));
    }

    @MessageMapping(value = "/getUserCompanies")
    public void getUserCompanies(@Header("token") String token) {
        socketService.sendToCurrenctUser(token, "getUserCompanies", companyManagementService.getUserCompanies(token));
    }

    @MessageMapping(value = "/getProjects")
    public void getProjects(@Header("token") String token, GetUsersForCompanyRequest request) {
        this.template.convertAndSend("/topic/getProjects", companyManagementService.getProjects(request));
    }

    @MessageMapping(value = "/createProject")
    public void createProject(@Header("token") String token, CreateProjectRequest request) {
        companyManagementService.createProject(request);
        this.template.convertAndSend("/topic/getProjects", companyManagementService.getProjects(new GetUsersForCompanyRequest().setCompanyId(request.getCompanyId())));
    }

    @MessageMapping(value = "/updateProjectInfo")
    public void updateProjectInfo(@Header("token") String token, CreateProjectRequest request) {
        companyManagementService.updateProjectInfo(request);
        this.template.convertAndSend("/topic/getProjects", companyManagementService.getProjects(new GetUsersForCompanyRequest().setCompanyId(request.getCompanyId())));
    }

    @MessageMapping(value = "/deleteProject")
    public void deleteProject(@Header("token") String token, CreateProjectRequest request) {
        companyManagementService.deleteProject(request);
        this.template.convertAndSend("/topic/getProjects", companyManagementService.getProjects(new GetUsersForCompanyRequest().setCompanyId(request.getCompanyId())));
    }

    @MessageMapping(value = "/addUserIntoProject")
    public void addUserIntoProject(@Header("token") String token, AddUsersIntoProjectRequest request) {
        companyManagementService.addUserIntoProject(request);
//        this.template.convertAndSend("/topic/"+ token +"/getProjects", companyManagementService.getProjects(new GetUsersForCompanyRequest().setCompanyId(request.getCompanyId())));
    }

    @MessageMapping(value = "/addUserToCompany")
    public void addUserIntoProject(@Header("token") String token, AddUserToCompanyRequest request) {
        companyManagementService.addUserToCompany(request);
//        this.template.convertAndSend("/topic/"+ token +"/getProjects", companyManagementService.getProjects(new GetUsersForCompanyRequest().setCompanyId(request.getCompanyId())));
    }
}
