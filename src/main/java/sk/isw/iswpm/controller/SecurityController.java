package sk.isw.iswpm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sk.isw.iswpm.controllerEntity.PasswordResetRequest;
import sk.isw.iswpm.controllerEntity.PasswordResetResponse;
import sk.isw.iswpm.entity.LoginRequest;
import sk.isw.iswpm.entity.LoginResponse;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.entity.UserEntity;
import sk.isw.iswpm.service.SecurityService;

@Slf4j
@Api(tags = "Security")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/security")
public class SecurityController {

    private final SecurityService securityService;
    private final SimpMessagingTemplate template;

    @ApiOperation(value = "", response = LoginResponse.class)
    @PostMapping(value = "/login")
    public LoginResponse login(@ApiParam(required = true) @RequestBody LoginRequest loginRequest) {
        return securityService.authentificate(loginRequest);
    }

    @ApiOperation(value = "")
    @PostMapping(value = "/resetPassword")
    public PasswordResetResponse resetPassword(@RequestBody PasswordResetRequest request) {
        return securityService.resetPassword(request);
    }


    @SendTo("/topic/forceLogoutClient")
    public Boolean forceLogoutClient(String token) {
        return true;
    }


    @MessageMapping("logout")
    public void logout(@Header(value = "token") String token) {
        this.securityService.logout(token);
        log.info("LOGOUT");
    }

    @PostMapping(value = "/getUserDetail")
    public User getPersmisions(@RequestHeader("token") String token) throws Exception {
        User user = new User();
        try {
            UserEntity userEntity = securityService.getUser(token);
            user.setUserId(userEntity.getUserId());
            user.setFirstName(userEntity.getFirstName());
            user.setLastName(userEntity.getLastName());
            user.setEmail(userEntity.getEmail());
            user.setLogin(userEntity.getLogin());
            user.setCompaniesWithPermisions(securityService.getPermisions(token));
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, "Unauthorized", e);
        }
        return user;
    }

}
