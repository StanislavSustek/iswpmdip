package sk.isw.iswpm.types;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "admin.GetCompanyRequest")
@Data
@Accessors(chain = true)
public class GetCompanyRequest {
    private CompanyType companyType;
}
