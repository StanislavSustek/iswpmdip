package sk.isw.iswpm.types;

public enum CompanyType {
    ALL, INTERNAL, CUSTOMER
}
