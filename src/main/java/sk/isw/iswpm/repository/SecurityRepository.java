package sk.isw.iswpm.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.repository.mapper.UserMapper;

import java.util.List;

@Repository
public class SecurityRepository {

    private final JdbcOperations jdbcOperations;

    public SecurityRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    public User loadUserByUsername(String login) {
        List<User> user = jdbcOperations.query("SELECT * from USER WHERE username=?)",
                new Object[]{login},
                new UserMapper()
        );
        if (user.size() > 0) {
            return user.get(0);
        } else {
            return null;
        }
    }

    /*private int getUserStatusId(UserStatus userStatus, Connection connection) throws SQLException {
        if (userStatus == null || connection == null) {
            throw new SQLException("getUserStatusId - userStatus=" + userStatus + " ,connection=" + connection);
        }
        String sql = "SELECT user_status_id FROM user_status WHERE LOWER(\"name\") = LOWER(?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, userStatus.name());
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt("user_status_id");
        } else {
            throw new SQLException("getUserStatusId - no UserStatus found");
        }
    }*/
}
