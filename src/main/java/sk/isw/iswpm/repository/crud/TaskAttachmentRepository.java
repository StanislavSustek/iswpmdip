package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.TaskAttachmentEntity;

import java.util.List;

public interface TaskAttachmentRepository extends CrudRepository<TaskAttachmentEntity, Long> {
    @Query(value = "select * from task_attachment where task_id=:taskId", nativeQuery = true)
    public List<TaskAttachmentEntity> getTaskAttachement(@Param("taskId") Long taskId);
}
