package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderItemStatusEntity;

public interface OrderItemStatusRepository extends CrudRepository<OrderItemStatusEntity, Long> {
}
