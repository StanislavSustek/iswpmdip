package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.TaskReportEntity;

public interface TaskReportRepository extends CrudRepository<TaskReportEntity, Long> {
}
