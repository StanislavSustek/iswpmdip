package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ReportSummaryEntity;

public interface ReportSummaryRepository extends CrudRepository<ReportSummaryEntity, Long> {
}
