package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.MessagesEntity;

import java.util.List;

public interface MessagesRepository extends CrudRepository<MessagesEntity, Long> {

    @Query(value = "SELECT * from messages m join user_messages using(message_id) where user_id=:userId", nativeQuery = true)
    List<MessagesEntity> getUserMessages(@Param("userId") long userId);
}
