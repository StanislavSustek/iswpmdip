package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectSkillEntity;

public interface ProjectSkillRepository extends CrudRepository<ProjectSkillEntity, Long> {
}
