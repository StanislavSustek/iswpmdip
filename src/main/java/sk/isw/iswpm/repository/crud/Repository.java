package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.entity.UserEntity;

public interface Repository extends CrudRepository<UserEntity, Long> {
}
