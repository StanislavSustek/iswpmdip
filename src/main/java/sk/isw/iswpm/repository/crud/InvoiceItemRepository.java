package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.InvoiceItemEntity;

public interface InvoiceItemRepository extends CrudRepository<InvoiceItemEntity, Long> {
}
