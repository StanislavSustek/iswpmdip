package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectLogEntity;

public interface ProjectLogRepository extends CrudRepository<ProjectLogEntity, Long> {
}
