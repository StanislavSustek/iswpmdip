package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderLogEntity;

public interface OrderLogRepository extends CrudRepository<OrderLogEntity, Long> {
}
