package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerContactEntity;

public interface CompanyCustomerContactRepository extends CrudRepository<CompanyCustomerContactEntity, Long> {
}
