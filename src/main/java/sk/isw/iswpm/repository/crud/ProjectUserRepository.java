package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.ProjectUserEntity;

import java.util.List;

public interface ProjectUserRepository extends CrudRepository<ProjectUserEntity, Long> {
    @Query(value = "select * from project_user where user_id=:userId", nativeQuery = true)
    List<ProjectUserEntity> getProjectByUser(@Param("userId") long userId);

    @Query(value = "delete from project_user where user_id=:userId and project_id=:projectId", nativeQuery = true)
    ProjectUserEntity deleteUserFromProject(@Param("userId") long userId, @Param("projectId") long projectId);
}
