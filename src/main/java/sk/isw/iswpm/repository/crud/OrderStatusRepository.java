package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderStatusEntity;

public interface OrderStatusRepository extends CrudRepository<OrderStatusEntity, Long> {
}
