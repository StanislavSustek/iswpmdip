package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectUserStatusEntity;

public interface ProjectUserStatusRepository extends CrudRepository<ProjectUserStatusEntity, Long> {
}
