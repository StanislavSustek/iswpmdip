package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerLogEntity;

public interface CompanyCustomerLogRepository extends CrudRepository<CompanyCustomerLogEntity, Long> {
}
