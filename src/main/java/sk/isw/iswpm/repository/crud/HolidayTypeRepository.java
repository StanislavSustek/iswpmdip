package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.HolidayTypeEntity;

public interface HolidayTypeRepository extends CrudRepository<HolidayTypeEntity, Long> {
}
