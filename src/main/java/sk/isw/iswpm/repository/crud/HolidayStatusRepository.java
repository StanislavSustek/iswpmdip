package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.HolidayStatusEntity;

public interface HolidayStatusRepository extends CrudRepository<HolidayStatusEntity, Long> {
}
