package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.EmailNotificationUserEntity;

public interface EmailNotificationUserRepository extends CrudRepository<EmailNotificationUserEntity, Long> {
}
