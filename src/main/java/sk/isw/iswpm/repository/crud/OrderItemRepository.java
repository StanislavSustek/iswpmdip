package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderItemEntity;

public interface OrderItemRepository extends CrudRepository<OrderItemEntity, Long> {
}
