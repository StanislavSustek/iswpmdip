package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.HolidayRestrictionEntity;

public interface HolidayRestrictionRepository extends CrudRepository<HolidayRestrictionEntity, Long> {
}
