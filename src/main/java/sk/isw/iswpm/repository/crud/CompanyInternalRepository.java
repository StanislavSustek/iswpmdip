package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.CompanyInternalEntity;
import sk.isw.iswpm.entity.ProjectEntity;

import java.util.List;

public interface CompanyInternalRepository extends CrudRepository<CompanyInternalEntity, Long> {
    @Query(value = "select * from company_internal where company_id=:companyId", nativeQuery = true)
    List<CompanyInternalEntity> getByCompanyId(@Param("companyId") long companyId);
}
