package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.TaskStatusEntity;

public interface TaskStatusRepository extends CrudRepository<TaskStatusEntity, Long> {
}
