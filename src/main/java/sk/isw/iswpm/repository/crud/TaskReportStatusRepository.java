package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.TaskReportStatusEntity;

public interface TaskReportStatusRepository extends CrudRepository<TaskReportStatusEntity, Long> {
}
