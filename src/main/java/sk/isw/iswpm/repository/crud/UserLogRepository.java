package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.UserLogEntity;

public interface UserLogRepository extends CrudRepository<UserLogEntity, Long> {
}
