package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.AdditionalPermisionLogEntity;

public interface AdditionalPermisionLogRepository extends CrudRepository<AdditionalPermisionLogEntity, Long> {
}
