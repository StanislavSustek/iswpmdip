package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.CompanyEntity;

import java.util.List;

public interface CompanyRepository extends CrudRepository<CompanyEntity, Long> {

    @Query(value = "select * from public.company c join company_internal ci using(company_id)", nativeQuery = true)
    public List<CompanyEntity> getInternalCompanies();

    @Query(value = "select * from public.company c join company_internal ci using(company_id)", nativeQuery = true)
    public List<CompanyEntity> getCustomersCompanies();

    @Query(value =
            "select * from public.company c " +
            "join company_internal ci using(company_id)" +
            "join company_user cu using(company_internal_id)" +
                    "where cu.user_id = :id",
            nativeQuery = true)
    public List<CompanyEntity> getInternalCompaniesForUser(@Param("id") long id);


}
