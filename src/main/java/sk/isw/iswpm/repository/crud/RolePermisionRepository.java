package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.RolePermisionEntity;

import java.util.List;

public interface RolePermisionRepository extends CrudRepository<RolePermisionEntity, Long> {
    @Query(value = "delete from role_permision rp where rp.role_id=:roleId and rp.permision_id=:permisionId", nativeQuery = true)
    public RolePermisionEntity deleteByRoleIdAndPermisionId(@Param("roleId") Long roleId, @Param("permisionId") Long permisionId);
}
