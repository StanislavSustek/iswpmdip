package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.HolidayUserRestrictionEntity;

public interface HolidayUserRestrictionRepository extends CrudRepository<HolidayUserRestrictionEntity, Long> {
}
