package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.controllerEntity.RoleWithPermisions;
import sk.isw.iswpm.entity.RoleEntity;

import java.util.List;

public interface RoleRepository extends CrudRepository<RoleEntity, Long> {

    @Query(value = "select * from role join user_role ur on role.role_id = ur.role where user_id=:userId", nativeQuery = true)
    public List<RoleEntity> getUserRoles(@Param("userId") long userId);

    @Query(value = "select new sk.isw.iswpm.controllerEntity.RoleWithPermisions(r, p) from RoleEntity r " +
            "left join RolePermisionEntity pr on r.roleId = pr.roleId " +
            "left join PermisionEntity p on pr.permisionId= p.permisionId")
    public List<RoleWithPermisions> getRolesWithPersmisions();

    @Query(value = "select new sk.isw.iswpm.controllerEntity.RoleWithPermisions(r, p) from RoleEntity r " +
            "join CompanyInternalEntity ci on ci.companyInternalId=r.companyInternalId " +
            "left join RolePermisionEntity pr on r.roleId = pr.roleId " +
            "left join PermisionEntity p on pr.permisionId= p.permisionId " +
            "where ci.companyId=:companyId")
    public List<RoleWithPermisions> getRolesWithPersmisionsByCompanyId(@Param("companyId") Long companyId);

    @Query(value = "select r from RoleEntity r join CompanyInternalEntity ci on r.companyInternalId = ci.companyInternalId where ci.companyId=:companyId")
    public List<RoleEntity> getCompanyRoles(@Param("companyId") long companyId);
}
