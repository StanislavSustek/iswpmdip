package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.PermisionEntity;

import java.util.List;

public interface PermisionRepository extends CrudRepository<PermisionEntity, Long> {
    @Query(value = "select * from permision join role_permision rp on permision.permision_id = rp.permision_id where role_id=:roleId", nativeQuery = true)
    public List<PermisionEntity> getRolePermisions(@Param("roleId") long roleId);
}
