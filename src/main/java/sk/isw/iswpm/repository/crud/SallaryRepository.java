package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.SallaryEntity;

public interface SallaryRepository extends CrudRepository<SallaryEntity, Long> {
}
