package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyInternalLogEntity;

public interface CompanyInternalLogRepository extends CrudRepository<CompanyInternalLogEntity, Long> {
}
