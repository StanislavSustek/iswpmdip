package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.SavedSallaryEntity;

public interface SavedSallaryRepository extends CrudRepository<SavedSallaryEntity, Long> {
}
