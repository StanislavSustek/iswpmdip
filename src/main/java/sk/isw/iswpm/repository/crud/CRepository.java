package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.Company;

public interface CRepository extends CrudRepository<Company, Long> {
}
