package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerContactStatusEntity;

public interface CompanyCustomerContactStatusRepository extends CrudRepository<CompanyCustomerContactStatusEntity, Long> {
}
