package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.entity.UserEntity;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserRespository extends CrudRepository<UserEntity, Long> {

    @Query(value = "SELECT * from public.user e where e.login =:name", nativeQuery = true)       // using @query
     List<UserEntity> findByUserName(@Param("name") String name);

    @Query(value = "SELECT * from public.user e where not exists (select 'x' from company_user cu join company_internal ci on cu.company_internal_id = ci.company_internal_id where cu.user_id= e.user_id and ci.company_id =:companyId)", nativeQuery = true)       // using @query
    List<UserEntity> getUsersForCompanyAdd(@Param("companyId") long companyId);

    @Query(value = "SELECT * from public.user e where exists (select 'x' from company_user cu join company_internal ci on cu.company_internal_id = ci.company_internal_id where cu.user_id= e.user_id and ci.company_id =:companyId)", nativeQuery = true)       // using @query
    List<UserEntity> getCompanyUsers(@Param("companyId") long companyId);

    @Query(value = "SELECT * from public.user e join project_user u on e.user_id = u.user_id where u.project_id=:projectId", nativeQuery = true)       // using @query
    List<UserEntity> getProjectUsers(@Param("projectId") long projectId);


}
