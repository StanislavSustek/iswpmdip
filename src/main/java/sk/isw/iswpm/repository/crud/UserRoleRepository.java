package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.UserRoleEntity;

import java.util.List;

public interface UserRoleRepository extends CrudRepository<UserRoleEntity, Long> {
    @Query(value = "select * from user_role where user_id=:userId", nativeQuery = true)
    public List<UserRoleEntity> getRoleByUserId(@Param("userId") long userId);
}
