package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectBudgetEntity;

public interface ProjectBudgetRepository extends CrudRepository<ProjectBudgetEntity, Long> {
}
