package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.UserStatusEntity;

public interface UserStatusRepository extends CrudRepository<UserStatusEntity, Long> {
}
