package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.PasswordResetTokenEntity;

import java.util.List;

public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetTokenEntity, Long> {
    @Query(value = "select * from password_reset_token where token=:token and current_timestamp < expired and used=false", nativeQuery = true)
    public List<PasswordResetTokenEntity> getToken(@Param("token") String token);
}
