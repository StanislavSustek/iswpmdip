package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.EmailNotificationEntity;

public interface EmailNotificationRepository extends CrudRepository<EmailNotificationEntity, Long> {
}
