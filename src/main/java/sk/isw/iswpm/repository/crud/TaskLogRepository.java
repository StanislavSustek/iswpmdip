package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.TaskEntity;
import sk.isw.iswpm.entity.TaskLogEntity;

import java.util.List;

public interface TaskLogRepository extends CrudRepository<TaskLogEntity, Long> {
    @Query(value = "Select * from task_log where task_id=:taskId", nativeQuery = true)
    List<TaskLogEntity> getTaskLogByTaskId(@Param("taskId") long taskId);
}
