package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyInternalLogoEntity;

public interface CompanyInternalLogoRepository extends CrudRepository<CompanyInternalLogoEntity, Long> {
}
