package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.InvoiceLogEntity;

public interface InvoiceLogRepository extends CrudRepository<InvoiceLogEntity, Long> {
}
