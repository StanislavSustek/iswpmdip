package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyInternalStatusEntity;

public interface CompanyInternalStatusRepository extends CrudRepository<CompanyInternalStatusEntity, Long> {
}
