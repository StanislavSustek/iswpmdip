package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.SallaryHistoryEntity;

public interface SallaryHistoryRepository extends CrudRepository<SallaryHistoryEntity, Long> {
}
