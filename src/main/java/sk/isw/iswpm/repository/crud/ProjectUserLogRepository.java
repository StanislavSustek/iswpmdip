package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectUserLogEntity;

public interface ProjectUserLogRepository extends CrudRepository<ProjectUserLogEntity, Long> {
}
