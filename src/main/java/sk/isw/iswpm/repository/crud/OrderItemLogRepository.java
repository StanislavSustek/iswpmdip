package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderItemLogEntity;

public interface OrderItemLogRepository extends CrudRepository<OrderItemLogEntity, Long> {
}
