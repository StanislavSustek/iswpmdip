package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.UserCompanySkillEntity;

public interface UserCompanySkillRepository extends CrudRepository<UserCompanySkillEntity, Long> {
}
