package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.SettingEntity;

public interface SettingRepository extends CrudRepository<SettingEntity, Long> {
}
