package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.AdditionalPermisionLogEntity;
import sk.isw.iswpm.entity.TaskCommentEntity;
import sk.isw.iswpm.entity.UserMessagesEntity;

import java.util.List;

public interface TaskCommentRepository extends CrudRepository<TaskCommentEntity, Long> {
    @Query(value = "select * from task_comment um  where task_id=:taskId", nativeQuery = true)
    List<TaskCommentEntity> getCommentsByTaskId(@Param("taskId") long taskId);
}
