package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.ProjectEntity;

import java.util.List;

public interface ProjectRepository extends CrudRepository<ProjectEntity, Long> {

    @Query(value = "select * from project join company_internal ci on project.company_internal_id = ci.company_internal_id where company_id=:companyId and project_status_id<>5", nativeQuery = true)
    List<ProjectEntity> getCompanyProjects(@Param("companyId") long companyId);

    @Query(value = "select * from project join project_user ci on project.project_id = ci.project_id where user_id=:userId ", nativeQuery = true)
    List<ProjectEntity> getuserProjects(@Param("userId") long userId);

    /*@Query(value = "select * from project join company_internal ci on project.company_ichudatnternal_id = ci.company_internal_id where company_id=:comanyId", nativeQuery = true)
    public List<ProjectEntity> getUserProjectsByCompany(@Param("companyId") long companyId);*/
}
