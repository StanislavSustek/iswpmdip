package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.MessagesEntity;
import sk.isw.iswpm.entity.UserMessagesEntity;

import java.util.List;

public interface UserMessageRepository extends CrudRepository<UserMessagesEntity, Long> {
    @Query(value = "select * from user_messages um  where user_id=:userId and message_id=:messageId", nativeQuery = true)
    List<UserMessagesEntity> getMessageByUserIdAndMessageId(@Param("userId") long userId, @Param("messageId") long messageId);
}
