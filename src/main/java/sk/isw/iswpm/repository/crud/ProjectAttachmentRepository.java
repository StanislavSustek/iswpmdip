package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.controllerEntity.ProjectDocuments;
import sk.isw.iswpm.entity.CompanyInternalEntity;
import sk.isw.iswpm.entity.ProjectAttachmentEntity;

import java.util.List;

public interface ProjectAttachmentRepository extends CrudRepository<ProjectAttachmentEntity, Long> {
    @Query(value = "select pa from ProjectAttachmentEntity pa where pa.projectId=:projectId")
    List<ProjectAttachmentEntity> getByProjectId(@Param("projectId") long projectId);

    @Query(value = "select new sk.isw.iswpm.controllerEntity.ProjectDocuments(pa.filename, pa.comment, pa.projectAtachmentId) from ProjectAttachmentEntity pa where pa.projectId=:projectId")
    List<ProjectDocuments> getDocumentByProjectId(@Param("projectId") long projectId);
}
