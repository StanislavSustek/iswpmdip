package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.HolidayLogEntity;

public interface HolidayLogRepository extends CrudRepository<HolidayLogEntity, Long> {
}
