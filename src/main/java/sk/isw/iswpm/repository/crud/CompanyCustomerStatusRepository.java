package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerStatusEntity;

public interface CompanyCustomerStatusRepository extends CrudRepository<CompanyCustomerStatusEntity, Long> {
}
