package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.OrderEntity;

public interface OrderRepository extends CrudRepository<OrderEntity, Long> {
}
