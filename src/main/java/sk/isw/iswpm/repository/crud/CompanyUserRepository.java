package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyUserEntity;

public interface CompanyUserRepository extends CrudRepository<CompanyUserEntity, Long> {
}
