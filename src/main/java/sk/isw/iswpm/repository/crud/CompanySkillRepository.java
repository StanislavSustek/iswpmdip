package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanySkillEntity;

public interface CompanySkillRepository extends CrudRepository<CompanySkillEntity, Long> {
}
