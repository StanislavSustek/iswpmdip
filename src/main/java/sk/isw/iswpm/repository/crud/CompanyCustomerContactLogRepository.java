package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerContactLogEntity;

public interface CompanyCustomerContactLogRepository extends CrudRepository<CompanyCustomerContactLogEntity, Long> {
}
