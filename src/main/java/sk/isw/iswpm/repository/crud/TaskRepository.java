package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.TaskEntity;

import java.util.List;

public interface TaskRepository extends CrudRepository<TaskEntity, Long> {

    @Query(value = "Select * from task where project_id=:projectId and task_status_id not in(3,6)", nativeQuery = true)
    List<TaskEntity> getProjectTask(@Param("projectId") long projectId);

    @Query(value = "Select * from task where user_id=:userId and task_status_id not in(3,6) and project_id=:projectId", nativeQuery = true)
    List<TaskEntity> getUserTask(@Param("userId") long userId, @Param("projectId") long projectId);

    @Query("Select count(t) from TaskEntity t where t.projectId=:projectId and t.taskStatusId=5")
    Integer getResolved(@Param("projectId") long projectId);

    @Query("Select count(t) from TaskEntity t where t.projectId=:projectId and t.taskStatusId not in(2,5,6)")
    Integer getUnResolved(@Param("projectId") long projectId);

    @Query("Select count(t) from TaskEntity t where t.projectId=:projectId")
    Integer getAllCount(@Param("projectId") long projectId);
}
