package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectStatusEntity;

public interface ProjectStatusRepository extends CrudRepository<ProjectStatusEntity, Long> {
}
