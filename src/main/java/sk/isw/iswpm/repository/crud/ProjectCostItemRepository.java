package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.ProjectCostItemEntity;

public interface ProjectCostItemRepository extends CrudRepository<ProjectCostItemEntity, Long> {
}
