package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.MeasureUnitEntity;

public interface MeasureUnitRepository extends CrudRepository<MeasureUnitEntity, Long> {
}
