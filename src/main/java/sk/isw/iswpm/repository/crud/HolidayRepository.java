package sk.isw.iswpm.repository.crud;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sk.isw.iswpm.entity.HolidayEntity;

import java.util.List;

public interface HolidayRepository extends CrudRepository<HolidayEntity, Long> {
    @Query("select h from HolidayEntity h " +
            "join CompanyInternalEntity ci on h.companyInternalId=ci.companyInternalId " +
            "where ci.companyId=:companyId " +
            "and (:userIds is null or h.userId in (:userIds)) " +
            "and (:stateId is null or h.holidayStatusId=:stateId) " +
            "and (:holidayType is null or h.holidayTypeId=:holidayType)")
    List<HolidayEntity> getHolidayByFilter(@Param("holidayType") Long holidayType, @Param("userIds") List<Long> userIds,  @Param("companyId") Long companyId, @Param("stateId") Long stateId);
}
