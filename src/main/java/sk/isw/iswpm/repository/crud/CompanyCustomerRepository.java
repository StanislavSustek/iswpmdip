package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.CompanyCustomerEntity;

public interface CompanyCustomerRepository extends CrudRepository<CompanyCustomerEntity, Long> {
}
