package sk.isw.iswpm.repository.crud;

import org.springframework.data.repository.CrudRepository;
import sk.isw.iswpm.entity.InvoiceStatusEntity;

public interface InvoiceStatusRepository extends CrudRepository<InvoiceStatusEntity, Long> {
}
