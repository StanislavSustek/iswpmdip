package sk.isw.iswpm.repository.mapper;

import org.springframework.jdbc.core.RowMapper;
import sk.isw.iswpm.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;


public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(final ResultSet rs, final int i) throws SQLException {
        final User user = new User()
                .setFirstName(rs.getString("first_name"))
                .setLastName(rs.getString("last_name"))
                .setEmail(rs.getString("email"))
                .setPassword(rs.getString("password"))
                .setLogin(rs.getString("login"))
                .setComment(rs.getString("comment"))
//                .setModified(rs.getTimestamp("modified").toLocalDateTime())
                .setUserId(rs.getLong("user_id"));
//                .setCreated(rs.getTimestamp("created").toLocalDateTime())
//                .setLastLogin(rs.getTimestamp("last_login").toLocalDateTime());
        return user;
    }
}
