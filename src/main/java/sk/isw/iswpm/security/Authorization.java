package sk.isw.iswpm.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sk.isw.iswpm.controllerEntity.CacheUser;
import sk.isw.iswpm.entity.PermisionEntity;
import sk.isw.iswpm.repository.crud.PermisionRepository;
import sk.isw.iswpm.service.UserService;
import sk.isw.iswpm.service.socket.SocketService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class Authorization {
    private final UserService userService;
    private final SocketService socketService;
    private final PermisionRepository permisionRepository;


    public boolean authorize(String token, String role) {

        CacheUser cacheUser = userService.saveUserLogin(null, null, token, null);
        if (cacheUser.getId() != null) {
            List<PermisionEntity> entities = new ArrayList<>();
            cacheUser.getRoleId().forEach(r -> {
                entities.addAll(permisionRepository.getRolePermisions(r).stream().filter(permisionEntity -> permisionEntity.getName().equals(role)).collect(Collectors.toList()));
            });
            if (entities.size() > 0) {
                return true;
            } else {
                log.info("Používateľ nemá oprávnenie.");
                return false;
            }
        } else {
            log.info("Používateľ neexistuje.");
            return false;
        }
    }
}
