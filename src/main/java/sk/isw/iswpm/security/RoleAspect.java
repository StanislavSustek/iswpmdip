package sk.isw.iswpm.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.stomp.StompConversionException;
import org.springframework.web.client.HttpClientErrorException;
import sk.isw.iswpm.config.ApiException;

import javax.xml.ws.http.HTTPException;
import java.lang.reflect.Method;
import java.net.SocketException;

@Slf4j
@Aspect
@Configuration
@AllArgsConstructor
public class RoleAspect {
    private final Authorization authorization;

    @Before("execution(public * *(..)) && @annotation(HasRole)")
    public void before(JoinPoint call) throws SocketException {

        String token = (String)call.getArgs()[0];
        MethodSignature signature = (MethodSignature) call.getSignature();
        Method method = signature.getMethod();
        HasRole myAnnotation = method.getAnnotation(HasRole.class);
        String role = myAnnotation.value();

        if (!authorization.authorize(token, role)) {
            log.info("Neplatná rola: {}, method: {}", role, method.getName());
            throw new SocketException("ROLE, Neplatná rola (".concat(role).concat(")."));
        }
    }
}
