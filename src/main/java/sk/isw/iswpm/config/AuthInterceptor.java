package sk.isw.iswpm.config;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import sk.isw.iswpm.controllerEntity.CacheUser;
import sk.isw.iswpm.entity.UserEntity;
import sk.isw.iswpm.service.UserService;

import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;

@Service
@AllArgsConstructor
class AuthInterceptor implements ChannelInterceptor {

    private final UserService userService;

    @SneakyThrows
    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        StompCommand command = accessor.getCommand();

        String token = null;
        LinkedMultiValueMap<String, String> nativeHeaders = (LinkedMultiValueMap<String, String>) message.getHeaders().get("nativeHeaders");
        token = nativeHeaders != null && nativeHeaders.get("token") != null && nativeHeaders.get("token").size() > 0 ? nativeHeaders.get("token").get(0) : null;
        if (token != null) {
            CacheUser cacheUser = userService.saveUserLogin(null, null, token, null);
            if (cacheUser.getId() != null && cacheUser.getExpiration() != null) {
                if (Instant.now().compareTo(cacheUser.getExpiration()) < 0) {
                    UserEntity u = new UserEntity();
                    u.setUserId(cacheUser.getId());
                    this.userService.saveUserLogin(u, cacheUser.getRoleId(), token, Instant.now().plus(Duration.ofMinutes(30)));
                    return message;
                } else {
                    throw new SocketException("Expirovany token!");
                }
            } else {
                throw new SocketException("Neplatny token!");
            }
        } else {
            return message;
        }
    }
}
