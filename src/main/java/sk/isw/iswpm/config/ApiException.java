package sk.isw.iswpm.config;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ApiException extends RuntimeException {
    private String system;
    private String code;

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String system, String message, Throwable cause) {
        super(message, cause);

        this.system = system;
    }

    public ApiException(String system, String code, String message, Throwable cause) {
        super(message, cause);

        this.system = system;
        this.code = code;
    }

}
