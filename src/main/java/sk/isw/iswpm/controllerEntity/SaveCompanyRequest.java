package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.CompanyEntity;

@Data
@Accessors(chain = true)
public class SaveCompanyRequest {
    private CompanyEntity company;
    private Boolean activate;
    private boolean internal;
}
