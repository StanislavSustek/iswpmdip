package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.PermisionEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class AddPrivilegeToRoleRequest {
    private Long roleId;
    private Long companyId;
    private List<PermisionEntity> addPrivileges;
    private List<PermisionEntity> removePrivileges;
}
