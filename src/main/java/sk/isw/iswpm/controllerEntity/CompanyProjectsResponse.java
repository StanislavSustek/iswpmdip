package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.ProjectEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class CompanyProjectsResponse {
    private List<ProjectEntity> projectList;
    private long companyId;
}
