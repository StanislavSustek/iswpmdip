package sk.isw.iswpm.controllerEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.security.Timestamp;
import java.time.Instant;
import java.util.List;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CacheUser implements Serializable {
    private Long id;
    private String token;
    private List<Long> roleId;
    private Instant expiration;
}
