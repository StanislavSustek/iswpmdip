package sk.isw.iswpm.controllerEntity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "security.PasswordResetResponse")
@Data
@Accessors(chain = true)
public class PasswordResetResponse {
    private boolean success;
    private boolean tokenValid;
}
