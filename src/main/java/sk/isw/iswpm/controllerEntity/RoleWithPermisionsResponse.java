package sk.isw.iswpm.controllerEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.PermisionEntity;
import sk.isw.iswpm.entity.RoleEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class RoleWithPermisionsResponse {
    private RoleEntity role;
    private List<PermisionEntity> privileges;
}
