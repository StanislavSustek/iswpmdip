package sk.isw.iswpm.controllerEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.ProjectAttachmentEntity;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class ProjectDocuments {
    private String name;
    private String comment;
    private Long documentId;
}
