package sk.isw.iswpm.controllerEntity;

import lombok.Data;

@Data
public class AddUserToCompanyRequest {
    private Long companyId;
    private Long userId;
    private Long roleId;
}
