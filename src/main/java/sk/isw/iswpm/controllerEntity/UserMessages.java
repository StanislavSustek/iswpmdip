package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserMessages {
    private long messageId;
    private String header;
    private String text;
    private boolean active;
    private boolean read;
}
