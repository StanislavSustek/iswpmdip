package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MessageChannelResponse {
    private String message;
    private boolean refresh;
    private boolean response;
}
