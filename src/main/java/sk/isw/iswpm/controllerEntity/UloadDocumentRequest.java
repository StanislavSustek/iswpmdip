package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UloadDocumentRequest {
    private String name;
    private String description;
    private String extension;
    private String fileBase;
    private Long taskId;
    private Long projectId;
}
