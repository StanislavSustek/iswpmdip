package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.entity.UserEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class GetUsersForCompanyResponse {
    private Long companyId;
    private List<UserEntity> users;
}
