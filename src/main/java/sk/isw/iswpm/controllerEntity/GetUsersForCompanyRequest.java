package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.User;

@Data
@Accessors(chain = true)
public class GetUsersForCompanyRequest {
    private Long companyId;
}
