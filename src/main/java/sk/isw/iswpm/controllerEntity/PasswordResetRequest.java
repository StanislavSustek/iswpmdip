package sk.isw.iswpm.controllerEntity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "security.PasswordResetRequest")
@Data
@Accessors(chain = true)
public class PasswordResetRequest {
    private String password;
    private String repeatPassword;
    private String token;
}
