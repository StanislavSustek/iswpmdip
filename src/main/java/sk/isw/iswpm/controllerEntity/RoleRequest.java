package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RoleRequest {
    private long roleId;
    private String description;
    private String name;
    private long companyId;
    private boolean readOnly;
}
