package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class HolidayFilterRequest {
    private Long type;
    private List<Long> userIds;
    private Long companyId;
    private Long stateId;
}
