package sk.isw.iswpm.controllerEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TaskComment {
    private String commnent;
    private LocalDateTime dateTime;
    private long userId;
}
