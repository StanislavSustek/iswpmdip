package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class AddUsersIntoProjectRequest {
    private List<Integer> userIds;
    private long projectId;
}
