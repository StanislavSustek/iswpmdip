package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.ProjectEntity;

@Data
@Accessors(chain = true)
public class CreateProjectRequest {
    private ProjectEntity project;
    private long companyId;
}
