package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProjectDetailRequest {
    private long projectId;
}
