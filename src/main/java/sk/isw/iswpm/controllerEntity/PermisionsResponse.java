package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.PermisionEntity;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class PermisionsResponse {
    private List<PermisionEntity> permisions;

    public PermisionsResponse() {
        permisions = new ArrayList<>();
    }
}
