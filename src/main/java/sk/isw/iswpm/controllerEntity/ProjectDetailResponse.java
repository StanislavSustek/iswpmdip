package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ProjectDetailResponse {
    private List<ProjectDocuments> documents;
    private Integer closeTask;
    private Integer notcloseTask;
    private Integer allTask;
    private Integer afterDate;
}
