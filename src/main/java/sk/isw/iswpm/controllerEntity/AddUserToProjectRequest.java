package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.User;

import java.util.List;

@Data
@Accessors(chain = true)
public class AddUserToProjectRequest {
    private Long projectId;
    private Long companyId;
    private List<User> addUsers;
    private List<User> removeUsers;
}
