package sk.isw.iswpm.controllerEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.TaskAttachmentEntity;
import sk.isw.iswpm.entity.TaskCommentEntity;
import sk.isw.iswpm.entity.TaskEntity;
import sk.isw.iswpm.entity.TaskLogEntity;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class TaskDetail {
    private TaskEntity task;
    private List<TaskLogEntity> taskLog;
    private List<TaskCommentEntity> comments;
    private List<TaskAttachmentEntity> taskFiles;

    public TaskDetail() {
        taskLog = new ArrayList<>();
        comments = new ArrayList<>();
    }
}
