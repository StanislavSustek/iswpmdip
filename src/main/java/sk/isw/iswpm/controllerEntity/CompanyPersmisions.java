package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.CompanyEntity;
import sk.isw.iswpm.entity.RoleEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class CompanyPersmisions {
    private CompanyEntity company;
    private List<String> permisions;
    private RoleEntity role;
}
