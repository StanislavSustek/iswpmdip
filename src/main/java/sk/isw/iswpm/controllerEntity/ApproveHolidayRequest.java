package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.HolidayEntity;

@Data
@Accessors(chain = true)
public class ApproveHolidayRequest {
    private HolidayEntity holiday;
}
