package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class AddUserIntoCompanyRequest {
    private List<Integer> addedUsers;
    private List<Integer> removedUsers;
    private long companyId;
}
