package sk.isw.iswpm.controllerEntity;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.isw.iswpm.entity.CompanyEntity;
import sk.isw.iswpm.entity.RoleEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class GetCompaniesResponse {
    private CompanyEntity company;
    private List<RoleEntity> roles;
}
