package sk.isw.iswpm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.*;
import sk.isw.iswpm.entity.LoginRequest;
import sk.isw.iswpm.entity.LoginResponse;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.service.SecurityService;

import java.util.List;

@SpringBootApplication
@RestController
@EnableCaching
public class PMApplication {

    public static void main(String[] args) {
        SpringApplication.run(PMApplication.class, args);
    }
}
