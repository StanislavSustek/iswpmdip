package sk.isw.iswpm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.entity.*;
import sk.isw.iswpm.repository.crud.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CompanyManagementService {

    private final CompanyRepository companyRepository;
    private final CompanyInternalRepository companyInternalRepository;
    private final ProjectRepository projectRepository;
    private final ProjectUserRepository projectUserRepository;
    private final CompanyUserRepository companyUserRepository;
    private final UserRespository userRepository;
    private final UserService userService;
    private final MessagesService messagesService;
    private final UserRoleRepository userRoleRepository;

    public List<CompanyEntity> getCompanyForUser(UserEntity user) {
        return companyRepository.getInternalCompaniesForUser(user.getUserId());
    }

    public Boolean addUserIntoCompany(AddUserIntoCompanyRequest addUserIntoCompanyRequest) {
        //TODO dorobit remove
        for (long id : addUserIntoCompanyRequest.getAddedUsers()) {
            CompanyUserEntity companyUserEntity = new CompanyUserEntity();
            companyUserEntity.setCompanyUserId(id);
            companyUserEntity.setUserId(id);
            companyUserEntity.setCompanyInternalId(
                    companyInternalRepository.getByCompanyId(addUserIntoCompanyRequest.getCompanyId()).get(0).getCompanyInternalId()
            );
            companyUserEntity.setDeleted(false);
            companyUserEntity.setModified(new Timestamp(new Date().getTime()));
            companyUserRepository.save(companyUserEntity);
        }
        return true;
    }

    public GetUsersForCompanyResponse getUsersForCompany(GetUsersForCompanyRequest request) {
        GetUsersForCompanyResponse response = new GetUsersForCompanyResponse();
        response.setUsers(userRepository.getUsersForCompanyAdd(request.getCompanyId()));
        response.setCompanyId(request.getCompanyId());
        return response;
    }

    public GetUsersForCompanyResponse getCompanyUsers(GetUsersForCompanyRequest request) {
        GetUsersForCompanyResponse response = new GetUsersForCompanyResponse();
        response.setUsers(userRepository.getCompanyUsers(request.getCompanyId()));
        response.setCompanyId(request.getCompanyId());
        return response;
    }

    public List<ProjectEntity> getProjects(GetUsersForCompanyRequest request) {
        return projectRepository.getCompanyProjects(request.getCompanyId().longValue());
    }

    public List<ProjectEntity> getProjectsForUser(String token, Long userId) {
        if (token != null) {
            return projectRepository.getuserProjects(userService.saveUserLogin(null,null,token, null).getId());
        } else {
            return projectRepository.getuserProjects(userId);
        }
    }

    public void createProject(CreateProjectRequest request) {
        ProjectEntity projectEntity = request.getProject();
        projectEntity.setProjectStatusId(1);
        projectEntity.setCreated(new Timestamp(new Date().getTime()));
        long internalId = companyInternalRepository.getByCompanyId(request.getCompanyId()).get(0).getCompanyInternalId();
        projectEntity.setCompanyInternalId(internalId);
        long id = projectRepository.save(projectEntity).getProjectId();
        MessagesEntity message = new MessagesEntity();
        message.setActive(true);
        message.setHeader("Nový projekt");
        message.setText("Bol vytvorený nový projekt s názvom " + projectEntity.getName());
        messagesService.createMessage(message, null);
    }

    public void updateProjectInfo(CreateProjectRequest request) {
        ProjectEntity projectEntity = request.getProject();
        projectEntity.setCreated(new Timestamp(new Date().getTime()));
        long id = projectRepository.save(projectEntity).getProjectId();
        MessagesEntity message = new MessagesEntity();
        message.setActive(true);
        message.setHeader("Uprava projektu");
        message.setText("Bol upravený projekt " + projectEntity.getName());
        messagesService.createMessage(message, null);
    }

    public void deleteProject(CreateProjectRequest request) {
        ProjectEntity projectEntity = request.getProject();
        projectEntity.setCreated(new Timestamp(new Date().getTime()));
        projectEntity.setProjectStatusId(5);
        long id = projectRepository.save(projectEntity).getProjectId();
        MessagesEntity message = new MessagesEntity();
        message.setActive(true);
        message.setHeader("Zmazanie projektu");
        message.setText("Projekt " + projectEntity.getName() + " bol zmazaný");
        messagesService.createMessage(message, null);
    }

    public List<CompanyEntity> getUserCompanies(String token) {
        return companyRepository.getInternalCompaniesForUser(userService.saveUserLogin(null,null, token, null).getId());
    }

    public void addUserIntoProject(AddUsersIntoProjectRequest request) {
        request.getUserIds().forEach( integer -> {
            ProjectUserEntity pu = new ProjectUserEntity();
            pu.setProjectId(request.getProjectId());
            pu.setUserId(integer);
            projectUserRepository.save(pu);
        });
    }

    public boolean addUserToCompany(AddUserToCompanyRequest request) {
        CompanyUserEntity ce = new CompanyUserEntity();
        ce.setUserId(request.getUserId());
        ce.setCompanyInternalId(companyInternalRepository.getByCompanyId(request.getCompanyId()).get(0).getCompanyInternalId());
        ce.setDeleted(false);
        ce.setModified(new Timestamp(new Date().getTime()));
        companyUserRepository.save(ce);
        UserRoleEntity re = new UserRoleEntity();
        re.setModified(new Timestamp(new Date().getTime()));
        re.setUserId(request.getUserId());
        re.setRole(request.getRoleId());
        userRoleRepository.save(re);
        return true;
    }
}
