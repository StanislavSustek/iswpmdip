package sk.isw.iswpm.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.entity.*;
import sk.isw.iswpm.repository.crud.*;
import sk.isw.iswpm.types.CompanyType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AdminService {

    private final CompanyRepository companyRepository;
    private final CompanyInternalRepository companyInternalRepository;
    private final CompanyInternalStatusRepository companyInternalStatusRepository;
    private final PermisionRepository permisionRepository;
    private final RoleRepository roleRepository;
    private final RolePermisionRepository rolePermisionRepository;

    public List<GetCompaniesResponse> getCompaniesCompanies(CompanyType type) {
        List<GetCompaniesResponse> response = new ArrayList<>();
        List<CompanyEntity> companies = new ArrayList<>();
        switch (type) {
            case ALL: case INTERNAL:
                this.companyRepository.findAll().forEach(companies::add);
                response = companies.stream().map(companyEntity -> new GetCompaniesResponse()
                        .setCompany(companyEntity)
                        .setRoles(roleRepository.getCompanyRoles(companyEntity.getCompanyId()))
                ).collect(Collectors.toList());
                break;
            case CUSTOMER:
                companies =this.companyRepository.getCustomersCompanies();
                response = companies.stream().map(x -> new GetCompaniesResponse().setCompany(x)).collect(Collectors.toList());
                break;
        }

        return response;
    }

    public boolean createCompany(CompanyEntity companyEntity) {
        if (companyEntity.getCompanyId() != 0) {
            CompanyEntity c = companyRepository.findById(companyEntity.getCompanyId()).get();
            if (!c.equals(companyEntity)) {
                companyEntity.setModified(new Timestamp(new Date().getTime()));
                this.companyRepository.save(companyEntity);
                return true;
            }
            return false;
        } else {
            long companyId = this.companyRepository.save(companyEntity).getCompanyId();
            CompanyInternalStatusEntity cisE = companyInternalStatusRepository.findById(new Long("1").longValue()).get();
            CompanyInternalEntity internalEntity = new CompanyInternalEntity();
            internalEntity.setModified(new Timestamp(new Date().getTime()));
            internalEntity.setCompanyInternalStatusId(cisE.getCompanyInternalStatusId());
            internalEntity.setCompanyId(companyId);
            Long internalId = companyInternalRepository.save(internalEntity).getCompanyInternalId();
            RoleEntity roleEntity = new RoleEntity();
            roleEntity.setName("COMPANY_ADMIN");
            roleEntity.setDescription("Administrátor spoločnosti" + companyEntity.getCompanyName());
            roleEntity.setReadOnly(true);
            roleEntity.setCompanyInternalId(internalId);
            roleRepository.save(roleEntity);
            return true;
        }
    }

    public void deleteCompany(CompanyEntity companyEntity) {
        this.companyRepository.deleteById(companyEntity.getCompanyId());
    }

    public PermisionsResponse getAllPermisions() {
        PermisionsResponse response = new PermisionsResponse();
        permisionRepository.findAll().forEach(response.getPermisions()::add);
        return response;
    }

    public List<RoleWithPermisionsResponse> getRolesWithPermissions(GetRoleWithPrivilegesRequest request) {
        List<RoleWithPermisionsResponse> response = new ArrayList<>();
        List<RoleWithPermisions> rp;
        if (request.getCompanyId() != null) {
            rp = roleRepository.getRolesWithPersmisionsByCompanyId(request.getCompanyId());
        }  else {
            rp = roleRepository.getRolesWithPersmisions();
        }
            Map<String, List<RoleWithPermisions>> map = rp.stream().collect(Collectors.groupingBy(w -> w.getRole().getName()));
            map.values().stream().forEach(x -> response.add(new RoleWithPermisionsResponse().setRole(x.get(0).getRole()).setPrivileges(x.stream().map(r -> r.getPrivilegeName()).collect(Collectors.toList()).stream().filter(z -> z != null).collect(Collectors.toList()))));
        return response;
    }

    public List<RoleWithPermisionsResponse> addPrivilegeToRole(AddPrivilegeToRoleRequest request) {
        request.getAddPrivileges().forEach(add -> {
            RolePermisionEntity rp = new RolePermisionEntity();
            rp.setModified(new Timestamp(new Date().getTime()));
            rp.setPermisionId(add.getPermisionId());
            rp.setRoleId(request.getRoleId());
            rolePermisionRepository.save(rp);
        });
        request.getRemovePrivileges().forEach(remove -> {
            rolePermisionRepository.deleteByRoleIdAndPermisionId(request.getRoleId(), remove.getPermisionId());
        });

        return this.getRolesWithPermissions(new GetRoleWithPrivilegesRequest().setCompanyId(request.getCompanyId()));
    }

    public boolean createRole(RoleRequest request) {
        RoleEntity entity = new RoleEntity();
        entity.setCompanyInternalId(companyInternalRepository.getByCompanyId(request.getCompanyId()).get(0).getCompanyInternalId());
        entity.setReadOnly(false);
        entity.setDescription(request.getDescription());
        entity.setName(request.getName());
        roleRepository.save(entity);
        return true;
    }

}
