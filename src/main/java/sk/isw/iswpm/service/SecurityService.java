package sk.isw.iswpm.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.CacheUser;
import sk.isw.iswpm.controllerEntity.CompanyPersmisions;
import sk.isw.iswpm.controllerEntity.PasswordResetRequest;
import sk.isw.iswpm.controllerEntity.PasswordResetResponse;
import sk.isw.iswpm.entity.*;
import sk.isw.iswpm.repository.crud.*;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityService {

    private final UserRespository userRespository;
    private final UserService userService;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final PermisionRepository permisionRepository;
    private final EmailService emailService;
    private final PasswordResetTokenRepository passwordResetTokenRepository;
    private final CompanyInternalRepository companyInternalRepository;
    private final CompanyRepository companyRepository;

    public List<UserEntity> getAllUsers() {
        List<UserEntity> u = new ArrayList<>();
        userRespository.findAll().forEach(u::add);
        return u;
    }

    public LoginResponse authentificate(LoginRequest loginRequest) {
        LoginResponse response = new LoginResponse();
        try {
            List<UserEntity> users = userRespository.findByUserName(loginRequest.getLogin());
            if (users.size() != 0) {
                UserEntity user = users.get(0);
                if (user != null && user.getPassword().equals(toHexString(getSHA(loginRequest.getPassword())))) {
                    String token = UUID.randomUUID().toString().replaceAll("-", "");
                    List<RoleEntity> role = this.roleRepository.getUserRoles(user.getUserId());
                    userService.saveUserLogin(user, role.stream().map(x -> x.getRoleId()).collect(Collectors.toList()), token, Instant.now().plus(Duration.ofMinutes(30)));
                    response.setSuccess(true);
                    response.setTokken(token);
                    return response;
                } else {
                    response.setSuccess(false);
                }
            } else {
                response.setSuccess(false);
            }
        } catch (Exception e) {
            log.error("Nepodarilo sa prihlásiť užívateľa");

        }
        return response;
    }

    public byte[] getSHA(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public String toHexString(byte[] hash) {
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

    public CacheUser getUserFromCache(String token) {
        return null;
    }

    public CacheUser isValidUser(String token) {
        CacheUser u = userService.saveUserLogin(null, null, token, null);
        return u;
    }

    public void logout(String token) {
        userService.deleteUserFromCahe(token);
    }

    public UserEntity getUser(String token) {
        CacheUser cacheUser = userService.saveUserLogin(null, null, token, null);
        if (cacheUser != null) {
            return userRespository.findById(cacheUser.getId()).get();
        } else {
            return null;
        }
    }

    public List<CompanyPersmisions> getPermisions(String token) {
        List<CompanyPersmisions> result = new ArrayList<>();
        CacheUser u = userService.saveUserLogin(null, null, token, null);
        List<CompanyEntity> comp = companyRepository.getInternalCompaniesForUser(u.getId());
        List<RoleEntity> roles = roleRepository.getUserRoles(u.getId());
        comp.forEach(companyEntity -> {
            CompanyPersmisions c = new CompanyPersmisions();
            c.setCompany(companyEntity);
            if (roles.size() > 0) {
                c.setRole(roles.stream().filter(roleEntity -> roleEntity.getCompanyInternalId() == companyInternalRepository.getByCompanyId(companyEntity.getCompanyId()).get(0).getCompanyInternalId()).findFirst().get());
                if (c.getRole().getName().equals("COMPANY_ADMIN")) {
                    c.setPermisions(permisionRepository.getRolePermisions(0).stream().map(permisionEntity -> permisionEntity.getName()).collect(Collectors.toList()));
                } else {
                    c.setPermisions(permisionRepository.getRolePermisions(c.getRole().getRoleId()).stream().map(permisionEntity -> permisionEntity.getName()).collect(Collectors.toList()));
                }
            }
            result.add(c);
        });
//        RoleEntity r = roleRepository.getUserRoles(u.getId()).get(0);
        /*List<PermisionEntity> permisionEntityList = permisionRepository.getRolePermisions(r.getRoleId());
        permisionEntityList.stream().map(x -> x.getName()).collect(Collectors.toList());*/
        return result;
    }

    public boolean createUser(User request) {
        UserEntity userEntity = request.getUserId() != null && request.getUserId() != 0 ? userRespository.findById(request.getUserId()).get() : new UserEntity();
        userEntity.setCreated(new Timestamp(new Date().getTime()));
        userEntity.setEmail(request.getEmail());
        userEntity.setFirstName(request.getFirstName());
        userEntity.setLastName(request.getLastName());
        userEntity.setModified(new Timestamp(new Date().getTime()));
        userEntity.setLogin(request.getLogin());
        UserRoleEntity entity;
        if (request.getUserId() != null) {
             List<UserRoleEntity> roleEntities = userRoleRepository.getRoleByUserId(request.getUserId());
            userEntity.setUserStatusId(request.getUserStatusId());
            entity = roleEntities.size() > 0 ? roleEntities.get(0) : new UserRoleEntity();
//            entity.setRole(request.getRoleId());
            entity.setUserId(request.getUserId());
            entity.setModified(new Timestamp(new Date().getTime()));
            userRespository.save(userEntity);
            userRoleRepository.save(entity);

        } else {
            entity = new UserRoleEntity();
            entity.setRole(request.getRoleId());
            entity.setUserId(request.getUserId());
            entity.setModified(new Timestamp(new Date().getTime()));
            userEntity.setUserStatusId(4);
            UserEntity u = userRespository.save(userEntity);
            userRoleRepository.save(entity);


            PasswordResetTokenEntity tokenEntity = new PasswordResetTokenEntity();
            tokenEntity.setCreated(new Timestamp(new Date().getTime()));
            tokenEntity.setExpired(new Timestamp(new Date(System.currentTimeMillis() + 30 * 60 * 1000).getTime()));
            tokenEntity.setUsed(false);
            tokenEntity.setUserId(u.getUserId());

            String token = UUID.randomUUID().toString().replaceAll("-", "");
            tokenEntity.setToken(token);
            passwordResetTokenRepository.save(tokenEntity);
            try {
                String link = "http://localhost:4200/password-reset?token=" + token;
                String email = "Dobrý deň bol vám vytvorený účet v aplikácií ISWPM. <br> Prihlasovacie meno: <b>" + request.getLogin() + "</b><br> Kliknutím na odkaz budete presmerovaný do aplikácie kde si nastavíte prvé heslo: <a href=\"" + link + "\"><b>ODKAZ</b></a>";
                emailService.sendEmail(request.getEmail(), "Vytvorenie konta v aplikácií ISWPM", email);
            } catch (MailException e) {
                log.error("Nepodarilo sa odoslat email", e);
            }
        }
        return true;
    }

    public PasswordResetResponse resetPassword(PasswordResetRequest request) {
        PasswordResetResponse response = new PasswordResetResponse();
        try {
            List<PasswordResetTokenEntity> tokenEntity = passwordResetTokenRepository.getToken(request.getToken());
            if (tokenEntity.size() > 0) {
                log.info("Platný token pre resetovanie hesla");
                PasswordResetTokenEntity t = tokenEntity.get(0);
                t.setUsed(true);
                passwordResetTokenRepository.save(t);

                UserEntity userEntity = userRespository.findById(t.getUserId()).get();
                userEntity.setPassword(toHexString(getSHA(request.getPassword())));
                userEntity.setUserStatusId(1);
                userRespository.save(userEntity);
                response.setSuccess(true);
                response.setTokenValid(true);
                log.info("Resetovanie hesla bolo úspešné.");
                return response;
            }
            log.info("Neplatný token pre resetovanie hesla");
            response.setTokenValid(false);
            response.setSuccess(false);
            return response;
        } catch (Exception e) {
            log.info("Nepodarilo sa zmeniť heslo.");
        }
        return response;
    }

}
