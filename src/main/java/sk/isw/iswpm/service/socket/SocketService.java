package sk.isw.iswpm.service.socket;

import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class SocketService {

    private final SimpMessagingTemplate template;
    private final UserService userService;

    public void sendToCurrenctUser(String token, String path, Object o) {
        String userId = String.valueOf(userService.saveUserLogin(null,null,token, null).getId());
        template.convertAndSendToUser(userId, path, o);
    }

    public void sendToCustomUser(long userId, String path, Object o) {
        template.convertAndSendToUser(String.valueOf(userId), path, o);
    }

    public void sendToObject(long objectId, String prefix, String path, Object o) {
        template.convertAndSend("/topic/" + prefix + "/" + objectId + "/" + path, o);
    }

    /*Odoslanie spravy pre odhlasenie uzivatela a zatvorenie socketoveho spojenia*/
    public void invalidateMessage(String token) {
        template.convertAndSend("/invalidate/" + token , true);
    }

    public void sendForAll(String path, Object o) {
        template.convertAndSend("/topic/" + path, o);
    }
}
