package sk.isw.iswpm.service.socket;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UserChannel {
    private String userId;
    private List<String> activeChannels;
}
