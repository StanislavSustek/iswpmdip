package sk.isw.iswpm.service;

import lombok.AllArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.*;
import sk.isw.iswpm.entity.*;
import sk.isw.iswpm.repository.crud.*;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final TaskLogRepository taskLogRepository;
    private final UserRespository userRespository;
    private final TaskCommentRepository taskCommentRepository;
    private final TaskAttachmentRepository taskAttachmentRepository;
    private final ProjectAttachmentRepository projectAttachmentRepository;
    private final UserService userService;

    public List<TaskEntity> getProjectTasks(GetProjectTaskRequest request) {
        return taskRepository.getProjectTask(request.getProjectId());
    }

    public List<TaskEntity> getUserTasks(String token, long projectId, long userId) {
        if (token != null) {
            CacheUser u = userService.saveUserLogin(null, null, token, null);
            return taskRepository.getUserTask(u.getId(), projectId);
        } else {
            return taskRepository.getUserTask(userId, projectId);
        }
    }

    public TaskEntity updateTask(TaskEntity request,String token) {
        CacheUser u = userService.saveUserLogin(null, null, token, null);
        if (request.getTaskId() != 0) {
            TaskEntity taskEntity = taskRepository.findById(request.getTaskId()).get();
            TaskLogEntity logEntity = new TaskLogEntity();
            logEntity.setComment(createComment(taskEntity, request));
            logEntity.setDateFrom(taskEntity.getDateFrom());
            logEntity.setDateTo(taskEntity.getDateTo());
            logEntity.setHoursAssigned(taskEntity.getHoursAssigned());
            logEntity.setHoursReported(taskEntity.getHoursReported());
            logEntity.setModified(new Timestamp(new Date().getTime()));
            logEntity.setModifiedUserId(u.getId());
            logEntity.setName(taskEntity.getName());
            logEntity.setProgress(taskEntity.getProgress());
            logEntity.setPriority(taskEntity.getPriority());
            logEntity.setTaskId(taskEntity.getTaskId());
            logEntity.setUserId(taskEntity.getUserId());
            taskLogRepository.save(logEntity);
        }
        return taskRepository.save(request);
    }

    private String createComment(TaskEntity oldValue, TaskEntity newValue) {
        String comment = "";
        if (oldValue.getUserId() != newValue.getUserId()) {
            List<UserEntity> userEntities = new ArrayList<>();
            userRespository.findAll().forEach(userEntities::add);
            UserEntity oldUser = userEntities.stream().filter(x -> x.getUserId() == oldValue.getUserId()).findFirst().get();
            UserEntity newUser = userEntities.stream().filter(x -> x.getUserId() == newValue.getUserId()).findFirst().get();
            comment += "Úloha bola preradená z používateľa <b>"+ oldUser.getFirstName() + " " + oldUser.getLastName() + "</b> na používateľa <b>" + newUser.getFirstName() + " " + newUser.getLastName() + "</b>.<br>" ;
        }

        if (!oldValue.getName().equals(newValue.getName())) {
            comment += "Názov úlohy bol zmenený z <b>"+ oldValue.getName() + "</b> na <b>" + newValue.getName() + "</b>.<br>" ;
        }

        if (!oldValue.getDescription().equals(newValue.getDescription())) {
            comment += "Popis úlohy bol zmenený.<br>" ;
        }

        if (!oldValue.getPriority().equals(newValue.getPriority())) {
            comment += "Priorita úlohy bola zmenená z <b>"+ oldValue.getPriority() + "</b> na <b>" + newValue.getPriority() + "</b>.<br>" ;
        }

        if (oldValue.getProgress() != newValue.getProgress()) {
            comment += "Postup úlohy bol zmenená z <b>"+ oldValue.getProgress() + "%</b> na <b>" + newValue.getProgress() + "%</b>.<br>" ;
        }

        // TODO dokoncit pre ostatne pripady

        return comment;
    }

    public TaskDetail getTaskDetail(TaskDetailRequest request) {
        TaskDetail response = new TaskDetail();
        response.setTask(taskRepository.findById(request.getTaskId()).get());
        response.setTaskLog(taskLogRepository.getTaskLogByTaskId(response.getTask().getTaskId()));
        response.setComments(taskCommentRepository.getCommentsByTaskId(request.getTaskId()));
        response.setTaskFiles(taskAttachmentRepository.getTaskAttachement(request.getTaskId()));
        return response;
    }

    public boolean sendComment(SendCommentRequest request, String token) {
        TaskCommentEntity te = new TaskCommentEntity();
        te.setComment(request.getComment());
        te.setTaskId(request.getTaskId());
        te.setUserId(userService.saveUserLogin(null, null, token, null).getId());
        taskCommentRepository.save(te);
        return true;
    }

    public ProjectAttachmentEntity getProjectDocument(GetProjectDocumentRequest request) {
        return projectAttachmentRepository.findById(request.getDocumentId()).get();
    }

    public void uloadProjectDocument(UloadDocumentRequest request, String token) {
        byte[] file = null;

        try {
            file = Base64.decodeBase64(request.getFileBase());
            ProjectAttachmentEntity attachmentEntity = new ProjectAttachmentEntity();
            attachmentEntity.setActive(true);
            attachmentEntity.setFile(file);
            attachmentEntity.setPath("s");
            attachmentEntity.setComment(request.getDescription());
            attachmentEntity.setExtension(request.getExtension());
            attachmentEntity.setFilename(request.getName());
            attachmentEntity.setProjectId(request.getProjectId());
            projectAttachmentRepository.save(attachmentEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uloadDocument(UloadDocumentRequest request, String token) {
        byte[] file = null;

        try {
            file = Base64.decodeBase64(request.getFileBase());
            TaskAttachmentEntity attachmentEntity = new TaskAttachmentEntity();
            attachmentEntity.setActive(true);
            attachmentEntity.setCreatedate(new Timestamp(new Date().getTime()));
            attachmentEntity.setFile(file);
            attachmentEntity.setPath("s");
            attachmentEntity.setExtension(request.getExtension());
            attachmentEntity.setFilename(request.getName());
            attachmentEntity.setTaskId(request.getTaskId());
            taskAttachmentRepository.save(attachmentEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
