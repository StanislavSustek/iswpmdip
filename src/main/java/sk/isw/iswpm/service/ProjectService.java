package sk.isw.iswpm.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.AddUserToProjectRequest;
import sk.isw.iswpm.controllerEntity.ProjectDetailRequest;
import sk.isw.iswpm.controllerEntity.ProjectDetailResponse;
import sk.isw.iswpm.entity.ProjectAttachmentEntity;
import sk.isw.iswpm.entity.ProjectUserEntity;
import sk.isw.iswpm.entity.UserEntity;
import sk.isw.iswpm.repository.crud.ProjectAttachmentRepository;
import sk.isw.iswpm.repository.crud.ProjectUserRepository;
import sk.isw.iswpm.repository.crud.TaskRepository;
import sk.isw.iswpm.repository.crud.UserRespository;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjectService {

    private final UserRespository userRespository;
    private final ProjectUserRepository projectUserRepository;
    private final ProjectAttachmentRepository projectAttachmentRepository;
    private final TaskRepository taskRepository;

    public List<UserEntity> getProjectUsers(long projectId) {
        return userRespository.getProjectUsers(projectId);
    }

    public boolean addUserToProject(AddUserToProjectRequest request) {
        request.getAddUsers().forEach(x -> {
            ProjectUserEntity p = new ProjectUserEntity();
            p.setProjectId(request.getProjectId());
            p.setUserId(x.getUserId());
            projectUserRepository.save(p);
        });
        request.getRemoveUsers().forEach(x -> {
            projectUserRepository.deleteUserFromProject(x.getUserId(), request.getProjectId());
        });

        return true;
    }

    public ProjectDetailResponse getProjectDetail(ProjectDetailRequest request) {
        ProjectDetailResponse response = new ProjectDetailResponse();
        response.setAfterDate(0);
        response.setAllTask(taskRepository.getAllCount(request.getProjectId()));
        response.setCloseTask(taskRepository.getResolved(request.getProjectId()));
        response.setNotcloseTask(taskRepository.getUnResolved(request.getProjectId()));
        response.setDocuments(projectAttachmentRepository.getDocumentByProjectId(request.getProjectId()));
        return response;
    }
}
