package sk.isw.iswpm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.UserMessageChangeRequest;
import sk.isw.iswpm.controllerEntity.UserMessages;
import sk.isw.iswpm.entity.MessagesEntity;
import sk.isw.iswpm.entity.UserMessagesEntity;
import sk.isw.iswpm.repository.crud.MessagesRepository;
import sk.isw.iswpm.repository.crud.UserMessageRepository;
import sk.isw.iswpm.repository.crud.UserRespository;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MessagesService {

    private final MessagesRepository messagesRepository;
    private final UserMessageRepository userMessageRepository;
    private final UserService userService;
    private final UserRespository userRepository;

    public List<MessagesEntity> getUserMessagees(String token) {
        List<UserMessages> result = new LinkedList<>();
        List<MessagesEntity> messages = messagesRepository.getUserMessages(userService.saveUserLogin(null, null, token, null).getId());
        return messages;
    }

    public Boolean markAsRead(String token, UserMessageChangeRequest request) {
        UserMessagesEntity um = userMessageRepository.getMessageByUserIdAndMessageId(userService.saveUserLogin(null, null, token, null).getId(), request.getMessageId()).get(0);
        um.setRead(true);
        userMessageRepository.save(um);
        return true;
    }

    public Boolean deleteUserMessage(String token, UserMessageChangeRequest request) {
        UserMessagesEntity um = userMessageRepository.getMessageByUserIdAndMessageId(userService.saveUserLogin(null, null, token, null).getId(), request.getMessageId()).get(0);
        userMessageRepository.delete(um);
        return true;
    }

    public Boolean createMessage(MessagesEntity messagesEntity, Long userId) {
        long id = messagesRepository.save(messagesEntity).getMessageId();
        if (userId == null) {
            userRepository.findAll().forEach(userEntity -> {
                UserMessagesEntity u = new UserMessagesEntity();
                u.setMessageId(id);
                u.setUserId(userEntity.getUserId());
                userMessageRepository.save(u);
            });
            return true;
        } else {
            UserMessagesEntity u = new UserMessagesEntity();
            u.setMessageId(id);
            u.setUserId(userId);
            userMessageRepository.save(u);
            return true;
        }
    }


}
