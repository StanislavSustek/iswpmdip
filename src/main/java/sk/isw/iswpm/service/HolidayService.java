package sk.isw.iswpm.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.AddUserToProjectRequest;
import sk.isw.iswpm.controllerEntity.CreateHolidayRequest;
import sk.isw.iswpm.controllerEntity.HolidayFilterRequest;
import sk.isw.iswpm.entity.*;
import sk.isw.iswpm.repository.crud.CompanyInternalRepository;
import sk.isw.iswpm.repository.crud.HolidayRepository;
import sk.isw.iswpm.repository.crud.ProjectUserRepository;
import sk.isw.iswpm.repository.crud.UserRespository;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@AllArgsConstructor
public class HolidayService {

    private final UserRespository userRespository;
    private final HolidayRepository holidayRepository;
    private final CompanyInternalRepository companyInternalRepository;
    private final MessagesService messagesService;

    public List<HolidayEntity> getHolidays(HolidayFilterRequest request) {
        return holidayRepository.getHolidayByFilter(request.getType(), request.getUserIds(), request.getCompanyId(), request.getStateId());
    }

    public boolean createHoliday(CreateHolidayRequest request) {
        if (request.getHoliday().getHolidayStatusId() != 2 && request.getHoliday().getHolidayStatusId() != 3) {
            CompanyInternalEntity ce = companyInternalRepository.getByCompanyId(request.getHoliday().getCompanyInternalId()).get(0);
            request.getHoliday().setCompanyInternalId(ce.getCompanyInternalId());
        }
        holidayRepository.save(request.getHoliday());
        if (request.getHoliday().getHolidayStatusId() == 2) {
            MessagesEntity m = new MessagesEntity();
            m.setHeader("Schválenie pracovného voľna");
            m.setText("Pracovné voľno od " +
                    request.getHoliday().getFrom().toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) +
                    " do " + request.getHoliday().getTo().toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " je schválené");
            m.setActive(true);
            messagesService.createMessage(m, request.getHoliday().getUserId());
        }
        if (request.getHoliday().getHolidayStatusId() == 3) {
            MessagesEntity m = new MessagesEntity();
            m.setHeader("Zamietnutie pracovného voľna");
            m.setText("Pracovné voľno od " +
                    request.getHoliday().getFrom().toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) +
                    " do " + request.getHoliday().getTo().toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " je zamietnuté");
            m.setActive(true);
            messagesService.createMessage(m, request.getHoliday().getUserId());
        }
        return true;
    }

}
