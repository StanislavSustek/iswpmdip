package sk.isw.iswpm.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import sk.isw.iswpm.controllerEntity.CacheUser;
import sk.isw.iswpm.entity.User;
import sk.isw.iswpm.entity.UserEntity;

import java.time.Instant;
import java.util.List;

import static java.util.Optional.ofNullable;

@CacheConfig(cacheNames={"users"})
@Service
public class UserService {

    @Cacheable(value = "users", key = "#token")
    public CacheUser saveUserLogin(UserEntity user, List<Long> roleId, String token, Instant time) {
        return ofNullable(user).map(u -> new CacheUser().setId(u.getUserId()).setRoleId(roleId).setToken(token).setExpiration(time))
                .orElseGet(() -> new CacheUser().setToken(token));
    }

    @CacheEvict(value="users", key="#token")
    public void deleteUserFromCahe(String token) {
    }
}
