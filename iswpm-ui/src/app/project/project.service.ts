import { Injectable } from '@angular/core';
import { Project } from '../shared/backend/entity/project';
import { User } from '../shared/backend/entity/user';
import { Task } from '../shared/backend/entity/task';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { TaskDetail } from '../shared/backend/entity/task-detail';
import { UloadDocumentRequest } from '../shared/backend/entity/uload-document-request';
import { AddUserToProjectRequest } from '../shared/backend/entity/add-user-to-project-request';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  openedProject: Project = null;
  isUser = false;
  table = true;

  users = new BehaviorSubject<User[]>(null);
  tasks = new BehaviorSubject<Task[]>(null);
  tasksDetail = new BehaviorSubject<TaskDetail>(null);
  userTask = new BehaviorSubject<Task[]>(null);
  editTask: Task = null;
  projectDetail =  new BehaviorSubject<any>(null);


  constructor(
    private socketService: PmSocket2Service
  ) {
  }

  initProjectUsersChannel() {
    if (!this.users.getValue()) {
      this.socketService.stream(`project/${this.openedProject.projectId}/users`).subscribe(val => {
        this.users.next(val);
      });
      this.refreshUsers();
    }
  }

  initProjectTaskChannel() {
    if (!this.tasks.getValue()) {
      this.socketService.stream(`project/${this.openedProject.projectId}/task`).subscribe(val => {
        this.tasks.next(val);
      });
    }
    this.refreshTasks();
  }

  initUserProjectTaskChannel() {
    if (!this.tasks.getValue()) {
      this.socketService.stream(`/project/task`, true).subscribe(val => {
        this.userTask.next(val);
      });
    }
    this.refreshUserTasks();
  }

  refreshTasks() {
    this.socketService.send('/projects/getTasks', {projectId: this.openedProject.projectId});
  }

  refreshUserTasks() {
    this.socketService.send('/projects/getUserTasks', {projectId: this.openedProject.projectId});
  }

  refreshUsers() {
    this.socketService.send('/projects/getUsers', {projectId: this.openedProject.projectId});
  }

  updateTask(task: Task) {
    this.socketService.send('/projects/updateTask', task);
  }

  initTaskDetailChannel() {
    this.socketService.stream(`project/task/${this.editTask.taskId}/detail`).subscribe(val => {
      this.tasksDetail.next(val);
    });
    this.socketService.send(`/projects/getTaskDetail`, {taskId: this.editTask.taskId})
  }

  initProjectDetailChannel() {
    this.socketService.stream(`project/${this.openedProject.projectId}/detail`).subscribe(val => {
      this.projectDetail.next(val);
    });
    this.socketService.send(`/projects/getProjectDetail`, {projectId: this.openedProject.projectId})
  }

  translateUser(id) {
    const users = this.users.getValue();
    if (users && users.length > 0) {
      const user = users.find(x => x.userId === id);
      if (user) {
        return `${user.firstName} ${user.lastName}`;
      } else {
        return `${id}`;
      }
    }

    return id;
  }

  sendComment(taskComment) {
    console.log('Send_COMMENT', taskComment);
    this.socketService.send('/projects/sendComment', taskComment);
  }

  uloadDocument(request: UloadDocumentRequest) {
    console.log('UPLOAD_DOCUMENT', request);
    this.socketService.send('/projects/uloadTaskDocument', request);
  }

  addUserToProject(request: AddUserToProjectRequest) {
    console.log('ADD_USER', request);
    this.socketService.send('/projects/addUserToProject', request);
  }

  getProjectDocument(request): Observable<any> {
    this.socketService.send('/projects/getProjectDocument', {documentId: request});
    return  this.socketService.stream('/getProjectDocument', true);
  }

  uploadProjectDocument(request : UloadDocumentRequest) {
    this.socketService.send('/projects/uploadProjectDocument', request);
  }
}
