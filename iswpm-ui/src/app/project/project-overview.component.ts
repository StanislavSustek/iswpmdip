import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project } from '../shared/backend/entity/project';
import { ProjectService } from './project.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalService } from '../shared/services/modal.service';
import { AddProjectModalComponent } from '../company/company-projects/add-project-modal.component';
import { CompanyService } from '../company/company.service';

@Component({
  selector: 'isw-project-overview',
  templateUrl: './project-overview.component.html',
})
export class ProjectOverviewComponent implements OnInit, OnDestroy {

  project: Project = null;
  detail = null;
  destroy$ = new Subject();

  constructor(
    private projectService: ProjectService,
    private modalService: ModalService,
    private companyService: CompanyService,
  ) { }

  ngOnInit() {
    this.companyService.projects.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.project = val.find(x => x.projectId === this.projectService.openedProject.projectId)
    });
    this.projectService.projectDetail.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.detail = val;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  editDetail() {
      const modalRef = this.modalService.openCustomModal(AddProjectModalComponent);
      modalRef.componentInstance.project = this.project;
  }

}
