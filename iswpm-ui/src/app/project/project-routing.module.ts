import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectComponent } from './project.component';
import { CreateTaskComponent } from './project-tasks/create-task/create-task.component';
import { ProjectOverviewComponent } from './project-overview.component';
import { ProjectTasksComponent } from './project-tasks/project-tasks.component';
import { ProjectWikiComponent } from './project-wiki/project-wiki.component';
import { ProjectDocumentsComponent } from './project-documents/project-documents.component';
import { TaskDetailComponent } from './project-tasks/task-detail/task-detail.component';

export const projectRouting : Routes = [
  {path: 'overview', component: ProjectOverviewComponent},
  {path: 'tasks', component: ProjectTasksComponent},
  {path: 'documents', component: ProjectDocumentsComponent},
  {path: 'wiki', component: ProjectWikiComponent},
  {path: 'create-task', component: CreateTaskComponent},
  {path: 'task-detail', component: TaskDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(projectRouting)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {
}
