import { Component, OnDestroy, OnInit } from '@angular/core';
import { Task } from '../../shared/backend/entity/task';
import { ProjectService } from '../project.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'isw-project-tasks',
  templateUrl: './project-tasks.component.html',
})
export class ProjectTasksComponent implements OnInit, OnDestroy {

  table = true;
  tasks: Task[] = [];
  destroy$ = new Subject();

  constructor(
    private projectService: ProjectService
  ) { }

  ngOnInit() {
    this.table = this.projectService.table;
    this.projectService.initUserProjectTaskChannel();
    this.projectService.userTask.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.tasks = val;
      }
    });

  }

  showTable() {
    this.table = true;
    this.projectService.table = this.table;
  }

  showKanban() {
    this.table = false;
    this.projectService.table = this.table;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
