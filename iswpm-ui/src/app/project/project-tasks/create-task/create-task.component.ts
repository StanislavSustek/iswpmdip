import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from '../../project.service';
import { ContentService } from '../../../shared/services/content.service';
import { Router } from '@angular/router';
import { Task } from '../../../shared/backend/entity/task';
import { BasicFormComponent } from '../../../basic/basic-form/basic-form.component';
import * as moment from 'moment';

@Component({
  selector: 'isw-create-task',
  templateUrl: './create-task.component.html',
})
export class CreateTaskComponent extends BasicFormComponent<Task> implements OnInit {

  form: FormGroup;
  projectUsers = [];
  priorityOptions = [
    {key: 'A', label: 'A'},
    {key: 'B', label: 'B'},
    {key: 'C', label: 'C'},
    {key: 'D', label: 'D'},
    {key: 'E', label: 'E'},
  ];
  statusOptions = [];

  progressOptions = [];
  atr1 = 'taskId';
  dateAtr = 'created';

  constructor(
    private fb: FormBuilder,
    private projectService: ProjectService,
    private contentService: ContentService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    if (this.projectService.editTask) {
      this.value = this.projectService.editTask;
      this.dataForRefresh = this.projectService.userTask;
    }

    this.projectUsers = this.projectService.users.getValue().map(x => ({key: x.userId, label: `${x.firstName} ${x.lastName}`}));
    this.statusOptions = this.contentService.getCodelistForDropdown('TASK-STATUS', true);
    for (let i = 0; i<= 20; i++) {
      this.progressOptions.push({key: i*5, label: i*5 + '%'});
    }

    this.form = this.fb.group({
      name: [this.value ? this.value.name : '', [Validators.required]],
      user: [this.value ? this.value.userId : null, [Validators.required]],
      priority: [this.value ? this.value.priority : null, [Validators.required]],
      description: [this.value ? this.value.description : '', [Validators.required]],
      status: [this.value ? this.value.taskStatusId : '', [Validators.required]],
      hours: [this.value ? this.value.hoursAssigned : '', [Validators.required]],
      from: [this.value ? moment(this.value.dateFrom).format('YYYY-MM-DD') : '', [Validators.required]],
      to: [this.value ? moment(this.value.dateTo).format('YYYY-MM-DD') : '', [Validators.required]],
      progress: [this.value ? this.value.progress : 0, [Validators.required]],
    })
  }

  submit() {
    const task: Task = {
      taskStatusId: this.form.get('status').value,
      userId: this.form.get('user').value,
      description: this.form.get('description').value,
      progress: this.form.get('progress').value,
      name: this.form.get('name').value,
      dateFrom: this.form.get('from').value,
      dateTo: this.form.get('to').value,
      taskId: this.value ? this.value.taskId : null,
      hoursAssigned: this.form.get('hours').value,
      priority: this.form.get('priority').value,
      projectId: this.projectService.openedProject.projectId
    };

    this.projectService.updateTask(task);
    this.navigateBack();
  }

  back() {
    this.navigateBack();
  }
  private navigateBack() {
    if (this.projectService.editTask) {
      this.router.navigate(['projects', 'detail']);

    } else {
      this.router.navigate(['projects', 'detail']);

    }
  }

}
