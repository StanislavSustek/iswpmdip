import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TableComponent } from '../../shared/components/table/table.component';
import { ContentService } from '../../shared/services/content.service';
import { TableCol } from '../../shared/components/table/table-col';
import { Privilege } from '../../shared/backend/entity/privilege';
import { TableCell } from '../../shared/components/table/table-cell';
import { Task } from '../../shared/backend/entity/task';
import { ProjectService } from '../project.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'isw-project-task-table',
  templateUrl: '../../shared/components/table/table.component.html',
})
export class ProjectTaskTableComponent extends TableComponent<Task>{

  constructor(
    cd: ChangeDetectorRef,
    contentService: ContentService,
    private projectService: ProjectService,
    private router: Router
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: 'Názov', fieldType: 'string',fieldName: 'name' , sortable: true},
      {label: 'Stav', fieldType: 'string',fieldName: 'state' , sortable: true},
      {label: 'Riešitel', fieldType: 'string',fieldName: 'userId' , sortable: true},
      {label: 'Priorita', fieldType: 'string',fieldName: 'priority' , sortable: true},
      {label: 'Progres', fieldType: 'number',fieldName: 'progress' , sortable: true},
      {label: 'Do', fieldType: 'string',fieldName: 'string' , sortable: true},
      {label: '', fieldType: 'string',fieldName: 'string' , sortable: false},
    ];
  }

  getData(row: Task): TableCell<Privilege>[] {
    return [
      {label: row.name},
      {label: this.contentService.getCodelistValue('TASK-STATUS', row.taskStatusId, true) },
      {label: this.projectService.translateUser(row.userId)},
      {label: row.priority},
      {label: `${row.progress}%`},
      {label: moment(row.dateTo).format('DD.MM.YYYY')},
      {
        dropdown: [
          {icon: 'search', text: 'Detail', action: 'detail'},
          {icon: 'pen', text: 'Upravit', action: 'edit'},
        ]
      }
    ]
  }

  onActionClick(action, rowData, componentShowed) {
    switch (action) {
      case 'detail':
        this.projectService.editTask = rowData;
        this.router.navigate(['company', 'project', 'task-detail']);
        break;
      case 'edit':
        this.projectService.editTask = rowData;
        this.router.navigate(['company', 'project', 'create-task']);
        break;
      default: super.onActionClick(action, rowData, componentShowed);
    }
  }

}
