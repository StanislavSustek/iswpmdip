import { Component, OnDestroy, OnInit } from '@angular/core';
import { Task } from '../../../shared/backend/entity/task';
import { ProjectService } from '../../project.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { TaskDetail } from '../../../shared/backend/entity/task-detail';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UloadDocumentRequest } from '../../../shared/backend/entity/uload-document-request';

@Component({
  selector: 'isw-task-detail',
  templateUrl: './task-detail.component.html',
})
export class TaskDetailComponent implements OnInit, OnDestroy {

  task: Task = null;
  taskDetail: TaskDetail = null;
  destroy$ = new Subject();
  history = false;
  showAddDocument = false;
  form: FormGroup;
  commentForm: FormGroup;
  file: {
    name: string,
    ext: string,
    base: string
  } = null;

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      description: ''
    });
    this.commentForm = this.fb.group({
      comment: ''
    });
    this.task = this.projectService.editTask;
    this.projectService.initTaskDetailChannel();
    this.projectService.tasksDetail.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.taskDetail = val;
      console.log('TASK_DETAIL', val);
    });
  }

  onFileChange(ev) {
    console.log('aa', ev);
    const file = ev.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload= (event: any) => {
      this.file = {
        name: file.name,
        ext: file.type,
        base: event.target.result.split(',')[1]};
    };
    console.log(this.file);
  }

  saveDocument() {
    this.showAddDocument = false;
    const req: UloadDocumentRequest = {
      description: this.form.get('description').value,
      name: this.file.name,
      extension: this.file.ext,
      fileBase: this.file.base,
      taskId: this.task.taskId
    };
    this.projectService.uloadDocument(req);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  goback() {
    this.router.navigate(['user', 'projects','detail' , 'tasks'])
  }

  historyOpen() {
    this.history = !this.history;
  }

  addDocument() {
    this.showAddDocument = !this.showAddDocument;
  }

  sendComment() {
    this.projectService.sendComment({
      comment: this.commentForm.get('comment').value,
      taskId: this.task.taskId
    });
  }

  downloadFile(file, name, ext) {
    let byteString = window.atob(file);
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ia], {type: ext});
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = name;
    link.download = fileName;
    link.click();
  }

}
