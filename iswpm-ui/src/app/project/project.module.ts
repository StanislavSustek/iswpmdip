import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatProgressBarModule } from '@angular/material';
import { CreateTaskComponent } from './project-tasks/create-task/create-task.component';
import { ComponentsModule } from '../shared/components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectOverviewComponent } from './project-overview.component';
import { ProjectTaskKanbanComponent } from './project-tasks/project-task-kanban.component';
import { RouterModule } from '@angular/router';
import { ProjectTasksComponent } from './project-tasks/project-tasks.component';
import { ProjectTaskTableComponent } from './project-tasks/project-task-table.component';
import { ProjectWikiComponent } from './project-wiki/project-wiki.component';
import { ProjectDocumentsComponent } from './project-documents/project-documents.component';
import { TaskDetailComponent } from './project-tasks/task-detail/task-detail.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProjectComponent,
    CreateTaskComponent,
    ProjectOverviewComponent,
    ProjectTaskKanbanComponent,
    ProjectTasksComponent,
    ProjectTaskTableComponent,
    ProjectWikiComponent,
    ProjectDocumentsComponent,
    TaskDetailComponent,
  ],
  imports: [
    CommonModule,
    DragDropModule,
    MatProgressBarModule,
    ComponentsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule
  ],
  exports: [
  ],

})
export class ProjectModule {
}
