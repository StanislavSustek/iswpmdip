import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ProjectService } from './project.service';
import { Project } from '../shared/backend/entity/project';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Task } from '../shared/backend/entity/task';
import { ContentService } from '../shared/services/content.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { User } from '../shared/backend/entity/user';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'isw-project',
  templateUrl: './project.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ProjectComponent implements OnInit, OnDestroy {
  destroy$ = new Subject();
  grouped = [];
  taskStatuses = this.contentService.codelists.get('TASK-STATUS').filter(x => x.code !== 6);
  connectedTo = [];
  users: User[] = [];
  tasks: Task[] = [];

  project: Project = null;

  constructor(
    private projectService: ProjectService,
    private contentService: ContentService,
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.projectService.initProjectDetailChannel();
    this.projectService.projectDetail.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        console.log('PROJECT_DETAIL', val);
      }
    });
    this.project = this.projectService.openedProject;
    this.projectService.initProjectUsersChannel();

    if (this.projectService.isUser) {
      console.log("USER_PROJECT");
      this.projectService.initUserProjectTaskChannel();
      this.projectService.userTask.pipe(takeUntil(this.destroy$)).subscribe(val => {
        if (val) {
          console.log(val);
          this.tasks = val;
          this.sortingTask();
        }
      });
    } else {
      console.log("COMPANY_PROJECT");
      this.projectService.initProjectTaskChannel();
      this.projectService.tasks.pipe(takeUntil(this.destroy$)).subscribe(val => {
        if (val) {
          this.tasks = val;
          this.sortingTask();
        }
      });
    }
    this.projectService.users.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.users = val;
      }
    });
    this.connectedTo = this.contentService.codelists.get('TASK-STATUS').filter(x => x.code !== 6).map(x => x.value);

  }

  private sortingTask() {
    this.grouped.splice(0, this.grouped.length);
    const grouped = this.groupBy(this.tasks, 'taskStatusId');
    this.taskStatuses.forEach((x, i) => {
      if (Object.keys(grouped).includes(x.code + '')) {
        this.grouped.push(grouped[x.code + '']);
      } else {
        this.grouped.push([]);
      }
    });
    console.log(this.grouped);
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  drop(event: CdkDragDrop<string[]>) {
    // TODO mozno osetrenie aby sa vkuse neresetlo pri userovi ktory robi zmenu
    const task = JSON.parse(JSON.stringify(event.previousContainer.data[event.previousIndex]));
    task.taskStatusId = this.contentService.codelists.get('TASK-STATUS').find(x => x.value === event.container.id).code;
    this.projectService.updateTask(task);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  translateUser(id) {
    return this.projectService.translateUser(id);
  }

  createTask() {
    this.projectService.isUser = false;
    this.router.navigate(['company', 'project', 'create-task'])
  }

}
