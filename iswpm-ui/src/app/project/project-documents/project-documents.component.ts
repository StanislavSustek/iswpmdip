import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProjectDocument } from '../../shared/backend/entity/projectDocument';
import * as moment from 'moment';
import { UloadDocumentRequest } from '../../shared/backend/entity/uload-document-request';
import { ProjectService } from '../project.service';
import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'isw-project-documents',
  templateUrl: './project-documents.component.html',
  styleUrls: ['./project-documents.component.scss']
})
export class ProjectDocumentsComponent implements OnInit, OnDestroy {

  form: FormGroup;
  documents = [];
  file: {
    name: string,
    ext: string,
    base: string
  } = null;
  destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private projectService: ProjectService
  ) {
  }

  ngOnInit() {
    this.projectService.projectDetail.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.documents = val.documents
    });
    this.form = this.fb.group({
      description: ''
    })
  }

  addDocument() {
    const req: UloadDocumentRequest = {
      description: this.form.get('description').value,
      name: this.file.name,
      extension: this.file.ext,
      fileBase: this.file.base,
      projectId: this.projectService.openedProject.projectId
    };
    this.projectService.uploadProjectDocument(req);
  }

  onFileChange(event) {
    console.log('aa', event);
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload= (event: any) => {
      this.file = {
        name: file.name,
        ext: file.type,
        base: event.target.result.split(',')[1]};
    };
    console.log(this.file);
  }

  getProjectDocument(documentId) {
    this.projectService.getProjectDocument(documentId).pipe(first()).subscribe(val => {
      console.log(val);
      this.downloadFile(val.file, val.fileName, val.extension);
    });
  }

  downloadFile(file, fileName: string, ext) {
    let byteString = window.atob(file);
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ia], {type: ext});
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
  }

  removeFile(id) {

  }

  editFile(item: ProjectDocument) {

  }

  format(date: Date) {
    return moment(date).format('DD.MM.YYYY HH:mm');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}

