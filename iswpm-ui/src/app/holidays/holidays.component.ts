import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { timer } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'isw-holidays',
  templateUrl: './holidays.component.html',
})
export class HolidaysComponent implements OnInit {

  companyAdminMode = true;
  hour = 0;
  minute = 0;
  second = 0;
  date = moment('30.4.2020 02:30:00');

  constructor() {
  }

  ngOnInit() {
    this.initTimer()
  }

  //TODO dobry napad
  initTimer() {
    timer(1000).pipe(first()).subscribe(val => {
      this.hour = (+moment().format('HH')) - (+this.date.format('HH'));
      this.minute = +moment().format('mm') - +this.date.format('mm');
      this.second = +moment().format('ss') - +this.date.format('ss');
      this.initTimer();
    })
  }

}
