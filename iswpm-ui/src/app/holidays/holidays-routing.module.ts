import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HolidaysComponent } from './holidays.component';
import { HolidaysApprovingComponent } from './holidays-approving/holidays-approving.component';
import { HolidayCalendarComponent } from './holiday-calendar/holiday-calendar.component';

export const holidaysRouting : Routes = [
  {path: 'my-holidays', component: HolidayCalendarComponent},
  {path: 'approving', component: HolidaysApprovingComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(holidaysRouting)],
  exports: [RouterModule]
})
export class HolidaysRoutingModule {
}
