import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HolidaysComponent } from './holidays.component';
import { RouterModule } from '@angular/router';
import { HolidaysApprovingComponent } from './holidays-approving/holidays-approving.component';
import { HolidayCalendarComponent } from './holiday-calendar/holiday-calendar.component';
import {
  CalendarDateFormatter,
  CalendarModule,
  CalendarMomentDateFormatter,
  DateAdapter,
  MOMENT
} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../shared/components/components.module';
import { SharedModule } from '../shared/shared.module';
import localeSk from '@angular/common/locales/sk';
import { HolidaysApprovingTableComponent } from './holidays-approving/holidays-approving-table.component'
registerLocaleData(localeSk);
export function momentAdapterFactory() {
  return adapterFactory(moment);
}

@NgModule({
  declarations: [
    HolidaysComponent,
    HolidaysApprovingComponent,
    HolidayCalendarComponent,
    HolidaysApprovingTableComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    CalendarModule.forRoot(
      {
        provide: DateAdapter,
        useFactory: momentAdapterFactory,
      },
      {
        dateFormatter: {
          provide: CalendarDateFormatter,
          useClass: CalendarMomentDateFormatter,
        },
      }
    ),
    SharedModule
  ],
  exports: [
    HolidaysComponent,
    HolidaysApprovingComponent,
    HolidayCalendarComponent
  ],
  providers: [
    {
      provide: MOMENT,
      useValue: moment,
    },
  ],
})
export class HolidaysModule { }
