import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TableComponent } from '../../shared/components/table/table.component';
import { Holiday } from '../../shared/backend/entity/holiday';
import { ContentService } from '../../shared/services/content.service';
import { TableCol } from 'src/app/shared/components/table/table-col';
import { TableCell } from '../../shared/components/table/table-cell';
import { CompanyService } from 'src/app/company/company.service';
import { DateUtils } from '../../shared/utils/date-utils';
import { ModalService } from '../../shared/services/modal.service';
import { first } from 'rxjs/operators';
import { HolidayService } from '../holiday.service';
import { CreateHolidayRequest } from '../../shared/backend/entity/create-holiday-request';

@Component({
  selector: 'isw-holidays-approving-table',
  templateUrl: '../../shared/components/table/table.component.html',
})
export class HolidaysApprovingTableComponent extends TableComponent<Holiday>{

  constructor(
    contentService: ContentService,
    cd: ChangeDetectorRef,
    private companyService: CompanyService,
    private modalService: ModalService,
    private holidayService: HolidayService,
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: 'Zamestnanec', fieldName: 'userId', fieldType: 'string', sortable: true},
      {label: 'Typ', fieldName: 'holidayTypeId', fieldType: 'number', sortable: true},
      {label: 'Stav', fieldName: '', fieldType: 'string', sortable: true},
      {label: 'Od', fieldName: 'from', fieldType: 'localDate', sortable: true},
      {label: 'Do', fieldName: 'to', fieldType: 'localDate', sortable: true},
      {label: '', fieldName: '', fieldType: 'string', sortable: false},
    ]
  }

  getData(row: Holiday): TableCell<Holiday>[] {
    return [
      {label: this.getUserName(row.userId)},
      {label: this.contentService.getCodelistValue('HOLIDAY-TYPE', row.holidayTypeId, true)},
      {label: this.contentService.getCodelistValue('HOLIDAY-STATE', row.holidayStatusId, true)},
      {label: DateUtils.formatDateTime(row.from)},
      {label: DateUtils.formatDateTime(row.to)},
      {
        icons: [
          {icon: 'check', action: 'approve'},
          {icon: 'times', action: 'cancel'},
        ]
      }
    ]
  }

  private getUserName(userId: number): string {
    const user = this.companyService.employers.getValue().find(x => x.userId === userId);
    return user ? `${user.firstName} ${user.lastName}` : userId + '';
  }

  onActionClick(action, rowData, componentShowed) {
    switch (action) {
      case 'approve':
        this.modalService.openInformationModal(
         'Schválenie',
          `Naozaj chcete <b>SCHVÁLIŤ</b> pracovné voľno pre zamestnanca <b>${this.getUserName(rowData.userId)}</b>?`,
           'confirm'
          ).pipe(first()).subscribe(val => {
            if (val) {
              let req: CreateHolidayRequest = {holiday: rowData};
              req.holiday.holidayStatusId = 2;
              this.holidayService.createHoliday(req)
            }
        });
        break;
      case 'cancel':
        this.modalService.openInformationModal(
          'Zamietnutie',
          `Naozaj chcete <b>ZAMIETNUŤ</b> pracovné voľno pre zamestnanca <b>${this.getUserName(rowData.userId)}</b>?`,
          'confirm'
        ).pipe(first()).subscribe(val => {
          if (val) {
            let req: CreateHolidayRequest = {holiday: rowData};
            req.holiday.holidayStatusId = 3;
            this.holidayService.createHoliday(req)
          }
        });
        break;
    }
  }

}
