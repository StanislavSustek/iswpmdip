import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidaysApprovingComponent } from './holidays-approving.component';

describe('HolidaysApprovingComponent', () => {
  let component: HolidaysApprovingComponent;
  let fixture: ComponentFixture<HolidaysApprovingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidaysApprovingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidaysApprovingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
