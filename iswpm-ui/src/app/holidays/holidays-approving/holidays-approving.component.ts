import { Component, OnDestroy, OnInit } from '@angular/core';
import { HolidayService } from '../holiday.service';
import { Holiday } from 'src/app/shared/backend/entity/holiday';
import { HolidayFilterRequest } from '../../shared/backend/entity/holiday-filter-request';
import { CompanyService } from '../../company/company.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserService } from '../../shared/services/user.service';

@Component({
  selector: 'isw-holidays-approving',
  templateUrl: './holidays-approving.component.html',
  styleUrls: ['./holidays-approving.component.scss']
})
export class HolidaysApprovingComponent implements OnInit, OnDestroy {

  holidaysForApproval: Holiday[] = [];
  request: HolidayFilterRequest = {
    userIds: null,
    stateId: 1,
    companyId: this.companyService.company.companyId
  };
  destroy$ = new Subject();
  constructor(
    private holidayService: HolidayService,
    private companyService: CompanyService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.holidayService.initHolidayForApprovalChannel(this.request);
    this.holidayService.holidaysForApproval.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.holidaysForApproval = JSON.parse(JSON.stringify(val));
      }
    });

    this.userService.refresh.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val && val.message === 'getHolidaysForApproval' && val.refresh) {
        this.holidayService.filterHolidaysForApproval(this.request);
      }
      if (val && val.message === 'getMessages' && val.refresh) {
        this.userService.refreshMessage();
      }
    });

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
