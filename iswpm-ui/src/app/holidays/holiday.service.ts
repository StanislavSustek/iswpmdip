import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Holiday } from '../shared/backend/entity/holiday';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { HolidayFilterRequest } from '../shared/backend/entity/holiday-filter-request';
import { CreateHolidayRequest } from '../shared/backend/entity/create-holiday-request';

@Injectable({
  providedIn: 'root'
})
export class HolidayService {

  holidays = new BehaviorSubject<Holiday[]>(null);
  holidaysForApproval = new BehaviorSubject<Holiday[]>(null);

  constructor(
    private socketService: PmSocket2Service
  ) { }

  initHolidayChannel(request: HolidayFilterRequest) {
    if (this.holidays.getValue() == null) {
      this.socketService.stream('getHolidays', true).subscribe(val => {
        this.holidays.next(val);
      })
    }
    this.filterHolidays(request);
  }

  initHolidayForApprovalChannel(request: HolidayFilterRequest) {
    if (this.holidaysForApproval.getValue() == null) {
      this.socketService.stream('getHolidaysForApproval', true).subscribe(val => {
        this.holidaysForApproval.next(val);
      })
    }
    this.filterHolidaysForApproval(request);
  }

  filterHolidays(request: HolidayFilterRequest) {
    this.socketService.send('/holiday/getHolidays', request);
  }

  filterHolidaysForApproval(request: HolidayFilterRequest) {
    this.socketService.send('/holiday/getHolidaysForApproval', request);
  }

  createHoliday(request: CreateHolidayRequest) {
    this.socketService.send('/holiday/createHoliday', request);
  }

}
