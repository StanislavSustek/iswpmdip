import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalService } from '../../shared/services/modal.service';
import { isSameDay, isSameMonth, } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventTimesChangedEvent, CalendarView, } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ContentService } from '../../shared/services/content.service';
import { CompanyService } from '../../company/company.service';
import { HolidayService } from '../holiday.service';
import { HolidayFilterRequest } from '../../shared/backend/entity/holiday-filter-request';
import { takeUntil } from 'rxjs/operators';
import { CreateHolidayRequest } from '../../shared/backend/entity/create-holiday-request';
import { DateUtils } from '../../shared/utils/date-utils';
import { AppService } from '../../shared/services/app.service';
import { UserService } from '../../shared/services/user.service';
import { Holiday } from '../../shared/backend/entity/holiday';

@Component({
  selector: 'isw-holiday-calendar',
  templateUrl: './holiday-calendar.component.html',
})
export class HolidayCalendarComponent implements OnInit, OnDestroy {

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
  };
  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;
  holidayForm: FormGroup;
  filterForm: FormGroup;
  companyUsers = [];
  holidayTypes = this.contentService.getCodelistForDropdown('HOLIDAY-TYPE', true);
  states = this.contentService.getCodelistForDropdown('HOLIDAY-STATE', true);
  destroy$ = new Subject();

  constructor(
    private modalService: ModalService,
    private fb: FormBuilder,
    private contentService: ContentService,
    private companyService: CompanyService,
    private holidayService: HolidayService,
    private appService: AppService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {

    this.companyUsers = this.companyService.employers.getValue().map(x => ({
      key: x.userId,
      label: `${x.firstName} ${x.lastName}`
    }));
    this.filterForm = this.fb.group({
      type: null,
      user: [this.appService.user.getValue().userId],
      stateId: 4,
    });
    this.userService.refresh.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val && val.message === 'getHolidays' && val.refresh) {
        this.filterData();
      }
    });
    this.holidayForm = this.fb.group({
      type: null,
      fromDate: new Date(),
      fromTime: moment(),
      toDate: new Date(),
      toTime: moment(),
      comment: '',
    });
    const req: HolidayFilterRequest = {
      companyId: this.companyService.company.companyId,
      stateId: this.filterForm.get('stateId').value,
      type: this.filterForm.get('type').value,
      userIds: this.filterForm.get('user').value,
    };
    this.holidayService.initHolidayChannel(req);
    this.holidayService.holidays.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        console.log('HOLIDAYS', val);
        this.events.splice(0, this.events.length);
        const arr = [];
        val.forEach(x => {
          arr.push({
            start: new Date(x.from),
            end: new Date(x.to),
            title: this.getComment(x),
            color: this.colors.red,
            allDay: true,
            resizable: {
              beforeStart: true,
              afterEnd: true,
            },
            draggable: false,
          })
        });
        this.events = this.events.concat(arr);
      }
    })
  }

  dayClicked({date, events}: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd,
                    }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = {event, action};
    this.modalService.openInformationModal('a', 'a', 'confirm');
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  filterData() {
    const req: HolidayFilterRequest = {
      companyId: this.companyService.company.companyId,
      stateId: this.filterForm.get('stateId').value,
      type: this.filterForm.get('type').value,
      userIds: [this.filterForm.get('user').value],
    };
    console.log('Filter', req);
    this.holidayService.filterHolidays(req);
  }

  crateHoliday() {
    const req: CreateHolidayRequest = {
      holiday: {
        userId: this.appService.user.getValue().userId,
        comment: this.holidayForm.get('comment').value,
        from: DateUtils.getLocalDateTimeFromDateAndTimePicker(this.holidayForm.get('fromDate').value, this.holidayForm.get('fromTime').value),
        to: DateUtils.getLocalDateTimeFromDateAndTimePicker(this.holidayForm.get('toDate').value, this.holidayForm.get('toTime').value),
        holidayStatusId: 1,
        holidayTypeId: this.holidayForm.get('type').value,
        companyInternalId: this.companyService.company.companyId,
        holidayId: null
      }
    };
    console.log(req);
    this.holidayService.createHoliday(req);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private getComment(item: Holiday): string {
    const user = this.companyUsers.find(x => x.key === item.userId);
    return `${user.label}: ${item.comment}`;
  }

}
