import { Injectable } from '@angular/core';
import { Company } from '../shared/backend/entity/company';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { CreateProjectRequest } from '../shared/backend/entity/create-project-request';
import { BehaviorSubject } from 'rxjs';
import { Project } from '../shared/backend/entity/project';
import { UserService } from '../shared/services/user.service';
import { User } from '../shared/backend/entity/user';
import { AddUserToCompanyRequest } from '../shared/backend/entity/add-user-to-company-request';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  employers = new BehaviorSubject<User[]>(null);
  employersCandidates = new BehaviorSubject<User[]>(null);
  company: Company = null;
  projects = new BehaviorSubject<Project[]>(null);
  userProjects = new BehaviorSubject<Project[]>(null);

  constructor(
    private pmSocket: PmSocket2Service,
    private userService: UserService
  ) {
  }

  clear() {
    this.employers.next(null);
    this.company = null;
    this.projects.next(null);
    this.userProjects.next(null);
  }

  createProject(request: CreateProjectRequest) {
    this.pmSocket.send('/company/createProject', request);
  }

  updateProject(request: CreateProjectRequest) {
    this.pmSocket.send('/company/updateProjectInfo', request);
  }

  deleteProject(request: CreateProjectRequest) {
    this.pmSocket.send('/company/deleteProject', request);
  }

  getProjects(companyId: number) {
    this.pmSocket.send('/company/getProjects', {companyId: companyId});
  }

  initCompanyProjectChanel(withRefresh = false) {
    if (!this.projects.getValue()) {

      this.pmSocket.stream('getProjects').subscribe(val => {
        if (val) {
          this.userService.refreshMessage();
          this.projects.next(val);
        }
      });
      if (withRefresh) {
        this.getProjects(this.company.companyId);
      }
    }
  }

  getUserProjects() {
    this.pmSocket.send('/company/getUserProjects', {});
  }

  initUserProjectChanel(withRefresh = false) {
    this.pmSocket.stream('getUserProjects', true).subscribe(val => {
      if (val) {
        this.userProjects.next(val);
      }
    });
    if (withRefresh) {
      this.getUserProjects();
    }
  }

  initCompanyEmployers() {
    this.pmSocket.stream('/getCompanyUsers').subscribe(val => {
      if (val) {
        this.employers.next(val.users);
      }
    });
    this.pmSocket.send('/company/getCompanyUsers', {companyId: this.company.companyId});
  }

  initEmployerCandidates() {
    if (this.employersCandidates.getValue() == null) {
      this.pmSocket.stream('getUsersForCompany').subscribe(val => {
        if (val) {
          this.employersCandidates.next(val.users);
        }
      });
    }
    this.pmSocket.send('/company/getUsersForCompany', {companyId: this.company.companyId});
  }

  addEmployerToCompany(request: AddUserToCompanyRequest) {
    this.pmSocket.send('/company/getProjects', request);
  }
}
