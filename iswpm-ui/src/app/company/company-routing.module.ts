import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyProjectsComponent } from './company-projects/company-projects.component';
import { CompanyOverviewComponent } from './company-overview/company-overview.component';
import { projectRouting } from '../project/project-routing.module';
import { ProjectGuard } from '../project/project.guard';

export const companyRouting : Routes = [
  {path: 'overview', component: CompanyOverviewComponent},
  {path: 'projects', component: CompanyProjectsComponent},
  {path: 'project', children: projectRouting, canActivate: [ProjectGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(companyRouting)],
  exports: [RouterModule]
})
export class CompanyRoutingModule {
}
