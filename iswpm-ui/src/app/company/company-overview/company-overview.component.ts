import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Company } from '../../shared/backend/entity/company';
import { AdminService } from '../../admin/admin.service';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { CompanyService } from '../company.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Project } from '../../shared/backend/entity/project';
import { ModalService } from '../../shared/services/modal.service';
import { AddEmployerToCompanyComponent } from '../add-employer-to-company/add-employer-to-company.component';

@Component({
  selector: 'isw-company-overview',
  templateUrl: './company-overview.component.html',
})
export class CompanyOverviewComponent implements OnInit, OnDestroy {
  company: Company = null;
  employers = null;
  projects: Project[] = [];
  destroy$ = new Subject();

  constructor(
    private userService: UserService,
    private adminService: AdminService,
    private pmSocket: PmSocket2Service,
    private companyService: CompanyService,
    private modalService: ModalService,
  ) {
  }

  ngOnInit() {
    this.companyService.projects.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.projects = val;
      }
    });

    this.companyService.employers.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.employers = val;
      }
    });

    this.userService.userCompanies.subscribe(val => {
      if (val) {
        this.company = val[0];
      }
    })
  }

  addEmployer() {
    this.modalService.openCustomModal(AddEmployerToCompanyComponent)
  }


  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
