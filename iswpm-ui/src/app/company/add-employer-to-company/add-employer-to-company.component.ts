import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AdminService } from '../../admin/admin.service';
import { CompanyService } from '../company.service';
import { AddUserToCompanyRequest } from '../../shared/backend/entity/add-user-to-company-request';
import { User } from '../../shared/backend/entity/user';

@Component({
  selector: 'isw-add-employer-to-company',
  templateUrl: './add-employer-to-company.component.html',
})
export class AddEmployerToCompanyComponent implements OnInit {

  form: FormGroup;
  roles = [];
  users = [];

  @Input() user: User = null;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private companyService: CompanyService,
    private dialogRef: MatDialogRef<AddEmployerToCompanyComponent>
  ) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      user: null,
      role: null
    });

    this.companyService.initEmployerCandidates();
    this.adminService.initRolesWithPrivilegesChanel({companyId: this.companyService.company.companyId});
    this.companyService.employersCandidates.subscribe(val => {
      if (val) {
        this.users = val.map(x => ({key: x.userId, label: `${x.lastName} ${x.lastName}`}))
      }
    });
    this.adminService.roleWithPrivileges.subscribe(val => {
      console.log('aaa',val);
      if (val) {
        this.roles = val.map(x => ({key: x.role.roleId, label: x.role.description}))
      }
    });
  }

  close() {
    this.dialogRef.close();
  }

  submit() {
    if (this.form.valid) {
      const req: AddUserToCompanyRequest = {
        companyId: this.companyService.company.companyId,
        roleId: this.form.get('role').value,
        userId: this.form.get('user').value
      };
      this.companyService.addEmployerToCompany(req);
    }
  }

}
