import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyOverviewComponent } from './company-overview/company-overview.component';
import { CompanyProjectsComponent } from './company-projects/company-projects.component';
import { AdminModule } from '../admin/admin.module';
import { CompanyProjectsTableComponent } from './company-projects/company-projects-table/company-projects-table.component';
import { ComponentsModule } from '../shared/components/components.module';
import { AddProjectModalComponent } from './company-projects/add-project-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BasicModule } from '../basic/basic.module';
import { MatDatepickerModule, MatFormFieldModule, MatNativeDateModule } from '@angular/material';
import { CompanyProjectsAddUsersComponent } from './company-projects/company-projects-add-users/company-projects-add-users.component';
import { AddEmployerToCompanyComponent } from './add-employer-to-company/add-employer-to-company.component';

@NgModule({
  declarations: [
    CompanyOverviewComponent,
    CompanyProjectsComponent,
    CompanyProjectsTableComponent,
    AddProjectModalComponent,
    CompanyProjectsAddUsersComponent,
    AddEmployerToCompanyComponent
  ],
  imports: [
    CommonModule,
    AdminModule,
    ComponentsModule,
    ReactiveFormsModule,
    BasicModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule
  ],
  exports: [
    CompanyOverviewComponent, CompanyProjectsComponent
  ],
  entryComponents: [
    AddProjectModalComponent,
    CompanyProjectsAddUsersComponent,
    AddEmployerToCompanyComponent
  ]
})
export class CompanyModule {
}
