import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Project } from '../../shared/backend/entity/project';
import { BasicFormComponent } from '../../basic/basic-form/basic-form.component';
import { MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { CompanyService } from '../company.service';
import { CreateProjectRequest } from '../../shared/backend/entity/create-project-request';
import * as moment from 'moment';

@Component({
  selector: 'isw-add-project-modal',
  templateUrl: './add-project-modal.component.html',
})
export class AddProjectModalComponent extends BasicFormComponent<Project> implements OnInit {

  form: FormGroup;

  @Input() set project(v: Project) {
    this.value = v;
  };

  destroy = new Subject();
  atr1 = 'projectId';
  dateAtr = 'created';

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddProjectModalComponent>,
    private companyService: CompanyService,
  ) {
    super();
  }

  ngOnInit() {
    this.dataForRefresh = this.companyService.projects;
    super.ngOnInit();

    this.form = this.fb.group({
      name: this.value ? this.value.name : '',
      description: this.value ? this.value.description : '',
      budget: this.value ? this.value.budget : '',
      dateFrom: this.value ? moment(this.value.dateFrom).format('YYYY-MM-DD') : '',
      dateTo: this.value ? moment(this.value.dateTo).format('YYYY-MM-DD') : '',
    });
  }

  close() {
    this.dialogRef.close();
  }

  restore() {
    super.restore();
    this.form.get('name').setValue(this.value.name);
    this.form.get('description').setValue(this.value.description);
    this.form.get('budget').setValue(this.value.budget);
    this.form.get('dateFrom').setValue(moment(this.value.dateFrom).format('YYYY-MM-DD'));
    this.form.get('dateTo').setValue(moment(this.value.dateTo).format('YYYY-MM-DD'));

  }

  submit() {
    const req: CreateProjectRequest = {
      companyId: this.companyService.company.companyId,
      project: {
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        budget: this.form.get('budget').value,
        dateFrom: this.form.get('dateFrom').value,
        dateTo: this.form.get('dateTo').value,
        projectStatusId: 1,
        created: this.value ? this.value.created : null,
        comment: this.value ? this.value.comment : '',
        companyInternalId: this.value ? this.value.companyInternalId : null,
        projectId: this.value ? this.value.projectId : null
      }
    };
    if (this.value) {
      this.companyService.updateProject(req);
    } else {
      this.companyService.createProject(req);
    }
    this.dialogRef.close();
  }

  cancel() {
    super.cancel();
  }

}
