import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project } from '../../shared/backend/entity/project';
import { ContentService } from '../../shared/services/content.service';
import * as moment from 'moment';
import { CompanyService } from '../company.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalService } from '../../shared/services/modal.service';
import { AddProjectModalComponent } from './add-project-modal.component';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { ProjectService } from '../../project/project.service';
import { Router } from '@angular/router';
import { CompanyProjectsAddUsersComponent } from './company-projects-add-users/company-projects-add-users.component';

@Component({
  selector: 'isw-company-projects',
  templateUrl: './company-projects.component.html',
})
export class CompanyProjectsComponent implements OnInit, OnDestroy {

  projects: Project[] = [];
  destroy$ = new Subject();

  constructor(
    private contentService: ContentService,
    private companyService: CompanyService,
    private modalService: ModalService,
    private projectService: ProjectService,
    private router: Router,
    private pm2: PmSocket2Service,
  ) {
  }

  ngOnInit() {
    this.companyService.initCompanyProjectChanel(true)
    this.companyService.projects.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.projects = val;
    });
    this.pm2.stream('/project/task', true).subscribe(val => {
      console.log("TASK_FOR_USER_1", val);
    })
    this.pm2.stream('project/tasks').subscribe(val => {
      console.log("TASK_FOR_ALL", val);
    })
  }

  translate(item) {
    return this.contentService.projectStatuses.find(x => x.code === item).value
  }

  format(item) {
    return moment(item).format('DD.MM.YYYY');
  }

  openProject(item) {
    this.projectService.openedProject = item;
    this.router.navigate(['projects', 'detail'])
  }

  addProject() {
    this.modalService.openCustomModal(AddProjectModalComponent)
  }

  editProject(item) {
    const modalRef = this.modalService.openCustomModal(AddProjectModalComponent)
    modalRef.componentInstance.project = item;
  }

  projectUsers(item) {
    const modalRef = this.modalService.openCustomModal(CompanyProjectsAddUsersComponent);
    this.projectService.openedProject = item.projectId;
    modalRef.componentInstance.project = item;
  }

  deleteProject(project: Project) {
    this.modalService.openInformationModal(
      'Upozornenie',
      'Naozaj chcete odstraniť projekt ' + project.name,
      'confirm'
      ).subscribe(val => {
        if (val) {
          const req = {project: project, companyId: this.companyService.company.companyId};
          this.companyService.deleteProject(req)
        }
    })
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
