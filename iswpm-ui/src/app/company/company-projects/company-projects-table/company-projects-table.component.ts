import { ChangeDetectorRef, Component } from '@angular/core';
import { TableComponent } from '../../../shared/components/table/table.component';
import { Project } from '../../../shared/backend/entity/project';
import { ContentService } from '../../../shared/services/content.service';
import { TableCell } from '../../../shared/components/table/table-cell';
import { TableCol } from '../../../shared/components/table/table-col';
import * as moment from 'moment';

@Component({
  selector: 'isw-company-projects-table',
  templateUrl: '../../../shared/components/table/table.component.html',
})
export class CompanyProjectsTableComponent extends TableComponent<Project> {

  constructor(
    cd: ChangeDetectorRef,
    contentService: ContentService,
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: 'Názov', fieldType: 'string', fieldName: 'name', sortable: true},
      {label: 'Popis', fieldType: 'string', fieldName: 'description', sortable: true},
      {label: 'Vytvorený', fieldType: 'string', fieldName: 'created', sortable: true},
      {label: 'Stav', fieldType: 'string', fieldName: 'name', sortable: true},
    ]
  }

  getData(row: Project): TableCell<Project>[] {
    return [
      {label: row.name},
      {label: row.description},
      {label: moment(row.created).format('DD.MM.YYYY')},
      {label: this.contentService.projectStatuses.find(x => x.code === row.projectStatusId).value},
    ];
  }

}
