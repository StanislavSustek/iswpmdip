import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProjectsTableComponent } from './company-projects-table.component';

describe('CompanyProjectsTableComponent', () => {
  let component: CompanyProjectsTableComponent;
  let fixture: ComponentFixture<CompanyProjectsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyProjectsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProjectsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
