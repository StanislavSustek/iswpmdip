import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProjectsAddUsersComponent } from './company-projects-add-users.component';

describe('CompanyProjectsAddUsersComponent', () => {
  let component: CompanyProjectsAddUsersComponent;
  let fixture: ComponentFixture<CompanyProjectsAddUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyProjectsAddUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProjectsAddUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
