import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Project } from '../../../shared/backend/entity/project';
import { Subject } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from '../../../shared/backend/entity/user';
import { PmSocket2Service } from '../../../shared/services/pm-socket2.service';
import { AdminService } from '../../../admin/admin.service';
import { takeUntil } from 'rxjs/operators';
import { CompanyService } from '../../company.service';
import { AddUserToProjectRequest } from '../../../shared/backend/entity/add-user-to-project-request';
import { ProjectService } from '../../../project/project.service';

@Component({
  selector: 'isw-company-projects-add-users',
  templateUrl: './company-projects-add-users.component.html',
  styleUrls: ['./company-projects-add-users.component.scss']
})
export class CompanyProjectsAddUsersComponent implements OnInit, OnDestroy {
  form: FormGroup;

  @Input() project: Project;
  destroy = new Subject();

  projectUsers: User[] = [];
  notSelectedUsers: User[] = [];
  isEdit = false;
  result: User[] = [];
  destroy$ = new Subject();
  request: AddUserToProjectRequest = {
    addUsers: [],
    removeUsers: [],
    companyId: this.companyService.company.companyId,
    projectId: null
  };

  constructor(
    private pmSocket: PmSocket2Service,
    private adminService: AdminService,
    private companyService: CompanyService,
    private projectService: ProjectService,
    private modalRef: MatDialogRef<CompanyProjectsAddUsersComponent>
  ) {
  }

  ngOnInit() {
    console.log(this.project);
    this.projectService.initProjectUsersChannel();
    this.projectService.users.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.projectUsers = val;
        this.result = JSON.parse(JSON.stringify(val));
      }
    });
    this.companyService.employers.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.notSelectedUsers = val;
      }
    });
  }

  close() {
    this.modalRef.close();
  }

  submit() {
    if (this.request.removeUsers.length > 0 || this.request.addUsers.length > 0) {
      this.request.projectId = this.project.projectId;
      this.projectService.addUserToProject(this.request);
    }
    this.isEdit = false;
  }

  edit() {
    this.isEdit = true;
  }

  add(item: User) {
    this.notSelectedUsers.splice(this.notSelectedUsers.findIndex(x => x.userId === item.userId), 1);
    this.result.push(item);
    const user = this.projectUsers.find(x => x.userId === item.userId);
    if (!user) {
      this.request.addUsers.push(item);
    }
  }

  remove(item: User) {
    this.result.splice(this.result.findIndex(x => x.userId === item.userId), 1);
    this.notSelectedUsers.push(item);
    const user = this.projectUsers.find(x => x.userId === item.userId);
    if (user) {
      this.request.removeUsers.push(item);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
