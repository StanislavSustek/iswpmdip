import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from './auth/auth.module';
import { PmSocketService } from './shared/services/pm-socket.service';
import { SecurityBackendService } from './shared/backend/security-backend.service';
import { HttpClientModule } from '@angular/common/http';
import { AdminModule } from './admin/admin.module';
import { BasicModule } from './basic/basic.module';
import { SharedModule } from './shared/shared.module';
import { BasicFormComponent } from './basic/basic-form/basic-form.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { MatBottomSheetModule } from '@angular/material';
import { CompanyModule } from './company/company.module';
import { ProjectModule } from './project/project.module';
import { UserModule } from './user/user.module';
import { HolidaysModule } from './holidays/holidays.module';

@NgModule({
  declarations: [
    AppComponent,
    BasicFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AuthModule,
    HttpClientModule,
    AdminModule,
    BasicModule,
    SharedModule,
    DashboardModule,
    MatBottomSheetModule,
    CompanyModule,
    ProjectModule,
    UserModule,
    HolidaysModule
  ],
  providers: [
    PmSocketService,
    SecurityBackendService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
