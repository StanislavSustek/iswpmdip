import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDateTime'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, onlyDate = false): any {
    if (onlyDate) {
      return moment(value).format('DD.MM.YYYY');
    }

    return moment(value).format('DD.MM.YYYY HH:mm');
  }

}
