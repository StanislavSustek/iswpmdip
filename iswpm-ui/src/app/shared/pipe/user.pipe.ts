import { Pipe, PipeTransform } from '@angular/core';
import { ContentService } from '../services/content.service';
import { ProjectService } from '../../project/project.service';

@Pipe({
  name: 'translateUser'
})

export class UserNamePipe implements PipeTransform {

  constructor(private projectService: ProjectService) {

  }

  transform(value): any {
   return  this.projectService.translateUser(value);
  }
}
