import { Pipe, PipeTransform } from '@angular/core';
import { ContentService } from '../services/content.service';

@Pipe({
  name: 'translateCodelist'
})

export class CodelistPipe implements PipeTransform {

  constructor(private contentService: ContentService) {

  }

  transform(value, codelistName: string, sk = false): any {
    const codes = this.contentService.getCodelist(codelistName);
    if (codes) {
      const c = codes.find(val => val.code === value || (!!value && val.code === value.toString()));
      if (c) {
        if (sk) {
          return c.sk;
        }
        return c.value;
      }
    }
    return value || '';
  }
}
