import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: '[isw-validation-message]',
  templateUrl: './validation-message.component.html'
})
export class ValidationMessageComponent implements OnChanges {

  @Input() cc: AbstractControl;
  private sub: Subscription;
  error = '';
  keys: Map<string, string> = new Map([
    ['numeric', 'Povolené sú len čísla.'],
    ['email', 'Zadajte emailovú adresu v správnom tvare.'],
    ['alfa-numeric', 'Povolené sú len alfanumerické znaky.'],
    ['minlength', 'Hodnota je príliš krátka.'],
    ['maxlength', 'Hodnota je príliš dlhá.'],
    ['mod11_error', 'Nevalidné číslo účtu.'],
    ['ngbDate', 'Dátum nie je v povolenom rozmedzí'],
    ['invalid-password', 'Heslo musí mať min. 8 a max. 48 znakov, môže obsahovať znaky A..Z,a..z,0..9.'],
    ['date_weekend_error', 'Zadaný dátum nie je pracovný deň.'],
    ['string_invalid_char_error', 'Nepovolené znaky v čiastke'],
    ['number_too_high_precision_error', 'Nesprávny formát čiastky'],
    ['number_too_low_or_equal_error', 'Zadaná čiastka je príliš nízka'],
    ['number_too_low_error', 'Zadaná čiastka je príliš nízka'],
    ['number_invalid_format_error', 'Nesprávny formát čiastky'],
    ['date_current_error', 'Po 17:30 nie je možné zadať aktuálny deň'],
    ['cp-account-cif', 'Zvolené bežné účty majú rôzne CIF majiteľa účtu. Upravte prosím výber účtov.'],
    ['cp-sel-account-cif', 'Zvolený účet má CIF majiteľa účtu odlišné od CIF majiteľa investičného účtu. Upravte prosím výber účtu.'],
    ['pattern', 'Hodnota má nesprávny tvar.'],
    ['date_not_allowed', 'Dátum nie je povolený'],
    ['time_format', 'Čas musí mať formát hh:mm'],
  ]);

  constructor() {

  }

  ngOnChanges(change: SimpleChanges) {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    this.setError();
    this.sub = this.cc.statusChanges.subscribe(value => {
      this.setError();
    });
  }

  setError() {
    this.error = '';
    if (!!this.cc && !!this.cc.errors) {
      for (const key in this.cc.errors) {
        if (key !== 'required') {
          this.error = this.translate(key);
        }
      }
    } else {
      this.error = '';
    }
  }

  translate(key: string) {
    if (this.keys.has(key)) {
      return this.keys.get(key);
    }
    return key;
  }
}
