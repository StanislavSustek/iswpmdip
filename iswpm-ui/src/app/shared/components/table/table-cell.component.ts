import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  Input,
  OnInit, Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { TableCell } from './table-cell';
import { StringUtils } from '../../utils/string.utils';

@Component({
  selector: 'isw-table-cell',
  templateUrl: './table-cell.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableCellComponent<T> implements OnInit {

  @ViewChild('rendererContainer', { read: ViewContainerRef }) container;

  @Input() data;
  @Input() cell: TableCell<T>;
  @Output() onClick = new EventEmitter<{action: any, rowData: T}>();

  constructor(private resolver: ComponentFactoryResolver) {}

  ngOnInit() {
    if (this.cell.renderer) {
      const factory = this.resolver.resolveComponentFactory(this.cell.renderer);
      const componentRef = this.container.createComponent(factory);
      componentRef.instance.data = this.data;
      componentRef.instance.rendererData = this.cell.rendererData;
    }
  }

  onActionClick(action) {
    this.onClick.emit({ action: action, rowData: this.data });
  }

  textToHtml(text): string {
    return StringUtils.toHtml(text ? text + '' : '');
  }

}
