export interface TableCell<T> {
  label?: string;
  style?: string;
  action?: any;
  data?: T;
  icon?: string;
  icons?: {
    icon: string,
    action: string,
    style?: string,
  }[];
  renderer?: any;
  rendererData?: any;
  dropdown?: any;
}


