export interface TableCol {
  label: string;
  fieldName: string;
  fieldType?: 'string' | 'number' | 'localDate' | 'codelist' | 'custom';
  sortable?: boolean;
  labelSortFn?: (tableCol) => string;
  style?: string;
  content?: (tableCol, tableRow) => string;
  codeList?: string;
}
