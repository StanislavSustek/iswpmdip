import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'isw-table-cell-button',
  templateUrl: './table-cell-button.component.html',
})
export class TableCellButtonComponent implements OnInit {

  private _rendererData = null;

  get renderedData() {
    return this._rendererData;
  }

  @Input() set rendererData(v) {
    this._rendererData = v;
    this._rendererData.onClick(this.onClick);
  }

  @Output() onClick = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  click(e) {
    this.onClick.emit(e);
  }

}
