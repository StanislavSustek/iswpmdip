import { TableCol } from './table-col';
import { TableSortParams } from './table-sort-params';
import { TableRowAction } from "./table-row-action";

export interface TableSetup {
  cols: TableCol[];
  sort?: TableSortParams;
  primaryKey?: string;
  filter?: () => boolean;
  actions?: TableRowAction[];
}



