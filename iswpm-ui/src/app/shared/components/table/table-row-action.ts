export interface TableRowAction {
  id?: string;
  icon?: string;
  navigateTo?: (tableRow) => any;
  openModal?;
}
