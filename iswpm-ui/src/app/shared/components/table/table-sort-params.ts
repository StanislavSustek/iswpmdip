export interface TableSortParams {
  fieldName: string;
  ascending: boolean;
  async?: boolean;
  fieldType?: string;
  codelistName?: string;
  labelSortFn?: (v) => string;
}



