import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { TableCol } from './table-col';
import { TableSortParams } from './table-sort-params';
import { SortUtils } from '../../utils/sort.utils';
import { TableCell } from './table-cell';
import { CodelistPipe } from '../../pipe/codelist.pipe';
import { ContentService } from '../../services/content.service';

@Component({
  selector: 'isw-table',
  templateUrl: './table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent<T> {

  @Input() loading = true;
  @Input() withoutStyle = false;
  @Input() pagination = false;
  @Input() showMore = false;

  @Input() set noDataText(v) {
    if (v == null) {
      this._noDataText = 'Pre zadané kritéria neexistujú žiadne položky';
    } else {
      this._noDataText = v;
    }
  }

  isOpen = false;
  pageCount = 0;
  @Input() currentPage = 1;
  @Input() pageSize = 50;
  pages = [];
  components: any[];

  _noDataText = 'Pre zadané kritéria neexistujú žiadne položky';

  _data: T[] = [];
  @Input() set data(value: T[]) {
    this.setData(value);
  }

  private _tableData: { rowData: TableCell<T>[], checkId: string, dataObject: T, componentShowed: boolean[] }[];

  set tableData(v) {
    this._tableData = v;
  }

  get tableData() {
    return this._tableData;
  }

  checkId = 'id';
  sortParams: TableSortParams;
  @Input() error;


  private _checkable = false;

  @Input() set checkable(value) {
    this._checkable = value;
    if (!value) {
      this._checkedItems = [];
      this.checkedItems.emit(this._checkedItems);
    }
  }

  get checkable() {
    return this._checkable;
  }

  @Output() checkedItems = new EventEmitter();

  private _checkedItems = [];

  private _selectable = false;

  @Input() set selectable(value: boolean) {
    this._selectable = value;
    if (value) {
      this._selectedRow = null;
    }
  }

  get selectable() {
    return this._checkable;
  }

  private _selectedRow: T;

  setData(value: T[]) {
    if (value == null) {
      value = [];
    }
    if (!this.sortParams) {
      this.sortParams = this.getDefaultSortParams();
    }
    const arr = value && this.sortParams ? this.sort(value) : value;
    const td: { rowData: TableCell<T>[], checkId: string, dataObject: T, componentShowed: boolean[] }[] = [];
    this._data = arr;

    const componentShowedArr = [];
    if (this.components) {
      for (const comp of this.components) {
        componentShowedArr.push(false);
      }
    }

    if (this.pagination && value.length !== 0) {
      this.pages.splice(0, this.pages.length);
      this.pageCount = Math.floor(this._data.length / this.pageSize) + ((this._data.length % this.pageSize) > 0 ? 1 : 0);
      for (let i = 1; i <= this.pageCount; i++) {
        this.pages.push(i);
      }

      for (let i = (this.currentPage - 1) * this.pageSize; i < ((this.pageSize * this.currentPage) < this._data.length ? (this.pageSize * this.currentPage) : this._data.length); i++) {
        td.push({
          checkId: arr[i][this.checkId],
          dataObject: arr[i],
          rowData: this.getData(arr[i]),
          componentShowed: [...componentShowedArr],
        });
      }

      this.tableData = td;
    } else if (this.showMore && value.length !== 0 && !this.isOpen) {
      for (let i = 0; i < (arr.length < 10 ? arr.length : 10); i++) {
        td.push({
          checkId: arr[i][this.checkId],
          dataObject: arr[i],
          rowData: this.getData(arr[i]),
          componentShowed: [...componentShowedArr],
        });
      }
      this.tableData = td;

    } else {
      if (value.length === 0) {
        this.pageCount = 0;
        this.pages = [];
      }
      for (let i = 0; i < arr.length; i++) {
        td.push({
          checkId: arr[i][this.checkId],
          dataObject: arr[i],
          rowData: this.getData(arr[i]),
          componentShowed: [...componentShowedArr],
        });
      }
      this.tableData = td;
    }
    if (this.pagination && this.pageCount < this.currentPage) {
      this.getPageData(1);
    }
  }

  constructor(protected cd: ChangeDetectorRef, protected contentService: ContentService) {
  }

  getHeader(): TableCol[] {
    return [];
  }

  getColumnsCount(): number {
    return this.getHeader().length;
  }

  getData(row: any): TableCell<T>[] {
    return [];
  }

  getDefaultSortParams(): TableSortParams {
    return null;
  }

  get data() {
    return this._data;
  }

  onCheckboxChange(event, rowData: { rowData: TableCell<T>[], checkId: string, dataObject: T }) {
    const io = this._checkedItems.indexOf(rowData.checkId);
    if (io !== -1) {
      this._checkedItems.splice(io, 1);
    } else {
      this._checkedItems.push(rowData.checkId);
    }
    this.checkedItems.emit(this._checkedItems);
  }

  onSort(tableCol: TableCol) {
    if (this.sortParams == null) {
      this.sortParams = {
        fieldName: null,
        ascending: true
      };
    }
    this.sortParams.ascending = tableCol.fieldName === this.sortParams.fieldName ? !this.sortParams.ascending : true;
    this.sortParams.fieldName = tableCol.fieldName;
    this.sortParams.fieldType = tableCol.fieldType ? tableCol.fieldType : 'string';
    this.sortParams.codelistName = tableCol.codeList ? tableCol.codeList : null;
    this.sortParams.labelSortFn = tableCol.labelSortFn;
    this.data = this.data;
  }

  sort(data: T[]) {
    data.sort(SortUtils.getSortFunction(
      this.sortParams.fieldType,
      this.sortParams.fieldName,
      this.sortParams.ascending,
      new CodelistPipe(this.contentService),
      this.sortParams.codelistName,
      this.sortParams.labelSortFn
    ));
    return data;
  }

  cellClick(event, componentShowed) {
    this.onActionClick(event.action, event.rowData, componentShowed);
  }

  onActionClick(action, rowData, componentShowed) {
  }

  selectedRow(tableRow): boolean {
    return false;
  }

  onRowClick(tableRow, componentShowed) {
    if (this._selectable) {
      this._selectedRow = tableRow;
    }
  }

  getPageData(item?: number, option?: 'PREV' | 'NEXT') {
    if (option === 'PREV') {
      if (this.currentPage === 1) {
        return;
      }
      this.currentPage--;
    } else if (option === 'NEXT') {
      if (this.currentPage === this.pageCount) {
        return;
      }
      this.currentPage++;
    } else {
      this.currentPage = item;
    }
    const td = [];
    for (let i = this.pageSize * (this.currentPage - 1); i < ((this.pageSize * this.currentPage) < this._data.length ? (this.pageSize * this.currentPage) : this._data.length); i++) {
      const componentShowedArr = [];
      if (this.components) {
        for (const comp of this.components) {
          componentShowedArr.push(false);
        }
      }
      td.push({
        checkId: this._data[i][this.checkId],
        dataObject: this._data[i],
        rowData: this.getData(this._data[i]),
        componentShowed: [...componentShowedArr]
      });
    }
    this.tableData = td;
  }

  showMoreData() {
    const td = [];
    this.isOpen = !this.isOpen;
    if (this.tableData.length === 10) {
      for (let i = 0; i < this._data.length; i++) {
        td.push({
          checkId: this._data[i][this.checkId],
          dataObject: this._data[i],
          rowData: this.getData(this._data[i])
        });
      }
      this.tableData = td;

    } else {
      for (let i = 0; i < 10; i++) {
        td.push({
          checkId: this._data[i][this.checkId],
          dataObject: this._data[i],
          rowData: this.getData(this._data[i])
        });
      }

      this.tableData = td;
    }
  }

  get noDataText() {
    return this._noDataText;
  }
}

