import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: '[isw-validation-group]',
  templateUrl: './validation-group.component.html'
})
export class ValidationGroupComponent {

  @Input() cc: AbstractControl;
  @Input() decorations = true;
  @Input() loading = false;

  constructor() {

  }

}
