import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'isw-text-field',
  templateUrl: './text-field.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TextFieldComponent),
    multi: true
  }]
})
export class TextFieldComponent implements ControlValueAccessor {

  @Input() placeholder: string = '';
  @Input() type: string = 'text';
  @Input() darkmode: boolean = true;
  @Input() shadow: boolean = true;
  disabled: boolean;
  private _innerValue = '';
  onChange: any = () => {
  };
  onTouch: any = () => {
  };

  set value(val: any) {
    if (val && val !== this._innerValue) {
      this._innerValue = val;
      this.onChange(val);
      this.onTouch(val);
    }
  }

  get value() {
    return this._innerValue;
  }

  constructor() {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }


}
