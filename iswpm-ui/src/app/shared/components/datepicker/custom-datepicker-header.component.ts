import { ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MatCalendar, MatDateFormats } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'isw-custom-datepicker-header',
  templateUrl: './custom-datepicker-header.component.html',
})
export class CustomDatepickerHeaderComponent<D> implements OnDestroy, OnInit {
  private destroyed = new Subject<void>();
  formGroup: FormGroup;

  monthList: { key, label }[] = [
    { key: 0, label: 'Jan' },
    { key: 1, label: 'Feb' },
    { key: 2, label: 'Mar' },
    { key: 3, label: 'Apr' },
    { key: 4, label: 'Máj' },
    { key: 5, label: 'Jún' },
    { key: 6, label: 'Júl' },
    { key: 7, label: 'Aug' },
    { key: 8, label: 'Sep' },
    { key: 9, label: 'Okt' },
    { key: 10, label: 'Nov' },
    { key: 11, label: 'Dec' }
  ];

  months: { key, label }[] = this.getMonths();
  years: { key, label }[] = this.getYears();

  constructor(
    private calendar: MatCalendar<D>,
    private dateAdapter: DateAdapter<D>,
    @Inject(MAT_DATE_FORMATS) private dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef,
    private fb: FormBuilder) {
    calendar.stateChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => cdr.markForCheck());
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      monthSelect: this.dateAdapter.getMonth(this.calendar.activeDate),
      yearSelect: this.dateAdapter.getYear(this.calendar.activeDate),
    });

    this.formGroup.controls['monthSelect'].valueChanges.pipe(takeUntil(this.destroyed)).subscribe(value => {
      const months = value - this.dateAdapter.getMonth(this.calendar.activeDate);
      this.calendar.activeDate = this.dateAdapter.addCalendarMonths(this.calendar.activeDate, months);
    });
    this.formGroup.controls['yearSelect'].valueChanges.pipe(takeUntil(this.destroyed)).subscribe(value => {
      const years = value - this.dateAdapter.getYear(this.calendar.activeDate);
      this.calendar.activeDate = this.dateAdapter.addCalendarYears(this.calendar.activeDate, years);
      this.months = this.getMonths();
      const activeMonth = this.dateAdapter.getMonth(this.calendar.activeDate);
      if (this.months.find(x => x.key === activeMonth)) {
        this.formGroup.get('monthSelect').setValue(activeMonth);
      } else {
        this.formGroup.get('monthSelect').setValue(this.months[0].key);
      }
    });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  get periodLabel() {
    return this.dateAdapter
      .format(this.calendar.activeDate, this.dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }

  previousClicked() {
    this.calendar.activeDate = this.dateAdapter.addCalendarMonths(this.calendar.activeDate, -1);
    this.addYearToSelectIfMissing();
    this.formGroup.controls['monthSelect'].setValue(this.dateAdapter.getMonth(this.calendar.activeDate));
    this.formGroup.controls['yearSelect'].setValue(this.dateAdapter.getYear(this.calendar.activeDate));
  }

  private addYearToSelectIfMissing() {
    const yearInSelect = this.dateAdapter.getYear(this.calendar.activeDate);
    if (!this.years.some(x => x.key === yearInSelect)) {
      this.years.push({ key: yearInSelect, label: yearInSelect });
      this.years.sort(((a, b) => a.key > b.key ? 0 : -1));
    }
  }

  nextClicked() {
    this.calendar.activeDate = this.dateAdapter.addCalendarMonths(this.calendar.activeDate, 1);
    this.addYearToSelectIfMissing();
    this.formGroup.controls['monthSelect'].setValue(this.dateAdapter.getMonth(this.calendar.activeDate));
    this.formGroup.controls['yearSelect'].setValue(this.dateAdapter.getYear(this.calendar.activeDate));
  }

  getYears() {
    let minYear = moment().subtract(10, 'years').year();
    let maxYear = moment().add(10, 'years').year();

    const minDate = this.calendar.minDate;
    const maxDate = this.calendar.maxDate;

    if (minDate) {
      minYear = moment(minDate).toDate().getFullYear();
    } else {
      const activeMinYear = moment(this.calendar.activeDate).toDate().getFullYear();
      if (activeMinYear < minYear) {
        minYear = activeMinYear;
      }
    }

    if (maxDate) {
      maxYear = moment(maxDate).toDate().getFullYear();
    } else {
      const activeMaxYear = moment(this.calendar.activeDate).toDate().getFullYear();
      if (activeMaxYear > maxYear) {
        maxYear = activeMaxYear;
      }
    }

    const dates = [];
    for (let i = minYear; i <= maxYear; i++) {
      dates.push({ key: i, label: i.toString() });
    }
    return dates;
  }

  getMonths() {
    let minDateMoment: moment.Moment;
    let maxDateMoment: moment.Moment;
    const activeDateMoment = moment(this.calendar.activeDate);

    const months = [];

    if (this.calendar.minDate) {
      minDateMoment = moment(this.calendar.minDate);
    } else {
      if (activeDateMoment < moment().subtract(10, 'years')) {
        minDateMoment = activeDateMoment.startOf('year');
      } else {
        minDateMoment = moment().subtract(10, 'years').startOf('year');
      }
    }

    if (this.calendar.maxDate) {
      maxDateMoment = moment(this.calendar.maxDate);
    } else {
      if (activeDateMoment > moment().add(10, 'years')) {
        maxDateMoment = activeDateMoment.endOf('year');
      } else {
        maxDateMoment = moment().add(10, 'years').endOf('year');
      }
    }

    while (+minDateMoment.format('YYYYMM') <= +maxDateMoment.format('YYYYMM')) {
      if (activeDateMoment.format('YYYY') === minDateMoment.format('YYYY')) {
        months.push(+minDateMoment.format('MM') - 1);
      }
      minDateMoment.add(1, 'month');
    }

    return this.monthList.filter(x => months.some(month => month === x.key));
  }
}
