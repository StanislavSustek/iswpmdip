import { Component, ElementRef, EventEmitter, forwardRef, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateAdapter, MatDatepickerInputEvent } from '@angular/material';
import { CustomDatepickerHeaderComponent } from './custom-datepicker-header.component';
import * as moment from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatepickerComponent),
  multi: true
};

@Component({
  selector: 'isw-datepicker',
  templateUrl: './datepicker.component.html',
  providers: [
    CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR,
  ]
})
export class DatepickerComponent<D> implements ControlValueAccessor, OnInit {

  private innerModel = null;

  @ViewChild(MatDatepicker) picker: MatDatepicker<D>;
  @Output() blur = new EventEmitter();
  @Input() minDate;
  @Input() maxDate;
  @Input() placeholder: string = null;
  @Input() filterDates: Date[];
  @Input() forbiddenDates: Date[];
  @Input() filterWeekends = false;
  datepickerHeader = CustomDatepickerHeaderComponent;
  disabled = false;

  onChange: any = () => {};
  onTouch: any = () => {};

  constructor(private dateAdapter: DateAdapter<D>, private elementRef: ElementRef) {

  }

  ngOnInit(): void {
    this.dateAdapter.getFirstDayOfWeek = () => {
      return 1;
    };
    this.dateAdapter.setLocale('sk');
  }

  get value(): any {
    return this.innerModel;
  }

  set value(v: any) {
    this.innerModel = v;
    this.onChange(this.innerModel);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  onDateChange(event: MatDatepickerInputEvent<any>) {
    this.value = event.value;
  }

  change(event: MatDatepickerInputEvent<any>) {
    if (this.onChange) {
      this.onChange(event ? event.value : null);
    }
    if (this.onTouch) {
      this.onTouch(event ? event.value : null);
    }
  }

  dateClass = (d: Date) => {
    return 'isw-datepicker__calendar';
  }

  dateFilter = (date: Date) => {
    let isWeekend = null;
    let isFiltered = null;
    if (this.filterWeekends) {
      isWeekend = moment(date).get('day') !== 6 && moment(date).get('day') !== 0;
    }
    if (this.filterDates || this.forbiddenDates) {
      let dat = null;
      let forbiddenDat = null;
      if (this.filterDates) {
        dat = this.filterDates.map(v => moment(v).format('YYYY-MM-DD'));
      }
      if (this.forbiddenDates) {
        forbiddenDat = this.forbiddenDates.map(v => moment(v).format('YYYY-MM-DD'));
      }
      isFiltered = (dat ? dat.includes(moment(date).format('YYYY-MM-DD')) : true)
        && (forbiddenDat ? (!forbiddenDat.includes(moment(date).format('YYYY-MM-DD'))) : true);
    }

    return (isWeekend != null ? isWeekend : true) && (isFiltered != null ? isFiltered : true);
  }

  @HostListener('document:scroll', ['$event'])
  scrollout(event) {
    if (this.picker.opened) {
      this.picker.close();
    }
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.picker.opened) {
      if (this.elementRef.nativeElement.contains(event.target)) {
        return;
      }
      let par = event.target;
      let found = false;
      while (par.parentElement && !found) {
        if (par.classList && (par.classList.contains('mat-datepicker-content') || par.classList.contains('mat-menu-content'))) {
          found = true;
        }
        par = par.parentElement;
      }
      if (!found) {
        this.picker.close();
      }
    }
  }

  openPicker() {
    this.picker.open();
  }

}
