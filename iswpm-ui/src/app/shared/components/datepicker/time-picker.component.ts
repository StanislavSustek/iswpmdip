import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostListener,
  Input
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'isw-time-picker',
  templateUrl: './time-picker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimePickerComponent),
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimePickerComponent implements ControlValueAccessor {
  @Input() placeholder = '';
  innerValue: moment.Moment | string = moment();
  timeDropdownShowed = false;
  disabled: boolean = false;
  onChange: any = () => {
  };
  onTouch: any = () => {
  };

  constructor(private elementRef: ElementRef, private changeDetectorRef: ChangeDetectorRef) {
  }

  set value(val: string | moment.Moment) {
    if (val !== undefined && val !== null && moment(val, 'HH:mm').isValid()
      && ((((<string>val).length > 3) && (/^[\d]+$/.test(<string>val)))
        || (((<string>val).length > 4) && (/[\d][\d]:[\d][\d]$/.test(<string>val))))) {
      let time: string = (val as string);
      if (!time.includes(':')) {
        time = time.slice(0, 2) + ':' + time.slice(2);
      }
      this.onChange(time);
      this.innerValue = moment(time, 'HH:mm');
    } else if (val instanceof moment) {
      this.onChange((val as moment.Moment).format('HH:mm'));
      this.innerValue = val as moment.Moment;
    } else {
      this.onChange(val);
      this.innerValue = val;
    }
  }

  get value() {
    if (this.innerValue instanceof moment) {
      return (this.innerValue as moment.Moment).format('HH:mm');
    } else {
      return this.innerValue;
    }
  }

  get time() {
    if (this.innerValue instanceof moment) {
      return (this.innerValue as moment.Moment).format('HH:mm');
    } else {
      return this.innerValue;
    }
  }

  get hours(): string {
    if (this.innerValue instanceof moment) {
      return (this.innerValue as moment.Moment).format('HH');
    } else {
      return '00';
    }
  }

  get minutes(): string {
    if (this.innerValue instanceof moment) {
      return (this.innerValue as moment.Moment).format('mm');
    } else {
      return '00';
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: moment.Moment | string): void {
    if (obj !== null && obj !== undefined) {
      this.value = obj;
    } else {
      this.value = null;
    }
    this.changeDetectorRef.markForCheck();
  }

  toggleTimePicker() {
    this.timeDropdownShowed = !this.timeDropdownShowed;
  }

  private setTimeToMidnight() {
    let date: moment.Moment = this.innerValue instanceof moment ? (this.innerValue as moment.Moment) : null;
    if (!date) {
      date = moment().set({ hours: 0, minutes: 0 });
    }
    return date;
  }

  addHour() {
    let date = this.setTimeToMidnight();
    this.value = date.add(1, 'hour').format('HH:mm');
  }

  subtractHour() {
    let date = this.setTimeToMidnight();
    this.value = date.subtract(1, 'hour').format('HH:mm');
  }

  addMinute() {
    let date = this.setTimeToMidnight();
    this.value = date.add(1, 'minute').format('HH:mm');
  }

  subtractMinute() {
    let date = this.setTimeToMidnight();
    this.value = date.subtract(1, 'minute').format('HH:mm');
  }


  @HostListener('document:scroll', ['$event'])
  scrollout(event) {
    if (this.timeDropdownShowed) {
      this.toggleTimePicker();
    }
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.timeDropdownShowed) {
      if (this.elementRef.nativeElement.contains(event.target)) {
        return;
      }
      let par = event.target;
      let found = false;
      while (par.parentElement && !found) {
        if (par.classList && (par.classList.contains('timepicker-dropdown'))) {
          found = true;
        }
        par = par.parentElement;
      }
      if (!found) {
        this.toggleTimePicker();
      }
    }
  }

  wheelHours(event: WheelEvent) {
    if (event.deltaY > 0) {
      this.addHour();
    }
    if (event.deltaY < 0) {
      this.subtractHour();
    }
  }

  wheelMinutes(event: WheelEvent) {
    if (event.deltaY > 0) {
      this.addMinute();
    }
    if (event.deltaY < 0) {
      this.subtractMinute();
    }
  }
}
