import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  templateUrl: './information-modal.component.html',
  styleUrls: ['./information-modal.component.scss']
})
export class InformationModalComponent {

  @Input() header = '';
  @Input() content = '';
  @Input() type: 'success' | 'confirm' | 'info' | 'cancel' = 'info';

  constructor(public activeModal: MatDialogRef<InformationModalComponent>) {
  }

}
