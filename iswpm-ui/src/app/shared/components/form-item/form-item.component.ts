import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'isw-form-item',
  templateUrl: './form-item.component.html',
})
export class FormItemComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  @Input() labelText;
  @Input() width = 12;
  @Input() labelWidth = 0;
  @Output() onClick = new EventEmitter();
  @Input() layout: 'vertical' | 'horizontal' = 'horizontal';
  @Input() required: boolean;
  @Input() control: AbstractControl;
  @Input() loading: false;

  constructor() {
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get labelWidthClass(): string {
    if (this.layout === 'horizontal') {
      return 'col-' + this.labelWidth.toString();
    } else {
      return 'col-12';
    }
  }

  get valueWidthClass(): string {
    if (this.layout === 'horizontal') {
      return 'col-' + (12 - this.labelWidth).toString();
    } else {
      return 'col-12';
    }
  }

  get widthClass(): string {
    if (this.width > 0 && this.width <= 12){
      return 'col-' + this.width.toString();
    }
    return 'col-12';
  }
}
