import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'isw-refresh-data',
  templateUrl: './refresh-data.component.html'
})
export class RefreshDataComponent implements OnInit {

  @Input() show = null;
  @Output() onCancel = new EventEmitter();
  @Output() onRestore = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onCancelClick() {
    this.onCancel.emit();
  }

  onRestoreClick() {
    this.onRestore.emit();
  }

}
