import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Host,
  Input,
  OnInit,
  Optional,
  Renderer2,
  SkipSelf
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  ControlValueAccessor,
  FormArray,
  FormGroup,
  FormGroupDirective,
  FormGroupName,
  NG_VALUE_ACCESSOR
} from '@angular/forms';

export const SELECT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectComponent),
  multi: true,
};

@Component({
  selector: 'isw-select',
  templateUrl: './select.component.html',
  providers: [
    SELECT_VALUE_ACCESSOR,
    { provide: AbstractControl, useExisting: forwardRef(() => SelectComponent) },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent implements ControlValueAccessor, OnInit {

  innerValue;
  onChange;
  onTouched;
  form: FormGroup;
  originalData;

  @Input() placeholder = '-- vyberte si --';
  @Input() keyField = 'key';
  @Input() labelField = 'label';
  @Input() readonly = false;
  @Input() searching = false;
  @Input() customItemClass = '';
  @Input() multi = false;
  @Input() noSelectionText = '-- vyberte si --';
  @Input() allSelectedText: string;

  private _selectionRequired = false;

  @Input() set selectionRequired(value: boolean) {
    this._selectionRequired = value;
    if (this._selectionRequired && this._data) {
      const defautValue = this._data.shift();
      if (defautValue['key'] != null) {
        this._data.unshift(defautValue);
      }
    }
  }

  get selectionRequired() {
    return this._selectionRequired;
  }

  private _data: any[];

  @Input() set data(value: any[]) {
    const all = {};
    const none = {};
    const custom = {};
    all[this.keyField] = '_ALL_';
    all[this.labelField] = 'všetky';
    none[this.keyField] = '_NONE_';
    none[this.labelField] = 'žiadny';
    custom[this.keyField] = null;
    custom[this.labelField] = this.noSelectionText;

    if (this.multi) {
      this._data = [all, none, ...value];
    } else {
      if (!this.selectionRequired) {
        this._data = [custom, ...value];
      } else {
        this._data = value;
      }
    }
  }

  get data(): any[] {
    return this._data;
  }

  private _isDisabled = false;
  @Input() formControlName: string;
  control: AbstractControl;
  text = '';

  constructor(private renderer: Renderer2,
              private cd: ChangeDetectorRef,
              @Optional() @Host() @SkipSelf() private controlContainer: ControlContainer,
  ) {
  }

  ngOnInit() {
    if (this.searching) {
      this.originalData = this._data;
    }
    if (this.formControlName && this.controlContainer instanceof FormGroupName && this.controlContainer.formDirective != null) {
      if (this.controlContainer.formDirective instanceof FormGroupDirective) {

        const getContainerControls = (path: string[], controls: { [key: string]: AbstractControl } | AbstractControl[]): { [key: string]: AbstractControl } | AbstractControl[] => {
          const pathItem = path[0];
          const control = controls[pathItem];

          let containerControls: { [key: string]: AbstractControl } | AbstractControl[];

          if (control instanceof FormGroup) {
            containerControls = (<FormGroup>control).controls;
          } else if (control instanceof FormArray) {
            containerControls = (<FormArray>control).controls;
          } else {
            throw new Error('Unexpected AbstractControl type on input')
          }

          if (path.length > 1) {
            path.shift();
            return getContainerControls(path, containerControls);
          } else {
            return containerControls;
          }
        };

        const container = getContainerControls(this.controlContainer.path.slice(), (<FormGroupDirective>this.controlContainer.formDirective).form.controls);
        this.control = container[this.formControlName];
      }
    } else {
      if (this.formControlName && this.controlContainer instanceof FormGroupDirective) {
        this.control = (<FormGroupDirective>this.controlContainer).form.controls[this.formControlName];
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._isDisabled = isDisabled;
    this.cd.detectChanges();
  }

  get disabledInput() {
    return this._isDisabled;
  }

  writeValue(obj: any): void {
    if (!obj && this.multi) {
      this.innerValue = [];
    } else {
      this.innerValue = obj;
    }
    this.cd.markForCheck();
  }

  onItemClick(option, event) {
    if (this.multi) {
      event.stopPropagation();
      if (option[this.keyField] === '_ALL_') {
        this.innerValue.splice(0, this.innerValue.length);
        this.data.forEach(x => {
          if (x.key != null && x.key !== '_ALL_' && x.key !== '_NONE_') {
            this.innerValue.push(x.key);
          }
        });
      } else if (option[this.keyField] === '_NONE_') {
        this.innerValue.splice(0, this.innerValue.length);
      } else if (option[this.keyField] == null) {

      } else {
        const indexOf = this.innerValue.indexOf(option[this.keyField]);
        if (indexOf === -1) {
          this.innerValue.push(option[this.keyField]);
        } else {
          this.innerValue.splice(indexOf, 1);
        }
      }
    } else {
      this.innerValue = option[this.keyField];
    }
    this.change(event);
  }

  change(event) {
    if (this.onChange) {
      this.onChange(this.innerValue);
    }
    if (this.onTouched) {
      this.onTouched(this.innerValue);
    }
    this.cd.detectChanges();
  }

  getChoosedlabel() {
    if (this.data) {
      if (this.multi && this.innerValue != null) {
        if (this.allSelectedText != null && this.innerValue.length + 2 === this.data.length) {
          return this.allSelectedText;
        }
        const label = this.data.filter(v => this.innerValue.includes(v.key)).map(v => v.label).join(', ');
        if (label === '') {
          return this.noSelectionText;
        }
        return label;
      } else {
        if (this.innerValue == null) {
          if (!this.multi && this.disabledInput) {
            return '';
          }
          return this.noSelectionText;
        }
        if (this.data.find(v => v.key === this.innerValue || v.label === this.innerValue)) {
          return this.data.find(v => v.key === this.innerValue || v.label === this.innerValue)[this.labelField];
        }
      }
    } else {
      if (!this.selectionRequired && !this.multi) {
        return this.placeholder;
      }
    }
    return this.noSelectionText;
  }

  disableSelectClick() {
    if (this.disabledInput) {
      event.stopPropagation();
    }
  }

  filterSelectData(value) {
    if (value === '') {
      this._data = this.originalData;
    } else {
      const newData = this.originalData.filter(x => x.label.toLowerCase().indexOf(value) !== -1);
      this._data = newData.length !== 0 ? newData : [{ key: null, label: this.noSelectionText }];
    }
  }

  isSelectedOption(key) {
    if (key === '_ALL_') {
      return true;
    }
    if (this.multi) {
      if (this.innerValue) {
        return this.innerValue.includes(key);
      }
    } else {
      return key === this.innerValue;
    }
  }
}
