import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';

@Component({
  selector: 'isw-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent {

  @HostBinding('class.isw-button-look') buttonLook = true;

  private _disabled = false;

  @Input() set disabled(value) {
    this._disabled = value;
  }

  get disabled() {
    return this._disabled;
  }

  @Input() type: 'primary' | 'secondary' | 'icon' = 'primary';
  @Input() icon: 'search' | 'filter' | 'export';
  @Input() isActive = false;
  @Input() customClass = '';
  @Input() buttonType: 'submit' | 'button' = 'button';

  @Output() onClick = new EventEmitter();

  constructor() {
  }

  onButtonClick(event) {
    if (!this.disabled) {
      this.onClick.emit(event);
    }
  }

  get isIconButton() {
    return this.type === 'icon';
  }

  getIconType() {
    switch (this.icon) {
      case 'search': return 'fa-search';
      case 'filter': return 'fa-tasks';
      case 'export': return 'fa-file-excel-o';
      default: return '';
    }
  }


}
