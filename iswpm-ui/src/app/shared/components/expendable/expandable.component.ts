import { Component, Input } from '@angular/core';

@Component({
  selector: 'isw-expandable',
  templateUrl: './expandable.component.html',
})
export class ExpandableComponent {
  @Input() rowData: any;
  @Input() componentsShowed: boolean[];
  @Input() loading = true;
  @Input() errorMessage = null;
  @Input() width: number = null;
  @Input() containerClass = 'table-panel';

  constructor() {
  }

  getWidth() {
    if (!this.width) {
      return `100%`;
    } else {
      return `${this.width}px`;
    }
  }

}
