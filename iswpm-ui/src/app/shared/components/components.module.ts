import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextFieldComponent } from './textfield/text-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
import { TableCellComponent } from './table/table-cell.component';
import { TableCellButtonComponent } from './table/table-cell-button.component';
import { DynamicModule } from 'ng-dynamic-component';
import { ExpandableComponent } from './expendable/expandable.component';
import { ButtonComponent } from './button/button.component';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatButtonModule,
  MatDatepickerModule,
  MatMenuModule
} from '@angular/material';
import { RefreshDataComponent } from './refresh-data/refresh-data.component';
import { InformationModalComponent } from './information-modal/information-modal.component';
import { SelectComponent } from './select/select.component';
import { FormItemComponent } from './form-item/form-item.component';
import { ValidationGroupComponent } from './validation-group.component';
import { ValidationMessageComponent } from './validation-message.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { CustomDatepickerHeaderComponent } from './datepicker/custom-datepicker-header.component';
import { TimePickerComponent } from './datepicker/time-picker.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { UserNamePipe } from '../pipe/user.pipe';
import { CodelistPipe } from '../pipe/codelist.pipe';


const myFormat = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD.MM.YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  declarations: [
    TextFieldComponent,
    TableComponent,
    TableCellComponent,
    TableCellButtonComponent,
    ExpandableComponent,
    ButtonComponent,
    RefreshDataComponent,
    InformationModalComponent,
    SelectComponent,
    FormItemComponent,
    ValidationGroupComponent,
    ValidationMessageComponent,
    DatepickerComponent,
    CustomDatepickerHeaderComponent,
    TimePickerComponent,
    UserNamePipe,
    CodelistPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    DynamicModule.withComponents([]),
    MatMenuModule,
    MatButtonModule,
    MatDatepickerModule,
    ReactiveFormsModule,

  ],
  exports: [
    CommonModule,
    SelectComponent,
    TextFieldComponent,
    TableComponent,
    TableCellComponent,
    TableCellButtonComponent,
    ExpandableComponent,
    ButtonComponent,
    DynamicModule,
    RefreshDataComponent,
    InformationModalComponent,
    FormItemComponent,
    ValidationGroupComponent,
    ValidationMessageComponent,
    DatepickerComponent,
    CustomDatepickerHeaderComponent,
    TimePickerComponent,
    MatDatepickerModule,
    ReactiveFormsModule,
    UserNamePipe,
    CodelistPipe,
  ],
  entryComponents: [
    TableCellButtonComponent,
    InformationModalComponent,
    CustomDatepickerHeaderComponent
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: myFormat}
  ]
})
export class ComponentsModule { }
