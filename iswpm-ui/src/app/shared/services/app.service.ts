import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../backend/entity/user';
import { CompanyPermision } from '../backend/entity/company-permision';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  user = new BehaviorSubject<User>(null);
  company = new BehaviorSubject<CompanyPermision>(null);

  constructor(
  ) { }

  clear() {
    this.user.next(null);
    this.company.next(null);
  }

  hasPermision(val: string) {
    return this.company.getValue() && this.company.getValue().permisions && this.company.getValue().permisions.includes(val);
  }
}
