import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  projectStatuses = [
    {code: 1, value: 'OPEN'},
    {code: 2, value: 'COMPLETED'},
    {code: 3, value: 'IN_PROGRESS'},
    {code: 4, value: 'STOPPED'},
    {code: 5, value: 'DELETED'},
  ];

  codelists = new Map<string, {code: any, value: any, sk?: any}[]>();

  constructor() {
    this.codelists.set('TASK-STATUS', [
      {code: 1, value: 'new', sk: 'Nová'},
      {code: 4, value: 'in_progress', sk: 'Rieši sa'},
      {code: 5, value: 'resolved', sk: 'Vyriešená'},
      {code: 2, value: 'closed', sk: 'Zatvorená'},
      {code: 6, value: 'deleted', sk: 'Zmazaná'},
      // {code: 3, value: 'rejected', sk: 'Zrušená'},
    ]);
    this.codelists.set('USER-STATE', [
      {code: 1, value: 'ACTIVE', sk: 'Atívny'},
      {code: 2, value: 'DELETED', sk: 'Zmazaný'},
      {code: 3, value: 'BLOCKED', sk: 'Blokovaný'},
      {code: 4, value: 'BLOCKED_PASSWORD_CHANGING', sk: 'Neaktívny - Zmena hesla'},
      // {code: 3, value: 'rejected', sk: 'Zrušená'},
    ]);
    this.codelists.set('HOLIDAY-STATE', [
      {code: 1, value: 'REQUESTED', sk: 'Požiadaná'},
      {code: 2, value: 'CONFIRMED', sk: 'Potvrdená'},
      {code: 3, value: 'DENIED', sk: 'Zamietnutá'},
      {code: 4, value: 'ACTIVE', sk: 'Aktívna'},
      {code: 5, value: 'ENDED', sk: 'Ukončená'},
    ]);
    this.codelists.set('HOLIDAY-TYPE', [
      {code: 1, value: 'HOLIDAY', sk: 'Dovolenka'},
      {code: 2, value: 'SICKNESS_ABSENCE', sk: 'PN'},
      {code: 3, value: 'UNPAID_HOLIDAY', sk: 'Neplatené voľno'},
      {code: 4, value: 'DOCTOR', sk: 'Lekárske vyšetrenie'},
    ]);
  }

  getCodelist(name: string): any[] {
    return this.codelists.get(name);
  }

  getCodelistForDropdown(key: string, sk = false): {key: any, label: any}[] {
    return this.codelists.get(key).map(x => ({key: x.code, label: sk ? x.sk : x.value}));
  }

  getCodelistValue(key: any, value, sk = false) {
    const code = this.codelists.get(key).find(x => x.code === value);
    return code ? (sk ? code.sk : code.value) : '';
  }

}
