import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable()

export class PmSocketService {

  SERVER_URL = 'ws://localhost:8080/socket';
  constructor() { }

  socket;

  public initSocket(): void {
    this.socket = socketIo(this.SERVER_URL);
  }

  public send(message): void {
    this.socket.emit('/hello', message);
  }

  public onMessage(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('topic/greetings', (data: {name: string}) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>(observer => {
      this.socket.on(event, () => observer.next());
    });
  }
}
