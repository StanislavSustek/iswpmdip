import { Injectable } from '@angular/core';
import { PmSocket2Service } from './pm-socket2.service';
import { BehaviorSubject } from 'rxjs';
import { Company } from '../backend/entity/company';
import { Project } from '../backend/entity/project';
import { MessageChannelResponse } from '../backend/entity/message-channel-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  messages: BehaviorSubject<any> = new BehaviorSubject(null);
  userCompanies: BehaviorSubject<Company[]> = new BehaviorSubject(null);
  userProjects: BehaviorSubject<Project[]> = new BehaviorSubject(null);
  refresh = new BehaviorSubject<MessageChannelResponse>(null);

  constructor(private socketService: PmSocket2Service) {
  }

  clear() {
    this.messages.next(null);
    this.userCompanies.next(null);
    this.userProjects.next(null);
  }

  initMessageChannel(withRefresh = false) {
    if (!this.messages.getValue()) {
      this.socketService.stream('getMessages', true).subscribe(val => {
        if (val) {
          this.messages.next(val);
          console.log(val);
        }
      });
      if (withRefresh) {
        this.refreshMessage();
      }
    }
  }

  initUserCompaniesChannel(withRefresh = false) {
    if (!this.userCompanies.getValue()) {

      this.socketService.stream('getUserCompanies', true).subscribe(val => {
        if (val) {
          this.userCompanies.next(val);
          console.log('COMPANY_USER', val);
        }
      });
      if (withRefresh) {
        this.refreshUserCompanies();
      }
    }
  }

  initUserProjectsChannel(withRefresh = false) {
    if (!this.userProjects.getValue()) {

      this.socketService.stream('getProjects', true).subscribe(val => {
        if (val) {
          this.userProjects.next(val);
          console.log('COMPANY_USER', val);
        }
      });
      if (withRefresh) {
        this.refreshUserProjects();
      }
    }
  }

  refreshMessage() {
    this.socketService.send('/message/getMessages', {});
  }

  refreshUserProjects() {
    this.socketService.send('/projects/getUserProjects', {});
  }

  refreshUserCompanies() {
    this.socketService.send('/company/getUserCompanies', {});
  }

  markAsRead(messageId: string) {
    this.socketService.send('/message/markAsRead', {messageId: messageId});
  }

  deleteMessage(messageId: string) {
    this.socketService.send('/message/deleteMessage', {messageId: messageId});
  }

  refreshCompany() {
    this.socketService.send('/admin/getCompanies', {companyType: 'ALL'});
  }

  saveCompany(company: Company) {
    this.socketService.send('/admin/createCompany', company);
  }

  deleteCompany(company: Company) {
    this.socketService.send('/admin/deleteCompany', company);
  }

  initRefreshChannel() {
    if (this.refresh.getValue() == null) {
      this.socketService.stream('refreshChanel', false).subscribe(val => {
        this.refresh.next(val);
      })
    }
  }
}
