import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { InformationModalComponent } from '../components/information-modal/information-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor(private matDialog: MatDialog) {
  }

  openInformationModal(header: string, content: string, type: 'success' | 'confirm' | 'info' | 'cancel'): Observable<any> {

    const ref: MatDialogRef<InformationModalComponent> = this.matDialog.open(
      InformationModalComponent,
      {
        panelClass: 'information-modal',
        disableClose: true,
        autoFocus: false
      }
    );
    ref.componentInstance.header = header;
    ref.componentInstance.content = content;
    ref.componentInstance.type = type;

    return ref.afterClosed();
  }

  openCustomModal(component, panelClass: string | string[] = 'modal-md' , disableClose = true, autoFocus = false): MatDialogRef<any> {
    return this.matDialog.open(
      component,
      {
        panelClass: panelClass ? panelClass : null,
        disableClose: disableClose,
        autoFocus: autoFocus
      }
    );
  }

  closeAll() {
    this.matDialog.closeAll();
  }
}
