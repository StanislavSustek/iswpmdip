import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PmSocket2Service } from './pm-socket2.service';
import { AppService } from './app.service';
import { CompanyService } from '../../company/company.service';
import { AdminService } from '../../admin/admin.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private socketService: PmSocket2Service,
    private appService: AppService,
    private companyService: CompanyService,
    private adminService: AdminService
  ) { }

  isLoggedIn(): boolean {
    let token = localStorage.getItem('token');
    return !!token;
  }

  logout(backend = false) {
    this.appService.clear();
    this.companyService.clear();
    this.adminService.clear();

    if (backend) {
      this.socketService.send('/security/logout', {})
    }
    this.appService.user.next(null);
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
