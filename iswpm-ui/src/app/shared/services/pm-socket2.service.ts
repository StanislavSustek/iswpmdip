import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { StompConfig, StompService, StompState } from '@stomp/ng2-stompjs';
import { map } from 'rxjs/operators';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root'
})
export class PmSocket2Service {

  private stompService: StompService;

  constructor(
    private appService: AppService,
  ) {
  }

  disconnect() {
    this.stompService.disconnect();
  }

  config(socketUrl: string = 'ws://localhost:8080/api/socket', streamUrl: string = '/topic/greetings'): Observable<any> {
    const res = new Subject();
    let stompConfig: StompConfig = {
      url: socketUrl,
      headers: null,
      heartbeat_in: 0,
      heartbeat_out: 2000,
      reconnect_delay: 5000,
      debug: false
    };

    this.stompService = new StompService(stompConfig);
    this.stompService.connectObservable.subscribe(val => {
      res.next();
    });

    return res;
  }

  public stream(endpointName: string, user = false, prefix = '/topic'): Observable<any> {
    let token = "";
    if (user) {
      prefix = `/user/${this.appService.user.getValue().userId}`;
    }
    return this.stompService.subscribe(`${prefix}/${endpointName}`).pipe(map(x => JSON.parse(x.body)));
  }

  public send(url: string, message?: any) {
    return this.stompService.publish(url, JSON.stringify(message), {'token': localStorage.getItem('token')});
  }

  public state(): BehaviorSubject<StompState> {
    if (this.stompService) {
      return this.stompService.state;
    }
    return new BehaviorSubject(null);
  }
  public errors(): Subject<any> {
    return this.stompService.errorSubject;
  }

  public invalidate(): Observable<any> {
    let token = localStorage.getItem('token');
    return this.stompService.subscribe(`invalidate/${token}`).pipe(map(x => JSON.parse(x.body)));
  }
}
