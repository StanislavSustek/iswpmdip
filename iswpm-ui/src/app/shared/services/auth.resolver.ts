import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { PmSocket2Service } from './pm-socket2.service';
import { SecurityBackendService } from '../backend/security-backend.service';
import { first } from 'rxjs/operators';
import { AppService } from './app.service';
import { UserService } from './user.service';
import { AdminService } from '../../admin/admin.service';
import { CompanyService } from '../../company/company.service';

@Injectable({
  providedIn: 'root'
})
export class AuthResolver implements Resolve<any> {

  constructor(private authService: AuthService,
              private pm2: PmSocket2Service,
              private securityBackendService: SecurityBackendService,
              private router: Router,
              private appService: AppService,
              private userService: UserService,
              private adminService: AdminService,
              private companyService: CompanyService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.userService.clear();
    if (this.authService.isLoggedIn()) {
      this.securityBackendService.getUserDetail().pipe(first()).subscribe(val => {
        console.log('userDetail', val);

        this.pm2.config().subscribe(() => {
          if (this.appService.user.getValue() == null) {
            this.appService.user.next(val);
          }
            this.pm2.invalidate().subscribe(v => {
              console.log('INVALIDATE',v);
            });
            console.log('userDetail', val);
            this.userService.refreshMessage();
            this.userService.initUserCompaniesChannel(true);
            if (val.companiesWithPermisions[0] && val.companiesWithPermisions[0].permisions && val.companiesWithPermisions[0].permisions.find(x => x === 'SYSTEM_ADMIN')) {
              this.appService.company.next(val.companiesWithPermisions[0]);
              this.companyService.company = val.companiesWithPermisions[0].company;
              this.router.navigate(['admin']);
            }
          });

      }, error1 => {
        this.authService.logout()
      });
    } else {
      this.authService.logout();
    }
    return true;
  }
}
