
export class HelpUtils {
  private static
}

/*
  private static addDraftDesc(arr: string[], value: string) {
    if (arr.indexOf(value) === -1) {
      arr.push(value);
    }
  }

  static formatNumber(value: number, digits: number = 2, spaces: boolean = true) {
    if (value != null) {
      if (spaces) {
        return value.toFixed(digits).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').replace('.', ',');
      } else {
        return value.toFixed(digits).toString().replace('.', ',');
      }
    } else {
      return '';
    }
  }

  static floorNumber(value, digits): number {
    return (Math.floor(value * Math.pow(10, digits)) / Math.pow(10, digits));
  }

  static digitsOfNumber(value: number, digits: number) {
    const numberBeforeDecimal = parseInt(value.toString().split('.')[0], 10);
    const digitsBeforeDecimal = +(numberBeforeDecimal.toString().length);

    if (digitsBeforeDecimal < digits) {
      const decimal = digits - digitsBeforeDecimal;
      return HelpUtils.formatNumber(value, decimal, false)
    }
    return HelpUtils.formatNumber(value);
  }

  static formatDateTime(value: LocalDateTime, short: boolean = false): string {
    if (!value) {
      return null;
    } else {
      const parts = value.split('T')[0].split('-');
      let res = parts[2] + '.' + parts[1] + '.' + parts[0];
      if (value.split('T').length > 1 && !short) {
        const timeParts = value.split('T')[1].split(':');
        res += ' ' + timeParts[0] + ':' + timeParts[1] + ':' + timeParts[2].replace('Z', '');
      }
      return res;
    }
  }


  /!**
   * Skopiruje objekt bez atributov definovanych v excludeParams
   *!/
  static deepCopy(obj: any, excludeParams: string[] = []) {
    let copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || 'object' !== typeof obj) {
      return obj;
    }

    // Handle Date
    if (obj instanceof Date) {
      copy = new Date();
      copy.setTime(obj.getTime());
      return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
      copy = [];
      for (let i = 0, len = obj.length; i < len; i++) {
        copy[i] = this.deepCopy(obj[i], excludeParams);
      }
      return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
      copy = {};
      for (const attr in obj) {
        if (obj.hasOwnProperty(attr) && !excludeParams.includes(attr)) {
          copy[attr] = this.deepCopy(obj[attr], excludeParams);
        }
      }
      return copy;
    }

    throw new Error('Unable to copy obj! Its type isn\'t supported.');
  }

  static getNameWithTitles(name: string, surname: string, titleBefore: string, titleAfter: string): string {
    const firstName = name ? `${name}` : '';
    const secondName = surname ? `${surname}` : '';
    let fullname = `${firstName} ${secondName}`;
    if (titleBefore) {
      fullname = `${titleBefore} ${fullname}`;
    }
    if (titleAfter) {
      fullname = `${fullname} ${titleAfter}`;
    }
    return fullname;
  }
}
*/
