import * as moment from 'moment';

export class DateUtils {

  static formatDateTime(value: any, onlyDate = false): any {
  if (onlyDate) {
    return moment(value).format('DD.MM.YYYY');
  }

  return moment(value).format('DD.MM.YYYY HH:mm');
}

  static getLocalDateFromMoment(value) {
    if (value == null) {
      return value;
    }
    return <Date>(<unknown>(moment(value).toISOString(true).substr(0, 10)));
  }


  static getTimeAsMoment(timeControlValue: string): moment.Moment {
    if (timeControlValue) {
      return moment(timeControlValue, 'HH:mm');
    }
    return null;
  }

  static getDateTimeAsMoment(dateControlValue: moment.Moment, timeControlValue: string): moment.Moment {
    if (dateControlValue) {
      let time: moment.Moment = this.getTimeAsMoment('00:00');
      if (timeControlValue) {
        time = this.getTimeAsMoment(timeControlValue);
      }
      return moment(dateControlValue).set({ hours: time.get('hour'), minutes: time.get('minute'), seconds: 0 })
    }
    return null;
  }

  static getLocalDateTimeFromDateAndTimePicker(dateControlValue: moment.Moment, timeControlValue: string): Date {
    const date = this.getDateTimeAsMoment(dateControlValue, timeControlValue);
    if (date == null) {
      return null;
    }
    return <Date>(<unknown>(moment(date).toISOString(true)));
  }
}
