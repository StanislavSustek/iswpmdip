
export class StringUtils {

  static removeWhiteSpaces(value: string): string {
    if (!value) {
      return value;
    } else {
      return value.toString().replace(/\s/g, '');
    }
  }

  static parseNumber(value: string, returnZeroForUndefined = true) {
    if (!value) {
      if (returnZeroForUndefined) {
        return 0;
      } else {
        return null;
      }
    } else {
      return Number(this.removeWhiteSpaces(value).replace(',', '.'));
    }
  }

  static toHtml(value: string) {
    return value.replace(/(?:\r\n|\r|\n)/g, '<br>');
  }

  static getStringFormValue(value) {
    if (value != null && value.trim() !== '') {
      return value.trim();
    }
    return null;
  }

}
