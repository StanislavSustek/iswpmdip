export class SortUtils {

  static getSortFunction(type, field, ascending, pipe, codelistName, labelSortFn?: ((v) => string)) {
    let asc = this.getAscNum(ascending);

    if (type === 'string') {
      return (n1, n2) => {
        const f1 = !this.getFieldValue(n1, field) ? '' : this.getFieldValue(n1, field);
        const f2 = !this.getFieldValue(n2, field) ? '' : this.getFieldValue(n2, field);
        return f1.localeCompare(f2) * asc;
      };
    }

    if (type === 'number') {
      return (n1, n2) => { return ((this.getFieldValue(n1, field) - this.getFieldValue(n2, field)) * asc) > 0 ? 1 : ((this.getFieldValue(n1, field) - this.getFieldValue(n2, field)) * asc) < 0 ? -1 : 0 };
    }

    if (type === 'localDate') {
      return (n1, n2) => { return ((this.getFieldValue(n1, field).getDate().getTime() - this.getFieldValue(n2, field).getDate().getTime()) * asc) > 0 ? 1 : ((this.getFieldValue(n1, field).getDate().getTime() - this.getFieldValue(n2, field).getDate().getTime()) * asc) < 0 ? -1 : 0 };
    }

    if (type === 'codelist') {
      return (n1, n2) => { return pipe.transform(this.getFieldValue(n1, field), codelistName).localeCompare(pipe.transform(this.getFieldValue(n2, field) , codelistName)) * asc };
    }

    if (type === 'custom') {
      return (n1, n2) => {
        const f1 = labelSortFn(n1);
        const f2 = labelSortFn(n2);
        return f1.localeCompare(f2) * asc;
      };
    }
  }

  private static getFieldValue(obj, field: string) {
    const fields = field.split('.');
    let value = obj;
    for (const oneField of fields) {
      value = value[oneField];
      if (value == null) {
        return null;
      }
    }
    return value;
  }

  static getAscNum(value: boolean): number {
    return value ? 1 : -1;
  }

  static sort(n1Value, n2Value, type, ascending) {
    const asc = this.getAscNum(ascending);

    if (type === 'string') {
      const f1 = !n1Value ? '' : n1Value + '';
      const f2 = !n2Value ? '' : n2Value + '';
      return f1.localeCompare(f2) * asc;
    }
    if (type === 'number') {
      return ((n1Value - n2Value) * asc) > 0 ? 1 : ((n1Value - n2Value) * asc) < 0 ? -1 : 0;
    }

    if (type === 'localDate') {
      return ((n1Value.getDate().getTime() - n2Value.getDate().getTime()) * asc) > 0 ? 1 : ((n1Value.getDate().getTime() - n2Value.getDate().getTime()) * asc) < 0 ? -1 : 0;
    }
  }

}
