import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
})
export class SwitchComponent implements OnInit {
  @Output() isToggled = new EventEmitter();
  toggled = false;

  constructor() {
  }

  ngOnInit() {
  }

  toggle() {
    this.toggled = !this.toggled;
    this.isToggled.emit(this.toggled);
  }
}
