export interface Role {
  roleId?: number;
  description?: string;
  name?: string;
  companyId?: number;
  readOnly?: boolean;
}
