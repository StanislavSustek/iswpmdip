import { User } from './user';

export interface AddUserToProjectRequest {
  projectId: number;
  companyId: number;
  addUsers: User[];
  removeUsers: User[];
}
