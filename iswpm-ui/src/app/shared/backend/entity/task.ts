export interface Task {
  taskId?: number;
  name?: string ;
  description?: string ;
  specialRewardHour?: number;
  specialReward?: number;
  taskTaskId?: number;
  projectId?: number;
  taskPrice?: number;
  hoursAssigned?: number;
  hoursReported?: number;
  dateFrom?: Date ;
  dateTo?: Date;
  progress?: number;
  taskStatusId?: number;
  userId?: number;
  orderItemId?: number;
  confirmedHours?: number;
  priority?: string;
}
