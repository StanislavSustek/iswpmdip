import { Privilege } from './privilege';

export interface AddPrivilegeToRoleRequest {
   roleId: number;
   companyId: number;
   addPrivileges: Privilege[];
   removePrivileges: Privilege[];
}
