export interface HolidayFilterRequest {
  type?: number;
  userIds?: number[];
  companyId?: number;
  stateId?: number;
}
