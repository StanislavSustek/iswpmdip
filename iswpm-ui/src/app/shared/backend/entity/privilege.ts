export interface Privilege {
  permisionId?: number;
  description?: string;
  name?: string;
  group?: string;
}
