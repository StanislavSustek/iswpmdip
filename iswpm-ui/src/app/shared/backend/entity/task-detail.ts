import { Task } from './task';
import { Comment } from './comment';

export interface TaskDetail {
  task: Task;
  taskLog: TaskLog[];
  comments: Comment[];
  taskFiles: any[];
}
