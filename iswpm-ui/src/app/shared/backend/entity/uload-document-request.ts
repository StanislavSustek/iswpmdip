export interface UloadDocumentRequest {
  name: string;
  description: string;
  extension: string;
  fileBase: string;
  taskId?: number;
  projectId?: number;
}
