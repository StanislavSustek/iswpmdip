import { Project } from './project';

export interface CreateProjectRequest {
  project: Project;
  companyId: number;
}
