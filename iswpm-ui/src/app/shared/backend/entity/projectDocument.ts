export interface ProjectDocument {
  id: number;
  name: string;
  file: string;
  description: string;
  date: Date;
}
