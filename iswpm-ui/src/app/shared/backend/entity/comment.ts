export interface Comment {
  id: number;
  taskId: number;
  userId: number;
  comment: string;
}
