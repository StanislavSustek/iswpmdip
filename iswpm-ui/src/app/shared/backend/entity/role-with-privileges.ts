import { Role } from './role';
import { Privilege } from './privilege';

export interface RoleWithPrivileges {
  role: Role;
  privileges: Privilege[];
}
