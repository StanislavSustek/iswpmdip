export interface Company{
   companyId?: number ,
   ico?: string ,
   dic?: string ,
   street?: string ,
   streetNumber?: string ,
   psc?: string ,
   companyName?: string ,
   city?: string ,
   modified?: Date,
}
