export interface PasswordResetRequest {
  password: string ;
  repeatPassword: string ;
  token: string ;
}
