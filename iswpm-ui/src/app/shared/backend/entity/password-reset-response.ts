export interface PasswordResetResponse {
  success: boolean;
  tokenValid: boolean;
}
