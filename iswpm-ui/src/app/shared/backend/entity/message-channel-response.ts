export interface MessageChannelResponse {
  message: string;
  refresh: boolean;
  response: boolean;
}
