export interface Holiday {
  holidayId: number;
  userId: number;
  companyInternalId: number;
  from: Date;
  to: Date;
  comment: string;
  holidayStatusId: number;
  holidayTypeId: number;
}
