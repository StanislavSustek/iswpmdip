import { CompanyPermision } from './company-permision';

export interface User {
  userId?: number;
  firstName: string;
  lastName: string;
  email: string;
  login: string;
  modified?: Date;
  created?: Date;
  lastLogin?: Date;
  permisions?: string[];
  roleId?: number;
  userStatusId?: number;
  companiesWithPermisions?: CompanyPermision[];
}
