export interface AddUserToCompanyRequest {
   companyId: number;
   userId: number;
   roleId: number;
}
