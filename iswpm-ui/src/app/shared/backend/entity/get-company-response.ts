import { Company } from './company';
import { Role } from './role';

export interface GetCompanyResponse {
  company: Company;
  roles: Role[];
}
