export interface Project {
  projectId: number;
  name: string;
  description: string;
  budget: number;
  comment: string;
  dateFrom: Date;
  dateTo: Date;
  projectStatusId: number;
  companyInternalId: number;
  created: Date;
}
