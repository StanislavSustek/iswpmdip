interface TaskLog {
   taskLogId: number;
   taskId: number;
   taskStatusId: number;
   modifiedUserId: number;
   comment: string;
   name: string;
   specialRewardHour: number;
   specialReward: number;
   dateFrom: Date;
   dateTo: number;
   modified: number;
   progress: number;
   userId: number;
   orderItemId: number;
   hoursReported: number;
   hoursAssigned: number;
   taskPrice: number;
   priority: string;
}
