import { Holiday } from './holiday';

export interface CreateHolidayRequest {
  holiday: Holiday
}
