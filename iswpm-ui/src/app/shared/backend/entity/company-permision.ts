import { Company } from './company';
import { Role } from './role';

export interface CompanyPermision {
  company: Company;
  permisions: string[];
  role: Role;
}
