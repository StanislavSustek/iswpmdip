import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PasswordResetRequest } from './entity/password-reset-request';
import { PasswordResetResponse } from './entity/password-reset-response';
import { User } from './entity/user';

@Injectable()
export class SecurityBackendService {

  url = '/api/security';

  constructor(private http: HttpClient) { }

  login(request): Observable<any> {
    return this.http.post(`${this.url}/login`, request).pipe(map( x => x as any));
  }

  resetPassword(request: PasswordResetRequest): Observable<PasswordResetResponse> {
    return this.http.post(`${this.url}/resetPassword`, request).pipe(map( x => x as PasswordResetResponse));
  }

  getUserDetail(): Observable<User> {
    const header = new HttpHeaders().append('token', localStorage.getItem('token'));
    return this.http.post(`${this.url}/getUserDetail`, {}, {headers: header}).pipe(map( x => x as User));
  }

}
