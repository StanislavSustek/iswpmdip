import { Theme } from './theme';

export const lightTheme: Theme = {
  name: 'light',
  properties: {
    '--primary-color': '#9500ff',
    '--secondary-color': '#c6d7e1',
    '--surface-color': '#827d7f',
    '--neumorphism-light-color': '#ffffff',
    '--neumorphism-dark-color': '#858585',
    '--menu-color': '#6300ff',
    '--surface-secondary-color': '#bd9aff',
    '--background-color': '#efefef',
    '--text-color': '#2d2d2d',
  }
};
