import { Injectable } from '@angular/core';
import { Theme } from './theme';
import { darkTheme } from './dark-theme';
import { lightTheme } from './light-theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  darkTheme = false;

  toggleTheme() {
    if (this.darkTheme) {
      this.toggleLight();
    } else {
      this.toggleDark();
    }
  }

  toggleDark() {
    this.darkTheme = true;
    this.setTheme(darkTheme);
  }

  toggleLight() {
    this.darkTheme = false;
    this.setTheme(lightTheme);
  }

  private setTheme(theme: Theme) {
    Object.keys(theme.properties).forEach(property =>
      document.documentElement.style.setProperty(property, theme.properties[property])
    );
  }
}
