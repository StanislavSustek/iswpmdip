import { Theme } from './theme';

export const darkTheme: Theme = {
  name: 'dark',
  properties: {
    '--primary-color': '#9500ff',
    '--secondary-color': '#6b939b',
    '--surface-color': '#827d7f',
    '--menu-color': '#6300ff',
    '--surface-secondary-color': '#9500ff',
    '--background-color': '#1c1a19',
    '--text-color': '#fff',
    '--neumorphism-light-color': '#343434',
    '--neumorphism-dark-color': '#000000',
  }
};
