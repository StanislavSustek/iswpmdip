import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './components/components.module';
import { MatButtonModule, MatDialogModule, MatMenuModule } from '@angular/material';
import { SwitchComponent } from './switch/switch.component';
import { DateFormatPipe } from './pipe/date-format.pipe';

@NgModule({
  declarations: [
    SwitchComponent,
    DateFormatPipe
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    MatDialogModule,
    MatMenuModule,
    MatButtonModule
  ],
    exports: [
        CommonModule,
        ComponentsModule,
        MatDialogModule,
        MatMenuModule,
        MatButtonModule,
        SwitchComponent,
        DateFormatPipe
    ]
})
export class SharedModule { }
