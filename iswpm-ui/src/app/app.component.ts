import { Component, OnDestroy } from '@angular/core';
import { AppService } from './shared/services/app.service';
import { PmSocket2Service } from './shared/services/pm-socket2.service';
import { AuthService } from './shared/services/auth.service';
import { UserService } from './shared/services/user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'isw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy{
  title = 'iswpm-ui';
  showMenu = false;
  showPanels = false;
  destroy$ = new Subject();

  constructor(
    private appService: AppService,
    private socketService: PmSocket2Service,
    private authService: AuthService,
    private userService: UserService
  ) {
    this.appService.company.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.showPanels = true;
        this.socketService.errors().subscribe(error => {
          this.socketService.disconnect();
          this.appService.user.next(null);
          this.authService.logout();
        })
      } else {
        this.showPanels = false;
      }
    });
  }


  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toggle(e) {
    this.showMenu = !this.showMenu;
  }

  clickOnMenu(e) {
    this.showMenu = false;
  }

}
