import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogInComponent } from './auth/log-in.component';
import { AdminOverviewComponent } from './admin/admin-overview/admin-overview.component';
import { adminRoutes } from './admin/admin-routing.module';
import { AuthResolver } from './shared/services/auth.resolver';
import { DashboardComponent } from './dashboard/dashboard.component';
import { companyRouting } from './company/company-routing.module';
import { userRouting } from './user/user-routing.module';
import { ChangePasswordComponent } from './auth/change-password.component';
import { UserGuard } from './user/user.guard';
import { holidaysRouting } from './holidays/holidays-routing.module';
import { HolidaysComponent } from './holidays/holidays.component';
import { AdminPrivilegesComponent } from './admin/admin-privileges/admin-privileges.component';
import { AdminResolver } from './admin/admin.resolver';
import { ProjectComponent } from './project/project.component';
import { projectRouting } from './project/project-routing.module';
import { UserProjectsComponent } from './user/user-projects/user-projects.component';

const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {
    path: 'login',
    component: LogInComponent
  },
  {
    path: 'admin',
    component: AdminOverviewComponent,
    children: adminRoutes,
    resolve: {data: AuthResolver}
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    resolve: [AuthResolver]
  },
  {path: 'privileges', component: AdminPrivilegesComponent, resolve: {data: AdminResolver}, canActivate: [UserGuard]},
  {
    path: 'company',
    children: companyRouting,
    canActivate: [UserGuard]
  },
  {
    path: 'projects',
    component: UserProjectsComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'projects/detail',
    component: ProjectComponent,
    children: projectRouting,
    canActivate: [UserGuard]
  },
  {
    path: 'holidays',
    component: HolidaysComponent,
    children: holidaysRouting,
    canActivate: [UserGuard]
  },
  {
    path: 'password-reset',
    component: ChangePasswordComponent, resolve: []
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
