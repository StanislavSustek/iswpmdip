import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimesheetComponent } from './timesheet.component';

@NgModule({
  declarations: [TimesheetComponent],
  imports: [
    CommonModule
  ]
})
export class TimesheetModule { }
