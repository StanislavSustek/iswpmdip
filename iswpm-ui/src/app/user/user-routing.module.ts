import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectComponent } from '../project/project.component';
import { UserProjectsComponent } from './user-projects/user-projects.component';
import { UserGuard } from './user.guard';
import { projectRouting } from '../project/project-routing.module';

export const userRouting : Routes = [
  {path: '', component: UserProjectsComponent, canActivate: [UserGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(userRouting)],
  exports: [RouterModule]
})
export class CompanyRoutingModule {
}
