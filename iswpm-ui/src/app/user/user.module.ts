import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserProjectsComponent } from './user-projects/user-projects.component';

@NgModule({
  declarations: [
    UserComponent,
    UserProjectsComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    UserComponent,
    UserProjectsComponent
  ]
})
export class UserModule { }
