import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project } from '../../shared/backend/entity/project';
import { Subject } from 'rxjs';
import { ContentService } from '../../shared/services/content.service';
import { ModalService } from '../../shared/services/modal.service';
import { ProjectService } from '../../project/project.service';
import { Router } from '@angular/router';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { UserService } from '../../shared/services/user.service';

@Component({
  selector: 'isw-user-projects',
  templateUrl: './user-projects.component.html',
  styleUrls: ['./user-projects.component.scss']
})
export class UserProjectsComponent implements OnInit, OnDestroy {

  projects: Project[] = [];
  destroy$ = new Subject();

  constructor(
    private contentService: ContentService,
    private userService: UserService,
    private modalService: ModalService,
    private projectService: ProjectService,
    private router: Router,
    private pm2: PmSocket2Service,
  ) {
  }

  ngOnInit() {
    this.userService.initUserProjectsChannel(true);
    this.userService.userProjects.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.projects = val;
    });
  }

  translate(item) {
    return this.contentService.projectStatuses.find(x => x.code === item).value
  }

  format(item) {
    return moment(item).format('DD.MM.YYYY');
  }

  openProject(item) {
    this.projectService.isUser = true;
     this.projectService.openedProject = item;
    this.router.navigate(['projects', 'detail'])
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
