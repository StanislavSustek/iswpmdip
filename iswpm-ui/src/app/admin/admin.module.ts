import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminOverviewComponent } from './admin-overview/admin-overview.component';
import { BasicModule } from '../basic/basic.module';
import { AdminCompaniesComponent } from './admin-companies/admin-companies.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminUsersTableComponent } from './admin-users/admin-users-table.component';
import { ComponentsModule } from '../shared/components/components.module';
import { AdminCompaniesTableComponent } from './admin-companies/admin-companies-table.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddCompanyModalComponent } from './admin-companies/add-company-modal/add-company-modal.component';
import { AddCompanyUserComponent } from './admin-companies/add-company-user/add-company-user.component';
import { AdminPrivilegesComponent } from './admin-privileges/admin-privileges.component';
import { AdminPrivilegesTableComponent } from './admin-privileges/admin-privileges-table/admin-privileges-table.component';
import { AddUserModalComponent } from './admin-users/add-user-modal/add-user-modal.component';
import { CreateRoleModalComponent } from './admin-privileges/create-role-modal.component';

@NgModule({
  declarations: [
    AdminOverviewComponent,
    AdminCompaniesComponent,
    AdminUsersComponent,
    AdminUsersTableComponent,
    AdminCompaniesTableComponent,
    AddCompanyModalComponent,
    AddCompanyUserComponent,
    AdminPrivilegesComponent,
    AdminPrivilegesTableComponent,
    AddUserModalComponent,
    CreateRoleModalComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    AdminRoutingModule,
    BasicModule,
    ComponentsModule
  ],
  exports: [
    AdminOverviewComponent,
    AdminUsersTableComponent
  ],
  entryComponents: [
    AddCompanyModalComponent,
    AddCompanyUserComponent,
    AddUserModalComponent,
    CreateRoleModalComponent
  ]
})
export class AdminModule {
}
