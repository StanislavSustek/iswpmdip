import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { TableComponent } from '../../shared/components/table/table.component';
import { User } from '../../shared/backend/entity/user';
import { ContentService } from '../../shared/services/content.service';
import { TableCol } from '../../shared/components/table/table-col';
import { TableCell } from '../../shared/components/table/table-cell';
import * as moment from 'moment';
import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';
import { ModalService } from '../../shared/services/modal.service';
import { first } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import { AddEmployerToCompanyComponent } from '../../company/add-employer-to-company/add-employer-to-company.component';

@Component({
  selector: 'isw-admin-users-table',
  templateUrl: '../../shared/components/table/table.component.html'
})
export class AdminUsersTableComponent extends TableComponent<User> {

  @Input() type: 'COMPANY' | 'ADMIN' = 'COMPANY';

  constructor(
    cd: ChangeDetectorRef,
    contentService: ContentService,
    private modalService: ModalService,
    private adminService: AdminService,
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: '', fieldName: '', fieldType: 'string', sortable: true},
      {label: 'Meno', fieldName: 'lastName', fieldType: 'string', sortable: true},
      {label: 'login', fieldName: 'login', fieldType: 'string', sortable: true},
      {label: 'Email', fieldName: 'email', fieldType: 'string', sortable: true},
      {label: 'Stav', fieldName: 'name', fieldType: 'string', sortable: true},
      {label: 'Vytvorený', fieldName: 'name', fieldType: 'string', sortable: true},
    ];
  }

  getData(row: User): TableCell<User>[] {
    console.log(this.type);
    return [
      row.login != 'superuser' ? {
        dropdown: this.type === 'COMPANY' ? [
          {icon: 'pen', text: ' Upraviť', action: 'edit'},
          {icon: 'key', text: ' Reset hesla', action: 'reset'},
          {icon: 'user', text: ' Zmeniť oprávnenie', action: 'privileges'},
        ] : [
          {icon: 'pen', text: ' Upraviť', action: 'edit'},
          {icon: 'key', text: ' Reset hesla', action: 'reset'},
          {icon: 'state', text: ' Zmeniť stav', action: 'state'},
          {icon: 'trash', text: ' Zmazať', action: 'delete'},
        ]
      } : {},
      {label: `${row.firstName} ${row.lastName}`},
      {label: row.login},
      {label: row.email},
      {label: this.contentService.getCodelistValue('USER-STATE', row.userStatusId, true)},
      {label: moment(row.created).format('DD.MM.YYYY')},
    ];
  }

  onActionClick(action, rowData, componentShowed) {
    switch (action) {
      case 'edit':
        const modalRef = this.modalService.openCustomModal(AddUserModalComponent);
        modalRef.componentInstance.user = rowData;
        break;
      case 'reset':
        this.modalService.openInformationModal(
          'Upozornenie',
          `Naozaj chcete resetovať heslo používateľovi: <b> ${rowData.firstName} ${rowData.lastName}</b>`,
          'confirm'
        ).pipe(first()).subscribe(val => {
          if (val) {
            this.adminService.deleteUser(rowData);
          }
        });
        break;
      case 'state':

        break;
      case 'privileges':
        const mr = this.modalService.openCustomModal(AddEmployerToCompanyComponent);
        mr.componentInstance.user = rowData;
        break;
      case 'delete':
        this.modalService.openInformationModal(
          'Upozornenie',
          `Naozaj chcete odstrániť používateľa: <b>${rowData.firstName} ${rowData.lastName}</b>`,
          'confirm'
        ).pipe(first()).subscribe(val => {
          if (val) {
            this.adminService.deleteUser(rowData);
          }
        });
        break;

    }
  }

}
