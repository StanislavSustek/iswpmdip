import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/backend/entity/user';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AdminService } from '../admin.service';
import { ModalService } from '../../shared/services/modal.service';
import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';

@Component({
  selector: 'isw-admin-users',
  templateUrl: './admin-users.component.html',
  styles: []
})
export class AdminUsersComponent implements OnInit {

  users: User[] = [];
  destroy$ = new Subject();

  constructor(
    private adminService: AdminService,
    private modalService: ModalService,
  ) {
  }

  ngOnInit() {
    this.adminService.users.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        console.log('ALL_USERS', val);
        this.users = val;
      }
    });
    // this.userService.refreshUsers();
  }

  createUser() {
    this.modalService.openCustomModal(AddUserModalComponent)
  }

}
