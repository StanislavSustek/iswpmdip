import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BasicFormComponent } from '../../../basic/basic-form/basic-form.component';
import { User } from '../../../shared/backend/entity/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../../admin.service';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'isw-add-user-modal',
  templateUrl: './add-user-modal.component.html',
})
export class AddUserModalComponent extends BasicFormComponent<User> implements OnInit {

  @Input() set user(v: User) {
    this.value = v;
  };

  atr1 = 'userId';
  dateAtr = 'modified';
  form: FormGroup;
  roles = [];
  companies = [];
  loginError = null;
  emailError = null;

  constructor(
    private modalRef: MatDialogRef<AddUserModalComponent>,
    private fb: FormBuilder,
    private adminService: AdminService,
  ) {
    super();
  }

  ngOnInit() {
    if (this.adminService.roleWithPrivileges.getValue()) {
      this.roles = this.adminService.roleWithPrivileges.getValue().map(x => ({
        key: x.role.roleId,
        label: x.role.description
      }));
    }
    const companies = this.adminService.companies.getValue();
    if (this.value != null && companies) {
      this.companies = companies.map(x => ({key: x.company.companyId, label: x.company.companyName}));
    }
    this.initForm();
    super.ngOnInit();
  }

  initForm() {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      login: ['', Validators.required],
      email: ['', Validators.required],
      role: null,
      company: null,
    });

    Object.keys(this.form.controls).forEach(x => {
      if (!['comapny'].includes(x)) {
        this.form.controls[x].setValue(this.value[x]);
      }
    });

    this.form.get('company').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.roles = this.adminService.companies.getValue().find(x => x.company.companyId === val).roles.map(x => ({
        key: x.roleId,
        label: x.description
      }));
    });

    this.form.get('firstName').valueChanges.pipe(debounceTime(600), distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(val => {
      this.createLogin();
    });
    this.form.get('lastName').valueChanges.pipe(debounceTime(600), distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(val => {
      this.createLogin();
    });

    if (!this.value) {
      this.form.get('login').valueChanges.pipe(debounceTime(600), distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(val => {
        this.loginError = null;
        if (!this.validateLogin(val)) {
          this.loginError = 'Užívateľské meno už existuje.'
          this.form.get('login').setErrors({key: 'bad-login'});
        }
      });
    }

    this.form.get('email').valueChanges.pipe(debounceTime(600), distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(val => {
      this.emailError = null;
      if (this.adminService.users.getValue().some(x => x.email === val)) {
        this.emailError = 'Na zadaný email už je vytvorený účet.'
        this.form.get('email').setErrors({key: 'bad-email'});
      }
    });
  }

  private createLogin() {
    const firstName: string = this.form.get('firstName').value;
    const lastName: string = this.form.get('lastName').value;
    let login = '';
    if (firstName && lastName && firstName.length >= 3 && lastName.length >= 3) {
      login = firstName.substr(0, 3) + lastName.substr(0, 3);
      console.log('validateLogin', this.validateLogin(login));
      while (!this.validateLogin(login)) {
        login += Math.floor(Math.random() * Math.floor(999));
      }
    }
    this.form.get('login').setValue(login);
  }

  close() {
    this.modalRef.close();
  }

  private validateLogin(login) {
    return !this.adminService.users.getValue().some(x => x.login === login);
  }

  submit() {
    if (this.form.valid && !this.loginError && !this.emailError) {
      const req: User = {
        userId: this.value ? this.value.userId : null,
        firstName: this.form.get('firstName').value,
        lastName: this.form.get('lastName').value,
        email: this.form.get('email').value,
        login: this.form.get('login').value,
        roleId: this.form.get('role').value,
        userStatusId: 1
      };
      this.adminService.createUser(req);
      this.close();
    }
  }

  restore() {
    super.restore();
    Object.keys(this.form.controls).forEach(x => {
      this.form.controls[x].setValue(this.value[x]);
    });
  }

  cancel() {
    super.cancel();
  }

}
