import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Company } from '../../../shared/backend/entity/company';
import { UserService } from '../../../shared/services/user.service';
import { Subject } from 'rxjs';
import { BasicFormComponent } from '../../../basic/basic-form/basic-form.component';
import { AdminService } from '../../admin.service';
import { GetCompanyResponse } from '../../../shared/backend/entity/get-company-response';

@Component({
  selector: 'isw-add-company-modal',
  templateUrl: './add-company-modal.component.html',
})
export class AddCompanyModalComponent extends BasicFormComponent<GetCompanyResponse> implements OnInit {

  form: FormGroup;
  @Input() set company(v: GetCompanyResponse) {
    this.value = v;
  };
  destroy = new Subject();
  atr1 = 'company';
  atr2 = 'companyId';
  dateAtr = 'modified';

  constructor(
    private matRef: MatDialogRef<AddCompanyModalComponent>,
    private fb: FormBuilder,
    private userServiceService: AdminService
  ) {
    super();
  }

  ngOnInit() {
    this.dataForRefresh = this.userServiceService.companies;
    super.ngOnInit();
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      companyName: this.value ? this.value.company.companyName : '',
      ico: this.value ? this.value.company.ico : '',
      dic: this.value ? this.value.company.dic : '',
      street: this.value ? this.value.company.street : '',
      streetNumber: this.value ? this.value.company.streetNumber : '',
      psc: this.value ? this.value.company.psc : '',
      city: this.value ? this.value.company.city : '',
    });
  }

  submit() {
    const req: Company = {
      companyId: this.value ? this.value.company.companyId : null,
      companyName: this.form.get('companyName').value,
      ico: this.form.get('ico').value,
      dic: this.form.get('dic').value,
      street: this.form.get('street').value,
      streetNumber: this.form.get('streetNumber').value,
      psc: this.form.get('psc').value,
      city: this.form.get('city').value,
      modified:  this.value ? this.value.company.modified : new Date()
    };

    this.userServiceService.saveCompany(req);
    this.close();
  }

  close() {
    this.matRef.close();
  }

  restore() {
    super.restore();
    this.form.get('companyName').setValue(this.value.company.companyName);
    this.form.get('ico').setValue(this.value.company.ico);
    this.form.get('dic').setValue(this.value.company.dic);
    this.form.get('street').setValue(this.value.company.street);
    this.form.get('streetNumber').setValue(this.value.company.streetNumber);
    this.form.get('psc').setValue(this.value.company.psc);
    this.form.get('city').setValue(this.value.company.city);
  }

  cancel() {
    super.cancel();
  }
}
