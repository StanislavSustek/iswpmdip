import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalService } from '../../shared/services/modal.service';
import { AddCompanyModalComponent } from './add-company-modal/add-company-modal.component';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { AdminService } from '../admin.service';
import { GetCompanyResponse } from '../../shared/backend/entity/get-company-response';

@Component({
  selector: 'isw-admin-companies',
  templateUrl: './admin-companies.component.html',
  styles: []
})
export class AdminCompaniesComponent implements OnInit {

  companies: GetCompanyResponse[] = [];

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalService: ModalService,
    private pmSocket: PmSocket2Service
  ) { }

  ngOnInit() {
    this.adminService.companies.subscribe(val => {
      this.companies = val;
    });
  }

  addCompany() {
    this.modalService.openCustomModal(AddCompanyModalComponent)
  }

}
