import { ChangeDetectorRef, Component } from '@angular/core';
import { TableComponent } from '../../shared/components/table/table.component';
import { Company } from '../../shared/backend/entity/company';
import { ContentService } from '../../shared/services/content.service';
import { TableCol } from '../../shared/components/table/table-col';
import { TableCell } from '../../shared/components/table/table-cell';
import { UserService } from '../../shared/services/user.service';
import { ModalService } from '../../shared/services/modal.service';
import { AddCompanyModalComponent } from './add-company-modal/add-company-modal.component';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { AddCompanyUserComponent } from './add-company-user/add-company-user.component';
import { GetCompanyResponse } from '../../shared/backend/entity/get-company-response';

@Component({
  selector: 'isw-admin-companies-table',
  templateUrl: '../../shared/components/table/table.component.html'
})
export class AdminCompaniesTableComponent extends TableComponent<GetCompanyResponse> {

  constructor(
    cd: ChangeDetectorRef,
    contentService: ContentService,
    private userService: UserService,
    private modalService: ModalService,
    private pmSocket: PmSocket2Service
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: 'Názov', fieldName: 'companyName', fieldType: 'string', sortable: true},
      {label: 'ICO', fieldName: '', fieldType: 'string', sortable: true},
      {label: 'DIC', fieldName: '', fieldType: 'string', sortable: true},
      {label: 'Adresa', fieldName: '', fieldType: 'string', sortable: true},
      {label: '', fieldName: '', fieldType: 'string', sortable: true},
    ];
  }

  getData(row: GetCompanyResponse): TableCell<GetCompanyResponse>[] {
    return [
      {label: row.company.companyName},
      {label: row.company.ico},
      {label: row.company.dic},
      {label: row.company.street ? `${row.company.street} ${row.company.streetNumber}, ${row.company.psc} ${row.company.city}` : ''},
      {
        dropdown: [
          {icon: 'pen', text: 'Upraviť', action: 'edit'},
          {icon: 'user', text: 'Uzivatelia', action: 'users'},
          {icon: 'trash', text: 'Zmazať', action: 'delete'},
        ]
      }
    ];
  }

  onActionClick(action, rowData, componentShowed) {
    switch (action) {
      case 'delete':
        this.userService.deleteCompany(rowData);
        break;
      case 'edit':
        const ref = this.modalService.openCustomModal(AddCompanyModalComponent);
        ref.componentInstance.company = rowData;
        break;
      case 'users':
        const modalRef = this.modalService.openCustomModal(AddCompanyUserComponent);
        modalRef.componentInstance.companyId = rowData.company.companyId;
        break;
    }
  }

}
