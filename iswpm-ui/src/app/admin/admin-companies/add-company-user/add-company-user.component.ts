import { Component, Input, OnInit } from '@angular/core';
import { PmSocket2Service } from '../../../shared/services/pm-socket2.service';
import { MatDialogRef } from '@angular/material';
import { User } from '../../../shared/backend/entity/user';
import { AdminService } from '../../admin.service';

@Component({
  selector: 'isw-add-company-user',
  templateUrl: './add-company-user.component.html',
})
export class AddCompanyUserComponent implements OnInit {

  @Input() companyId = null;
  companyUsers: User[] = [];
  notSelectedUsers: User[] = [];
  isEdit = false;
  result: User[] = [];

  constructor(
    private pmSocket: PmSocket2Service,
    private adminService: AdminService,
    private modalRef: MatDialogRef<AddCompanyUserComponent>
  ) {
  }

  ngOnInit() {
    this.pmSocket.stream('/getCompanyUsers').subscribe(val => {
      if (val) {
        this.companyUsers = val.users;
      }
    });
    this.pmSocket.send('/company/getCompanyUsers', {companyId: this.companyId});

  }

  close() {
    this.modalRef.close();
  }

  submit() {
    console.log(this.result);
    this.adminService.addUserIntoCompany({
      companyId: this.companyId,
      addedUsers: this.result.map(x => x.userId)
    });
  }

  edit() {
    this.isEdit = true;
    this.pmSocket.stream('getUsersForCompany').subscribe(val => {
      if (val) {
        this.notSelectedUsers = val.users;
      }
    });
    this.pmSocket.send('/company/getUsersForCompany', {companyId: this.companyId});
  }

  add(item: User) {
    this.notSelectedUsers.splice(this.notSelectedUsers.findIndex(x => x.userId === item.userId),1);
    this.result.push(item);
  }

  remove(item: User) {
    this.result.splice(this.result.findIndex(x => x.userId === item.userId),1);
    this.notSelectedUsers.push(item);
  }
}
