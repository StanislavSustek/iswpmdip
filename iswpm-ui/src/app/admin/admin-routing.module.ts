import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminCompaniesComponent } from './admin-companies/admin-companies.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminPrivilegesComponent } from './admin-privileges/admin-privileges.component';
import { AdminResolver } from './admin.resolver';
import { AdminGuard } from '../shared/services/admin.guard';
import { UserGuard } from '../user/user.guard';

export const adminRoutes: Routes = [
  {path: 'companies', component: AdminCompaniesComponent, resolve: {data: AdminResolver}, canActivate: [AdminGuard]},
  {path: 'users', component: AdminUsersComponent, resolve: {data: AdminResolver}, canActivate: [AdminGuard]},
  {path: 'privileges', component: AdminPrivilegesComponent, resolve: {data: AdminResolver}, canActivate: [UserGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
