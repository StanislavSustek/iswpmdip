import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TableComponent } from '../../../shared/components/table/table.component';
import { ContentService } from '../../../shared/services/content.service';
import { TableCol } from '../../../shared/components/table/table-col';
import { Privilege } from '../../../shared/backend/entity/privilege';
import { TableCell } from '../../../shared/components/table/table-cell';

@Component({
  selector: 'isw-admin-privileges-table',
  templateUrl: '../../../shared/components/table/table.component.html',
})
export class AdminPrivilegesTableComponent extends TableComponent<Privilege>{

  constructor(
    cd: ChangeDetectorRef,
    contentService: ContentService
  ) {
    super(cd, contentService);
  }

  getHeader(): TableCol[] {
    return [
      {label: 'Názov', fieldType: 'string',fieldName: 'name' , sortable: true},
      {label: 'Popis', fieldType: 'string',fieldName: 'descritpion' , sortable: true},
    ];
  }

  getData(row: Privilege): TableCell<Privilege>[] {
    return [
      {label: row.name},
      {label: row.description},
    ]
  }
}
