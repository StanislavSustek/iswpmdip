import { Component, OnDestroy, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RoleWithPrivileges } from '../../shared/backend/entity/role-with-privileges';
import { Privilege } from '../../shared/backend/entity/privilege';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalService } from '../../shared/services/modal.service';
import { CreateRoleModalComponent } from './create-role-modal.component';
import { AddPrivilegeToRoleRequest } from '../../shared/backend/entity/add-privilege-to-role-request';
import { CompanyService } from '../../company/company.service';

@Component({
  selector: 'isw-admin-privileges',
  templateUrl: './admin-privileges.component.html',
})
export class AdminPrivilegesComponent implements OnInit, OnDestroy {

  allPrivileges: Privilege[] = [];
  rolesWithPrivilges: RoleWithPrivileges[] = [];
  rolesOptions = [];
  form: FormGroup;
  rolePrivileges: Privilege[] = [];
  destroy$ = new Subject();
  edit = false;
  notSelectedPrivileges: Privilege[] = [];
  privilegeForEdit: Privilege[] = [];
  request: {
    addPrivilege: Privilege[],
    removePrivilege: Privilege[]
  } = {addPrivilege: [], removePrivilege: []};
  loading = false;

  constructor(
    private adminService: AdminService,
    private modalService: ModalService,
    private companyService: CompanyService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      role: null
    });

    this.adminService.initPrivilegesChanel();
    this.adminService.privileges.subscribe(val => {
      if (val) {
        this.allPrivileges = val;
      }
    });
    this.adminService.roleWithPrivileges.subscribe(val => {
      console.log('ROLE_PRIVILEGE', val);
      if (val) {
        this.loading = false;
        this.rolesWithPrivilges = JSON.parse(JSON.stringify(val));
        if (this.form.get('role').value) {
          this.rolePrivileges = this.rolesWithPrivilges.find(x => x.role.name === this.form.get('role').value).privileges;
        }
        this.rolesOptions = val.map(x => ({key: x.role.name, label: x.role.description}));
      }
    });

    this.form.get('role').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        this.rolePrivileges = this.rolesWithPrivilges.find(x => x.role.name === val).privileges;
        this.notSelectedPrivileges = this.allPrivileges.filter(x => !this.rolePrivileges.map(z => z.name).includes(x.name));
        this.privilegeForEdit = JSON.parse(JSON.stringify(this.rolePrivileges));
      } else {
        this.rolePrivileges = [];
      }
    })
  }

  addRole() {
    this.modalService.openCustomModal(CreateRoleModalComponent);
  }

  editRolePrivilges(){
    this.edit = !this.edit;
  }

  updateRoles() {
    const req: AddPrivilegeToRoleRequest = {
      roleId: this.rolesWithPrivilges.find(p => p.role.name === this.form.get('role').value).role.roleId,
      addPrivileges: this.request.addPrivilege,
      removePrivileges: this.request.removePrivilege,
      companyId: this.companyService.company.companyId
    };

    this.adminService.addPrivilegeToRole(req);
    this.privilegeForEdit = [];
    this.edit = false;
    this.loading = true;
  }

  add(item: Privilege) {
    this.notSelectedPrivileges.splice(this.notSelectedPrivileges.findIndex(x => x.name === item.name),1);
    this.privilegeForEdit.push(item);
    if (!this.rolePrivileges.some(x => x.name === item.name)) {
      this.request.addPrivilege.push(item)
    }
  }

  remove(item: Privilege) {
    this.privilegeForEdit.splice(this.privilegeForEdit.findIndex(x => x.name === item.name),1);
    this.notSelectedPrivileges.push(item);
    if (this.rolePrivileges.some(x => x.name === item.name)) {
      this.request.removePrivilege.push(item)
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
