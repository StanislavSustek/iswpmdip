import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BasicFormComponent } from '../../basic/basic-form/basic-form.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../admin.service';
import { Role } from '../../shared/backend/entity/role';
import { CompanyService } from '../../company/company.service';

@Component({
  selector: 'isw-create-role-modal',
  templateUrl: './create-role-modal.component.html',
})
export class CreateRoleModalComponent extends BasicFormComponent<any> implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalRef: MatDialogRef<CreateRoleModalComponent>,
    private companyService: CompanyService
  ) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  close() {
    this.modalRef.close();
  }

  submit() {
    if (this.form.valid) {
      const req: Role = {
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        companyId: this.companyService.company.companyId
      };

      this.adminService.createRole(req);
      this.close();
    }

  }

  restore() {
    super.restore();
  }

  cancel() {
    super.cancel();
  }
}
