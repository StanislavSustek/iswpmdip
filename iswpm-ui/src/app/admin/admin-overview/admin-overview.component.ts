import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'isw-admin-overview',
  templateUrl: './admin-overview.component.html'
})
export class AdminOverviewComponent implements OnInit {

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.adminService.initCompanyChanel();
    this.adminService.initUserChanel();
  }

}
