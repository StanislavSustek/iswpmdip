import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { SecurityBackendService } from '../shared/backend/security-backend.service';
import { AppService } from '../shared/services/app.service';
import { UserService } from '../shared/services/user.service';
import { AdminService } from './admin.service';
import { CompanyService } from '../company/company.service';

@Injectable({
  providedIn: 'root'
})
export class AdminResolver implements Resolve<any> {

  constructor(
              private pm2: PmSocket2Service,
              private securityBackendService: SecurityBackendService,
              private router: Router,
              private appService: AppService,
              private userService: UserService,
              private adminService: AdminService,
              private companyService: CompanyService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.adminService.initRolesWithPrivilegesChanel({companyId: this.companyService.company.companyId});
    this.adminService.initUserChanel();
    this.adminService.initCompanyChanel();
    return true;
  }
}
