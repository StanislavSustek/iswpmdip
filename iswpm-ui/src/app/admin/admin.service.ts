import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../shared/backend/entity/user';
import { Company } from '../shared/backend/entity/company';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { RoleWithPrivileges } from '../shared/backend/entity/role-with-privileges';
import { Privilege } from '../shared/backend/entity/privilege';
import { Role } from '../shared/backend/entity/role';
import { GetCompanyResponse } from '../shared/backend/entity/get-company-response';
import { GetRoleWithPrivilegesRequest } from '../shared/backend/entity/get-role-with-privileges-request';
import { AddPrivilegeToRoleRequest } from '../shared/backend/entity/add-privilege-to-role-request';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  users: BehaviorSubject<User[]> = new BehaviorSubject(null);
  companies: BehaviorSubject<GetCompanyResponse[]> = new BehaviorSubject(null);
  privileges: BehaviorSubject<Privilege[]> = new BehaviorSubject(null);
  roleWithPrivileges: BehaviorSubject<RoleWithPrivileges[]> = new BehaviorSubject(null);

  constructor(
    private socketService: PmSocket2Service,
  ) {
  }

  clear() {
    this.users.next(null);
    this.companies.next(null);
    this.privileges.next(null);
    this.roleWithPrivileges.next(null);
  }

  initUserChanel() {
    this.socketService.stream('getAllUsers').subscribe(val => {
      if (val) {
        this.users.next(val);
      }
    });
    this.refreshUsers();
  }

  refreshUsers() {
    this.socketService.send('/admin/getUsers', {});
  }

  initCompanyChanel() {
    this.socketService.stream('getCompanies').subscribe(val => {
      console.log(val);
      if (val) {
        this.companies.next(val);
      }
    });
    this.refreshCompany();
  }

  refreshCompany() {
    this.socketService.send('/admin/getCompanies', {companyType: 'ALL'});
  }

  saveCompany(company: Company) {
    this.socketService.send('/admin/createCompany', company);
  }

  deleteCompany(company: Company) {
    this.socketService.send('/admin/deleteCompany', company);
  }

  initPrivilegesChanel(force = false) {
    if (this.privileges.getValue() == null) {
    this.socketService.stream('getPrivileges').subscribe(val => {
      console.log(val);
      if (val) {
        this.privileges.next(val.permisions);
      }
    });
    }

    if (this.privileges.getValue() == null || force) {
      this.getPrivileges();
    }
  }


  initRolesWithPrivilegesChanel(request: GetRoleWithPrivilegesRequest, force = false) {
    if (this.roleWithPrivileges.getValue() == null) {
      this.socketService.stream(`company/${request.companyId}/getRolesWithPrivileges`).subscribe(val => {
        console.log(val);
        if (val) {
          this.roleWithPrivileges.next(val);
        }
      });
    }
    if (this.roleWithPrivileges.getValue() == null || force) {
      this.getRolesWithPrivileges(request);
    }
  }


  getPrivileges() {
    this.socketService.send('/admin/getPrivileges');
  }

  getRolesWithPrivileges(request: GetRoleWithPrivilegesRequest) {
    this.socketService.send('/admin/getRolesWithPrivileges', request);
  }

  createRole(role: Role) {
    this.socketService.send('/admin/createRole', role);
  }

  createUser(user: User) {
    this.socketService.send('/admin/createUser', user);
  }

  //TODO
  deleteUser(user: User) {
    this.socketService.send('/admin/deleteUser', user);
  }

  addUserIntoCompany(user) {
    this.socketService.send('/company/addUserIntoCompany', user);
  }

  addPrivilegeToRole(request: AddPrivilegeToRoleRequest) {
    this.socketService.send('/admin/addPrivilegeToRole', request);
  }
}
