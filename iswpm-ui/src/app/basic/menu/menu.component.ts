import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppService } from '../../shared/services/app.service';

@Component({
  selector: 'isw-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  @Output() onclick = new EventEmitter();
  menuItems = [];

  constructor(
    private appService: AppService,
  ) { }

  ngOnInit() {
    this.menuItems = [
      {label: 'Spoločnosti', navLink: ['admin','companies'], icon: 'building', visible: this.hasPermision('SYSTEM_ADMIN')},
      {label: 'Používatelia', navLink: ['admin','users'], icon: 'user', visible: this.hasPermision('SYSTEM_ADMIN')},
      {label: 'Roly a Oprávenia', navLink: ['privileges'], icon: 'user-tag', visible: this.hasPermision('ADD_EDIT_DELETE_ROLE')},
      {label: 'Spoločnosť', navLink: ['company','overview'], icon: 'building', visible: !this.hasPermision('SYSTEM_ADMIN')},
      {label: 'Projekty spoločnosti', navLink: ['company','projects'], icon: 'pen', visible: !this.hasPermision('SYSTEM_ADMIN')},
      {label: 'Projekty', navLink: ['projects'], icon: 'project-diagram', visible: this.hasPermision('SHOW_PROJECTS_PAGE')},
      {label: 'Dovolenky', navLink: ['holidays','my-holidays'], icon: 'umbrella-beach', visible: this.hasPermision('SHOW_HOLIDAYS')},
    ];
  }


  private hasPermision(val) {
    return this.appService.hasPermision(val);
  }
}
