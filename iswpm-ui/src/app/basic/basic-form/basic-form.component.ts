import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'isw-basic-form',
  templateUrl: './basic-form.component.html',
})
export class BasicFormComponent<T> implements OnInit, OnDestroy {

  newValue = null;
  value: T = null;
  atr1 = '';
  atr2 = null;
  dateAtr = '';
  dataForRefresh: BehaviorSubject<any>;
  destroy$ = new Subject();

  constructor() { }

  ngOnInit() {
    this.dataForRefresh.pipe(takeUntil(this.destroy$)).subscribe(val => {
        if (val) {
          const newVal = this.atr2 ? val.find(x => this.value[this.atr1][this.atr2] === x[this.atr1][this.atr2]) : val.find(x => this.value[this.atr1] === x[this.atr1]);
          if (newVal[this.dateAtr] === this.value[this.dateAtr]){
            this.newValue = null;
          } else {
            this.newValue = JSON.parse(JSON.stringify(newVal));
          }
        }

    })
  }

  protected restore() {
    this.value = this.newValue;
    this.newValue = null;
  }

  protected cancel() {
    this.newValue = null;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
