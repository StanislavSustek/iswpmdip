import { Component, ElementRef, EventEmitter, HostListener, OnDestroy, OnInit, Output } from '@angular/core';
import { PmSocket2Service } from '../../shared/services/pm-socket2.service';
import { StompState } from '@stomp/ng2-stompjs';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatBottomSheet } from '@angular/material';
import { AuthService } from '../../shared/services/auth.service';
import { UserService } from '../../shared/services/user.service';
import { AppService } from '../../shared/services/app.service';
import { ThemeService } from '../../shared/themes/theme.service';
import { CompanyService } from '../../company/company.service';

@Component({
  selector: 'isw-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  connection: StompState = StompState.CLOSED;
  destroy$ = new Subject();
  messages = [];
  containerOpen = false;
  userOpen = false;
  userName: string = null;
  companyName = null;
  @Output() toggleMenu = new EventEmitter();

  constructor(
    private socketSevice: PmSocket2Service,
    private matBottom: MatBottomSheet,
    private authService: AuthService,
    private appService: AppService,
    private themeService: ThemeService,
    private userService: UserService,
    private companyService: CompanyService,
    private elementRef: ElementRef,
  ) {
  }

  ngOnInit() {
    this.companyName = this.companyService.company.companyName;
    this.socketSevice.state().pipe(takeUntil(this.destroy$)).subscribe(val => {
      this.connection = val
    });
    this.userService.initMessageChannel(true);
    this.userService.messages.subscribe(val => {
      this.messages = val;
    });

    this.appService.user.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value) {
        this.userName = value.firstName + ' ' + value.lastName;
      } else {
        this.userName = null
      }
    })
  }

  get getConnectionClass() {
    switch (this.connection) {
      case StompState.CLOSED:
        return 'connection-icon--disconnecting';
      case StompState.CONNECTED:
        return 'connection-icon--connect';
      case StompState.TRYING:
        return 'connection-icon--trying';
      case StompState.DISCONNECTING:
        return 'connection-icon--disconnecting';
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  logout() {
    this.authService.logout(true);
  }

  openMessageContainer() {
    this.userOpen = false;
    this.containerOpen = !this.containerOpen;
  }

  openUserContainer() {
    this.containerOpen = false;
    this.userOpen = !this.userOpen;
  }

  removeMessage(id) {
    this.userService.deleteMessage(id);
  }

  changeTheme(e) {
    this.themeService.toggleTheme();
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.containerOpen = false;
      this.userOpen = false;
    }
  }

  toggleMenuOpen() {
    this.toggleMenu.emit();
  }
}
