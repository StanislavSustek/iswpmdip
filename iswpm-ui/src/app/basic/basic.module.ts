import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SubHeaderComponent } from './sub-header/sub-header.component';
import { MenuComponent } from './menu/menu.component';
import { AppRoutingModule } from '../app-routing.module';
import { ComponentsModule } from '../shared/components/components.module';
import { MessageContainerComponent } from './message-container/message-container.component';
import { MatMenuModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    HeaderComponent,
    SubHeaderComponent,
    MenuComponent,
    MessageContainerComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    ComponentsModule,
    MatMenuModule,
    SharedModule
  ],
  exports: [
    HeaderComponent,
    SubHeaderComponent,
    MenuComponent
  ],
  entryComponents: [
    MessageContainerComponent
  ]
})
export class BasicModule { }
