import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'isw-message-container',
  templateUrl: './message-container.component.html',
})
export class MessageContainerComponent implements OnInit {

  constructor(private bottomRef: MatBottomSheetRef<MessageContainerComponent>) { }

  ngOnInit() {
  }

}
