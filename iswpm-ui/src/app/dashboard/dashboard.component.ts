import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';
import { CompanyService } from '../company/company.service';
import { ModalService } from '../shared/services/modal.service';
import { CompanySelectComponent } from './company-select/company-select.component';
import { AppService } from '../shared/services/app.service';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'isw-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {

  noCompanyMessage = false;
  noPrivilege = false;
  companyName = null;
  destroy$ = new Subject();

  constructor(
    private appService: AppService,
    private authService: AuthService,
    private companyService: CompanyService,
    private modalService: ModalService,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.appService.user.pipe(takeUntil(this.destroy$)).subscribe(val => {
      if (val) {
        const companies = val.companiesWithPermisions;
        if (companies.length === 1) {
          this.companyService.company = companies[0].company;
          this.appService.company.next(companies[0]);
          this.companyService.initCompanyEmployers();
          if (!companies[0].permisions) {
            this.companyName = companies[0].company.companyName;
            this.userService.initRefreshChannel();
            this.noPrivilege = true;
          }
        } else if (companies.length > 1) {
          console.log('OTVORENIEOKIENKA');
          const dialogRef = this.modalService.openCustomModal(CompanySelectComponent);
          dialogRef.componentInstance.companies = companies;
          dialogRef.afterClosed().subscribe(val => {
            this.companyService.company = val.company;
            if (val.permisions.length === 0) {
              this.companyName = val.company.companyName;
            }
            this.userService.initRefreshChannel();
            this.appService.company.next(val);
            this.companyService.initCompanyEmployers();
          })
        }
      }
    })
  }

  contact() {

  }

  logout() {
    this.authService.logout(true);
  }

  ngOnDestroy(): void {
   this.destroy$.next();
   this.destroy$.complete();
  }

}
