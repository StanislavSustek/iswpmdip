import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { BasicModule } from '../basic/basic.module';
import { SharedModule } from '../shared/shared.module';
import { CompanySelectComponent } from './company-select/company-select.component';

@NgModule({
  declarations: [DashboardComponent, CompanySelectComponent],
  imports: [
    CommonModule,
    BasicModule,
    SharedModule
  ], exports: [
    DashboardComponent
  ],
  entryComponents: [
    CompanySelectComponent
  ]
})
export class DashboardModule { }
