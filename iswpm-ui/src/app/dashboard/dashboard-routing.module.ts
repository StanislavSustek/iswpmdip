import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminCompaniesComponent } from '../admin/admin-companies/admin-companies.component';
import { AdminUsersComponent } from '../admin/admin-users/admin-users.component';
import { AdminPrivilegesComponent } from '../admin/admin-privileges/admin-privileges.component';
import { AdminResolver } from '../admin/admin.resolver';
import { UserGuard } from '../user/user.guard';

export const adminRoutes: Routes = [
  {path: 'companies', component: AdminCompaniesComponent},
  {path: 'users', component: AdminUsersComponent},
  {path: 'privileges', component: AdminPrivilegesComponent, resolve: {data: AdminResolver}, canActivate: [UserGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
