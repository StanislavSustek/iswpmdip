import { Component, Input, OnInit } from '@angular/core';
import { Company } from '../../shared/backend/entity/company';
import { MatDialogRef } from '@angular/material/dialog';
import { GetCompanyResponse } from '../../shared/backend/entity/get-company-response';

@Component({
  selector: 'isw-company-select',
  templateUrl: './company-select.component.html',
})
export class CompanySelectComponent implements OnInit {

  choosedItem: GetCompanyResponse = null;
  @Input() companies: GetCompanyResponse[] = [];

  constructor(
    private modalRef: MatDialogRef<CompanySelectComponent>
  ) { }

  ngOnInit() {
  }

  close(item) {
    this.modalRef.close(item);
  }

  choose(item: GetCompanyResponse) {
    this.choosedItem = item;
    this.close(item);
  }

}
