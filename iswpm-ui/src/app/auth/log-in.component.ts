import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SecurityBackendService } from '../shared/backend/security-backend.service';
import { first } from 'rxjs/operators';
import { PmSocket2Service } from '../shared/services/pm-socket2.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'isw-log-in',
  templateUrl: './log-in.component.html',
})
export class LogInComponent implements OnInit, OnDestroy {

  form: FormGroup;
  errorMessage = null;
  destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private securityBackendService: SecurityBackendService,
    private pm2: PmSocket2Service,
    private router: Router
  ) {
  }

  ngOnInit() {

    this.form = this.fb.group({
      username: '',
      password: ''
    })
  }

  onSubmit() {
    if (this.form.valid) {
      this.errorMessage = null;
      const req = {
        login: this.form.get('username').value,
        password: this.form.get('password').value
      };

      this.securityBackendService.login(req).pipe(first()).subscribe(val => {
        if (val && val.success) {
          localStorage.setItem('token', val.tokken);
          this.router.navigate(['dashboard']);
        } else {
          this.errorMessage = 'Nesprávne prihlasovacie meno alebo heslo'
        }
      }, error1 => {
        this.errorMessage = 'Nesprávne prihlasovacie meno alebo heslo'
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
