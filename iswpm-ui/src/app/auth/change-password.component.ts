import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordResetRequest } from '../shared/backend/entity/password-reset-request';
import { SecurityBackendService } from '../shared/backend/security-backend.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'isw-change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {

  errorMessage = null;
  form: FormGroup;
  token: string = null;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private securityBackendService: SecurityBackendService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    });
    this.token = this.route.snapshot.queryParams['token'] ? this.route.snapshot.queryParams['token'] : null;
  }

  onSubmit() {
    this.errorMessage = null;
    console.log('RESET', this.form);
    if (this.form.valid) {
      if (this.form.get('password').value != this.form.get('repeatPassword').value) {
        this.errorMessage = 'Heslá sa nezhodujú';
        return;
      }
      const req: PasswordResetRequest = {
        password: this.form.get('password').value,
        repeatPassword: this.form.get('repeatPassword').value,
        token: this.token
      }

      this.securityBackendService.resetPassword(req).pipe(first()).subscribe(val => {
        if (val.success) {
          this.router.navigate(['login']);
        } else if (!val.tokenValid) {
          this.errorMessage = 'Odkaz na resetovanie hesla je neplatný';
        }
      });
    }
  }

}
