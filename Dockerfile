FROM java:8
COPY /target/isw-pm.jar isw-pm.jar
ENTRYPOINT ["java","-jar","isw-pm.jar"]

